//
// Created by Terry Stillone on 16/12/2015.
// Copyright (c) 2015 Originware. All rights reserved.
//
//  This is a Public header file for the RxPatterns SDK.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// protocol IRxInstanceable: SDK Objects that have assigned instance ID for instance monitoring.
///

public protocol IRxInstanceable
{
    /// The InstanceID assigned to Self.
    var instanceID: RxInstanceID      { get }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// protocol IRxTraceMonitorable:  SDK Objects that are trace-monitor-able for instancing, notification and operation.
///

public protocol IRxTraceMonitorable
{
    /// Register Self for monitoring.
    /// - Parameter traceTag: The monitor output line tag.
    func trace(traceTag: String) -> Self
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// protocol IRxTaggable: SDK Objects that are URI tag-able.
///

public protocol IRxTaggable
{
    /// The tag to be assigned to Self.
    var tag : String                                { get }
}

/// IRxTaggable extension.
public extension IRxTaggable
{
    /// The separator character for the
    public var TagSeparator : String                { return "/" }

    /// Indicate whether the RxObject UIR tag contains the given sub-tag.
    public func matchesSubTag(subTag : String) -> Bool
    {
        if let range = tag.rangeOfString(subTag)
        {
            return range.endIndex == tag.endIndex
        }

        return false
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// enum eRxObjectType: Categorises the class in terms of its role. The Object type enum is formed by appending the class name to eRxObject_.
///
/// As many generic classes inherit from RxObject, collections of object instances can be handled as collections of RxObjects. Their actual type can determined through their objectType property.
///
///

public enum eRxObjectType : Int, CustomStringConvertible
{
    case eRxObject_RxObserver
    case eRxObject_RxObservable
    case eRxObject_RxSource
    case eRxObject_RxService
    case eRxObject_RxSubject
    case eRxObject_RxEvalQueueDispatcher
    case eRxObject_RxEvalQueue
    case eRxObject_RxNotifier
    case eRxObject_RxRequestor
    case eRxObject_RxEvalNode
    case eRxObject_RxDisposeAction
    case eRxObject_RxDisposable
    case eRxObject_RxDisposalMonitor
    case eRxObject_RxSubscription
    case eRxObject_RxNotificationGate
    case eRxObject_RxNotificationQueue
    case eRxObject_RxTimer
    case eRxObject_RxTimerSubscription
    case eRxObject_RxTestMonitoredObject
    case eRxObject_Window
    case eRxObject_Unknown

    /// CustomStringConvertible compliance
    public var description : String
    {
        return eRxObjectType.names[self.rawValue]
    }

    private static let names : [String] = [
            "RxObserver",
            "RxObservable",
            "RxSource",
            "RxService",
            "RxSubject",
            "RxEvalQueueDispatcher",
            "RxEvalQueue",
            "RxNotifier",
            "RxRequestor",
            "RxEvalNode",
            "RxDisposeAction",
            "RxDisposable",
            "RxDisposalMonitor",
            "RxSubscription",
            "RxNotificationGate",
            "RxNotificationQueue",
            "RxTimer",
            "RxTimerSubscription",
            "RxTestMonitoredObject",
            "RxWindow",
            "Unknown"
    ]
}
