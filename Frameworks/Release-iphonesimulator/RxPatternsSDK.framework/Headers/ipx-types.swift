//
// Created by Terry Stillone (http://www.originware.com) on 14/03/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
//  This is a Public header file for the RxPatterns SDK.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Core types.
//

/// The SDK advanced time type.
public typealias RxTime = NSDate

/// The SDK simple time type, as a double relative to 2001.
public typealias RxRelTime = CFAbsoluteTime

/// The SDK time duration type.
public typealias RxDuration = NSTimeInterval

/// The SDK time offset type.
public typealias RxTimeOffset = NSTimeInterval

/// The SDK Index type.
public typealias RxIndexType = Int

/// The SDK count type.
public typealias RxCountType = Int

/// The SDK Instance IDs type.
public typealias RxInstanceID = Int64

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxTypes: Typealiases for \<ItemType\> generic classes.
///
/// - RxItemActionDelegate: (ItemType) -> Void
/// - RxCompletedActionDelegate: (IRxError?) -> Void
/// - RxStateChangeActionDelegate: (eRxEvalStateChange) -> Void
/// - RxBeginSessionDelegate: () -> Void
/// - RxEndSessionDelegate: (IRxError?) -> Void
/// - RxRequestDelegate: (ItemType) -> Void
/// - RxEvalOp: (RxEvalNode<ItemType, ItemType>) -> Void
/// - RxGenAction: (index : RxIndexType, notifier : ARxNotifier<ItemType>) -> eRxSyncGenCommand
/// - TimingArrayType: [(timeOffset: RxTimeOffset, item : ItemType)]

public struct RxTypes<ItemType>
{
    // Item notification action delegate type.
    public typealias RxItemActionDelegate = (ItemType) -> Void

    // Completed/Error notification action delegate type.
    public typealias RxCompletedActionDelegate = (IRxError?) -> Void

    // Eval State Change notification action delegate type.
    public typealias RxStateChangeActionDelegate = (eRxEvalStateChange) -> Void

    // Requestor BeginSession Delegate.
    public typealias RxBeginSessionDelegate = () -> Void

    // Requestor EndSession Delegate.
    public typealias RxEndSessionDelegate = (IRxError?) -> Void

    // Requestor request Delegate.
    public typealias RxRequestDelegate = (ItemType) -> Void

    // Encapsulates the behaviour to be run for RxObservableExpr evaluation.
    public typealias RxEvalOp = (RxEvalNode<ItemType, ItemType>) throws -> Void

    // Generator type.
    public typealias RxGenAction = (index : RxIndexType, notifier : ARxNotifier<ItemType>) -> eRxSyncGenCommand

    // Timing Sequence Array type.
    public typealias TimingArrayType = [(timeOffset: RxTimeOffset, item : ItemType)]
}

///
/// RxTypes2: Typealiases for \<ItemInType, ItemOutType\> generic classes. Types include:
///
/// - RxItemNotifierAction: (ItemInType, ARxNotifier<ItemOutType>) -> Void
/// - RxCompletedNotifierAction: (IRxError?, ARxNotifier<ItemOutType>) -> Void
/// - RxStateChangeNotifierAction: (eRxEvalStateChange, ARxNotifier<ItemOutType>) -> Void
/// - RxEvalOp: (RxEvalNode<ItemInType, ItemOutType>) -> Void
/// - RxBuildMapTypeAction: (RxMapType<ItemInType, ItemOutType>) -> ()
/// - RxMapItemAction: (ItemInType) -> ItemOutType
///
public struct RxTypes2<ItemInType, ItemOutType>
{
    // Item notifier action delegate type.
    public typealias RxItemNotifierAction = (ItemInType, ARxNotifier<ItemOutType>) -> Void

    // Completed/Error notifier action delegate type.
    public typealias RxCompletedNotifierAction = (IRxError?, ARxNotifier<ItemOutType>) -> Void

    // Eval State Change notifier action delegate type.
    public typealias RxStateChangeNotifierAction = (eRxEvalStateChange, ARxNotifier<ItemOutType>) -> Void

    // Encapsulates the behaviour to be run for RxObservableExpr evaluation.
    public typealias RxEvalOp = (RxEvalNode<ItemInType, ItemOutType>) throws -> Void

    // Map item action.
    public typealias RxMapItemAction = (ItemInType) -> ItemOutType
}

///
/// RxMiscTypes: Typealiases for non-generics. Types include:
///
/// - RxTimestampComparator: (lhs: etRxTime, rhs: etRxTime) -> Bool
/// - RxSyncGenStateHandler: (eRxSyncGenCommand) -> Bool
///
public struct RxMiscTypes
{
    // Timestamp comparator op.
    public typealias RxTimestampComparator = (lhs: etRxTime, rhs: etRxTime) -> Bool

    // SyncGenPat State Handler.
    public typealias RxSyncGenStateHandler = (eRxSyncGenCommand) -> Bool
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IRxError: The SDK error representation. Used for error notifications and exceptions.
///
public protocol IRxError: CustomStringConvertible, ErrorType
{
    /// Equator.
    func isEqual(other : IRxError) -> Bool
}

/// Equatable operator for the { class.
public func ==(lhs: IRxError, rhs: IRxError) -> Bool
{
    return lhs.isEqual(rhs)
}

/// Equatable operator for the IRxError protocol.
public func ==(lhs: IRxError?, rhs: IRxError?) -> Bool
{
    switch (lhs, rhs)
    {
    case (.None, .None):
        return true
        
    case (.Some(let lhsError), .Some(let rhsError)):
        return lhsError.isEqual(rhsError)
        
    default:
        return false
    }
}

