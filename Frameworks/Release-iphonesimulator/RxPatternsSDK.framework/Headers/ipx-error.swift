//
// Created by Terry Stillone (http://www.originware.com) on 29/04/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
//  This is a Public header file for the RxPatterns SDK.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eRxErrorType: The standard error types for the RxPatterns SDK. Standard errors include:
///
/// - eException: An exception occurred.
/// - eSubscriptionFailed: Subscription failed (general).
/// - eNotiferThunkFailure: A thunked Notifier could not be created.
/// - eInternalSDKFailure: Internal SDK failure.
/// - eCustomError: Custom error, for general use.
///

public enum eRxErrorType : Equatable
{
    /// An exception occurred.
    case eException(ErrorType)

    /// Subscription failed (general).
    case eSubscriptionFailure(String)

    /// A thunked Notifier could not be created.
    case eNotiferThunkFailure(String)

    /// Internal SDK failure.
    case eInternalSDKFailure(String)

    /// Custom error, for general use.
    case eCustomError(String)

    /// CustomStringConvertible compliance.
    public var description: String
    {
        switch self
        {
            case eException:                                    return "Exception"
            case eSubscriptionFailure(let errorDescription):    return "Subscription failure: \(errorDescription)"
            case eInternalSDKFailure(let errorDescription):     return "Internal SDK failure: \(errorDescription)"
            case eNotiferThunkFailure(let errorDescription):    return "Notifier thunk failure: \(errorDescription)"
            case eCustomError(let errorDescription):            return "Error: \(errorDescription)"
        }
    }
}

/// Equatable operator for the eRxErrorType enum.
public func ==(lhs: eRxErrorType, rhs: eRxErrorType) -> Bool
{
    switch (lhs, rhs)
    {
        case (.eException, .eException):                                                    return true
        case (.eSubscriptionFailure(let lhsDesc), .eSubscriptionFailure(let rhsDesc)):      return lhsDesc == rhsDesc
        case (.eInternalSDKFailure(let lhsDesc), .eInternalSDKFailure(let rhsDesc)):        return lhsDesc == rhsDesc
        case (.eCustomError(let lhsDesc), .eCustomError(let rhsDesc)):                      return lhsDesc == rhsDesc

        default:                                                            return false
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxError: The SDK Error representation.
///

public struct RxError : IRxError, Equatable
{
    /// The type of error.
    public let errortype : eRxErrorType

    /// The optional error context.
    public let context : String?

    /// CustomDebugStringConvertible Protocol conformance.
    public var description: String
    {
        let formattedContext = context != nil ? " context: \(context)" : ""

        return errortype.description + formattedContext
    }

    /// Initialise the error with error type and optional context.
    /// - Parameter errorType: Indicates the error type.
    /// - Parameter context: The optional error context.
    public init(_ errorType: eRxErrorType, _ context : String? = nil)
    {
        self.errortype = errorType
        self.context = context
    }

    /// Initialise the error with error type and optional context.
    /// - Parameter errorType: Indicates the exception error type.
    /// - Parameter context: The optional error context.
    public init(errorType: ErrorType, context : String? = nil)
    {
        self.errortype = .eException(errorType)
        self.context = context
    }

    /// Equator.
    public func isEqual(other : IRxError) -> Bool
    {
        if let errorOther = other as? RxError
        {
            switch (context, errorOther.context)
            {
                case (.None, .None):
                    return true

                case (.Some(let selfContext), .Some(let otherContext)):
                    return selfContext == otherContext

                default:
                    return false
            }
        }
        else
        {
            return false
        }
    }
}

/// Equatable operator for the RxError class.
public func ==(lhs: RxError, rhs: RxError) -> Bool
{
    return lhs.isEqual(rhs)
}
