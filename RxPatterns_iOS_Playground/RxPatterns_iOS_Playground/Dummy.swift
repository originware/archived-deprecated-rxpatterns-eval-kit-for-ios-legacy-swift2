//
//  Created by Terry Stillone on 14/12/2015.
//  Copyright © 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK
import RxPatternsLib

///
/// Dummy source to make sure RxPatternsSDK and RxPatternsLib are linked. This code will not be run.
///

func dummy()
{
    let source = RxSource.fromArray([1, 2, 3, 4])
    let observer = RxTestObserver<Int>(tag: "TestObserver")
    let subscription = source.subscribe(observer)
    
    subscription.waitForDisposal()
}
