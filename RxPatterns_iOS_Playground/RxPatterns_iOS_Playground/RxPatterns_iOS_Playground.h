//
//  RxPatterns_iOS_Playground.h
//  RxPatterns_iOS_Playground
//
//  Created by Terry Stillone on 14/12/2015.
//  Copyright © 2015 Originware. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for RxPatterns_iOS_Playground.
FOUNDATION_EXPORT double RxPatterns_iOS_PlaygroundVersionNumber;

//! Project version string for RxPatterns_iOS_Playground.
FOUNDATION_EXPORT const unsigned char RxPatterns_iOS_PlaygroundVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RxPatterns_iOS_Playground/PublicHeader.h>


