//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

//: ## Playground for Testing RxPattern Expressions.
//: ##
//: ## (Build the Project first with a 64 bit Simulator device such as iPhone6.)
//: ##

import RxPatterns_iOS_Playground
import RxPatternsSDK
import RxPatternsLib

//: ## fromArray([1, 2, 3, 4]), demonstrates:
//: * Asynchronous subscription and subscription waitForDisposal.
//: * The use of RxTestObserver to view the notifications that were observed.
do
{
    print("\n==>> fromArray start  <<==\n")
    
    let source = RxSource<Int>.fromArray([1, 2, 3, 4])
    let observer = RxTestObserver<Int>(tag: "TestObserver")
    let subscription = source.subscribe(observer)
    
    subscription.waitForDisposal()
    
    let result = observer.notifications
    
    print("==>> fromArray result ==>> \(result) <<==\n")
}

//: ## interval(0.01).take(10).traceAll(">>Interval Test>>"), demonstrates:
//: * Monitoring using traceAll(traceTag : String) which prints operational events to the console. See console output.
do
{
    print("\n==>> interval(0.01).take(10).monitorAll(\">>Interval Test>>\") start <<==\n")
    
    // Run the evaluation with tracing.
    
    let source = RxSource<Int>.interval(0.01)
    let observer = RxTestObserver<Int>(tag: "TestObserver")
    
    source.take(8).traceAll(">>Interval Test>>").runSync(observer)
    
    let result = observer.notifications
    
    print("\n==>> interval.take.monitorAll result ==>> \(result) <<==\n")
}

//: ## fromTimedArray([(t, 1), (t * 2, 2), (t * 3, 3), (t * 4, 4)]).map, demonstrates:
//: * Timed sequences using the fromTimedArray source.
//: * The mapping item notifications using the map observable.
//: * Subscription to an item action closure rather than to an Observer.
do
{
    print("\n==>> fromTimedArray.map start  <<==\n")
    
    let t : RxDuration = 0.01   // in seconds.
    let source = RxSource<Int>.fromTimedArray([(t, 1), (t * 2, 2), (t * 3, 3), (t * 4, 4)])
    let queue = RxNotificationQueue<String>(tag: "Results")
    let subscription = source.map({ (item : Int) -> String? in
        
        return "As string: \(item)"
        
    }).subscribe({ (item : String) in
        
        queue.queueItem(item)
        print(">> got: \(item)\n")
        
    })
    
    subscription.waitForDisposal()
    
    let results = queue
    
    print("==>> fromTimedArray.map result ==>> \(results) <<==\n")
}


//: ## Custom Observable, Implements an RxObservable that indicates when items have expired by emitting an additional nil item notification. This demonstrates:
//: * Timed sequences using the fromTimedArray source.
//: * The use of runSync for synchronous subscription.
extension RxObservableMap
{
    public func expire(duration : RxDuration) -> RxObservableMap<ItemType, ItemType?>
    {
        //if duration <= 0 { return RxEvalOpsConvenience.completedOpPat(RxLibError(.eInvalidEvalOp)) }
        
        let tag = "expire"
        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType?>) -> Void in
            
            let window = RxWindow(tag : "window." + tag)
            
            window <- eWindowCommand.eSetEvalQueue(evalNode.evalQueue) <- eWindowCommand.eSetWindowEndAction( { evalNode.syncOutNotifier.notifyItem(nil) } )
                
            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType?>) in
                
                window.createSingleWindow(RxTime(), duration: duration)
                
                notifier.notifyItem(item)
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType?>) in
                
                switch stateChange
                {
                case eRxEvalStateChange.eEvalEnd:
                    
                    window.stopCurrentWindow()
                    
                default:
                    // do nothing.
                    break
                }
            }
        }
        
        return RxObservableMap<ItemType, ItemType?>(tag: tag, evalOp: evalOp).chainObservable(self)
    }
}

//: Exercise the expire RxObservable.

do
{
    let t : RxDuration = 0.1
    let tickSource = RxSource<Int>.fromTimedArray([(t, 1), (t * 4, 2), (t * 5, 3), (t * 8, 4)])
    let logger = RxTestObserver<Int?>(tag: "Logger")
    
    tickSource.expire(t * 2).runSync(logger)
    
    let result = logger.notifications
    
    print("\n==>> expire result ==>> \(result) <<==\n")
}
