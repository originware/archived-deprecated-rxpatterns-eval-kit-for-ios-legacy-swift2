//
// Created by Terry Stillone (http://www.originware.com) on 8/04/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import XCTest

@testable import RxPatternsSDK
@testable import RxPatternsLib

class RxSubjectTests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testSubjectBasicItemNotifications()
    {
        let testSubjectName = "subject"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName: String, test:() -> Void, expected1:RxNotificationQueue<ItemType>, expected2:RxNotificationQueue<ItemType>)

            let observer1 = TxTestRigs<ItemType>.fullCheckingTestObserver("subject first observer")
            let observer2 = TxTestRigs<ItemType>.fullCheckingTestObserver("subject second observer")
            let subject = RxSubject<ItemType>(tag: "subject(\(testName)")

            let run1: RunEntry = (

                    runName: "No active subscription",
                    test: { subject.notifyItem("zero") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: [])
            )

            let run2: RunEntry = (

                    runName: "Subscription active",
                    test: { subject.notifyItem("one") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["one"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: [])
            )

            let run3: RunEntry = (

                    runName: "Subscription active, multiple calls",
                    test: { subject.notifyItem("two"); subject.notifyItem("three") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["two", "three"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: [])
            )

            let run4: RunEntry = (

                    runName: "Two subscriptions active",
                    test: { subject.notifyItem("four") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["four"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: ["four"])
            )

            let run5: RunEntry = (

                    runName: "First subscription disposed",
                    test: { subject.notifyItem("five") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: ["four", "five"])
            )

            let run6: RunEntry = (

                    runName: "Second subscription disposed",
                    test: { subject.notifyItem("six") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: ["four", "five"])
            )

            let runSingleTest = { (runEntry : RunEntry) -> Void in

                let (name, test, expected1, expected2) = runEntry
                let reportFunc1 = reportGenerator(name + " - first observer", expected1, observer1.notifications)
                let reportFunc2 = reportGenerator(name + " - second observer", expected2, observer2.notifications)

                self.trace(2, "\n= run begin ==> \(name) <==\n")
                test()
                self.trace(2, "= run end   ==> \(name) <==\n")

                XCTAssertTrue(expected1 == observer1.notifications, reportFunc1())
                XCTAssertTrue(expected2 == observer2.notifications, reportFunc2())

                observer1.notifications.removeAll()
            }

            runSingleTest(run1)

            let subscription1 = subject.subscribe(observer1)

            runSingleTest(run2)

            runSingleTest(run3)

            let subscription2 = subject.subscribe(observer2)


            runSingleTest(run4)

            subscription1.unsubscribe()

            runSingleTest(run5)

            subscription2.unsubscribe()

            runSingleTest(run6)
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSubjectName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testSubjectCompletionNotifications()
    {
        let testSubjectName = "subject(completion)"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, test:() -> Void, expected1:RxNotificationQueue<ItemType>, expected2:RxNotificationQueue<ItemType>)

            let testName = TxFactory.createTestName(#function)

            let observer1 = TxTestRigs<ItemType>.fullCheckingTestObserver("subject first observer")
            let observer2 = TxTestRigs<ItemType>.fullCheckingTestObserver("subject second observer")
            let subject = RxSubject<ItemType>(tag: "subject(\(testName))")

            let run1: RunEntry = (

                    runName: "Subscription active",
                    test: { subject.notifyItem("one") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["one"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: [])
            )

            let run2: RunEntry = (

                    runName: "Two subscriptions active",
                    test: { subject.notifyItem("four") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["four"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: ["four"])
            )

            let run3: RunEntry = (

                    runName: "First subscription disposed",
                    test: { subject.notifyItem("five") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: ["four", "five"])
            )

            let run4: RunEntry = (

                    runName: "Second subscription completed",
                    test: { subject.notifyCompleted() },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", itemsThenCompletion: ["four", "five"])
            )

            let run5: RunEntry = (

                    runName: "Test no item to be observed after completion",
                    test: { subject.notifyItem("five") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", itemsThenCompletion: ["four", "five"])
            )

            let runSingleTest = { (runEntry : RunEntry) -> Void in

                let (name, test, expected1, expected2) = runEntry
                let reportFunc1 = reportGenerator(name + " - first observer", expected1, observer1.notifications)
                let reportFunc2 = reportGenerator(name + " - second observer", expected2, observer2.notifications)

                self.trace(2, "\n= run begin ==> \(name) <==\n")
                test()
                self.trace(2, "= run end   ==> \(name) <==\n")

                XCTAssertTrue(expected1 == observer1.notifications, reportFunc1())
                XCTAssertTrue(expected2 == observer2.notifications, reportFunc2())

                observer1.notifications.removeAll()
            }

            let subscription1 = subject.subscribe(observer1)

            runSingleTest(run1)

            let subscription2 = subject.subscribe(observer2)

            assert(subscription2.instanceID != 0, "remove compiler warning")

            runSingleTest(run2)

            subscription1.unsubscribe()

            runSingleTest(run3)

            runSingleTest(run4)

            runSingleTest(run5)
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSubjectName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testSubjectErrorNotifications()
    {
        let testSubjectName = "subject(error)"
        let testName        = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, test:() -> Void, expected1:RxNotificationQueue<ItemType>, expected2:RxNotificationQueue<ItemType>)

            let observer1 = TxTestRigs<ItemType>.fullCheckingTestObserver("subject first observer")
            let observer2 = TxTestRigs<ItemType>.fullCheckingTestObserver("subject second observer")
            let subject   = RxSubject<ItemType>(tag: "(\(testName))")

            let run1: RunEntry = (

                    runName: "Subscription active",
                    test: { subject.notifyItem("one") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["one"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: [])
            )

            let run2: RunEntry = (

                    runName: "Two subscriptions active",
                    test: { subject.notifyItem("four") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["four"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: ["four"])
            )

            let run3: RunEntry = (

                    runName: "First subscription disposed",
                    test: { subject.notifyItem("five") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: ["four", "five"])
            )

            let run4: RunEntry = (

                    runName: "Second subscription error-ed",
                    test: { subject.notifyCompleted(self.error) },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: ["four", "five"], termination: .eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName: "Test no item to be observed after error",
                    test: { subject.notifyItem("five") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: ["four", "five"], termination: .eTermination_Error(error))
            )

            let runSingleTest = { (runEntry: RunEntry) -> Void in

                let (name, test, expected1, expected2) = runEntry
                let reportFunc1                        = reportGenerator(name + " - first observer", expected1, observer1.notifications)
                let reportFunc2                        = reportGenerator(name + " - second observer", expected2, observer2.notifications)

                self.trace(2, "\n= run begin ==> \(name) <==\n")
                test()
                XCTAssertTrue(expected1 == observer1.notifications, reportFunc1())

                XCTAssertTrue(expected2 == observer2.notifications, reportFunc2())

                self.trace(2, "= run end   ==> \(name) <==\n")

                observer1.notifications.removeAll()
            }

            let subscription1 = subject.subscribe(observer1)

            runSingleTest(run1)

            let subscription2 = subject.subscribe(observer2)

            assert(subscription2.instanceID != 0, "remove compiler warning")

            runSingleTest(run2)

            subscription1.unsubscribe()

            runSingleTest(run3)

            runSingleTest(run4)

            runSingleTest(run5)
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSubjectName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testSubjectReuse()
    {
        let testSubjectName = "subject(reuse)"
        let testName        = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, test:() -> Void, expected1:RxNotificationQueue<ItemType>, expected2:RxNotificationQueue<ItemType>, expected3:RxNotificationQueue<ItemType>)

            let observer1 = TxTestRigs<ItemType>.fullCheckingTestObserver("subject first observer")
            let observer2 = TxTestRigs<ItemType>.fullCheckingTestObserver("subject second observer")
            let observer3 = TxTestRigs<ItemType>.fullCheckingTestObserver("subject first second sub observer")
            let subject   = RxSubject<ItemType>(tag: "subject(\(testName))")

            let run1: RunEntry = (

                    runName: "Subscription active",
                    test: { subject.notifyItem("one") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected1", items: ["one"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected2", items: []),
                    expected3: RxNotificationQueue<ItemType>(tag: "expected3")
            )

            let run2: RunEntry = (

                    runName: "Two subscriptions active",
                    test: { subject.notifyItem("two") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected1", items: ["one", "two"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected2", items: ["two"]),
                    expected3: RxNotificationQueue<ItemType>(tag: "expected3")
            )

            let run3: RunEntry = (

                    runName: "All completed",
                    test: { subject.notifyCompleted() },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", itemsThenCompletion: ["one", "two"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", itemsThenCompletion: ["two"]),
                    expected3: RxNotificationQueue<ItemType>(tag: "expected3")
            )

            let run4: RunEntry = (

                    runName: "Resubscription",
                    test: { subject.notifyItem("first observer resubscription") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", itemsThenCompletion: ["one", "two"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", itemsThenCompletion: ["two"]),
                    expected3: RxNotificationQueue<ItemType>(tag: "expected", items: ["first observer resubscription"])
            )

            let runSingleTest = { (runEntry: RunEntry) -> Void in

                let (name, test, expected1, expected2, expected3) = runEntry
                let reportFunc1                                   = reportGenerator(name + " - first observer", expected1, observer1.notifications)
                let reportFunc2                                   = reportGenerator(name + " - second observer", expected2, observer2.notifications)
                let reportFunc3                                   = reportGenerator(name + " - first second sub observer", expected3, observer3.notifications)

                self.trace(2, "\n= run begin ==> \(name) <==\n")
                test()

                XCTAssertTrue(expected1 == observer1.notifications, reportFunc1())
                XCTAssertTrue(expected2 == observer2.notifications, reportFunc2())
                XCTAssertTrue(expected3 == observer3.notifications, reportFunc3())

                self.trace(2, "= run end   ==> \(name) <==\n")
            }

            self.trace(2, "\n= subscription1 run start <==\n")
            let subscription1 = subject.subscribe(observer1)
            assert(subscription1.instanceID != 0, "remove compiler warning")
            self.trace(2, "= subscription1 run end <==\n")

            runSingleTest(run1)

            self.trace(2, "\n= subscription2 run start <==\n")
            let subscription2 = subject.subscribe(observer2)
            assert(subscription2.instanceID != 0, "remove compiler warning")
            self.trace(2, "= subscription2 run end <==\n")

            runSingleTest(run2)

            runSingleTest(run3)

            self.trace(2, "\n= subscription3 run start <==\n")
            let subscription3 = subject.subscribe(observer3)
            assert(subscription3.instanceID != 0, "remove compiler warning")
            self.trace(2, "= subscription3 run end <==\n")

            runSingleTest(run4)

        }

        for iteration in 0 ..< TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSubjectName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}

class RxReplaySubjectTests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testReplaySubjectBasicItemNotifications()
    {
        let testSubjectName = "replaySubject"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, test:() -> Void, expected1:RxNotificationQueue<ItemType>, expected2:RxNotificationQueue<ItemType>)

            let observer1 = TxTestRigs<ItemType>.fullCheckingTestObserver("subject first observer")
            let observer2 = TxTestRigs<ItemType>.fullCheckingTestObserver("subject second observer")
            let subject = RxReplaySubject<ItemType>(tag: "subject(\(testName))")
            var subscription1 : RxSubscription? = nil
            var subscription2 : RxSubscription? = nil

            let run1: RunEntry = (

                    runName: "No active subscription",
                    test: { subject.notifyItem("zero") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: [])
            )

            let run2: RunEntry = (

                    runName: "Subscription active",
                    test: { subscription1 = subject.subscribe(observer1); subject.notifyItem("one") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["one"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: [])
            )

            let run3: RunEntry = (

                    runName: "Subscription active, multiple calls",
                    test: { subject.notifyItem("two"); subject.notifyItem("three") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["one", "two", "three"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: [])
            )

            let run4: RunEntry = (

                    runName: "Two subscriptions active",
                    test: { subscription2 = subject.subscribe(observer2); subject.notifyItem("four") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["one", "two", "three", "four"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: ["one", "two", "three", "four"])
            )

            let run5: RunEntry = (

                    runName: "First subscription disposed",
                    test: { subscription1!.unsubscribe(); subject.notifyItem("five") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["one", "two", "three", "four"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: ["one", "two", "three", "four", "five"])
            )

            let run6: RunEntry = (

                    runName: "Second subscription disposed",
                    test: { subscription2!.unsubscribe(); subject.notifyItem("six") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["one", "two", "three", "four"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: ["one", "two", "three", "four", "five"])
            )

            let runSingleTest = { (runEntry : RunEntry) -> Void in

                let (name, test, expected1, expected2) = runEntry
                let reportFunc1 = reportGenerator(name + " - first observer", expected1, observer1.notifications)
                let reportFunc2 = reportGenerator(name + " - second observer", expected2, observer2.notifications)

                self.trace(2, "\n= run begin ==> \(name) <==\n")
                test()
                self.trace(2, "= run end   ==> \(name) <==\n")

                XCTAssertTrue(expected1 == observer1.notifications, reportFunc1())
                XCTAssertTrue(expected2 == observer2.notifications, reportFunc2())
            }

            runSingleTest(run1)

            runSingleTest(run2)

            runSingleTest(run3)

            runSingleTest(run4)

            runSingleTest(run5)

            runSingleTest(run6)
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSubjectName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}

class RxBehaviourSubjectTests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testBehaviourSubjectBasicItemNotifications()
    {
        let testSubjectName = "behaviourSubject"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, test:() -> Void, expected1:RxNotificationQueue<ItemType>, expected2:RxNotificationQueue<ItemType>)

            let observer1 = TxTestRigs<ItemType>.fullCheckingTestObserver("subject first observer")
            let observer2 = TxTestRigs<ItemType>.fullCheckingTestObserver("subject second observer")
            let subject = RxBehaviourSubject<ItemType>(tag: "subject(\(testName))", defaultItem: "defaultItem")
            var subscription1 : RxSubscription? = nil
            var subscription2 : RxSubscription? = nil

            let run1: RunEntry = (

                    runName: "No active subscription",
                    test: { subject.notifyItem("zero") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: [])
            )

            let run2: RunEntry = (

                    runName: "Subscription active",
                    test: { subscription1 = subject.subscribe(observer1); subject.notifyItem("one") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["defaultItem", "one"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: [])
            )

            let run3: RunEntry = (

                    runName: "Subscription active, multiple calls",
                    test: { subject.notifyItem("two"); subject.notifyItem("three") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["defaultItem", "one", "two", "three"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: [])
            )

            let run4: RunEntry = (

                    runName: "Two subscriptions active",
                    test: { subscription2 = subject.subscribe(observer2); subject.notifyItem("four") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["defaultItem", "one", "two", "three", "four"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: ["three", "four"])
            )

            let run5: RunEntry = (

                    runName: "First subscription disposed",
                    test: { subscription1!.unsubscribe(); subject.notifyItem("five") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["defaultItem", "one", "two", "three", "four"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: ["three", "four", "five"])
            )

            let run6: RunEntry = (

            runName: "Second subscription disposed",
                    test: { subject.notifyCompleted(); subscription2!.unsubscribe(); subject.notifyItem("six") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: ["defaultItem", "one", "two", "three", "four"]),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", itemsThenCompletion: ["three", "four", "five"])
            )

            let runSingleTest = { (runEntry : RunEntry) -> Void in

                let (name, test, expected1, expected2) = runEntry
                let reportFunc1 = reportGenerator(name + " - first observer", expected1, observer1.notifications)
                let reportFunc2 = reportGenerator(name + " - second observer", expected2, observer2.notifications)

                self.trace(2, "\n= run begin ==> \(name) <==\n")
                test()
                self.trace(2, "= run end   ==> \(name) <==\n")

                XCTAssertTrue(expected1 == observer1.notifications, reportFunc1())
                XCTAssertTrue(expected2 == observer2.notifications, reportFunc2())
            }

            runSingleTest(run1)

            runSingleTest(run2)

            runSingleTest(run3)

            runSingleTest(run4)

            runSingleTest(run5)

            runSingleTest(run6)
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSubjectName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}

class RxAsyncSubjectTests : TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testAsyncSubjectBasicItemNotifications()
    {
        let testSubjectName = "asyncSubject"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, test:() -> Void, expected1:RxNotificationQueue<ItemType>, expected2:RxNotificationQueue<ItemType>)

            let observer1 = TxTestRigs<ItemType>.fullCheckingTestObserver("subject first observer")
            let observer2 = TxTestRigs<ItemType>.fullCheckingTestObserver("subject second observer")
            let subject = RxAsyncSubject<ItemType>(tag: "subject(\(testName))")
            var subscription1 : RxSubscription? = nil
            var subscription2 : RxSubscription? = nil

            let run1: RunEntry = (

                    runName: "No active subscription",
                    test: { subject.notifyItem("zero") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: [])
            )

            let run2: RunEntry = (

                    runName: "Subscription active",
                    test: { subscription1 = subject.subscribe(observer1); subject.notifyItem("one") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: [])
            )

            let run3: RunEntry = (

                    runName: "Subscription active, multiple calls",
                    test: { subject.notifyItem("two"); subject.notifyItem("three") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: [])
            )

            let run4: RunEntry = (

                    runName: "Two subscriptions active",
                    test: { subscription2 = subject.subscribe(observer2); assert(subscription2 != nil); subject.notifyItem("four") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: [])
            )

            let run5: RunEntry = (

                    runName: "First subscription disposed",
                    test: { subscription1!.unsubscribe(); subject.notifyItem("five") },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", items: [])
            )

            let run6: RunEntry = (

                    runName: "Complete",
                    test: { subject.notifyCompleted() },
                    expected1: RxNotificationQueue<ItemType>(tag: "expected", items: []),
                    expected2: RxNotificationQueue<ItemType>(tag: "expected", itemsThenCompletion: ["five"])
            )

            let runSingleTest = { (runEntry : RunEntry) -> Void in

                let (name, test, expected1, expected2) = runEntry
                let reportFunc1 = reportGenerator(name + " - first observer", expected1, observer1.notifications)
                let reportFunc2 = reportGenerator(name + " - second observer", expected2, observer2.notifications)

                self.trace(2, "\n= run begin ==> \(name) <==\n")
                test()
                self.trace(2, "= run end   ==> \(name) <==\n")

                XCTAssertTrue(expected1 == observer1.notifications, reportFunc1())
                XCTAssertTrue(expected2 == observer2.notifications, reportFunc2())
            }

            runSingleTest(run1)

            runSingleTest(run2)

            runSingleTest(run3)

            runSingleTest(run4)

            runSingleTest(run5)

            runSingleTest(run6)
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSubjectName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}

