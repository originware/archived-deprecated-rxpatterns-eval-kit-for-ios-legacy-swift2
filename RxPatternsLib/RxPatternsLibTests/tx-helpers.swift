//  Created by Terry Stillone on 30/03/2015.
//  Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//
import Foundation
import XCTest

import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Testing Framework: Tokenisation of notifications for testing purposes.
//

public enum eTxToken<DataItemType : Hashable> : Hashable
{
    case eItem(DataItemType)
    case eNewSubscriptionToken
    case eSubscribeToken
    case eBeginToken
    case eEndToken
    case eUnSubscribeToken
    case eCompleted(IRxError?)

    public init(_ token : eTxToken<DataItemType>)
    {
        self = token
    }
}

extension eTxToken where DataItemType : Equatable
{
    public func isEqual(other: eTxToken<DataItemType>) -> Bool
    {
        switch (self, other)
        {
        case (.eItem(let lhsItem), .eItem(let rhsItem)):
            return lhsItem == rhsItem

        case (.eNewSubscriptionToken, .eNewSubscriptionToken):
            return true

        case (.eSubscribeToken, .eSubscribeToken):
            return true
            
        case (.eBeginToken, .eBeginToken):
            return true
            
        case (.eEndToken, .eEndToken):
            return true
            
        case (.eUnSubscribeToken, .eUnSubscribeToken):
            return true
            
        case (.eCompleted(let lhsError), .eCompleted(let rhsError)):
            return lhsError == rhsError
            
        default:
            return false
        }
    }
}

extension eTxToken where DataItemType : CustomStringConvertible
{
    public var description: String
    {
        switch self
        {
            case .eItem(let item):
                return item.description

            case .eNewSubscriptionToken:
                return "NewSubscribeToken"

            case .eSubscribeToken:
                return "SubscribeToken"
                
            case .eBeginToken:
                return "BeginToken"
                
            case .eEndToken:
                return "EndToken"
                
            case .eUnSubscribeToken:
                return "UnSubscribeToken"
                
            case .eCompleted(let error):
                return error != nil ? error!.description : "Completed"
        }
    }
}

extension eTxToken where DataItemType : Hashable
{
    public var hashValue : Int
    {
        switch self
        {
        case .eItem(let item):
            return item.hashValue

        case .eNewSubscriptionToken:
            return 1

        case .eSubscribeToken:
            return 2
            
        case .eBeginToken:
            return 3
            
        case .eEndToken:
            return 4
            
        case .eUnSubscribeToken:
            return 5
            
        case .eCompleted(let error):
            return error != nil ? error!.description.hashValue : 6
        }
    }
}

public func ==<DataItemType : Equatable>(lhs: eTxToken<DataItemType>, rhs: eTxToken<DataItemType>) -> Bool
{
    return lhs.isEqual(rhs)
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Testing Frameworks: Timing and test matrices
//

public enum eTimingNotifyType<ItemType>
{
    case eItem(ItemType)                                    // Notification of item
    case eCompleted(etTerminationType)                      // Notification of termination as completed or error
    case eTerm
    
    func toNotification() -> RxNotification<ItemType>
    {
        switch self
        {
        case eItem(let item):
            return RxNotification(item : item)
            
        case eCompleted(let termination):
            return RxNotification(termination: termination)
            
        case eTerm:
            fatalError("Unexpected eTimingNotifyType")
        }
    }
    
    var description : String {
        
        switch self
        {
        case eItem(let item):
            return String(item)
            
        case eCompleted(let termination):
            return termination.description
            
        case eTerm:
            return "eEnd"
        }
    }
}

public class TxTimingBoard<ItemType : Equatable>
{
    public typealias TimingEntryType = (timeOffset : RxTimeOffset, tag : Int, timingNotifyType : eTimingNotifyType<ItemType>)
    public typealias TimingArrayType = [TimingEntryType]
    public typealias TimingResultType = (timeOffset : RxTimeOffset, notification : RxNotification<ItemType>)

    public let timingArray : TimingArrayType

    private var timerTagCounter : RxIndexType = 0

    init(_ timingArray : TimingArrayType)
    {
        self.timingArray = timingArray
    }

    func extractTimedItems(tags : Set<Int>) -> TimingArrayType
    {
        return timingArray.filter{ tags.contains($0.tag) }
    }

    func extractTimingArrayItems(tags : Set<Int>) -> (items: RxTypes<ItemType>.TimingArrayType, termination: etTerminationType)
    {
        var items : RxTypes<ItemType>.TimingArrayType = []
        var termination : etTerminationType = etTerminationType.eTermination_Completed

        for entry in timingArray
        {
            if tags.contains(entry.tag)
            {
                switch entry.timingNotifyType
                {
                    case .eItem(let item):
                        items.append(timeOffset: entry.timeOffset, item: item)

                    case .eCompleted(let term):
                        termination = term

                    default:
                        break
                }
            }
        }

        return (items, termination)
    }

    public func extractTimedNotifications(tags: Set<Int>) -> RxNotificationSequence<ItemType>
    {
        let queue = RxNotificationSequence<ItemType>(tag: "TxTimingBoard")
        var lastTermination : etTerminationType? = nil
        var lastTimeOffset : RxTimeOffset = 0

        for entry in timingArray
        {
            if tags.contains(entry.tag)
            {
                switch entry.timingNotifyType
                {
                case .eItem(let item):
                    queue.queueItem(item, timeOffset: entry.timeOffset)
                    
                case .eCompleted(let termination):
                    lastTermination = termination
                    lastTimeOffset = entry.timeOffset

                    if termination.error != nil
                    {
                        queue.queueTermination(termination, timeOffset: lastTimeOffset)

                        return queue
                    }
                    
                default:
                    break
                }
            }
        }

        if let termination = lastTermination
        {
            queue.queueTermination(termination, timeOffset: lastTimeOffset)
        }
        
        return queue
    }

    public func extractTimedNotifications(spec: [(Int, RxTimeOffset)]) -> RxNotificationSequence<ItemType>
    {
        let queue = RxNotificationSequence<ItemType>(tag: "TxTimingBoard")
        var timedEntryByTag = [Int : [TimingEntryType]]()

        for entry in timingArray
        {
            if var entries = timedEntryByTag[entry.tag]
            {
                entries.append(entry)

                timedEntryByTag[entry.tag] = entries
            }
            else
            {
                var entries = [TimingEntryType]()

                entries.append(entry)

                timedEntryByTag[entry.tag] = entries
            }
        }

        for (tag, baseTimeOffset) in spec
        {
            if let entries = timedEntryByTag[tag]
            {
                for entry in entries
                {
                    switch entry.timingNotifyType
                    {
                        case .eItem(let item):
                            queue.queueItem(item, timeOffset: entry.timeOffset + baseTimeOffset)

                        case .eCompleted(let termination):

                            if termination.error != nil
                            {
                                queue.queueTermination(termination, timeOffset: entry.timeOffset + baseTimeOffset)

                                return queue
                            }

                        default:
                            break
                    }
                }
            }
        }

        if let (tag, baseTimeOffset) = spec.last
        {
            if let entries = timedEntryByTag[tag]
            {
                for entry in entries
                {
                    switch entry.timingNotifyType
                    {
                        case .eCompleted(let termination):

                            queue.queueTermination(termination, timeOffset: entry.timeOffset + baseTimeOffset)

                        default:
                            break
                    }
                }
            }
        }

        return queue
    }

    func createTickGenerator(tags : Set<Int>, monitor: Bool = false, bufferTraceMessages: Bool = false) -> RxNotifier_NotifyConsumers<ItemType>
    {
        let queuer                  = RxEvalQueue(sourceTag: "Tick Notifier EvalQueue", evalQueueType: .eSerial)
        let tickNotifier            = RxNotifier_NotifyConsumers_Sync<ItemType>(tag : "Tick Notifier", queuer: queuer)
        let timer                   = RxTimer(tag: "test tick generator")
        let timings                 = extractTimedItems(tags)
        var hasStartedTickGenerator = false

        if monitor
        {
            let traceTag    = "<<<<"

            tickNotifier.trace(traceTag)
            tickNotifier.traceDependant(queuer)
            tickNotifier.traceDependant(timer)
        }

        tickNotifier.onConsumerChange({ [weak tickNotifier] (notifier: RxNotifier_NotifyConsumers) in

            if !hasStartedTickGenerator
            {
                hasStartedTickGenerator = true

                timer.addTickGenerator(RxTime(), generator: { [unowned timer] (index: RxIndexType) -> RxTimeOffset? in

                    if index == 0
                    {
                        return timings.first!.timeOffset
                    }

                    switch timings[index - 1].timingNotifyType
                    {
                        case .eItem(let item):

                            tickNotifier?.notifyItem(item)

                        case .eCompleted(let termination):
                            tickNotifier?.notifyTermination(termination)
                            return nil

                        case .eTerm:
                            timer.cancelAll()
                            return nil
                    }

                    return timings[index].timeOffset
                })
            }
        })

        return tickNotifier
    }

    func createTickSource(tags: Set<Int>, name : String, subscriptionType : eRxSubscriptionType) ->  RxSource<ItemType>
    {
        let tickNotifier = createTickGenerator(tags)

        return RxSource.redirectPat(name, subscriptionType : subscriptionType, notifier : tickNotifier)
    }

    func createTimedSource(tags : Set<Int>, name : String, subscriptionType : eRxSubscriptionType) -> RxSource<ItemType>
    {
        let timedItems = extractTimedItems(tags)
        let timesAreStrict = true

        var nextNotification: eTimingNotifyType<ItemType>? = nil

        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.asyncGenPat(timesAreStrict, generator: { (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxAsyncGenCommand in

            if let notification = nextNotification
            {
                switch notification
                {
                    case .eItem(let item):

                        notifier.notifyItem(item)

                    case .eCompleted(let termination):

                        notifier.notifyTermination(termination)

                        return eRxAsyncGenCommand.eStopTicking

                    case .eTerm:

                        return eRxAsyncGenCommand.eStopTicking
                }
            }

            assert(index < timedItems.count, "Expected timeboad for tags: \(tags) to have a completion notification")
            nextNotification = timedItems[Int(index)].timingNotifyType

            return eRxAsyncGenCommand.eNextTickAt(timedItems[Int(index)].timeOffset)
        })

        return RxSource<ItemType>(tag: name, subscriptionType: subscriptionType, evalOp : evalOp)
    }
}

public class TxTestMatrix<ItemType : Equatable> : RxLog
{
    typealias TestType = (subscriptionType : eRxSubscriptionType, timingBoard : TxTimingBoard<ItemType>)

    var subscriptionTypes : [eRxSubscriptionType] = []
    var timingBoard : TxTimingBoard<ItemType>

    var allTests : [TestType] {

        var tests = [TestType]()

        for subscriptionType in subscriptionTypes
        {
            tests.append((subscriptionType, timingBoard))
        }

        return tests
    }

    init(tag : String,
                timingBoard : TxTimingBoard<ItemType>,
                subscriptionTypes : [eRxSubscriptionType] = [.eCold, .eHot])
    {
        self.subscriptionTypes = subscriptionTypes
        self.timingBoard = timingBoard

        super.init(tag : tag)
    }

    init(tag : String,
                timingArray : TxTimingBoard<ItemType>.TimingArrayType,
                subscriptionTypes : [eRxSubscriptionType] = [.eCold, .eHot])
    {
        self.subscriptionTypes = subscriptionTypes
        self.timingBoard = TxTimingBoard<ItemType>(timingArray)

        super.init(tag : tag)
    }

    public func extractTimedNotiications(tags: Set<Int>) -> RxNotificationSequence<ItemType>
    {
        return timingBoard.extractTimedNotifications(tags)
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Testing Frameworks: TxFactory: create objects for Unit Tests.
//

public class TxTickNotifier<ItemType: Equatable> : RxNotifier_NotifyConsumers_Sync<ItemType>
{
    public typealias TimingEntryType = (timeOffset : RxTimeOffset, tag : Int, timingNotifyType : eTimingNotifyType<ItemType>)
    public typealias TimingArrayType = [TimingEntryType]

    let m_timingBoard             : TxTimingBoard<ItemType>
    var m_timer                   = RxTimer(tag: "test tick generator")
    var m_hasStartedTickGenerator = false

    public init(timingBoard : TxTimingBoard<ItemType>)
    {
        let queuer = RxEvalQueue(sourceTag: "Tick Notifier Queuer EvalQueue", evalQueueType: .eSerial)

        self.m_timingBoard = timingBoard

        super.init(tag : "Tick Notifier", queuer: queuer)
    }

    deinit
    {

    }

    public func start(matchTags : Set<Int>)
    {
        if !m_hasStartedTickGenerator
        {
            let timings               = extractTimedItems(matchTags)
            m_hasStartedTickGenerator = true

            m_timer.addTickGenerator(RxTime(), generator: { [unowned self] (index: RxIndexType) -> RxTimeOffset? in

                if index == 0
                {
                    return timings.first!.timeOffset
                }

                switch timings[index - 1].timingNotifyType
                {
                    case .eItem(let item):
                        self.notifyItem(item)

                    case .eCompleted(let termination):
                        self.notifyTermination(termination)
                        return nil

                    case .eTerm:
                        self.m_timer.cancelAll()
                        return nil
                }

                return timings[index].timeOffset
            })
        }
    }

    public func stop()
    {
        m_timer.cancelAll()
        clear()
    }

    func createTickSource(matchTags: Set<Int>, name : String, subscriptionType : eRxSubscriptionType) ->  RxSource<ItemType>
    {
        return RxSource.redirectPat(name, subscriptionType : subscriptionType, notifier : self)
    }

    public override func trace(traceTag: String) -> Self
    {
        super.trace(traceTag)

        traceDependant(queuer)
        traceDependant(m_timer)

        return self
    }

    public func extractTimedItems(tags : Set<Int>) -> TimingArrayType
    {
        return m_timingBoard.timingArray.filter{ tags.contains($0.tag) }
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Testing Frameworks: TxFactory: create objects for Unit Tests.
//

struct TxFactory
{
    /// Create a Unit Test name.
    /// - Parameter title: Title of the Unit Test.
    /// - Returns: The formalised test name.
    static func createTestName(title : String) -> String
    {
        do
        {
            let regex = try NSRegularExpression(pattern:"^[a-zA-Z0-9_]*", options: NSRegularExpressionOptions.DotMatchesLineSeparators)
            let range = regex.rangeOfFirstMatchInString(title, options : NSMatchingOptions.Anchored, range: NSMakeRange(0, title.characters.count))

            if range.length > 0
            {
                return title.substringWithRange((title.startIndex..<title.startIndex.advancedBy(range.length)))
            }
        }
        catch
        {
            fatalError("Unexpected Regex failure")
        }

        return title
    }

    /// Create a Unit Test error.
    /// - Parameter title: The title of the error.
    /// - Returns: The created error.
    static func createError(description : String) -> RxLibError
    {
        return RxLibError(.eCustomError, context: nil, description: description)
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Testing Frameworks: TxTestRigs: Test Rigs for general objects.
//
struct TxTestRigs<ItemType>
{
    static func fullCheckingTestObserver(tag: String, startTime : RxTime? = nil) -> RxTestObserver<ItemType>
    {
        let notificationChecker = RxNotificationChecker<ItemType>()

        notificationChecker.onNonAcceptance = { (error: String) in

            let errorMessage = "notificationChecker error: \(error)"

            RxLog.log(">> \(errorMessage)\n")

            assert(false, errorMessage)
        }

        return RxTestObserver<ItemType>(tag: tag, startTime: startTime, notificationChecker: notificationChecker)
    }

    static func fullCheckingTestObserverManagingSubscription(tag: String, startTime : RxTime? = nil, maxItemCount : RxCountType? = nil) -> RxTestObserverManagingSubscription<ItemType>
    {
        let notificationChecker = RxNotificationChecker<ItemType>()

        notificationChecker.onNonAcceptance = { (error: String) in

            let errorMessage = "notificationChecker error: \(error)"

            RxLog.log(">> \(errorMessage)\n")

            assert(false, errorMessage)
        }

        return RxTestObserverManagingSubscription<ItemType>(tag: tag, startTime: startTime, maxItemCount: maxItemCount, notificationChecker: notificationChecker)
    }

    static func simpleCheckingTestObserver(tag: String, startTime : RxTime? = nil) -> RxTestObserver<ItemType>
    {
        let notificationChecker = RxSimpleNotificationChecker<ItemType>()

        notificationChecker.onNonAcceptance = { (error: String) in

            let errorMessage = "notificationChecker error: \(error)"

            RxLog.log(">> \(errorMessage)\n")

            assert(false, errorMessage)
        }

        return RxTestObserver<ItemType>(tag: tag, startTime: startTime, notificationChecker: notificationChecker)
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Testing Frameworks: TxItemTestRigs: Test Rigs for item based objects.
//

struct TxDisposalTestRigs
{
    /// Wait for a subscription to dispose, timeout as given by TxConfig.TxDisposalTimeoutSec.
    static func waitForDisposal(subscription : RxSubscription)
    {
        let didNotTimeout = subscription.waitForDisposal(timeout: TxConfig.DisposalTimeoutSec)
        
        XCTAssertTrue(didNotTimeout, "Expected disposal to not timeout")
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Testing Frameworks: Report Generators.
//

func reportGenerator(tag: String, _ expected: RxObject, _ got: RxObject) -> (() -> String)
{
    return { () -> String in
        
        let expectedDecription = expected.description
        let gotDescription = got.description
        
        return "Unexpected result\n\n\ttest name ==> \(tag)\n\texpected  ==> \(expectedDecription)\n\tgot       ==> \(gotDescription)\n"
    }
}

func reportGenerator(tag: String, _ instanceMon : RxMonInstances) -> (() -> String)
{
    return { () -> String in

        String(format:"Unexpected non-deallocated objects\n\n\ttest name ==> \(tag)\n\tgot       ==>\n\n\t= report start ====================\n%@\t= report end ================= \n\n", instanceMon.reportNonDeallocatedObjects())
    }
}

func reportGeneratorForSingleItem<ItemType>(tag: String, _ expected: ItemType?, _ item: ItemType?) -> (() -> String)
{
    return { () -> String in

        "Unexpected result\n\n\ttest name ==> '\(tag)\n\texpected  ==> \(expected)\n\tgot       ==> \(item)\n"
    }
}

func reportGenerator<ItemType>(tag: String, _ expected: RxObject, _ got: TRxEnumerable<ItemType>) -> (() -> String)
{
    return { () -> String in

        let expectedDecription = expected.description
        let gotDescription = got.description

        return "Unexpected result\n\n\ttest name ==> \(tag)\n\texpected  ==> \(expectedDecription)\n\tgot       ==> \(gotDescription))\n"
    }
}

func reportItemGenerator<ItemType>(tag: String, _ expected: ItemType, _ item: ItemType) -> (() -> String)
{
    return { () -> String in

        "Unexpected result\n\n\ttest name ==> '\(tag)\n\texpected  ==> \(expected)\n\tgot       ==> \(item)\n"
    }
}

func monStats(action : () -> Void)
{
    let instanceMon = RxMonInstances()

    instanceMon.remarkOnInconsistencies = true
    RxMon.addMonitor("test", monitor: instanceMon)
    action()
    instanceMon.reportNonDeallocatedObjects()
    instanceMon.reportObjectStats()
}

func monInstances(action : () -> Void)
{
    let instanceMon = RxMonInstances()

    RxMon.addMonitor("test", monitor: instanceMon)
    action()
    instanceMon.reportNonDeallocatedObjects()
    instanceMon.reportObjectStats()
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Testing Frameworks: Support Operators
//

func ~=(pattern:String, text:String) -> Bool
{
    let matches : Int
    
    do
    {
        let regex = try NSRegularExpression(pattern:pattern, options: NSRegularExpressionOptions.DotMatchesLineSeparators)
        
        matches = regex.numberOfMatchesInString(text, options: NSMatchingOptions.WithoutAnchoringBounds, range: NSMakeRange(0, text.characters.count))

    }
    catch
    {
        matches = 0
    }

    return matches > 0
}
