//
//  Created by Terry Stillone on 19/03/2015.
//  Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import XCTest

@testable import RxPatternsSDK
@testable import RxPatternsLib

class RxObservable_Boolean_Tests : TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testAll()
    {
        let testObservableName = "all"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, predicate : RxTypes<ItemType>.RxPredicate, expected:RxNotificationQueue<Bool>)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let evenPredicate = { (item : ItemType) -> Bool in return (item % 2) == 0 }
            let oddPredicate =  { (item : ItemType) -> Bool in return (item % 2) == 1 }
            let truePredicate =  { (item : ItemType) -> Bool in return true }
            let falsePredicate =  { (item : ItemType) -> Bool in return false }

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    predicate:  evenPredicate,
                    expected:   RxNotificationQueue<Bool>(itemsThenCompletion: [false])
            )

            let run2: RunEntry = (

                    runName:    "Test no matches",
                    source:     source1,
                    predicate:  falsePredicate,
                    expected:   RxNotificationQueue<Bool>(itemsThenCompletion: [false])
            )

            let run3: RunEntry = (

                    runName:    "Test all matches",
                    source:     source1,
                    predicate:  truePredicate,
                    expected:   RxNotificationQueue<Bool>(itemsThenCompletion: [true])
            )

            let run4: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    predicate:  falsePredicate,
                    expected:   RxNotificationQueue<Bool>(itemsThenCompletion: [true])
            )

            let run5: RunEntry = (

                    runName:    "Test empty source with error",
                    source:     emptySourceWithError,
                    predicate:  truePredicate,
                    expected:   RxNotificationQueue<Bool>(items: [], termination: .eTermination_Error(error))
            )

            let run6: RunEntry = (

                    runName:    "Test no match with error",
                    source:     source1WithError,
                    predicate:  oddPredicate,
                    expected:   RxNotificationQueue<Bool>(itemsThenCompletion: [false])
            )

            let run7: RunEntry = (

                    runName:    "Test all match with error",
                    source:     source1WithError,
                    predicate:  truePredicate,
                    expected:   RxNotificationQueue<Bool>(items: [], termination: .eTermination_Error(error))
            )


            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7
            ] {
                let (runName, source, predicate, expected) = runSpec

                func runTest()
                {
                    let observer = RxTestObserver<Bool>(tag: "\(testObservableName) test observer")
                    let observable = source.all(predicate)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testAny()
    {
        let testObservableName = "any"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expected:RxNotificationQueue<Bool>)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    expected:   RxNotificationQueue<Bool>(itemsThenCompletion: [true])
            )

            let run2: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    expected:   RxNotificationQueue<Bool>(itemsThenCompletion: [false])
            )

            let run3: RunEntry = (

                    runName:    "Test empty source with error",
                    source:     emptySourceWithError,
                    expected:   RxNotificationQueue<Bool>(items: [], termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3
            ] {
                let (runName, source, expected) = runSpec

                func runTest()
                {
                    let observer = RxTestObserver<Bool>(tag: "\(testObservableName) test observer")
                    let observable = source.any()
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testIsEmpty()
    {
        let testObservableName = "isEmpty"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expected:RxNotificationQueue<Bool>)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    expected:   RxNotificationQueue<Bool>(itemsThenCompletion: [false])
            )

            let run2: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    expected:   RxNotificationQueue<Bool>(itemsThenCompletion: [true])
            )

            let run3: RunEntry = (

                    runName:    "Test empty source with error",
                    source:     emptySourceWithError,
                    expected:   RxNotificationQueue<Bool>(items: [], termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3
            ] {
                let (runName, source, expected) = runSpec

                func runTest()
                {
                    let observer = RxTestObserver<Bool>(tag: "\(testObservableName) test observer")
                    let observable = source.isEmpty()
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testSequenceEqual()
    {
        let testObservableName = "sequenceEqual"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, source1:RxSource<ItemType>, source2:ARxProducer<ItemType>, expected:RxNotificationQueue<Bool>)

            let emptySource1 = RxSource<ItemType>.empty()
            let emptySource2 = RxSource<ItemType>.empty()
            let normalSource1 = RxSource<ItemType>.fromArray(["one", "two", "three", "four", "five"], tag:"normalSource1")
            let normalSource2 = RxSource<ItemType>.fromArray(["one", "two", "three", "four", "five"],tag: "normalSource2")
            let normalSource3 = RxSource<ItemType>.fromArray(["one", "two", "not equal", "four", "five"], tag: "normalSource3")
            let sourceShort1 = RxSource<ItemType>.fromArray(["one", "two", "three"], tag: "shortSource1")
            let source1WithError = RxSource<ItemType>.fromArray(["one", "two", "three", "four", "five"], tag: "sourceWithError1", termination: .eTermination_Error(error))
            let emptyErrorSource = RxSource<ItemType>.throwError(error)

            let trueResult = RxNotificationQueue<Bool>(tag: "true", itemsThenCompletion: [true])
            let falseResult = RxNotificationQueue<Bool>(tag: "false", itemsThenCompletion: [false])
            let errorResult = RxNotificationQueue<Bool>(tag: "false", termination: .eTermination_Error(error))

            let run1: RunEntry = (

                    runName:    "Test streams equal",
                    source1:     normalSource1,
                    source2:     normalSource2,
                    expected:    trueResult
            )

            let run2: RunEntry = (

                    runName:    "Test first stream short but equal",
                    source1:     sourceShort1,
                    source2:     normalSource2,
                    expected:    falseResult
            )

            let run3: RunEntry = (

                    runName:    "Test second stream short but equal",
                    source1:     normalSource1,
                    source2:     sourceShort1,
                    expected:    falseResult
            )

            let run4: RunEntry = (

                    runName:    "Test streams not equal",
                    source1:    normalSource1,
                    source2:    normalSource3,
                    expected:   falseResult
            )

            let run5: RunEntry = (

                    runName:    "Test both streams empty",
                    source1:    emptySource1,
                    source2:    emptySource2,
                    expected:   trueResult
            )

            let run6: RunEntry = (

                    runName:    "Test first stream empty",
                    source1:    emptySource1,
                    source2:    normalSource1,
                    expected:   falseResult
            )

            let run7: RunEntry = (

                    runName:    "Test second stream empty",
                    source1:    normalSource1,
                    source2:    emptySource1,
                    expected:   falseResult
            )

            let run8: RunEntry = (

                    runName:    "Test first stream error",
                    source1:    source1WithError,
                    source2:    normalSource2,
                    expected:   errorResult
            )

            let run9: RunEntry = (

                    runName:    "Test second stream error",
                    source1:    normalSource1,
                    source2:    source1WithError,
                    expected:   errorResult
            )

            let run10: RunEntry = (

                    runName:    "Test first empty stream error",
                    source1:    emptyErrorSource,
                    source2:    source1WithError,
                    expected:   errorResult
            )

            let run11: RunEntry = (

                    runName:    "Test second empty stream error",
                    source1:    source1WithError,
                    source2:    emptyErrorSource,
                    expected:   errorResult
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7,
                    run8,
                    run9,
                    run10,
                    run11
            ] {
                let (runName, source1, source2, expected) = runSpec

                func runTestInlineVersion()
                {
                    let observer = RxTestObserver<Bool>(tag: "\(testObservableName) test observer")
                    let observable = source1.sequenceEqual(source2, equateOp: (==)).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                func runTestSourceVersion()
                {
                    let observer = RxTestObserver<Bool>(tag: "\(testObservableName) test observer")
                    let stream1 : ARxProducer<ItemType> = source1
                    let stream2 : ARxProducer<ItemType> = source2
                    let observable = RxSource.sequenceEqual(stream1, stream2: stream2, equateOp: (==)).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTestInlineVersion()
                runTestSourceVersion()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testContains()
    {
        let testObservableName = "contains"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, item:ItemType, expected:RxNotificationQueue<Bool>)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray(["one", "two", "three", "four", "five"])
            let source1WithError = RxSource<ItemType>.fromArray(["one", "two", "three", "four", "five"], termination: .eTermination_Error(error))
            let emptyErrorSource = RxSource<ItemType>.throwError(error)

            let trueResult = RxNotificationQueue<Bool>(tag: "true", itemsThenCompletion: [true])
            let falseResult = RxNotificationQueue<Bool>(tag: "false", itemsThenCompletion: [false])
            let emptyErrorResult = RxNotificationQueue<Bool>(termination: .eTermination_Error(error))

            let run1: RunEntry = (

                    runName:    "Test match found",
                    source:     source1,
                    item:       "two",
                    expected:   trueResult
            )

            let run2: RunEntry = (

                    runName:    "Test match not found",
                    source:     source1,
                    item:       "no match",
                    expected:   falseResult
            )

            let run3: RunEntry = (

                    runName:    "Test no source items with default source empty",
                    source:     emptySource,
                    item:       "one",
                    expected:   falseResult
            )

            let run4: RunEntry = (

                    runName:    "Test empty source with error",
                    source:     emptyErrorSource,
                    item:       "one",
                    expected:   emptyErrorResult
            )

            let run5: RunEntry = (

                    runName:    "Test source with no match and error",
                    source:     source1WithError,
                    item:       "no match",
                    expected:   emptyErrorResult
            )

            let run6: RunEntry = (

                    runName:    "Test source with match and error",
                    source:     source1WithError,
                    item:       "two",
                    expected:   trueResult
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6

            ] {
                let (runName, source, item, expected) = runSpec

                func runTestWithCompareOp()
                {
                    let observer = RxTestObserver<Bool>(tag: "\(testObservableName) test observer")
                    let observable : RxObservableMap<ItemType, Bool> = source.contains(item, equateOp: (==)).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                func runTestWithComparable()
                {
                    let observer = RxTestObserver<Bool>(tag: "\(testObservableName) test observer")
                    let observable : RxObservableMap<ItemType, Bool> = source.contains(item, equateOp: (==)).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTestWithCompareOp()
                runTestWithComparable()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}


class RxObservable_Filter_Tests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testDistinct()
    {
        let testObservableName = "distinct"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expected:RxNotificationQueue<ItemType>)

            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 21, 44, 21, 50])
            let source2 = RxSource<ItemType>.fromArray([10, 10, 10, 10, 10, 10])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 44, 44, 50, 21], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test duplicates",
                    source:     source1,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 44, 50])
            )

            let run2: RunEntry = (

                    runName:    "All the same",
                    source:     source2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10])
            )

            let run3: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run4: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    expected:   RxNotificationQueue<ItemType>(items: [10, 21, 44, 50], termination: .eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:    "Test empty source with error",
                    source:     emptySourceWithError,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))

            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5
            ] {
                let (runName, source, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.distinct().appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                func runTestWithHashOp()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.distinct({ return $0 + 1 }).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                runTestWithHashOp()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testDistinctUntilChanged()
    {
        let testObservableName = "distinctUntilChanged"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expected:RxNotificationQueue<ItemType>)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 21, 44, 21, 50])
            let source2 = RxSource<ItemType>.fromArray([10, 10, 10, 10, 10, 10])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 44, 44, 50, 21], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test duplicates",
                    source:     source1,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 44, 21, 50])
            )

            let run2: RunEntry = (

                    runName:    "All the same",
                    source:     source2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10])
            )

            let run3: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run4: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    expected:   RxNotificationQueue<ItemType>(items: [10, 21, 44, 50, 21], termination: .eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:    "Test empty source with error",
                    source:     emptySourceWithError,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))

            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5
            ] {
                let (runName, source, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.distinctUntilChanged().appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                func runTestWithEquateOp()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.distinctUntilChanged({ return $0 == $1 }).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                runTestWithEquateOp()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testIgnoreElements()
    {
        let testObservableName = "ignoreElements"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expected:RxNotificationQueue<ItemType>)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 21, 44, 21, 50])
            let source2 = RxSource<ItemType>.fromArray([10, 10, 10, 10, 10, 10])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 44, 44, 50, 21], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test with items",
                    source:     source1,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run2: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName:    "Test empty source with error",
                    source:     emptySourceWithError,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))

            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4
            ] {
                let (runName, source, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.ignoreElements().appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testSkip()
    {
        let testObservableName = "skip"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, count:RxIndexType, expected:RxNotificationQueue<ItemType>)

            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [35, 44, 50])
            )

            let run2: RunEntry = (

                    runName:    "Test zero count",
                    source:     source1,
                    count:      0,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 35, 44, 50])
            )

            let run3: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run4: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(items: [35, 44, 50], termination: .eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:    "Test not enough elements",
                    source:     source1,
                    count:      10,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run6: RunEntry = (

                    runName:    "Test not enough elements with error",
                    source:     source1WithError,
                    count:      10,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))

            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, source, count, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.skip(count).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testSkipLast()
    {
        let testObservableName = "skipLast"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, count:RxIndexType, expected:RxNotificationQueue<ItemType>)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 35])
            )

            let run2: RunEntry = (

                    runName:    "Test zero count",
                    source:     source1,
                    count:      0,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 35, 44, 50])
            )

            let run3: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run4: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(items: [10, 21, 35], termination: .eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:    "Test not enough elements",
                    source:     source1,
                    count:      10,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run6: RunEntry = (

                    runName:    "Test not enough elements with error",
                    source:     source1WithError,
                    count:      10,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))

            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, source, count, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.skipLast(count).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testSkipWhile()
    {
        let testObservableName = "skipWhile"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Bool
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, predicate:RxTypes<ItemType>.RxPredicate, expected:RxNotificationQueue<ItemType>)

            let emptySource = RxSource<ItemType>.empty()
            let baseItemList = [true, false, false, true, true]
            let source1 = RxSource<ItemType>.fromArray(baseItemList)
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let source1WithError = RxSource<ItemType>.fromArray(baseItemList, termination: .eTermination_Error(error))

            var skipCount = 0
            let skipByCountPredicate = {(item :ItemType) -> Bool in

                skipCount += 1

                return skipCount < 5
            }

            let skipByValuePredicate = {(item : ItemType) -> Bool in

                return item
            }

            let skipAllPredicate = {(item : ItemType) -> Bool in

                return true
            }

            let skipNonePredicate = {(item : ItemType) -> Bool in

                return false
            }

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    predicate:  skipByCountPredicate,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [true])
            )

            let run2: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    predicate:  skipByValuePredicate,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    predicate:  skipByValuePredicate,
                    expected:   RxNotificationQueue<ItemType>(items : [false, false, true, true], termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName:    "Test empty source with error",
                    source:     emptySourceWithError,
                    predicate:  skipByValuePredicate,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:    "Test skip all",
                    source:     source1,
                    predicate:  skipAllPredicate,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])

            )

            let run6: RunEntry = (

                    runName:    "Test skip none",
                    source:     source1,
                    predicate:  skipNonePredicate,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: baseItemList)

            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, source, predicate, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.skipWhile(predicate).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testSkipUntil()
    {
        let testObservableName = "skipUntil"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, skipSource:RxSource<ItemType>, expected:RxNotificationSequence<ItemType>)

            let t : RxDuration = TxConfig.TestTickUnit
            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromTimedArray([(t, 1), (t * 2, 2), (t * 3, 3)])
            let source2 = RxSource<ItemType>.fromTimedArray([(0, 0)])
            let source3 = RxSource<ItemType>.fromTimedArray([(t * 1.5, 0)])
            let source4 = RxSource<ItemType>.fromTimedArray([(t * 5, 0)])
            let emptySourceWithError = RxSource<ItemType>.fromTimedArray([], termination: .eTermination_Error(error))
            let source1WithError = RxSource<ItemType>.fromTimedArray([(t, 1), (t * 2, 2), (t * 3, 3)], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test skip during",
                    source:     source1,
                    skipSource: source3,
                    expected:   RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t * 2, 2), (t * 3, 3)])
            )

            let run2: RunEntry = (

                    runName:    "Test skip source before",
                    source:     source1,
                    skipSource: source2,
                    expected:   RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, 1), (t * 2, 2), (t * 3, 3)])
            )


            let run3: RunEntry = (

                    runName:    "Test skip source after",
                    source:     source1,
                    skipSource: source4,
                    expected:   RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run4: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    skipSource: source3,
                    expected:   RxNotificationSequence<ItemType>(timedItems : [(t * 2, 2), (t * 3, 3)], termination: etTerminationType.eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:    "Test source with error-before",
                    source:     source1WithError,
                    skipSource: source2,
                    expected:   RxNotificationSequence<ItemType>(timedItems: [(t, 1), (t * 2, 2), (t * 3, 3)], termination: .eTermination_Error(error))
            )

            let run6: RunEntry = (

                    runName:    "Test source with error-after",
                    source:     source1WithError,
                    skipSource: source4,
                    expected:   RxNotificationSequence<ItemType>(termination: .eTermination_Error(error))
            )

            let run7: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    skipSource: source4,
                    expected:   RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run8: RunEntry = (

                    runName:    "Test empty skip-source",
                    source:     source1,
                    skipSource: emptySource,
                    expected:   RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run9: RunEntry = (

                    runName:    "Test error empty skip-source",
                    source:     source1,
                    skipSource: emptySourceWithError,
                    expected:   RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7,
                    run8,
                    run9
            ] {
                let (runName, source, skipSource, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.skipUntil(skipSource).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testTake()
    {
        let testObservableName = "take"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, count:RxIndexType, expected:RxNotificationQueue<ItemType>)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21])
            )

            let run2: RunEntry = (

                    runName:    "Test zero count",
                    source:     source1,
                    count:      0,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run4: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21])
            )

            let run5: RunEntry = (

                    runName:    "Test not enough elements",
                    source:     source1,
                    count:      10,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 35, 44, 50])
            )

            let run6: RunEntry = (

                    runName:    "Test not enough elements with error",
                    source:     source1WithError,
                    count:      10,
                    expected:   RxNotificationQueue<ItemType>(items: [10, 21, 35, 44, 50], termination: .eTermination_Error(error))

            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, source, count, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.take(count).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testTakeLast()
    {
        let testObservableName = "takeLast"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, count:RxIndexType, expected:RxNotificationQueue<ItemType>)

            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [44, 50])
            )

            let run2: RunEntry = (

                    runName:    "Test zero count",
                    source:     source1,
                    count:      0,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run4: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:    "Test not enough elements",
                    source:     source1,
                    count:      10,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 35, 44, 50])
            )

            let run6: RunEntry = (

                    runName:    "Test not enough elements with error",
                    source:     source1WithError,
                    count:      10,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))

            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, source, count, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.takeLast(count).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testTakeLastBuffer()
    {
        let testObservableName = "takeLastBuffer"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, count:RxIndexType, expected:RxNotificationQueue<[ItemType]>)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    count:      2,
                    expected:   RxNotificationQueue<[ItemType]>(itemsThenCompletion: [[44, 50]])
            )

            let run2: RunEntry = (

                    runName:    "Test zero count",
                    source:     source1,
                    count:      0,
                    expected:   RxNotificationQueue<[ItemType]>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    count:      2,
                    expected:   RxNotificationQueue<[ItemType]>(itemsThenCompletion: [])
            )

            let run4: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    count:      2,
                    expected:   RxNotificationQueue<[ItemType]>(items: [], termination: .eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:    "Test not enough elements",
                    source:     source1,
                    count:      10,
                    expected:   RxNotificationQueue<[ItemType]>(itemsThenCompletion: [[10, 21, 35, 44, 50]])
            )

            let run6: RunEntry = (

                    runName:    "Test not enough elements with error",
                    source:     source1WithError,
                    count:      10,
                    expected:   RxNotificationQueue<[ItemType]>(items: [], termination: .eTermination_Error(error))

            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, source, count, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<[ItemType]>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.takeLastBuffer(count).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testTakeWhile()
    {
        let testObservableName = "takeWhile"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Bool
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, predicate:RxTypes<ItemType>.RxPredicate, expected:RxNotificationQueue<ItemType>)

            let emptySource = RxSource<ItemType>.empty()
            let baseItemList = [true, false, false, true, true]
            let source1 = RxSource<ItemType>.fromArray(baseItemList)
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let source1WithError = RxSource<ItemType>.fromArray(baseItemList, termination: .eTermination_Error(error))

            var takeCount = 0
            let takeByCountPredicate = {(item :ItemType) -> Bool in

                takeCount += 1

                return takeCount < 5
            }

            let takeByValuePredicate = {(item : ItemType) -> Bool in

                return item
            }

            let takeAllPredicate = {(item : ItemType) -> Bool in

                return true
            }

            let takeNonePredicate = {(item : ItemType) -> Bool in

                return false
            }

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    predicate:  takeByCountPredicate,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [true, false, false, true])
            )

            let run2: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    predicate:  takeByValuePredicate,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:        "Test source with error",
                    source:     source1WithError,
                    predicate:  takeAllPredicate,
                    expected:   RxNotificationQueue<ItemType>(items : baseItemList, termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName:        "Test empty source with error",
                    source:     emptySourceWithError,
                    predicate:  takeByValuePredicate,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:        "Test take all",
                    source:     source1,
                    predicate:  takeAllPredicate,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: baseItemList)

            )

            let run6: RunEntry = (

                    runName:    "Test take none",
                    source:     source1,
                    predicate:  takeNonePredicate,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])

            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, source, predicate, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.takeWhile(predicate).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(testName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(name) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(name) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testTakeUntil()
    {
        let testObservableName = "takeUntil"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, triggerSource:RxSource<ItemType>, expected:RxNotificationSequence<ItemType>)


            let t : RxDuration = TxConfig.TestTickUnit
            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromTimedArray([(t, 1), (t * 2, 2), (t * 3, 3)])
            let source2 = RxSource<ItemType>.fromTimedArray([(0, 0)])
            let source3 = RxSource<ItemType>.fromTimedArray([(t * 2, 0)])
            let source4 = RxSource<ItemType>.fromTimedArray([(t * 6, 0)])
            let source5 = RxSource<ItemType>.fromTimedArray([(t * 6, 0)])
            let source6 = RxSource<ItemType>.fromTimedArray([(t, 1), (t * 3, 2), (t * 4, 3)])
            let emptySourceWithError = RxSource<ItemType>.fromTimedArray([], termination: .eTermination_Error(error))
            let source1WithError = RxSource<ItemType>.fromTimedArray([(t, 1), (t * 3, 2), (t * 4, 3)], termination: .eTermination_Error(error))
            let source1WithError2 = RxSource<ItemType>.fromTimedArray([(t, 1), (t * 2, 2), (t * 3, 3)], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test take until during",
                    source:     source6,
                    triggerSource: source3,
                    expected:   RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t * 1, 1)])
            )

            let run2: RunEntry = (

                    runName:    "Test take until before",
                    source:     source1,
                    triggerSource: source2,
                    expected:   RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:    "Test take until after",
                    source:     source1,
                    triggerSource: source4,
                    expected:   RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, 1), (t * 2, 2), (t * 3, 3)])
            )

            let run4: RunEntry = (

                    runName:    "Test take until during with error",
                    source:     source1WithError,
                    triggerSource: source3,
                    expected:   RxNotificationSequence<ItemType>(timedItemsThenCompletion : [(t * 1, 1)])
            )

            let run5: RunEntry = (

                    runName:    "Test take until before with error",
                    source:     source1WithError,
                    triggerSource: source2,
                    expected:   RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run6: RunEntry = (

                    runName:    "Test take with error-after",
                    source:     source1WithError2,
                    triggerSource: source4,
                    expected:   RxNotificationSequence<ItemType>(timedItems: [(t, 1), (t * 2, 2), (t * 3, 3)], termination: .eTermination_Error(error))
            )

            let run7: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    triggerSource: source4,
                    expected:   RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run8: RunEntry = (

                    runName:    "Test empty take-source",
                    source:     source1,
                    triggerSource: emptySource,
                    expected:   RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, 1), (t * 2, 2), (t * 3, 3)])
            )

            let run9: RunEntry = (

                    runName:    "Test error empty take-source",
                    source:     source1,
                    triggerSource: emptySourceWithError,
                    expected:   RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, 1), (t * 2, 2), (t * 3, 3)])
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7,
                    run8,
                    run9
            ] {
                let (runName, source, triggerSource, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.takeUntil(triggerSource).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testElementAt()
    {
        let testObservableName = "elementAt"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, index:RxIndexType, expected:RxNotificationQueue<ItemType>)

            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    index:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [35])
            )

            let run2: RunEntry = (

                    runName:    "Test normal operation, first element",
                    source:     source1,
                    index:      0,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10])
            )

            let run3: RunEntry = (

                    runName:    "Test normal operation, last element",
                    source:     source1,
                    index:      4,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [50])
            )

            let run4: RunEntry = (

                    runName:    "Test empty and zero index",
                    source:     emptySource,
                    index:      0,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run5: RunEntry = (

                    runName:    "Test empty source and non-zero index",
                    source:     emptySource,
                    index:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run6: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    index:      1,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [21])
            )

            let run7: RunEntry = (

                    runName:    "Test not enough elements",
                    source:     source1,
                    index:      10,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run8: RunEntry = (

                    runName:    "Test not enough elements with error",
                    source:     source1WithError,
                    index:      10,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7,
                    run8
            ] {
                let (runName, source, index, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.elementAt(index).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testElementAtOrDefault()
    {
        let testObservableName = "elementAtOrDefault"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, defaultItem: ItemType, index:RxIndexType, expected:RxNotificationQueue<ItemType>)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    defaultItem:99,
                    index:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [35])
            )

            let run2: RunEntry = (

                    runName:    "Test normal operation, first element",
                    source:     source1,
                    defaultItem:99,
                    index:      0,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10])
            )

            let run3: RunEntry = (

                    runName:    "Test normal operation, last element",
                    source:     source1,
                    defaultItem:99,
                    index:      4,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [50])
            )

            let run4: RunEntry = (

                    runName:    "Test empty and zero index",
                    source:     emptySource,
                    defaultItem:99,
                    index:      0,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [99])
            )

            let run5: RunEntry = (

                    runName:    "Test empty source and non-zero index",
                    source:     emptySource,
                    defaultItem:99,
                    index:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [99])
            )

            let run6: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    defaultItem:99,
                    index:      1,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [21])
            )

            let run7: RunEntry = (

                    runName:    "Test not enough elements",
                    source:     source1,
                    defaultItem:99,
                    index:      10,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [99])
            )

            let run8: RunEntry = (

                    runName:    "Test not enough elements with error",
                    source:     source1WithError,
                    defaultItem:99,
                    index:      10,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7,
                    run8
            ] {
                let (runName, source, defaultItem, index, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.elementAtOrDefault(index, defaultItem: defaultItem).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testWhereByPredicate()
    {
        let testObservableName = "whereByPredicate"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, predicate : RxTypes<ItemType>.RxPredicate, expected:RxNotificationQueue<ItemType>)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let evenFilterFunc : RxTypes<ItemType>.RxPredicate =  { (item : ItemType) -> Bool in return (item % 2) == 0 }
            let oddFilterFunc =  { (item : ItemType) -> Bool in return (item % 2) == 1 }
            let trueFilterFunc =  { (item : ItemType) -> Bool in return true }
            let falseFilterFunc =  { (item : ItemType) -> Bool in return false }

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    predicate:  evenFilterFunc,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 44, 50])
            )

            let run2: RunEntry = (

                    runName:    "Test filter none",
                    source:     source1,
                    predicate:  trueFilterFunc,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 35, 44, 50])
            )

            let run3: RunEntry = (

                    runName:    "Test filter all",
                    source:     source1,
                    predicate:  falseFilterFunc,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run4: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    predicate:  trueFilterFunc,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run5: RunEntry = (

                    runName:    "Test empty source with error",
                    source:     emptySourceWithError,
                    predicate:  trueFilterFunc,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
            )

            let run6: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    predicate:  oddFilterFunc,
                    expected:   RxNotificationQueue<ItemType>(items: [21, 35], termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, source, filterFunc, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("take \(testObservableName) observer")
                    let observable = source.filter(filterFunc).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testDefaultIfEmpty()
    {
        let testObservableName = "defaultIfEmpty"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, defaultItem: ItemType, expected:RxNotificationQueue<ItemType>)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test more than one item",
                    source:     source1,
                    defaultItem:    99,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 35, 44, 50])
            )

            let run2: RunEntry = (

                    runName:        "Test empty source",
                    source:         emptySource,
                    defaultItem:    99,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [99])
            )

            let run3: RunEntry = (

                    runName:        "Test source with error",
                    source:         source1WithError,
                    defaultItem:    99,
                    expected:       RxNotificationQueue<ItemType>(items: [10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName:        "Test empty source with error",
                    source:         emptySourceWithError,
                    defaultItem:    99,
                    expected:       RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4
            ] {
                let (runName, source, defaultItem, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.defaultIfEmpty(defaultItem).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}

class RxObservable_Blocking_Tests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testFirst()
    {
        let testName = TxFactory.createTestName(#function)
        let testObservableName = "first"

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expected:ItemType?, expectedError:IRxError?)

            let emptySource          = RxSource<ItemType>.empty()
            let source1              = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError     = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                runName: "Test normal operation",
                source: source1,
                expected: 10,
                expectedError: nil
            )

            let run2: RunEntry = (

                runName: "Test empty source",
                source: emptySource,
                expected: nil,
                expectedError: RxLibError(eRxLibErrorType.eNoSuchElement)
            )

            let run3: RunEntry = (

                runName: "Test source with error",
                source: source1WithError,
                expected: 10,
                expectedError: error
            )

            let run4: RunEntry = (

                runName: "Test empty source with error",
                source: emptySourceWithError,
                expected: nil,
                expectedError: error
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
            ]
            {
                let (runName, source, expected, expectedError) = runSpec

                func runTest()
                {
                    let gotItem = source.first()

                    XCTAssertTrue(expected == gotItem, "Expected: \(expected) but got \(gotItem)")
                }

                func runThrowableTest()
                {
                    do
                    {
                        let gotItem = try source.firstThrowable()

                        XCTAssertTrue(expected == gotItem, "Expected: \(expected) but got \(gotItem)")
                    }
                    catch let ex as RxLibError
                    {
                        XCTAssertTrue(ex == expectedError, "Unexpected error thrown: \(ex), expected: \(expectedError)")
                    }
                    catch let ex
                    {
                        XCTFail("Unexpected Exception: \(ex)")
                    }
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                runThrowableTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
    func testLast()
    {
        let testObservableName = "last"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expected:ItemType?, expectedError: IRxError?)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:         "Test normal operation",
                    source:         source1,
                    expected:       50,
                    expectedError:  nil
            )

            let run2: RunEntry = (

                    runName:        "Test empty source",
                    source:         emptySource,
                    expected:       nil,
                    expectedError:  RxLibError(eRxLibErrorType.eNoSuchElement)
            )

            let run3: RunEntry = (

                    runName:        "Test source with error",
                    source:         source1WithError,
                    expected:       50,
                    expectedError:  error
            )

            let run4: RunEntry = (

                    runName:        "Test empty source with error",
                    source:         emptySourceWithError,
                    expected:       nil,
                    expectedError:  error
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
            ] {
                let (runName, source, expected, expectedError) = runSpec

                func runTest()
                {
                    let gotItem = source.last()

                    XCTAssertTrue(expected == gotItem, "Expected: \(expected) but got \(gotItem)")
                }

                func runThrowableTest()
                {
                    do
                    {
                        let gotItem = try source.lastThrowable()

                        XCTAssertTrue(expected == gotItem, "Expected: \(expected) but got \(gotItem)")
                    }
                    catch let ex as RxLibError
                    {
                        XCTAssertTrue(ex == expectedError, "Unexpected error thrown: \(ex), expected: \(expectedError)")
                    }
                    catch let ex
                    {
                        XCTFail("Unexpected Exception: \(ex)")
                    }
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                runThrowableTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testFirstOrDefault()
    {
        let testObservableName = "firstOrDefault"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, defaultItem: ItemType, expected:ItemType?, expectedError: IRxError?)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:        "Test normal operation",
                    source:         source1,
                    defaultItem:    99,
                    expected:       10,
                    expectedError:  nil
            )

            let run2: RunEntry = (

                    runName:        "Test empty source",
                    source:         emptySource,
                    defaultItem:    99,
                    expected:       99,
                    expectedError:  nil
            )

            let run3: RunEntry = (

                    runName:        "Test source with error",
                    source:         source1WithError,
                    defaultItem:    99,
                    expected:       10,
                    expectedError:  error
            )

            let run4: RunEntry = (

                    runName:        "Test empty source with error",
                    source:         emptySourceWithError,
                    defaultItem:    99,
                    expected:       99,
                    expectedError:  error
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
            ] {
                let (runName, source, defaultItem, expected, expectedError) = runSpec

                func runTest()
                {
                    let gotItem = source.firstOrDefault(defaultItem)

                    XCTAssertTrue(expected == gotItem, "Expected: \(expected) but got \(gotItem)")
                }

                func runThrowableTest()
                {
                    do
                    {
                        let gotItem = try source.firstOrDefaultThrowable(defaultItem)

                        XCTAssertTrue(expected == gotItem, "Expected: \(expected) but got \(gotItem)")
                    }
                    catch let ex as RxLibError
                    {
                        XCTAssertTrue(ex == expectedError, "Unexpected error thrown: \(ex), expected: \(expectedError)")
                    }
                    catch let ex
                    {
                        XCTFail("Unexpected Exception: \(ex)")
                    }
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                runThrowableTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testLastOrDefault()
    {
        let testObservableName = "lastOrDefault"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, defaultItem: ItemType, expected:ItemType?, expectedError: IRxError?)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:        "Test normal operation",
                    source:         source1,
                    defaultItem:    99,
                    expected:       50,
                    expectedError:  nil
            )

            let run2: RunEntry = (

                    runName:        "Test empty source",
                    source:         emptySource,
                    defaultItem:    99,
                    expected:       99,
                    expectedError:  nil
            )

            let run3: RunEntry = (

                    runName:        "Test source with error",
                    source:         source1WithError,
                    defaultItem:    99,
                    expected:       50,
                    expectedError:  nil
            )

            let run4: RunEntry = (

                    runName:        "Test empty source with error",
                    source:         emptySourceWithError,
                    defaultItem:    99,
                    expected:       99,
                    expectedError:  nil
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
            ] {
                let (runName, source, defaultItem, expected, expectedError) = runSpec

                func runTest()
                {
                    let gotItem = source.lastOrDefault(defaultItem)

                    XCTAssertTrue(expected == gotItem, "Expected: \(expected) but got \(gotItem)")
                }

                func runThrowableTest()
                {
                    do
                    {
                        let gotItem = try source.lastOrDefaultThrowable(defaultItem)

                        XCTAssertTrue(expected == gotItem, "Expected: \(expected) but got \(gotItem)")
                    }
                    catch let ex as RxLibError
                    {
                        XCTAssertTrue(ex == expectedError, "Unexpected error thrown: \(ex), expected: \(expectedError)")
                    }
                    catch let ex
                    {
                        XCTFail("Unexpected Exception: \(ex)")
                    }
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testSingle()
    {
        let testObservableName = "single"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expected:ItemType?, expectedError: IRxError?)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source2 = RxSource<ItemType>.fromArray([10])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            let source2WithError = RxSource<ItemType>.fromArray([10], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:        "Test more than one item",
                    source:         source1,
                    expected:       nil,
                    expectedError:  RxLibError(eRxLibErrorType.eNoSuchElement)
            )

            let run2: RunEntry = (

                    runName:        "Test one item",
                    source:         source2,
                    expected:       10,
                    expectedError:  nil
            )

            let run3: RunEntry = (

                    runName:        "Test empty source",
                    source:         emptySource,
                    expected:       nil,
                    expectedError:  RxLibError(eRxLibErrorType.eNoSuchElement)
            )

            let run4: RunEntry = (

                    runName:        "Test more than one item with error",
                    source:         source1WithError,
                    expected:       nil,
                    expectedError:  RxLibError(eRxLibErrorType.eNoSuchElement)
            )

            let run5: RunEntry = (

                    runName:        "Test one item with error",
                    source:         source2WithError,
                    expected:       10,
                    expectedError:  error
            )

            let run6: RunEntry = (

                    runName:        "Test empty source with error",
                    source:         emptySourceWithError,
                    expected:       nil,
                    expectedError:  error
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, source, expected, expectedError) = runSpec

                func runTest()
                {
                    let gotItem = source.single()

                    XCTAssertTrue(expected == gotItem, "Expected: \(expected) but got \(gotItem)")
                }

                func runThrowableTest()
                {
                    do
                    {
                        let gotItem = try source.singleThrowable()

                        XCTAssertTrue(expected == gotItem, "Expected: \(expected) but got \(gotItem)")
                    }
                    catch let ex as RxLibError
                    {
                        XCTAssertTrue(ex == expectedError, "Unexpected error thrown: \(ex), expected: \(expectedError)")
                    }
                    catch let ex
                    {
                        XCTFail("Unexpected Exception: \(ex)")
                    }
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                runThrowableTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testSingleOrDefault()
    {
        let testObservableName = "singleOrDefault"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, defaultItem: ItemType, expected:ItemType?, expectedError: IRxError?)

            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source2 = RxSource<ItemType>.fromArray([10])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            let source2WithError = RxSource<ItemType>.fromArray([10], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:        "Test more than one item",
                    source:         source1,
                    defaultItem:    99,
                    expected:       99,
                    expectedError:  nil
            )

            let run2: RunEntry = (

                    runName:        "Test one item",
                    source:         source2,
                    defaultItem:    99,
                    expected:       10,
                    expectedError:  nil
            )

            let run3: RunEntry = (

                    runName:        "Test empty source",
                    source:         emptySource,
                    defaultItem:    99,
                    expected:       99,
                    expectedError:  nil
            )

            let run4: RunEntry = (

                    runName:        "Test more than one item with error",
                    source:         source1WithError,
                    defaultItem:    99,
                    expected:       99,
                    expectedError:  error
            )

            let run5: RunEntry = (

                    runName:        "Test one item with error",
                    source:         source2WithError,
                    defaultItem:    99,
                    expected:       10,
                    expectedError:  error
            )

            let run6: RunEntry = (

                    runName:        "Test empty source with error",
                    source:         emptySourceWithError,
                    defaultItem:    99,
                    expected:       99,
                    expectedError:  error
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, source, defaultItem, expected, expectedError) = runSpec

                func runTest()
                {
                    let gotItem = source.singleOrDefault(defaultItem)

                    XCTAssertTrue(expected == gotItem, "Expected: \(expected) but got \(gotItem)")
                }

                func runThrowableTest()
                {
                    do
                    {
                        let gotItem = try source.singleOrDefaultThrowable(defaultItem)

                        XCTAssertTrue(expected == gotItem, "Expected: \(expected) but got \(gotItem)")
                    }
                    catch let ex as RxLibError
                    {
                        XCTAssertTrue(ex == expectedError, "Unexpected error thrown: \(ex), expected: \(expectedError)")
                    }
                    catch let ex
                    {
                        XCTFail("Unexpected Exception: \(ex)")
                    }
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                runThrowableTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}

class RxObservable_EventHooks_Tests: TxTestCase
{
    let testObservableName = "doOnItem"
    let testName = TxFactory.createTestName(#function)

    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testDoOnNotify()
    {
        func runAll()
        {
            typealias ItemType = Bool
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expectedResult:RxNotificationQueue<ItemType>, expectedDoNotifications : RxNotificationQueue<ItemType>)

            let emptySource = RxSource<ItemType>.empty()
            let baseItems1 = [true, false, true, true]
            let baseItems2 = [true, false, false]
            let source1 = RxSource<ItemType>.fromArray(baseItems1)
            let source1WithError = RxSource<ItemType>.fromArray(baseItems2, termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let run1: RunEntry = (

                    runName: "Test normal item operation",
                    source: source1,
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: baseItems1),
                    expectedDoNotifications : RxNotificationQueue<ItemType>(items: baseItems1)
            )

            let run2: RunEntry = (

                    runName: "Test empty source",
                    source: emptySource,
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: []),
                    expectedDoNotifications : RxNotificationQueue<ItemType>(items: [])
            )

            let run3: RunEntry = (

                    runName: "Test source with items and error",
                    source: source1WithError,
                    expectedResult: RxNotificationQueue<ItemType>(items: baseItems2, termination: .eTermination_Error(error)),
                    expectedDoNotifications : RxNotificationQueue<ItemType>(items: baseItems2)
            )

            let run4: RunEntry = (

                    runName: "Test empty source error",
                    source: emptySourceWithError,
                    expectedResult: RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error)),
                    expectedDoNotifications : RxNotificationQueue<ItemType>(items: [])
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
            ]
            {
                let (runName, source, expectedResult, expectedDoNotifications) = runSpec

                func runTest1()
                {
                    let doNotifications = RxNotificationQueue<ItemType>(tag: "doNotifications")

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.doOnNotify(

                    { (item: ItemType) in

                        doNotifications.queueItem(item)
                    },
                    onCompleted: nil
                    )

                    let reportFunc = reportGenerator(runName, expectedResult, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expectedDoNotifications == doNotifications, "unexpected \(testObservableName) action result")
                    XCTAssertTrue(expectedResult == observer.notifications, reportFunc())
                }

                func runTest2()
                {
                    let doNotifications = RxNotificationQueue<ItemType>(tag: "doNotifications")

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.doOnNotify(

                    { (item: ItemType) in

                        doNotifications.queueItem(item)
                    },
                    onCompleted: { (error : IRxError?) in
                        doNotifications.queueCompleted(error)
                    }
                    )

                    let reportFunc = reportGenerator(runName, expectedResult, observer.notifications)
                    let doReportFunc = reportGenerator(runName, expectedResult, doNotifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expectedResult == doNotifications, doReportFunc())
                    XCTAssertTrue(expectedResult == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest1()
                runTest2()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testFinally()
    {
        let testObservableName = "finally"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Bool
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expectedResult:RxNotificationQueue<ItemType>, expectedFinally : Bool)


            let error = TxFactory.createError(testName)
            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([true, false, true, true])
            let source1WithError = RxSource<ItemType>.fromArray([true, false, false], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let run1: RunEntry = (

                    runName: "Test normal item operation",
                    source: source1,
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: [true, false, true, true]),
                    expectedFinally : true
            )

            let run2: RunEntry = (

                    runName: "Test empty source",
                    source: emptySource,
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: []),
                    expectedFinally : true
            )

            let run3: RunEntry = (

                    runName: "Test source with items and error",
                    source: source1WithError,
                    expectedResult: RxNotificationQueue<ItemType>(items: [true, false, false], termination: .eTermination_Error(error)),
                    expectedFinally : false
            )

            let run4: RunEntry = (

                    runName: "Test empty source error",
                    source: emptySourceWithError,
                    expectedResult: RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error)),
                    expectedFinally : false
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
            ]
            {
                let (runName, source, expectedResult, expectedFinally) = runSpec

                func runTest()
                {
                    var haveRunFinally = false
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.finally({ haveRunFinally = true })
                    let reportFunc = reportGenerator(runName, expectedResult, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expectedFinally == haveRunFinally, "unexpected \(testObservableName) action result")
                    XCTAssertTrue(expectedResult == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}

class RxObservable_Transform_Tests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testSelect()
    {
        let testObservableName = "select"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, mapFunc: RxTypes2<ItemType, ItemType>.RxItemMapOptionalFunc, expected:RxNotificationQueue<ItemType>)

            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let incMapFunc = { (item : ItemType) -> ItemType? in return item + 1 }
            let oddMapFunc =  { (item : ItemType) -> ItemType? in return (item % 2) == 1 ? nil : item + 1 }
            let nilMapFunc =  { (item : ItemType) -> ItemType? in return nil }

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    mapFunc:    incMapFunc,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [11, 22, 36, 45, 51])
            )

            let run2: RunEntry = (

                    runName:    "Test map to nil",
                    source:     source1,
                    mapFunc:    nilMapFunc,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:    "Test intermittent map",
                    source:     source1,
                    mapFunc:    oddMapFunc,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [11, 45, 51])
            )

            let run4: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    mapFunc:  oddMapFunc,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run5: RunEntry = (

                    runName:    "Test empty source with error",
                    source:     emptySourceWithError,
                    mapFunc:    oddMapFunc,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
            )

            let run6: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    mapFunc:    oddMapFunc,
                    expected:   RxNotificationQueue<ItemType>(items: [11, 45, 51], termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, source, mapFunc, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.select(mapFunc).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func NotImpl_testSelectMany()
    {
        // To be implemented
    }

    func testScan()
    {
        let testObservableName = "scan"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {

            typealias ItemType = Float
            typealias RunEntry = (runName:String, source: RxSource<ItemType>, expected: RxNotificationQueue<ItemType>)


            let source1 = RxSource<ItemType>.fromArray([33.2, 21.4, 35.2, 44.6, 50.6])
            let sourceWithError = RxSource<ItemType>.fromArray([10.3, 21.2, 35.4, 44.1, 50.7], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let emptySource = RxSource<ItemType>.empty()

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [33.2, 54.6, 89.8, 134.4, 185.0])
            )

            let run2: RunEntry = (

                    runName:    "Test empty stream",
                    source:     emptySource,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:    "Test stream with error",
                    source:     sourceWithError,
                    expected:   RxNotificationQueue<ItemType>(items: [10.3, 31.5, 66.9, 111.0, 161.7], termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName:    "Test empty stream with error",
                    source:     emptySourceWithError,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4
            ] {
                let (runName, source, expected) = runSpec

                func runTest_With_SumOp()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let aggregateFunc = { (accumItem: ItemType, item: ItemType) -> ItemType in

                        return accumItem + item
                    }

                    let observable = source.scan(0, accumulator: aggregateFunc)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)


                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest_With_SumOp()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}


class RxObservable_Combine_Tests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testCombineLatest()
    {
        let testObservableName = "combineLatest"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, Set<Int>, Set<Int>, expected :RxNotificationSequence<ItemType>)
            typealias TimingArrayType = RxTypes<ItemType>.TimingArrayType

            let t : RxTimeOffset = TxConfig.TestTickUnit

            let timingBoard = TxTimingBoard<ItemType>([
                      (t,     1, .eItem("1-0")),
                      (t * 2, 2, .eItem("2-0")),
                      (t * 2, 4, .eCompleted(.eTermination_Completed)),
                      (t * 2, 5, .eItem("5-0")),
                      (t * 3, 1, .eItem("1-1")),
                      (t * 4, 3, .eItem("3-0")),
                      (t * 4, 5, .eCompleted(.eTermination_Error(error))),
                      (t * 5, 2, .eItem("2-1")),
                      (t * 6, 2, .eItem("2-2")),
                      (t * 7, 1, .eItem("1-2")),
                      (t * 8, 3, .eItem("3-1")),
                      (t * 9, 2, .eCompleted(.eTermination_Completed)),
                      (t * 10, 1, .eCompleted(.eTermination_Completed)),
                      (t * 11, 3, .eCompleted(.eTermination_Completed))
              ])

            let run1: RunEntry = (

                    runName:        "Test basic operation",
                    source1Tags:    [1],
                    source2Tags:    [2],
                    expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t * 2, "1-0,2-0"), (t * 3, "1-1,2-0"), (t * 5, "1-1,2-1"), (t * 6, "1-1,2-2"), (t * 7, "1-2,2-2")])
            )

            let run2: RunEntry = (

                    runName:        "Test first stream completing first",
                    source1Tags:    [3],
                    source2Tags:    [1],
                    expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t * 4, "3-0,1-1"), (t * 7, "3-0,1-2"), (t * 8, "3-1,1-2")])
            )

            let run3: RunEntry = (

                    runName:        "Test second stream completing first",
                    source1Tags:    [1],
                    source2Tags:    [3],
                    expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t * 4, "1-1,3-0"), (t * 7, "1-2,3-0"), (t * 8, "1-2,3-1")])
            )

            let run4: RunEntry = (

                    runName:        "Test both streams empty",
                    source1Tags:    [4],
                    source2Tags:    [4],
                    expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run5: RunEntry = (

                    runName:        "Test first stream empty",
                    source1Tags:    [4],
                    source2Tags:    [1],
                    expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run6: RunEntry = (

                    runName:        "Test second stream empty",
                    source1Tags:    [1],
                    source2Tags:    [4],
                    expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run7: RunEntry = (

                    runName:        "Test first stream error",
                    source1Tags:    [5],
                    source2Tags:    [1],
                    expected:       RxNotificationSequence<ItemType>(timedItems: [(t * 2, "5-0,1-0"), (t * 3, "5-0,1-1")], termination: etTerminationType.eTermination_Error(error))
            )

            let run8: RunEntry = (

                    runName:        "Test second stream error",
                    source1Tags:    [1],
                    source2Tags:    [5],
                    expected:       RxNotificationSequence<ItemType>(timedItems: [(t * 2, "1-0,5-0"), (t * 3, "1-1,5-0")], termination: etTerminationType.eTermination_Error(error))
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7,
                    run8
            ] {
                let (runName, source1Tags, source2Tags, expected) = runSpec

                func runTestInLineVersion()
                {
                    let subscriptionType = eRxSubscriptionType.eHot
                    let source1 = timingBoard.createTimedSource(source1Tags, name: runName + "/Source1", subscriptionType : subscriptionType)
                    let source2 = timingBoard.createTimedSource(source2Tags, name: runName + "/Source2", subscriptionType : subscriptionType)

                    let selector = { (item1 : ItemType, item2 : ItemType) -> ItemType? in

                        return item1 + "," + item2
                    }

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source1.combineLatest(source2, selector : selector)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                func runTestSourceVersion()
                {
                    let subscriptionType = eRxSubscriptionType.eHot
                    let source1 = timingBoard.createTimedSource(source1Tags, name: runName + "/Source1", subscriptionType : subscriptionType)
                    let source2 = timingBoard.createTimedSource(source2Tags, name: runName + "/Source2", subscriptionType : subscriptionType)

                    let selector = { (item1 : ItemType, item2 : ItemType) -> ItemType? in

                        return item1 + "," + item2
                    }

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = RxSource.combineLatest(source1, stream2: source2, selector : selector)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTestInLineVersion()
                runTestSourceVersion()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testMerge()
    {
        let testObservableName = "merge"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, Set<Int>, Set<Int>, expectedTags :Set<Int>)
            typealias TimingArrayType = RxTypes<ItemType>.TimingArrayType


            let t : RxTimeOffset = TxConfig.TestTickUnit

            let timingBoard = TxTimingBoard<ItemType>([
                      (t,     1, .eItem("1-0")),
                      (t * 2, 2, .eItem("2-0")),
                      (t * 2, 4, .eCompleted(.eTermination_Completed)),
                      (t * 3, 1, .eItem("1-1")),
                      (t * 4, 3, .eItem("3-0")),
                      (t * 4, 5, .eCompleted(.eTermination_Error(error))),
                      (t * 5, 2, .eItem("2-1")),
                      (t * 6, 2, .eItem("2-2")),
                      (t * 7, 1, .eItem("1-2")),
                      (t * 8, 3, .eItem("3-1")),
                      (t * 9, 2, .eCompleted(.eTermination_Completed)),
                      (t * 10, 1, .eCompleted(.eTermination_Completed)),
                      (t * 11, 3, .eCompleted(.eTermination_Completed))
              ])

            let run1: RunEntry = (

                    runName:        "Test basic operation",
                    source1Tags:    [1],
                    source2Tags:    [2],
                    expectedTags:   [1, 2]
            )

            let run2: RunEntry = (

                    runName:        "Test first stream completing first",
                    source1Tags:    [3],
                    source2Tags:    [1],
                    expectedTags:   [3, 1]
            )

            let run3: RunEntry = (

                    runName:        "Test second stream completing first",
                    source1Tags:    [1],
                    source2Tags:    [3],
                    expectedTags:   [3, 1]
            )

            let run4: RunEntry = (

                    runName:        "Test both streams empty",
                    source1Tags:    [4],
                    source2Tags:    [4],
                    expectedTags:   [4]
            )

            let run5: RunEntry = (

                    runName:        "Test first stream empty",
                    source1Tags:    [4],
                    source2Tags:    [1],
                    expectedTags:   [4, 1]
            )

            let run6: RunEntry = (

                    runName:        "Test second stream empty",
                    source1Tags:    [1],
                    source2Tags:    [4],
                    expectedTags:   [1, 4]
            )

            let run7: RunEntry = (

                    runName:        "Test first stream error",
                    source1Tags:    [5],
                    source2Tags:    [1],
                    expectedTags:   [5, 1]
            )

            let run8: RunEntry = (

                    runName:        "Test second stream error",
                    source1Tags:    [1],
                    source2Tags:    [5],
                    expectedTags:   [1, 5]
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7,
                    run8
            ] {
                let (runName, source1Tags, source2Tags, expectedTags) = runSpec

                func runTestInLineVersion()
                {
                    let subscriptionType = eRxSubscriptionType.eHot
                    let source1 = timingBoard.createTimedSource(source1Tags, name: runName + "/Source1", subscriptionType : subscriptionType)
                    let source2 = timingBoard.createTimedSource(source2Tags, name: runName + "/Source2", subscriptionType : subscriptionType)

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source1.merge(source2)
                    let expected = timingBoard.extractTimedNotifications(expectedTags)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                func runTestSourceVersion()
                {
                    let subscriptionType = eRxSubscriptionType.eHot
                    let source1 = timingBoard.createTimedSource(source1Tags, name: runName + "/Source1", subscriptionType : subscriptionType)
                    let source2 = timingBoard.createTimedSource(source2Tags, name: runName + "/Source2", subscriptionType : subscriptionType)

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = RxSource.merge([source1, source2])
                    let expected = timingBoard.extractTimedNotifications(expectedTags)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTestInLineVersion()
                runTestSourceVersion()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testConcat()
    {
        let testObservableName = "concat"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, Set<Int>, Set<Int>, expected:[(Int, RxTimeOffset)])
            typealias TimingArrayType = RxTypes<ItemType>.TimingArrayType


            let t : RxTimeOffset = TxConfig.TestTickUnit

            let timingBoard = TxTimingBoard<ItemType>([
                      (t,     1, .eItem("1-0")),
                      (t * 2, 4, .eCompleted(.eTermination_Completed)),
                      (t * 2, 2, .eItem("2-0")),
                      (t * 3, 1, .eItem("1-1")),
                      (t * 4, 5, .eCompleted(.eTermination_Error(error))),
                      (t * 4, 3, .eItem("3-0")),
                      (t * 5, 2, .eItem("2-1")),
                      (t * 6, 3, .eItem("3-1")),
                      (t * 7, 3, .eCompleted(.eTermination_Completed)),
                      (t * 8, 2, .eItem("2-2")),
                      (t * 9, 2, .eCompleted(.eTermination_Completed)),
                      (t * 10, 1, .eItem("1-2")),
                      (t * 11, 1, .eCompleted(.eTermination_Completed))
              ])

            let run1: RunEntry = (

                    runName:        "Test basic operation",
                    source1Tags:    [1],
                    source2Tags:    [2],
                    expected:       [(1, 0), (2, t * 12)]
            )

            let run2: RunEntry = (

                    runName:        "Test first stream completing first",
                    source1Tags:    [3],
                    source2Tags:    [1],
                    expected:       [(3, 0), (1,  t * 10)]
            )

            let run3: RunEntry = (

                    runName:    "Test second stream completing first",
                    source1:    [1],
                    source2:    [3],
                    expected:   [(1, 0), (3,  t * 12)]
            )


            let run4: RunEntry = (

                    runName:    "Test both streams empty",
                    source1:    [4],
                    source2:    [4],
                    expected:   [(4, 0)]
            )

            let run5: RunEntry = (

                    runName:    "Test first stream empty",
                    source1:    [4],
                    source2:    [1],
                    expected:   [(4, 0), (1, t * 2)]
            )

            let run6: RunEntry = (

                    runName:    "Test second stream empty",
                    source1:    [1],
                    source2:    [4],
                    expected:   [(1, 0), (4,  t * 12)]
            )

            let run7: RunEntry = (

                    runName:    "Test first stream error",
                    source1:    [5],
                    source2:    [1],
                    expected:   [(5, 0), (1,  t * 12)]
            )

            let run8: RunEntry = (

                    runName:    "Test second stream error",
                    source1:    [1],
                    source2:    [5],
                    expected:   [(1, 0), (5,  t * 10)]
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7,
                    run8
            ] {
                let (runName, source1Tags, source2Tags, expectedSpec) = runSpec

                func runTestInLineVersion()
                {
                    let subscriptionType = eRxSubscriptionType.eHot
                    let source1 = timingBoard.createTimedSource(source1Tags, name: runName + "/Source1", subscriptionType : subscriptionType)
                    let source2 = timingBoard.createTimedSource(source2Tags, name: runName + "/Source2", subscriptionType : subscriptionType)

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source1.concat(source2)
                    let expected = timingBoard.extractTimedNotifications(expectedSpec)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                func runTestSourceVersion()
                {
                    let subscriptionType = eRxSubscriptionType.eHot
                    let source1 = timingBoard.createTimedSource(source1Tags, name: runName + "/Source1", subscriptionType : subscriptionType)
                    let source2 = timingBoard.createTimedSource(source2Tags, name: runName + "/Source2", subscriptionType : subscriptionType)

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = RxSource.concat([source1, source2])
                    let expected = timingBoard.extractTimedNotifications(expectedSpec)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTestInLineVersion()
                runTestSourceVersion()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testZip()
    {
        let testObservableName = "zip"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias TargetItemType = String
            typealias RunEntry = (runName:String, source1:RxSource<ItemType>, source2:RxSource<ItemType>, expected:RxNotificationQueue<TargetItemType>)


            let normalSource1 = RxSource<ItemType>.fromArray(["1-0", "1-1", "1-2", "1-3", "1-4"])
            let normalSource2 = RxSource<ItemType>.fromArray(["2-0", "2-1", "2-2", "2-3", "2-4"])
            let normalSource3 = RxSource<ItemType>.fromArray(["2-0", "2-1", "2-2", "2-3"])
            let sourceWithError = RxSource<ItemType>.fromArray(["3-0", "3-1", "3-2", "3-3"], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let emptySource1 = RxSource<ItemType>.empty()
            let emptySource2 = RxSource<ItemType>.empty()

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source1:    normalSource1,
                    source2:    normalSource2,
                    expected:   RxNotificationQueue<TargetItemType>(itemsThenCompletion: ["1-0,2-0", "1-1,2-1", "1-2,2-2", "1-3,2-3", "1-4,2-4"])
            )

            let run2: RunEntry = (

                    runName:    "Test first stream less than second",
                    source1:    normalSource3,
                    source2:    normalSource1,
                    expected:   RxNotificationQueue<TargetItemType>(itemsThenCompletion: ["2-0,1-0", "2-1,1-1", "2-2,1-2", "2-3,1-3"])
            )

            let run3: RunEntry = (

                    runName:    "Test second stream less than second",
                    source1:    normalSource1,
                    source2:    normalSource3,
                    expected:   RxNotificationQueue<TargetItemType>(itemsThenCompletion: ["1-0,2-0", "1-1,2-1", "1-2,2-2", "1-3,2-3"])
            )


            let run4: RunEntry = (

                    runName:    "Test both streams empty",
                    source1:    emptySource1,
                    source2:    emptySource2,
                    expected:   RxNotificationQueue<TargetItemType>(itemsThenCompletion: [])
            )

            let run5: RunEntry = (

                    runName:    "Test first stream empty",
                    source1:    emptySource1,
                    source2:    normalSource1,
                    expected:   RxNotificationQueue<TargetItemType>(itemsThenCompletion: [])
            )

            let run6: RunEntry = (

                    runName:    "Test second stream empty",
                    source1:    normalSource2,
                    source2:    emptySource2,
                    expected:   RxNotificationQueue<TargetItemType>(itemsThenCompletion: [])
            )

            let run7: RunEntry = (

                    runName:    "Test first stream error",
                    source1:    sourceWithError,
                    source2:    normalSource1,
                    expected:   RxNotificationQueue<TargetItemType>(items: ["3-0,1-0", "3-1,1-1", "3-2,1-2", "3-3,1-3"], termination: .eTermination_Error(error))
            )

            let run8: RunEntry = (

                    runName:    "Test second stream error",
                    source1:    normalSource1,
                    source2:    sourceWithError,
                    expected:   RxNotificationQueue<TargetItemType>(items: ["1-0,3-0", "1-1,3-1", "1-2,3-2", "1-3,3-3"], termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7,
                    run8
            ] {
                let (runName, source1, source2, expected) = runSpec

                func runTestInLineVersion()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source1.zip(source2, selector : { (item1 : ItemType, item2 : ItemType) -> TargetItemType? in

                        return item1 + "," + item2
                    })

                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                func runTestSourceVersion()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = RxSource<TargetItemType>.zip([source1, source2], selector : { (item1 : ItemType, item2 : ItemType) -> TargetItemType? in

                        return item1 + "," + item2
                    })

                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTestInLineVersion()
                runTestSourceVersion()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}

class RxObservable_Timing_Tests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testBufferCountAndSkip()
    {
        let testObservableName = "bufferByCount"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, Set<Int>, Int, Int, expected :RxNotificationSequence<[ItemType]>)
            typealias TimingArrayType = RxTypes<ItemType>.TimingArrayType


            let t : RxTimeOffset = TxConfig.TestTickUnit

            let timingBoard = TxTimingBoard<ItemType>([
                      (t,     1, .eItem("1-0")),
                      (t * 2, 2, .eItem("2-0")),
                      (t * 2, 4, .eCompleted(.eTermination_Completed)),
                      (t * 3, 1, .eItem("1-1")),
                      (t * 4, 3, .eItem("3-0")),
                      (t * 4, 5, .eCompleted(.eTermination_Error(error))),
                      (t * 5, 2, .eItem("2-1")),
                      (t * 6, 2, .eItem("2-2")),
                      (t * 7, 1, .eItem("1-2")),
                      (t * 8, 3, .eItem("3-1")),
                      (t * 8, 1, .eItem("1-3")),
                      (t * 9, 2, .eCompleted(.eTermination_Completed)),
                      (t * 10, 1, .eCompleted(.eTermination_Completed)),
                      (t * 11, 3, .eCompleted(.eTermination_Error(error)))
              ])

            let run1: RunEntry = (

                    runName:        "Test basic operation where skip equals count",
                    sourceTags:     [1],
                    count:          2,
                    skip:           2,
                    expected:       RxNotificationSequence<[ItemType]>(timedItemsThenCompletion: [(t * 3, ["1-0", "1-1"]), (t * 9, ["1-2", "1-3"])])
            )

            let run2: RunEntry = (

                    runName:        "Test basic operation with different skip to count",
                    sourceTags:     [1],
                    count:          2,
                    skip:           1,
                    expected:       RxNotificationSequence<[ItemType]>(timedItemsThenCompletion: [(t * 3, ["1-0", "1-1"]), (t * 7, ["1-1", "1-2"]), (t * 8, ["1-2", "1-3"]), (t * 10, ["1-3"])])
            )

            let run3: RunEntry = (

                    runName:        "Test empty stream",
                    sourceTags:     [4],
                    count:          4,
                    skip:           0,
                    expected:       RxNotificationSequence<[ItemType]>(timedItemsThenCompletion: [])
            )

            let run4: RunEntry = (

                    runName:        "Test empty stream with error",
                    sourceTags:     [5],
                    count:          4,
                    skip:           0,
                    expected:       RxNotificationSequence<[ItemType]>(timedItems: [], termination: .eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:        "Test stream with error",
                    sourceTags:     [3],
                    count:          2,
                    skip:           0,
                    expected:       RxNotificationSequence<[ItemType]>(timedItems: [(t * 8, ["3-0", "3-1"])], termination: .eTermination_Error(error))
            )

            let run6: RunEntry = (

                    runName:        "Test zero count",
                    sourceTags:     [1],
                    count:          0,
                    skip:           0,
                    expected:       RxNotificationSequence<[ItemType]>(timedItemsThenCompletion: [(t * 10, ["1-0", "1-1", "1-2", "1-3"])])
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ]
            {
                let (runName, sourceTags, count, skip, expected) = runSpec

                func runTest()
                {
                    let subscriptionType = eRxSubscriptionType.eHot
                    let source = timingBoard.createTickSource(sourceTags, name: runName + "/Source", subscriptionType: subscriptionType)

                    let observer = TxTestRigs<[ItemType]>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.buffer(count, skip: skip)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue((==)(expected, observer.notifications), reportFunc())
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testBufferByDurationAndTimeShift()
    {
        let testObservableName = "bufferByDuration"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, Set<Int>, duration: RxDuration, timeshift: RxDuration, expected :RxNotificationSequence<[ItemType]>)
            typealias TimingArrayType = RxTypes<ItemType>.TimingArrayType


            let t : RxTimeOffset = TxConfig.TestTickUnit

            let timingBoard = TxTimingBoard<ItemType>([
                      (t,     1, .eItem("1-0")),
                      (t * 2, 2, .eItem("2-0")),
                      (t * 2, 3, .eItem("3-0")),
                      (t * 3, 4, .eCompleted(.eTermination_Completed)),
                      (t * 3, 1, .eItem("1-1")),
                      (t * 3, 5, .eCompleted(.eTermination_Error(error))),
                      (t * 5, 2, .eItem("2-1")),
                      (t * 6, 3, .eItem("3-1")),
                      (t * 6, 2, .eItem("2-2")),
                      (t * 7, 1, .eItem("1-2")),
                      (t * 9, 1, .eItem("1-3")),
                      (t * 9, 3, .eCompleted(.eTermination_Error(error))),
                      (t * 9, 2, .eCompleted(.eTermination_Completed)),
                      (t * 10, 1, .eCompleted(.eTermination_Completed))

              ])

            let run1: RunEntry = (

                    runName:        "Test basic operation",
                    sourceTags:     [1],
                    duration:       t * 4,
                    timeshift:      t * 4,
                    expected:       RxNotificationSequence<[ItemType]>(timedItemsThenCompletion: [(t * 4, ["1-0", "1-1"]), (t * 8, ["1-2"]), (t * 10, ["1-3"])])
            )

            let run2: RunEntry = (

                    runName:        "Test basic operation, with timeshift",
                    sourceTags:     [1],
                    duration:       t * 4,
                    timeshift:      t * 2,
                    expected:       RxNotificationSequence<[ItemType]>(timedItemsThenCompletion: [(t * 4, ["1-0", "1-1"]), (t * 8, ["1-2"])])
            )

            let run3: RunEntry = (

                    runName:        "Test empty stream",
                    sourceTags:     [4],
                    duration:       t * 2,
                    timeshift:      t * 2,
                    expected:       RxNotificationSequence<[ItemType]>(timedItemsThenCompletion: [(t * 2, [])])
            )

            let run4: RunEntry = (

                    runName:        "Test empty stream with error",
                    sourceTags:     [5],
                    duration:       t * 2,
                    timeshift:      t * 2,
                    expected:       RxNotificationSequence<[ItemType]>(timedItems: [(t * 5, [])], termination: .eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:        "Test stream with error",
                    sourceTags:     [3],
                    duration:       t * 4,
                    timeshift:      t * 2,
                    expected:       RxNotificationSequence<[ItemType]>(timedItems: [(t * 4, ["3-0"]), (t * 8, ["3-1"])], termination: .eTermination_Error(error))
            )

            let run6: RunEntry = (

                    runName:        "Test zero duration",
                    sourceTags:     [1],
                    duration:       0,
                    timeshift:      0,
                    expected:       RxNotificationSequence<[ItemType]>(timedItemsThenCompletion: [(t, ["1-0"]), (t * 3, ["1-1"]), (t * 7, ["1-2"]), (t * 9, ["1-3"])])
            )

            let run7: RunEntry = (

                    runName:        "Test negative duration",
                    sourceTags:     [1],
                    duration:       -t,
                    timeshift:      0,
                    expected:       RxNotificationSequence<[ItemType]>(timedItems: [], termination: .eTermination_Error(RxLibError(.eInvalidTimePeriod)))
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7
            ]
            {
                let (runName, sourceTags, duration, timeShift, expected) = runSpec

                func runTest()
                {
                    let subscriptionType = eRxSubscriptionType.eHot

                    let observer = TxTestRigs<[ItemType]>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    let tickNotifier = TxTickNotifier<ItemType>(timingBoard: timingBoard)
                    let source = tickNotifier.createTickSource(sourceTags, name: runName + "/source", subscriptionType: subscriptionType)
                    let observable = source.buffer(duration, timeShift: timeShift).traceAll("<<", bufferTraceMessages: true)

                    let subscription = observable.subscribe(observer)

                    tickNotifier.start(sourceTags)

                    subscription.runAsync()

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    tickNotifier.stop()

                    #if RxMonEnabled
                        if let mon = RxSDK.mon.trace where expected != observer.notifications
                        {
                            // Output a trace message to console.
                            RxLog.log(mon.traceMessageBuffer)
                        }
                    #endif

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(runName) <==\n")

                #if RxMonEnabled
                    if let mon = RxSDK.mon.trace
                    {
                        if !mon.isEmpty
                        {
                            RxLog.log(mon.traceMessageBuffer)
                        }

                        mon.clearTraceBuffer()
                    }
                #endif
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testBufferByCountAndDuration()
    {
        let testObservableName = "bufferByDuration"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, Set<Int>, duration: RxDuration, count: RxCountType, expected :RxNotificationSequence<[ItemType]>)
            typealias TimingArrayType = RxTypes<ItemType>.TimingArrayType

            let t : RxTimeOffset = TxConfig.TestTickUnit

            let timingBoard = TxTimingBoard<ItemType>([
                          (t,     1, .eItem("1-0")),
                          (t * 2, 2, .eItem("2-0")),
                          (t * 2, 4, .eCompleted(.eTermination_Completed)),
                          (t * 3, 1, .eItem("1-1")),
                          (t * 3, 3, .eItem("3-0")),
                          (t * 4, 5, .eCompleted(.eTermination_Error(error))),
                          (t * 5, 2, .eItem("2-1")),
                          (t * 6, 2, .eItem("2-2")),
                          (t * 7, 1, .eItem("1-2")),
                          (t * 7, 3, .eItem("3-1")),
                          (t * 9, 1, .eItem("1-3")),
                          (t * 9, 2, .eCompleted(.eTermination_Completed)),
                          (t * 10, 1, .eCompleted(.eTermination_Completed)),
                          (t * 11, 3, .eCompleted(.eTermination_Error(error)))
                  ])

            let run1: RunEntry = (

                    runName:        "Test basic time operation",
                    sourceTags:     [1],
                    duration:       t * 4,
                    count:          0,
                    expected:       RxNotificationSequence<[ItemType]>(timedItemsThenCompletion: [(t * 4, ["1-0", "1-1"]), (t * 8, ["1-2"]), (t * 10, ["1-3"])])
            )

            let run2: RunEntry = (

                    runName:        "Test basic count operation",
                    sourceTags:     [1],
                    duration:       0,
                    count:          2,
                    expected:       RxNotificationSequence<[ItemType]>(timedItemsThenCompletion: [(t * 3, ["1-0", "1-1"]), (t * 9, ["1-2", "1-3"])])
            )

            let run3: RunEntry = (

                    runName:        "Test time and count operation",
                    sourceTags:     [1],
                    duration:       t * 4,
                    count:          2,
                    expected:       RxNotificationSequence<[ItemType]>(timedItemsThenCompletion: [(t * 3, ["1-0", "1-1"]), (t * 4, []), (t * 8, ["1-2"]), (t * 10, ["1-3"])])
            )

            let run4: RunEntry = (

                    runName:        "Test empty stream",
                    sourceTags:     [4],
                    duration:       t * 3,
                    count:          2,
                    expected:       RxNotificationSequence<[ItemType]>(timedItemsThenCompletion: [])
            )

            let run5: RunEntry = (

                    runName:        "Test empty stream with error",
                    sourceTags:     [5],
                    duration:       t * 3,
                    count:          2,
                    expected:       RxNotificationSequence<[ItemType]>(timedItems: [(t * 3, [])], termination: .eTermination_Error(error))
            )

            let run6: RunEntry = (

                    runName:        "Test stream with error",
                    sourceTags:     [3],
                    duration:       t * 4,
                    count:          2,
                    expected:       RxNotificationSequence<[ItemType]>(timedItems: [(t * 4, ["3-0"]), (t * 8, ["3-1"])], termination: .eTermination_Error(error))
            )


            let run7: RunEntry = (

                    runName:        "Test zero duration and zero count",
                    sourceTags:     [1],
                    duration:       0,
                    count:          0,
                    expected:       RxNotificationSequence<[ItemType]>(timedItemsThenCompletion: [(t, ["1-0"]), (t * 3, ["1-1"]), (t * 7, ["1-2"]), (t * 9, ["1-3"])])
            )

            let run8: RunEntry = (

                    runName:        "Test negative duration",
                    sourceTags:     [1],
                    duration:       -t,
                    count:          0,
                    expected:       RxNotificationSequence<[ItemType]>(timedItems: [], termination: .eTermination_Error(RxLibError(.eInvalidTimePeriod)))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7,
                    run8
            ]
            {
                let (runName, sourceTags, duration, count, expected) = runSpec

                func runTest()
                {
                    let subscriptionType = eRxSubscriptionType.eHot

                    let observer = TxTestRigs<[ItemType]>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    let source = timingBoard.createTimedSource(sourceTags, name: runName + "/Source", subscriptionType: subscriptionType)
                    let observable = source.buffer(count, duration: duration)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue((==)(expected, observer.notifications), reportFunc())
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testDelay()
    {
        let testObservableName = "delay"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, sourceTags: Set<Int>, duration: RxDuration, expected: RxNotificationSequence<ItemType>)
            typealias TimingArrayType = RxTypes<ItemType>.TimingArrayType


            let t : RxTimeOffset = TxConfig.TestTickUnit

            let timingBoard = TxTimingBoard<ItemType>([
                      (t,     1, .eItem("1-0")),
                      (t * 2, 4, .eCompleted(.eTermination_Completed)),
                      (t * 2, 2, .eItem("2-0")),
                      (t * 3, 1, .eItem("1-1")),
                      (t * 4, 5, .eCompleted(.eTermination_Error(error))),
                      (t * 4, 3, .eItem("3-0")),
                      (t * 5, 2, .eItem("2-1")),
                      (t * 6, 3, .eItem("3-1")),
                      (t * 7, 3, .eCompleted(.eTermination_Error(error))),
                      (t * 8, 2, .eItem("2-2")),
                      (t * 9, 2, .eCompleted(.eTermination_Completed)),
                      (t * 10, 1, .eItem("1-2")),
                      (t * 11, 1, .eCompleted(.eTermination_Completed))
              ])

            let run1: RunEntry = (

                    runName:        "Test basic operation",
                    sourceTags:     [1],
                    duration:       t * 2,
                    expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t * 3, "1-0"), (t * 5, "1-1"), (t * 12, "1-2")])
            )

            let run2: RunEntry = (

                    runName:        "Test zero delay",
                    sourceTags:     [1],
                    duration:       0,
                    expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, "1-0"), (t * 3, "1-1"), (t * 10, "1-2")])
            )

            let run3: RunEntry = (

                    runName:        "Test stream empty",
                    sourceTags:     [4],
                    duration:       t * 2,
                    expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run4: RunEntry = (

                    runName:        "Test stream with error",
                    sourceTags:     [3],
                    duration:       t * 4,
                    expected:       RxNotificationSequence<ItemType>(timedItems: [(t * 8, "3-0"), (t * 10, "3-1")], termination: etTerminationType.eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:        "Test empty stream with error",
                    sourceTags:     [5],
                    duration:       t,
                    expected:       RxNotificationSequence<ItemType>(timedItems: [], termination: etTerminationType.eTermination_Error(error))
            )

            let run6: RunEntry = (

                    runName:        "Test past duration",
                    sourceTags:     [1],
                    duration:       0,
                    expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, "1-0"), (t * 3, "1-1"), (t * 10, "1-2")])
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, sourceTags, duration, expected) = runSpec

                func runTestByTimeOffset()
                {
                    let subscriptionType = eRxSubscriptionType.eHot
                    let source = timingBoard.createTimedSource(sourceTags, name: runName + "/Source", subscriptionType : subscriptionType)

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.delay(duration)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                func runTestByTime()
                {
                    let subscriptionType = eRxSubscriptionType.eHot
                    let source = timingBoard.createTimedSource(sourceTags, name: runName + "Source1.", subscriptionType : subscriptionType)

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let dueTime : RxTime = RxTime(timeIntervalSinceNow: duration)
                    let observable = source.delayTo(dueTime)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTestByTimeOffset()
                runTestByTime()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testSample()
    {
        let testObservableName = "sample"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, sourceTags: Set<Int>, duration: RxDuration, expected: RxNotificationSequence<ItemType>)
            typealias TimingArrayType = RxTypes<ItemType>.TimingArrayType

            let t : RxTimeOffset = TxConfig.TestTickUnit
            let timingBoard = TxTimingBoard<ItemType>([

                  (t,     1, .eItem("1-0")),
                  (t * 2, 4, .eCompleted(.eTermination_Completed)),
                  (t * 2, 2, .eItem("2-0")),
                  (t * 2, 1, .eItem("1-1")),
                  (t * 3, 1, .eItem("1-2")),
                  (t * 3, 3, .eItem("3-0")),
                  (t * 3, 2, .eCompleted(.eTermination_Completed)),
                  (t * 4, 5, .eCompleted(.eTermination_Error(error))),
                  (t * 6, 3, .eItem("3-1")),
                  (t * 7, 3, .eCompleted(.eTermination_Error(error))),

                  (t * 10, 1, .eItem("1-3")),
                  (t * 11, 1, .eCompleted(.eTermination_Completed))
            ])

            let run1: RunEntry = (

                runName:        "Test basic operation",
                sourceTags:     [1],
                duration:       t * 4,
                expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t * 4, "1-2"), (t * 12, "1-3")])
            )

            let run2: RunEntry = (

                runName:        "Test early completion",
                sourceTags:     [2],
                duration:       t * 5,
                expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t * 5, "2-0")])
            )

            let run3: RunEntry = (

                runName:        "Test zero delay",
                sourceTags:     [1],
                duration:       0,
                expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, "1-0"), (t * 2, "1-1"), (t * 3, "1-2"), (t * 10, "1-3")])
            )

            let run4: RunEntry = (

                runName:        "Test stream empty",
                sourceTags:     [4],
                duration:       t * 2,
                expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run5: RunEntry = (

                runName:        "Test stream with error",
                sourceTags:     [3],
                duration:       t * 4,
                expected:       RxNotificationSequence<ItemType>(timedItems: [(t * 8, "3-0"), (t * 12, "3-1")], termination: etTerminationType.eTermination_Error(error))
            )

            let run6: RunEntry = (

                runName:        "Test empty stream with error",
                sourceTags:     [5],
                duration:       t,
                expected:       RxNotificationSequence<ItemType>(timedItems: [], termination: etTerminationType.eTermination_Error(error))
            )

            let run7: RunEntry = (

                runName:        "Test negative period",
                sourceTags:     [1],
                duration:       -t,
                expected:       RxNotificationSequence<ItemType>(timedItems: [], termination: etTerminationType.eTermination_Error(RxLibError(.eInvalidTimePeriod)))
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7
            ] {
                let (runName, sourceTags, duration, expected) = runSpec

                func runTest()
                {
                    let subscriptionType = eRxSubscriptionType.eHot
                    let source = timingBoard.createTimedSource(sourceTags, name: runName + "/Source", subscriptionType : subscriptionType)

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.sample(duration)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                let observable = RxSource<Int>.fromArray([0, 1, 2, 3]).filter({ return $0 % 1 == 0 })
                let subscription = observable.subscribe()

                subscription.waitForDisposal()

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testTimeout()
    {
        let testObservableName = "timeout"
        let testName           = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, sourceTags:Set<Int>, duration:RxDuration, expected:RxNotificationSequence<ItemType>)
            typealias TimingArrayType = RxTypes<ItemType>.TimingArrayType

            let t: RxTimeOffset    = TxConfig.TestTickUnit

            let timingBoard = TxTimingBoard<ItemType>([

                      (t, 1, .eItem("1-0")),
                      (t * 2, 4, .eCompleted(.eTermination_Completed)),
                      (t * 2, 2, .eItem("2-0")),
                      (t * 2, 1, .eItem("1-1")),
                      (t * 3, 1, .eItem("1-2")),
                      (t * 3, 3, .eItem("3-0")),
                      (t * 3, 2, .eCompleted(.eTermination_Completed)),
                      (t * 4, 5, .eCompleted(.eTermination_Error(error))),
                      (t * 6, 3, .eItem("3-1")),
                      (t * 7, 3, .eCompleted(.eTermination_Error(error))),

                      (t * 10, 1, .eItem("1-3")),
                      (t * 11, 1, .eCompleted(.eTermination_Completed))
              ])

            let run1: RunEntry = (

                    runName: "Test basic operation with timeout",
                    sourceTags: [1],
                    duration: t * 3,
                    expected: RxNotificationSequence<ItemType>(timedItems: [(t, "1-0"), (t * 2, "1-1"), (t * 3, "1-2")], termination: etTerminationType.eTermination_Error(RxLibError(.eTimeout)))
            )

            let run2: RunEntry = (

                    runName: "Test basic operation with no timeout",
                    sourceTags: [1],
                    duration: t * 9,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, "1-0"), (t * 2, "1-1"), (t * 3, "1-2"), (t * 10, "1-3")])
            )

            let run3: RunEntry = (

                    runName: "Test zero timeout",
                    sourceTags: [1],
                    duration: 0,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, "1-0"), (t * 2, "1-1"), (t * 3, "1-2"), (t * 10, "1-3")])
            )

            let run4: RunEntry = (

                    runName: "Test stream empty",
                    sourceTags: [4],
                    duration: t * 4,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run5: RunEntry = (

                    runName: "Test stream with error",
                    sourceTags: [3],
                    duration: t * 10,
                    expected: RxNotificationSequence<ItemType>(timedItems: [(t * 8, "3-0"), (t * 12, "3-1")], termination: etTerminationType.eTermination_Error(error))
            )

            let run6: RunEntry = (

                    runName: "Test empty stream with error",
                    sourceTags: [5],
                    duration: t * 6,
                    expected: RxNotificationSequence<ItemType>(timedItems: [], termination: etTerminationType.eTermination_Error(error))
            )

            let run7: RunEntry = (

                    runName: "Test empty stream with error that should time-out",
                    sourceTags: [5],
                    duration: t * 3,
                    expected: RxNotificationSequence<ItemType>(timedItems: [], termination: etTerminationType.eTermination_Error(RxLibError(.eTimeout)))
            )

            let run8: RunEntry = (

                    runName: "Test negative timeout",
                    sourceTags: [1],
                    duration: -t,
                    expected: RxNotificationSequence<ItemType>(timedItems: [], termination: etTerminationType.eTermination_Error(RxLibError(.eInvalidTimePeriod)))
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7,
                    run8
            ]
            {
                let (runName, sourceTags, duration, expected) = runSpec

                func runTest()
                {
                    let subscriptionType = eRxSubscriptionType.eHot
                    let source           = timingBoard.createTimedSource(sourceTags, name: runName + "/Source", subscriptionType: subscriptionType)

                    let observer   = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.timeout(duration)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testTimeoutWithOther()
    {
        let testObservableName = "timeout"

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, sourceTags: Set<Int>, otherTags: Set<Int>, duration: RxDuration, expected: RxNotificationSequence<ItemType>)
            typealias TimingArrayType = RxTypes<ItemType>.TimingArrayType

            let testName = TxFactory.createTestName(#function)
            let t : RxTimeOffset = TxConfig.TestTickUnit

            let timingBoard = TxTimingBoard<ItemType>([

                      (t,     1, .eItem("1-0")),
                      (t * 2, 4, .eCompleted(.eTermination_Completed)),
                      (t * 2, 2, .eItem("2-0")),
                      (t * 2, 1, .eItem("1-1")),
                      (t * 3, 1, .eItem("1-2")),
                      (t * 3, 3, .eItem("3-0")),
                      (t * 3, 2, .eCompleted(.eTermination_Completed)),
                      (t * 4, 5, .eCompleted(.eTermination_Error(error))),
                      (t * 6, 3, .eItem("3-1")),
                      (t * 7, 3, .eCompleted(.eTermination_Error(error))),

                      (t * 10, 1, .eItem("1-3")),
                      (t * 11, 1, .eCompleted(.eTermination_Completed))
              ])

            let run1: RunEntry = (

                    runName:        "Test basic operation with timeout",
                    sourceTags:     [1],
                    otherTags:      [2],
                    duration:       t * 3,
                    expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, "1-0"), (t * 2, "1-1"), (t * 3, "1-2"), (t * 8, "2-0") ])
            )

            let run2: RunEntry = (

                    runName:        "Test basic operation with no timeout",
                    sourceTags:     [1],
                    otherTags:      [2],
                    duration:       t * 9,
                    expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, "1-0"), (t * 2, "1-1"), (t * 3, "1-2"), (t * 10, "1-3")])
            )

            let run3: RunEntry = (

                    runName:        "Test zero timeout",
                    sourceTags:     [1],
                    otherTags:      [1],
                    duration:       0,
                    expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, "1-0"), (t * 2, "1-1"), (t * 3, "1-2"), (t * 10, "1-3")])
            )

            let run4: RunEntry = (

                    runName:        "Test stream empty",
                    sourceTags:     [4],
                    otherTags:      [1],
                    duration:       t * 4,
                    expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run5: RunEntry = (

                    runName:        "Test stream with error",
                    sourceTags:     [3],
                    otherTags:      [1],
                    duration:       t * 10,
                    expected:       RxNotificationSequence<ItemType>(timedItems: [(t * 8, "3-0"), (t * 12, "3-1")], termination: etTerminationType.eTermination_Error(error))
            )

            let run6: RunEntry = (

                    runName:        "Test empty stream with error",
                    sourceTags:     [5],
                    otherTags:      [1],
                    duration:       t * 6,
                    expected:       RxNotificationSequence<ItemType>(timedItems: [], termination: etTerminationType.eTermination_Error(error))
            )

            let run7: RunEntry = (

                    runName:        "Test empty stream with error that should time-out",
                    sourceTags:     [5],
                    otherTags:      [2],
                    duration:       t * 3,
                    expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t * 8, "2-0")])
            )

            let run8: RunEntry = (

                    runName:        "Test negative timeout",
                    sourceTags:     [1],
                    otherTags:      [1],
                    duration:       -t,
                    expected:       RxNotificationSequence<ItemType>(timedItems: [], termination: etTerminationType.eTermination_Error(RxLibError(.eInvalidTimePeriod)))
            )

            let run9: RunEntry = (

                    runName:        "Test timeout with other stream empty",
                    sourceTags:     [1],
                    otherTags:      [4],
                    duration:       t * 3,
                    expected:       RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, "1-0"), (t * 2, "1-1"), (t * 3, "1-2") ])
            )

            let run10: RunEntry = (

                    runName:        "Test timeout with other stream in error",
                    sourceTags:     [1],
                    otherTags:      [3],
                    duration:       t * 3,
                    expected:       RxNotificationSequence<ItemType>(timedItems: [(t, "1-0"), (t * 2, "1-1"), (t * 3, "1-2"), (t * 9, "3-0"), (t * 12, "3-1")], termination: etTerminationType.eTermination_Error(error))
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7,
                    run8,
                    run9,
                    run10
            ] {
                let (runName, sourceTags, otherTags, duration, expected) = runSpec

                func runTest()
                {
                    let subscriptionType = eRxSubscriptionType.eHot
                    let source = timingBoard.createTimedSource(sourceTags, name: runName + "/Source", subscriptionType : subscriptionType)
                    let otherSource = timingBoard.createTimedSource(otherTags, name: runName + "/Source", subscriptionType : subscriptionType)

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.timeout(duration, other: otherSource)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testThrottle()
    {
        let testObservableName = "throttle"

        func runAll()
        {
            typealias ItemType = RxIndexType
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, dueTime:RxDuration, expected:RxNotificationSequence<ItemType>)

            let t: RxDuration = TxConfig.TestTickUnit
            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromTimedArray([(t, 1), (t * 2, 2), (t * 3.5, 3), (t * 4.5, 4), (t * 5.5, 5), (t * 6.5, 6), (t * 8, 7)])
            let source2 = RxSource<ItemType>.fromTimedArray([(0, 0)])
            let source3 = RxSource<ItemType>.fromTimedArray([(t, 1), (t * 2, 2), (t * 10, 7)])
            let emptySourceWithError = RxSource<ItemType>.fromTimedArray([], termination: .eTermination_Error(error))
            let source1WithError = RxSource<ItemType>.fromTimedArray([(t, 1), (t * 2 , 3), (t * 6, 4)], termination: .eTermination_Error(error))

            let run1: RunEntry = (

                    runName: "Test normal operation",
                    source: source1,
                    dueTime : 3 * t,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, 1), (t * 4.5, 4), (t * 7.5, 7)])
            )

            let run2: RunEntry = (

                    runName: "Test empty source",
                    source: emptySource,
                    dueTime : 3 * t,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName: "Test error",
                    source: source1WithError,
                    dueTime : 3 * t,
                    expected: RxNotificationSequence<ItemType>(timedItems: [(t, 1), (t * 5, 3), (t * 6, 4)], termination: etTerminationType.eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName: "Test empty source with error",
                    source: emptySourceWithError,
                    dueTime : 3 * t,
                    expected: RxNotificationSequence<ItemType>(timedItems: [], termination: etTerminationType.eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName: "Test zero due time",
                    source: source1,
                    dueTime : 0,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, 1), (t * 2, 2), (t * 3.5, 3), (t * 4.5, 4), (t * 5.5, 5), (t * 6.5, 6), (t * 7.5, 7)])
            )

            let run6: RunEntry = (

                    runName: "Test timeout",
                    source: source3,
                    dueTime : 5 * t,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, 1), (t * 7, 2), (t * 10, 7)])
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ]
            {
                let (runName, source, dueTime, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.throttle(dueTime)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func NotImpl_testWindowByCount()
    {
        // To be implemented
    }

    func NotImpl_testWindowByDuration()
    {
        // To be implemented
    }

    func NotImpl_testWindowByCountAndDuration()
    {
        // To be implemented
    }

    func NotImpl_testWindowByDurationAndTimeshift()
    {
        // To be implemented
    }
}

class RxObservable_Generation_Tests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testRepeatForever()
    {
        let testObservableName = "repeatForever"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, count:RxIndexType, expected:RxNotificationQueue<ItemType>)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    count:      12,
                    expected:   RxNotificationQueue<ItemType>(items: [10, 21, 35, 44, 10, 21, 35, 44, 10, 21, 35, 44])
            )

            let run2: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    count:      0,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    count:      12,
                    expected:   RxNotificationQueue<ItemType>(items: [10, 21, 35, 44], termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3
            ] {
                let (runName, source, count, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserverManagingSubscription("\(runName) test observer", maxItemCount : count)
                    let observable = source.repeatForever().appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testRepeatWithCount()
    {
        let testObservableName = "repeatWithCount"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Float
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, count:RxIndexType, expected:RxNotificationQueue<ItemType>)


            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([1.0, 2.0, 3.0])
            let source1WithError = RxSource<ItemType>.fromArray([4.0, 5.0, 6.0], termination: .eTermination_Error(error))
            let emptyErrorSource = RxSource<ItemType>.throwError(error)

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [1.0, 2.0, 3.0, 1.0, 2.0, 3.0, 1.0, 2.0, 3.0])
            )

            let run2: RunEntry = (

                    runName:    "Test zero count",
                    source:     source1,
                    count:      0,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [1.0, 2.0, 3.0])
            )

            let run3: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run4: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(items: [4.0, 5.0, 6.0], termination: .eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:    "Test empty with error",
                    source:     emptyErrorSource,
                    count:      10,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))

            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5
            ] {
                let (runName, source, count, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.repeatWithCount(count).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}


class RxObservable_Partition_Tests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func NotImpl_testGroupBy()
    {
        // To be implemented
    }

    func NotImpl_testGroupByUntil()
    {
        // To be implemented
    }
}

extension RxSource
{
    public class func retryableSource(sources : [ARxProducer<ItemType>]) -> RxSource<ItemType>
    {
        let tag = "retryableSource"
        var sourceIndex = 0

        let evalOp = { (evalNode : RxEvalNode<ItemType, ItemType>) -> Void in

            var streamEnumerable : RxObservableEnumerable<ItemType>? = nil

            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemType>) in

                func emit(index : Int)
                {
                    if index < sources.count
                    {
                        streamEnumerable = RxObservableEnumerable<ItemType>(streams: [sources[index]])

                        streamEnumerable!.sendToNotifier(notifier, sendCompleted: true)
                    }
                    else
                    {
                        notifier.notifyCompleted()
                    }
                }

                switch stateChange
                {
                    case eRxEvalStateChange.eSubscriptionBegin:

                        // Emit subsequent interations.
                        if sourceIndex > 0
                        {
                            emit(sourceIndex)

                            sourceIndex += 1
                        }

                    case eRxEvalStateChange.eEvalBegin:

                        // Emit the first iteration.
                        emit(sourceIndex)

                        sourceIndex += 1

                    case eRxEvalStateChange.eEvalEnd:

                        streamEnumerable?.terminate(.eTermination_Completed)

                    default:
                        break
                }
            }
        }

        return RxSource<ItemType>(tag: tag, subscriptionType: .eHot, evalOp : evalOp)
    }
}

class RxObservable_ErrorHandling_Tests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testRetry()
    {
        let testObservableName = "retry"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, sources: [ARxProducer<ItemType>], expected: RxNotificationQueue<ItemType>)

            let source1 = RxSource<ItemType>.fromArray([0, 1, 2])
            let source2 = RxSource<ItemType>.fromArray([10, 11, 12])
            let source3 = RxSource<ItemType>.fromArray([20, 20, 40])
            let sourceWithError = RxSource<ItemType>.fromArray([30, 31, 32], termination: .eTermination_Error(error))
            let sourceWithError2 = RxSource<ItemType>.fromArray([40, 41, 42], termination: .eTermination_Error(error))
            let sourceWithError3 = RxSource<ItemType>.fromArray([50, 51, 52], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let emptySource = RxSource<ItemType>.empty()

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:        "Test normal operation",
                    sources:        [source1, source2],
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [0, 1, 2])
            )

            let run2: RunEntry = (

                    runName:        "Test empty stream",
                    sources:        [emptySource, source2],
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:        "Test stream with error",
                    sources:        [sourceWithError, source1],
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [30, 31, 32, 0, 1, 2])
            )

            let run4: RunEntry = (

                    runName:        "Test empty stream with error",
                    sources:        [emptySourceWithError, source2],
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 11, 12])
            )

            let run5: RunEntry = (

                    runName:        "Test two streams with error and then complete",
                    sources:        [sourceWithError, sourceWithError2, source1],
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [30, 31, 32, 40, 41, 42, 0, 1, 2])
            )

            let run6: RunEntry = (

                    runName:        "Test three streams with error",
                    sources:        [sourceWithError, sourceWithError2, sourceWithError3, source1],
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [30, 31, 32, 40, 41, 42, 50, 51, 52, 0, 1, 2])
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, sources, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = RxSource<ItemType>.retryableSource(sources).retry()

                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testRetryWithCount()
    {
        let testObservableName = "retryWithCount"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, sources: [ARxProducer<ItemType>], count: Int, expected: RxNotificationQueue<ItemType>)


            let source1 = RxSource<ItemType>.fromArray([0, 1, 2])
            let source2 = RxSource<ItemType>.fromArray([10, 11, 12])
            let source3 = RxSource<ItemType>.fromArray([20, 20, 40])
            let sourceWithError = RxSource<ItemType>.fromArray([30, 31, 32], termination: .eTermination_Error(error))
            let sourceWithError2 = RxSource<ItemType>.fromArray([40, 41, 42], termination: .eTermination_Error(error))
            let sourceWithError3 = RxSource<ItemType>.fromArray([50, 51, 52], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let emptySource = RxSource<ItemType>.empty()

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:        "Test normal operation",
                    sources:        [source1, source2],
                    count:          2,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [0, 1, 2])
            )

            let run2: RunEntry = (

                    runName:        "Test empty stream",
                    sources:        [emptySource, source2],
                    count:          2,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:        "Test stream with error",
                    sources:        [sourceWithError, source1],
                    count:          1,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [30, 31, 32, 0, 1, 2])
            )

            let run4: RunEntry = (

                    runName:        "Test stream with error and zero count",
                    sources:        [sourceWithError, source1],
                    count:          0,
                    expected:       RxNotificationQueue<ItemType>(items: [30, 31, 32], termination: .eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:        "Test empty stream with error",
                    sources:        [emptySourceWithError, source2],
                    count:          1,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 11, 12])
            )

            let run6: RunEntry = (

                    runName:        "Test two streams with error and then complete",
                    sources:        [sourceWithError, sourceWithError2, source1],
                    count:          2,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [30, 31, 32, 40, 41, 42, 0, 1, 2])
            )

            let run7: RunEntry = (

                    runName:        "Test three streams with error and should complete",
                    sources:        [sourceWithError, sourceWithError2, sourceWithError3, source1],
                    count:          3,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [30, 31, 32, 40, 41, 42, 50, 51, 52, 0, 1, 2])
            )

            let run8: RunEntry = (

                    runName:        "Test three streams with error and should error",
                    sources:        [sourceWithError, sourceWithError2, sourceWithError3, source1],
                    count:          2,
                    expected:       RxNotificationQueue<ItemType>(items: [30, 31, 32, 40, 41, 42, 50, 51, 52], termination: .eTermination_Error(error))
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6,
                    run7,
                    run8
            ] {
                let (runName, sources, count, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = RxSource<ItemType>.retryableSource(sources).retry(count)

                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testCatchError()
    {
        let testObservableName = "catchError"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {

            typealias ItemType = Float
            typealias RunEntry = (runName:String, source: RxSource<ItemType>, catchSource: RxSource<ItemType>, expected: RxNotificationQueue<ItemType>)

            let source1 = RxSource<ItemType>.fromArray([1.0, 2.0, 3.0, 4.0], tag: "source1")
            let source2 = RxSource<ItemType>.fromArray([10.5, 20.6, 40.5], tag: "source2")
            let sourceWithError = RxSource<ItemType>.fromArray([10.3, 21.2], termination: .eTermination_Error(error), tag: "sourceWithError")
            let sourceWithError2 = RxSource<ItemType>.fromArray([12.3, 22.2], termination: .eTermination_Error(error), tag: "sourceWithError2")
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let emptySource = RxSource<ItemType>.empty()

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:        "Test normal operation",
                    source:         source1,
                    catchSource:    source2,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [1.0, 2.0, 3.0, 4.0])
            )

            let run2: RunEntry = (

                    runName:        "Test empty stream",
                    source:         emptySource,
                    catchSource:    source2,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:        "Test stream with error",
                    source:         sourceWithError,
                    catchSource:    source2,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [10.3, 21.2, 10.5, 20.6, 40.5])
            )

            let run4: RunEntry = (

                    runName:        "Test empty stream with error",
                    source:         emptySourceWithError,
                    catchSource:    source2,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [10.5, 20.6, 40.5])
            )

            let run5: RunEntry = (

                    runName:        "Test two streams with error",
                    source:         sourceWithError,
                    catchSource:    sourceWithError2,
                    expected:       RxNotificationQueue<ItemType>(items: [10.3, 21.2, 12.3, 22.2], termination: .eTermination_Error(error))
            )

            let run6: RunEntry = (

                    runName:        "Test stream with error and error stream empty",
                    source:         sourceWithError,
                    catchSource:    emptySource,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [10.3, 21.2])
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, source, catchSource, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.catchError(catchSource)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testOnErrorResumeNext()
    {
        let testObservableName = "onErrorResumeNext"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int64
            typealias RunEntry = (runName:String, source: RxSource<ItemType>, nextSources: [RxSource<ItemType>], expected: RxNotificationQueue<ItemType>)


            let source1 = RxSource<ItemType>.fromArray([0, 1, 2])
            let source2 = RxSource<ItemType>.fromArray([10, 11, 12])
            let source3 = RxSource<ItemType>.fromArray([20, 20, 40])
            let sourceWithError = RxSource<ItemType>.fromArray([30, 31, 32], termination: .eTermination_Error(error))
            let sourceWithError2 = RxSource<ItemType>.fromArray([40, 41, 42], termination: .eTermination_Error(error))
            let sourceWithError3 = RxSource<ItemType>.fromArray([50, 51, 52], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let emptySource = RxSource<ItemType>.empty()

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:        "Test normal operation",
                    source:         source1,
                    nextSources:    [source2, source3],
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [0, 1, 2])
            )

            let run2: RunEntry = (

                    runName:        "Test empty stream",
                    source:         emptySource,
                    nextSources:    [source2, source3],
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:        "Test stream with error",
                    source:         sourceWithError,
                    nextSources:    [source2, source3],
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [30, 31, 32, 10, 11, 12])
            )

            let run4: RunEntry = (

                    runName:        "Test empty stream with error",
                    source:         emptySourceWithError,
                    nextSources:    [source2, source3],
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 11, 12])
            )

            let run5: RunEntry = (

                    runName:        "Test two streams with error and then complete",
                    source:         sourceWithError,
                    nextSources:    [sourceWithError2, source1],
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [30, 31, 32, 40, 41, 42, 0, 1, 2])
            )

            let run6: RunEntry = (

                    runName:        "Test three streams with error",
                    source:         sourceWithError,
                    nextSources:    [sourceWithError2, sourceWithError3],
                    expected:       RxNotificationQueue<ItemType>(items: [30, 31, 32, 40, 41, 42, 50, 51, 52], termination: .eTermination_Error(error))
            )


            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, source, nextSources, expected) = runSpec

                func runTestProducersAsArray()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.onErrorResumeNext(nextSources)

                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                func runTestProducersAsStream()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let stream = RxSource.fromArray(nextSources)
                    let observable = source.onErrorResumeNext(stream)

                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTestProducersAsArray()
                runTestProducersAsStream()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testOnErrorReturn()
    {
        let testObservableName = "onErrorReturn"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Float
            typealias RunEntry = (runName:String, source: RxSource<ItemType>, expected: RxNotificationQueue<ItemType>)

            let source1 = RxSource<ItemType>.fromArray([1.0, 2.0, 3.0, 4.0])
            let source2 = RxSource<ItemType>.fromArray([10.5, 20.6, 40.5])
            let sourceWithError = RxSource<ItemType>.fromArray([10.3, 21.2], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let emptySource = RxSource<ItemType>.empty()

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:        "Test normal operation",
                    source:         source1,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [1.0, 2.0, 3.0, 4.0])
            )

            let run2: RunEntry = (

                    runName:        "Test empty stream",
                    source:         emptySource,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:        "Test stream with error",
                    source:         sourceWithError,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [10.3, 21.2, 99.0])
            )

            let run4: RunEntry = (

                    runName:        "Test empty stream with error",
                    source:         emptySourceWithError,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [99.0])
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,

            ] {
                let (runName, source, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.onErrorReturn(99.0)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}


class RxObservable_Connectable_Tests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testReplayByCountBasic()
    {
        let testObservableName = "replayByCount"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, count:RxIndexType, expected:RxNotificationQueue<ItemType>, expectedReplay:RxNotificationQueue<ItemType>)


            let emptySource = RxSource<ItemType>.fromArray([], subscriptionType: .eHot)
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], subscriptionType: .eHot)
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error), subscriptionType: .eHot)
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error), subscriptionType: .eHot)

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 35, 44, 50]),
                    expectedReplay: RxNotificationQueue<ItemType>(itemsThenCompletion: [44, 50])
            )

            let run2: RunEntry = (

                    runName:    "Test zero count",
                    source:     source1,
                    count:      0,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 35, 44, 50]),
                    expectedReplay: RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: []),
                    expectedReplay: RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run4: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(items: [10, 21, 35, 44, 50], termination: .eTermination_Error(error)),
                    expectedReplay: RxNotificationQueue<ItemType>(items: [44, 50], termination: .eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:    "Test empty source with error",
                    source:     emptySourceWithError,
                    count:      10,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error)),
                    expectedReplay: RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
            ] {
                let (runName, source, count, expected, expectedReplay) = runSpec

                func runTest()
                {
                    var replayNotifications = RxNotificationConstrainedSequence<ItemType>(tag: "replayNotifications")

                    replayNotifications.maxItemCount = count

                    let observer1 = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer1")
                    let observer2 = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer2")

                    let observable = source.replay(&replayNotifications).appendTag(runName)

                    let reportFunc1 = reportGenerator(runName, expected, observer1.notifications)
                    let reportFunc2 = reportGenerator(runName, expected, observer2.notifications)

                    let subscription1 = observable.subscribe(observer1)

                    TxDisposalTestRigs.waitForDisposal(subscription1)

                    let subscription2 = observable.subscribe(observer2)

                    TxDisposalTestRigs.waitForDisposal(subscription2)

                    XCTAssertTrue(expected == observer1.notifications, reportFunc1())
                    XCTAssertTrue(expectedReplay == observer2.notifications, reportFunc2())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testReplayBasic()
    {
        let testObservableName = "replay"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expected:RxNotificationQueue<ItemType>)

            let emptySource = RxSource<ItemType>.fromArray([], subscriptionType: .eHot)
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], subscriptionType: .eHot)
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error), subscriptionType: .eHot)
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error), subscriptionType: .eHot)

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 35, 44, 50])
            )

            let run2: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    expected:   RxNotificationQueue<ItemType>(items: [10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName:    "Test empty source with error",
                    source:     emptySourceWithError,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4
            ] {
                let (runName, source, expected) = runSpec

                func runTest()
                {
                    var replayNotifications = RxNotificationConstrainedSequence<ItemType>(tag: "replayNotifications")

                    let observable = source.replay(&replayNotifications).appendTag(runName)
                    let observer1 = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer1")
                    let observer2 = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer2")

                    let reportFunc1 = reportGenerator(runName, expected, observer1.notifications)
                    let reportFunc2 = reportGenerator(runName, expected, observer2.notifications)

                    let subscription1 = observable.deferredSubscribe(observer1)
                    let subscription2 = observable.deferredSubscribe(observer2)

                    subscription1.runAsync()
                    subscription2.runAsync()

                    TxDisposalTestRigs.waitForDisposal(subscription1)
                    TxDisposalTestRigs.waitForDisposal(subscription2)

                    let observer3 = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer3")
                    let reportFunc3 = reportGenerator(runName, expected, observer3.notifications)

                    let subscription3 = observable.subscribe(observer3)

                    TxDisposalTestRigs.waitForDisposal(subscription3)

                    XCTAssertTrue(expected == observer1.notifications, reportFunc1())
                    XCTAssertTrue(expected == observer2.notifications, reportFunc2())
                    XCTAssertTrue(expected == observer3.notifications, reportFunc3())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func NotImpl_testReplayByTime()
    {
        // To be implemented
    }

    func NotImpl_testReplayByCountAndTime()
    {
        // To be implemented
    }

    func testPublish()
    {
        typealias ItemType = Int
        typealias RunEntry = (runName:String, source:RxSource<ItemType>, expected:RxNotificationQueue<ItemType>)

        let testObservableName = "publish"
        let testName = TxFactory.createTestName(#function)

        func runTestItemNotifications_UsingColdRxSubject(runName : String, iteration : Int)
        {
            let subject = RxSubject<ItemType>(tag: "Subject", subscriptionType : .eCold)
            let observer1 = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer1")
            let observer2 = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer2")
            let observer3 = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer3")

            let expected1 = RxNotificationQueue<ItemType>(items: [0, 1, 2])
            let reportFunc1 = reportGenerator(runName + "(Observer1)", expected1, observer1.notifications)

            let expected2 = RxNotificationQueue<ItemType>(items: [1, 2, 3, 4])
            let reportFunc2 = reportGenerator(runName + "(Observer2)", expected2, observer2.notifications)

            let expected3 = RxNotificationQueue<ItemType>(items: [2, 3])
            let reportFunc3 = reportGenerator(runName + "(Observer3)", expected3, observer3.notifications)

            RxSDK.mon.startTraceSession(RxRelTime.now, traceTag: ">>", bufferTraceMessages: true)

            let publish = subject.publish().traceAll(">>")

            subject.notifyItem(-1)

            RxMon.monAction(.eComment("------ start -------------\n"))

            let subscription1 = subject.subscribe(observer1)

            subject.notifyItem(0)

            let subscription2 = publish.subscribe(observer2)

            subject.notifyItem(1)

            let subscription3 = publish.subscribe(observer3)

            subject.notifyItem(2)

            subscription1.unsubscribe()

            subject.notifyItem(3)

            subscription3.unsubscribe()

            subject.notifyItem(4)

            subscription2.unsubscribe()

            subject.notifyItem(5)

            #if RxMonEnabled

                if let mon = RxSDK.mon.trace
                {
                    if expected1 != observer1.notifications  { RxLog.log("\n- error: \(reportFunc1())\n") }
                    if expected2 != observer2.notifications  { RxLog.log("\n- error: \(reportFunc2())\n") }
                    if expected3 != observer3.notifications  { RxLog.log("\n- error: \(reportFunc3())\n") }

                    if (expected1 != observer1.notifications) || (expected2 != observer2.notifications) || (expected3 != observer3.notifications)
                    {
                        // Output a trace message to console.
                        RxLog.log(mon.traceMessageBuffer)
                    }

                    mon.clearTraceBuffer()
                }
            #endif

            XCTAssertTrue(expected1 == observer1.notifications, reportFunc1())
            XCTAssertTrue(expected2 == observer2.notifications, reportFunc2())
            XCTAssertTrue(expected3 == observer3.notifications, reportFunc3())
        }

        func runTestCompletion_UsingColdRxSubject(runName : String, iteration : Int)
        {
            let subject = RxSubject<ItemType>(tag: "Subject", subscriptionType : .eCold)
            let observer1 = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer1")
            let observer2 = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer2")
            let observer3 = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer3")

            let expected1 = RxNotificationQueue<ItemType>(itemsThenCompletion: [0, 1])
            let reportFunc1 = reportGenerator(runName + "(Observer1)", expected1, observer1.notifications)

            let expected2 = RxNotificationQueue<ItemType>(itemsThenCompletion: [1])
            let reportFunc2 = reportGenerator(runName + "(Observer2)", expected2, observer2.notifications)

            let expected3 = RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            let reportFunc3 = reportGenerator(runName + "(Observer3)", expected3, observer3.notifications)

            RxSDK.mon.startTraceSession(RxRelTime.now, traceTag: ">>", bufferTraceMessages: true)

            let publish = subject.publish().traceAll(">>")

            RxMon.monAction(.eComment("------ start -------------\n"))

            subject.notifyCompleted()

            let subscription1 = subject.subscribe(observer1)

            subject.notifyItem(0)

            let subscription2 = publish.subscribe(observer2)

            subject.notifyItem(1)

            let subscription3 = publish.subscribe(observer3)

            subject.notifyCompleted(nil)

            subject.notifyItem(2)

            #if RxMonEnabled

                if let mon = RxSDK.mon.trace
                {
                    if expected1 != observer1.notifications  { RxLog.log("\n-1- error: \(reportFunc1())\n") }
                    if expected2 != observer2.notifications  { RxLog.log("\n-2- error: \(reportFunc2())\n") }
                    if expected3 != observer3.notifications  { RxLog.log("\n-3- error: \(reportFunc3())\n") }

                    if (expected1 != observer1.notifications) || (expected2 != observer2.notifications) || (expected3 != observer3.notifications)
                    {
                        // Output a trace message to console.
                        RxLog.log(mon.traceMessageBuffer)

                        RxLog.log(observer3.notifications.description + "\n")
                        RxLog.log(observer3.notifications.description + "\n")
                        RxLog.log(observer3.notifications.description + "\n")
                    }

                    mon.clearTraceBuffer()
                }
            #endif

            XCTAssertTrue(expected1 == observer1.notifications, reportFunc1())
            XCTAssertTrue(expected2 == observer2.notifications, reportFunc2())
            XCTAssertTrue(expected3 == observer3.notifications, reportFunc3())

            // Remove compiler warnings.
            subscription1.tag
            subscription2.tag
            subscription3.tag
        }

        func runTestError_UsingColdRxSubject(runName : String, iteration : Int)
        {
            let subject = RxSubject<ItemType>(tag: "Subject", subscriptionType : .eCold)
            let observer1 = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer1")
            let observer2 = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer2")
            let observer3 = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer3")

            let expected1 = RxNotificationQueue<ItemType>(items: [0], termination: .eTermination_Error(error))
            let reportFunc1 = reportGenerator(runName + "(Observer1)", expected1, observer1.notifications)

            let expected2 = RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
            let reportFunc2 = reportGenerator(runName + "(Observer2)", expected2 , observer2.notifications)

            let expected3 = RxNotificationQueue<ItemType>(itemsThenCompletion: [2])
            let reportFunc3 = reportGenerator(runName + "(Observer3)", expected3, observer3.notifications)

            RxSDK.mon.startTraceSession(RxRelTime.now, traceTag: ">>", bufferTraceMessages: true)

            let publish = subject.publish().traceAll(">>", bufferTraceMessages: true)

            RxMon.monAction(.eComment("------ start -------------\n"))

            subject.notifyCompleted(error)

            let subscription1 = subject.subscribe(observer1)

            subject.notifyItem(0)

            let subscription2 = publish.subscribe(observer2)

            subject.notifyCompleted(error)

            subject.notifyItem(1)

            let subscription3 = publish.subscribe(observer3)

            subject.notifyItem(2)

            subject.notifyCompleted()

            #if RxMonEnabled

                if let mon = RxSDK.mon.trace
                {
                    if expected1 != observer1.notifications  { RxLog.log("\n--- error: \(reportFunc1())\n") }
                    if expected2 != observer2.notifications  { RxLog.log("\n--- error: \(reportFunc2())\n") }
                    if expected3 != observer3.notifications  { RxLog.log("\n--- error: \(reportFunc3())\n") }

                    if (expected1 != observer1.notifications) || (expected2 != observer2.notifications) || (expected3 != observer3.notifications)
                    {
                        // Output a trace message to console.
                        RxLog.log(mon.traceMessageBuffer)
                    }

                    mon.clearTraceBuffer()
                }
            #endif

            XCTAssertTrue(expected1 == observer1.notifications, reportFunc1())
            XCTAssertTrue(expected2 == observer2.notifications, reportFunc2())
            XCTAssertTrue(expected3 == observer3.notifications, reportFunc3())

            // Remove compiler warnings.
            subscription1.tag
            subscription2.tag
            subscription3.tag
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")

            do
            {
                let runName = "runTestItemNotifications_UsingColdRxSubject"

                trace(2, "\n= run begin ==> \(testName) : \(runName) iteration (\(iteration))<==\n")
                runTestItemNotifications_UsingColdRxSubject(runName, iteration : iteration)
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")

                RxMon.shutdown()
            }

            do
            {
                let runName = "runTestCompletion_UsingColdRxSubject"

                trace(2, "\n= run begin ==> \(testName) : \(runName) iteration (\(iteration)) <==\n")
                runTestCompletion_UsingColdRxSubject(runName, iteration: iteration)
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")

                RxMon.shutdown()
            }

            do
            {
                let runName = "runTestError_UsingColdRxSubject"

                trace(2, "\n= run begin ==> \(testName) : \(runName) iteration (\(iteration)) <==\n")
                runTestError_UsingColdRxSubject(runName, iteration: iteration)
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")

                RxMon.shutdown()
            }
        }
    }
}


class RxObservable_Aggregation_Tests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        RxSDK.control.shutdown()
    }

    func testCount()
    {
        let testObservableName = "count"
        let testName           = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Float
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expected:RxNotificationQueue<RxCountType>)


            let source1              = RxSource<ItemType>.fromArray([33.2, 21.4, 35.2, 44.6, 50.6])
            let sourceWithError      = RxSource<ItemType>.fromArray([10.3, 21.2, 35.4, 44.1, 50.7], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let emptySource          = RxSource<ItemType>.empty()

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName: "Test normal operation",
                    source: source1,
                    expected: RxNotificationQueue<RxCountType>(itemsThenCompletion: [5])
            )

            let run2: RunEntry = (

                    runName: "Test empty stream",
                    source: emptySource,
                    expected: RxNotificationQueue<RxCountType>(itemsThenCompletion: [0])
            )

            let run3: RunEntry = (

                    runName: "Test stream with error",
                    source: sourceWithError,
                    expected: RxNotificationQueue<RxCountType>(termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName: "Test empty stream with error",
                    source: emptySourceWithError,
                    expected: RxNotificationQueue<RxCountType>(termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4
            ]
            {
                let (runName, source, expected) = runSpec

                func runTest()
                {
                    let observer     = RxTestObserver<RxCountType>(tag: "\(testObservableName) test observer")
                    let observable   = source.count()
                    let subscription = observable.subscribe(observer)
                    let reportFunc   = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0 ..< 1
        {
            trace(2, "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) <==\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testAggregate()
    {
        let testObservableName = "aggregate"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Float
            typealias RunEntry = (runName:String, source: RxSource<ItemType>, expected: RxNotificationQueue<ItemType>)


            let source1 = RxSource<ItemType>.fromArray([33.2, 21.4, 35.2, 44.6, 50.6])
            let sourceWithError = RxSource<ItemType>.fromArray([10.3, 21.2, 35.4, 44.1, 50.7], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let emptySource = RxSource<ItemType>.empty()

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [185])
            )

            let run2: RunEntry = (

                    runName:    "Test empty stream",
                    source:     emptySource,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [0])
            )

            let run3: RunEntry = (

                    runName:    "Test stream with error",
                    source:     sourceWithError,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName:    "Test empty stream with error",
                    source:     emptySourceWithError,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4
            ] {
                let (runName, source, expected) = runSpec

                func runTest_With_SumOp()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let aggregateFunc = { (accumItem: ItemType, item: ItemType) -> ItemType in

                        return accumItem + item
                    }

                    let observable = source.aggregate(0, accumulator: aggregateFunc)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)


                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest_With_SumOp()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testMin()
    {
        let testObservableName = "min"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Float
            typealias RunEntry = (runName:String, source: RxSource<ItemType>, expected: RxNotificationQueue<ItemType>)

            let source1 = RxSource<ItemType>.fromArray([33.2, 21.4, 35.2, 44.6, 50.6])
            let sourceWithError = RxSource<ItemType>.fromArray([10.3, 21.2, 35.4, 44.1, 50.7], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let emptySource = RxSource<ItemType>.empty()

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [21.4])
            )

            let run2: RunEntry = (

                    runName:    "Test empty stream",
                    source:     emptySource,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(RxLibError(.eNoSuchElement)))
            )

            let run3: RunEntry = (

                    runName:    "Test stream with error",
                    source:     sourceWithError,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName:    "Test empty stream with error",
                    source:     emptySourceWithError,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4
            ] {
                let (runName, source, expected) = runSpec

                func runTest_With_CompareOp()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.min((<))
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)


                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                func runTest_Comparable()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.min()
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)


                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest_With_CompareOp()
                runTest_Comparable()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }

        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testMax()
    {
        let testObservableName = "max"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Float
            typealias RunEntry = (runName:String, source: RxSource<ItemType>, expected: RxNotificationQueue<ItemType>)

            let source1 = RxSource<ItemType>.fromArray([33.2, 21.4, 35.2, 44.6, 50.6])
            let sourceWithError = RxSource<ItemType>.fromArray([10.3, 21.2, 35.4, 44.1, 50.7], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let emptySource = RxSource<ItemType>.empty()

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [50.6])
            )

            let run2: RunEntry = (

                    runName:    "Test empty stream",
                    source:     emptySource,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(RxLibError(.eNoSuchElement)))
            )

            let run3: RunEntry = (

                    runName:    "Test stream with error",
                    source:     sourceWithError,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName:    "Test empty stream with error",
                    source:     emptySourceWithError,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4
            ] {
                let (runName, source, expected) = runSpec

                func runTest_With_CompareOp()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("max test observer")
                    let observable = source.max((<))
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                func runTest_Comparable()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.max()
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)


                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest_With_CompareOp()
                runTest_Comparable()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testSum()
    {
        let testObservableName = "sum"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int32
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expected:RxNotificationQueue<ItemType>)


            let source1 = RxSource<ItemType>.fromArray([33, 21, 35, 44, 50])
            let sourceWithSingleValue = RxSource<ItemType>.fromArray([35])
            let sourceWithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let emptySource = RxSource<ItemType>.empty()

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName: "Test normal operation",
                    source: source1,
                    expected: RxNotificationQueue<ItemType>(itemsThenCompletion: [183])
            )

            let run2: RunEntry = (

                    runName:    "Test source with single item",
                    source:     sourceWithSingleValue,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [35])
            )

            let run3: RunEntry = (

                    runName:    "Test zero count",
                    source:     emptySource,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [0])
            )

            let run4: RunEntry = (

                    runName:    "Test stream with error",
                    source:     sourceWithError,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:    "Test empty stream with error",
                    source:     emptySourceWithError,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5
            ] {
                let (runName, source, expected) = runSpec

                func runTestWithSummer()
                {
                    var sum: ItemType = 0

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.sum({ (item: ItemType?) -> ItemType in

                        if item == nil { return 0  }

                        sum += item!

                        return sum
                    })

                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.sum()

                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testAverage()
    {
        let testObservableName = "average"
        let testName           = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Float
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expected:RxNotificationQueue<ItemType>)

            let source1 = RxSource<ItemType>.fromArray([33.2, 21.4, 35.2, 44.6, 50.6])
            let sourceWithSingleValue = RxSource<ItemType>.fromArray([35.4])
            let sourceWithError = RxSource<ItemType>.fromArray([10.3, 21.2, 35.4, 44.1, 50.7], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let emptySource = RxSource<ItemType>.empty()

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName: "Test normal operation",
                    source: source1,
                    expected: RxNotificationQueue<ItemType>(itemsThenCompletion: [37])
            )

            let run2: RunEntry = (

                    runName:    "Test source with single item",
                    source:     sourceWithSingleValue,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [35.4])
            )

            let run3: RunEntry = (

                    runName:    "Test zero count",
                    source:     emptySource,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(RxLibError(.eNoSuchElement)))
            )

            let run4: RunEntry = (

                    runName:    "Test stream with error",
                    source:     sourceWithError,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName:    "Test empty stream with error",
                    source:     emptySourceWithError,
                    expected:   RxNotificationQueue<ItemType>(termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5
            ] {
                let (runName, source, expected) = runSpec

                func runTest()
                {
                    var count: ItemType = 0
                    var sum: ItemType = 0

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.average({ (item: ItemType?) -> ItemType? in

                        if item == nil { return nil }

                        sum += item!
                        count += 1

                        return sum / count
                    })

                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)


                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0 ..< 1
        {
            trace(2, "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) <==\n")
            runAll()

            RxMon.shutdown()
        }
    }
}

class RxObservable_Utility_Tests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testUsing()
    {
        let testObservableName = "using"
        let t: RxDuration = TxConfig.TestTickUnit

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expected: RxNotificationSequence<ItemType>)

            let testName = TxFactory.createTestName(#function)

            let source1 = RxSource<ItemType>.fromTimedArray([(t, 0), (t * 2, 1), (t * 3.5, 2), (t * 4.5, 3), (t * 5.5, 4), (t * 6.5, 5), (t * 8, 6)])
            let source2 = RxSource<ItemType>.fromTimedArray([])
            let emptySource = RxSource<ItemType>.empty()
            let sourceWithError = RxSource<ItemType>.fromTimedArray([(t, 0), (t * 2, 1), (t * 3.5, 2), (t * 4.5, 3)], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let run1: RunEntry = (

                    runName: "Test normal item operation",
                    source: source1,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, 0), (t * 2, 1), (t * 3.5, 2), (t * 4.5, 3), (t * 5.5, 4), (t * 6.5, 5), (t * 8, 6)])
            )

            let run2: RunEntry = (

                    runName: "Test empty source",
                    source: emptySource,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName: "Test source with items and error",
                    source: sourceWithError,
                    expected: RxNotificationSequence<ItemType>(timedItems: [(t, 0), (t * 2, 1), (t * 3.5, 2), (t * 4.5, 3)], termination: etTerminationType.eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName: "Test empty source error",
                    source: emptySourceWithError,
                    expected: RxNotificationSequence<ItemType>(timedItems: [], termination: etTerminationType.eTermination_Error(error))
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
            ]
            {
                let (runName, source, expected) = runSpec

                func runTest(runName : String)
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)
                    let observable = source.take(20)
                    var wasUsed = false

                    let subscription = observable.deferredSubscribe(observer).onUnsubscription({
                        wasUsed = true
                    })

                    subscription.runAsync()

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(wasUsed, "Expected wasUsed to be changed")
                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest(runName)
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testTimeInterval()
    {
        let testName = TxFactory.createTestName(#function)
        let testObservableName = "timeInterval"

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expectedResult:[RxDuration])

            let t: RxDuration = TxConfig.TestTickUnit
            let source1 = RxSource<ItemType>.fromTimedArray([(t, 0), (t * 2, 1), (t * 3.5, 2), (t * 4.5, 3), (t * 5.5, 4), (t * 6.5, 5), (t * 8, 6)])
            let source2 = RxSource<ItemType>.fromTimedArray([(0, 0)])
            let emptySource = RxSource<ItemType>.empty()
            let sourceWithError = RxSource<ItemType>.fromTimedArray([(t, 0), (t * 2, 1), (t * 3.5, 2), (t * 4.5, 3)], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let run1: RunEntry = (

                    runName: "Test normal item operation",
                    source: source1,
                    expectedResult: [t, t, t, t * 1.5, t, t, t, t * 1.5]
            )

            let run2: RunEntry = (

                    runName: "Test empty source",
                    source: emptySource,
                    expectedResult: []
            )

            let run3: RunEntry = (

                    runName: "Test source with items and error",
                    source: sourceWithError,
                    expectedResult: [t, t, t * 1.5, t]
            )

            let run4: RunEntry = (

                    runName: "Test empty source error",
                    source: emptySourceWithError,
                    expectedResult: []
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
            ]
            {
                let (runName, source, expectedResult) = runSpec

                func decodeTimeInterval(timestamp : String, expectedDuration : RxDuration) -> (Int, RxDuration)
                {
                    if let range = timestamp.rangeOfString("@")
                    {
                        let sequenceNoText = timestamp.substringToIndex(range.startIndex)
                        let timeIntervalText = timestamp.substringFromIndex(range.endIndex)

                        let sequenceNo = Int(sequenceNoText)!
                        let timeInterval = RxDateFormatter.durationDecoder(timeIntervalText)!
                        let timeIntervalDiff = abs(timeInterval - expectedDuration)

                        return (sequenceNo, timeIntervalDiff)
                    }
                    else
                    {
                        fatalError("\(testObservableName) has no @ character")
                    }
                }

                func runTest()
                {
                    let observer = TxTestRigs<String>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.timeInterval()

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    var index = 0
                    let timeDiffs : [RxDuration] = observer.notifications.map({ (notification : RxNotification<String>) in

                        switch notification.notifyType
                        {
                            case .eNotification_Item(let item):

                                let (sequenceNo, timeIntervalDiff) = decodeTimeInterval(item, expectedDuration: expectedResult[index])

                                XCTAssertTrue(sequenceNo == index, "Expected timestamp sequence number to correspond")

                                index += 1

                                return timeIntervalDiff

                            case .eNotification_Termination:

                                fatalError("Unexpected code point")
                        }
                    }).filter({ return $0 > t })

                    XCTAssertTrue(timeDiffs.count == 0, "\"\(testObservableName)\" failure, some items were out of time: \(timeDiffs)\n")
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testTimestamp()
    {
        let testName = TxFactory.createTestName(#function)
        let testObservableName = "timestamp"

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expectedResult:[RxDuration])



            let t: RxDuration = TxConfig.TestTickUnit

            let source1 = RxSource<ItemType>.fromTimedArray([(t, 0), (t * 2, 1), (t * 3.5, 2), (t * 4.5, 3), (t * 5.5, 4), (t * 6.5, 5), (t * 8, 6)])
            let source2 = RxSource<ItemType>.fromTimedArray([])
            let emptySource = RxSource<ItemType>.empty()
            let sourceWithError = RxSource<ItemType>.fromTimedArray([(t, 0), (t * 2, 1), (t * 3.5, 2), (t * 4.5, 3)], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let run1: RunEntry = (

                    runName: "Test normal item operation",
                    source: source1,
                    expectedResult: [t, t * 2, t * 3.5, t * 4.5, t * 5.5, t * 6.5, t * 8]
            )

            let run2: RunEntry = (

                    runName: "Test empty source",
                    source: emptySource,
                    expectedResult: []
            )

            let run3: RunEntry = (

                    runName: "Test source with items and error",
                    source: sourceWithError,
                    expectedResult: [t, t * 2, t * 3.5, t * 4.5]
            )

            let run4: RunEntry = (

                    runName: "Test empty source error",
                    source: emptySourceWithError,
                    expectedResult: []
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
            ]
            {
                let (runName, source, expectedResult) = runSpec
                let dateFormatter = NSDateFormatter()

                func decodeTimestamp(timestamp : String, startTime : RxTime, expectedDuration : RxDuration) -> (Int, RxDuration)
                {
                    if let range = timestamp.rangeOfString("@")
                    {
                        let sequenceNoText = timestamp.substringToIndex(range.startIndex)
                        let timestampText = timestamp.substringFromIndex(range.endIndex)

                        let sequenceNo = Int(sequenceNoText)!
                        let time = RxDateFormatter.timeDecoderMicroSec(timestampText, dateFormatter: dateFormatter)!
                        let timeDiff = abs(time.timeIntervalSinceDate(startTime) - expectedDuration)

                        return (sequenceNo, timeDiff)
                    }
                    else
                    {
                        fatalError("\(testObservableName) has no @ character")
                    }
                }

                func runTest(runName : String)
                {
                    func getIntervalDiffs(notifications : RxNotificationSequence<String>, startTime: RxTime) -> [(sequenceNo:Int, timeOffset:RxDuration)]
                    {
                        var index = 0

                        return notifications.map({ (notification : RxNotification<String>) in

                            switch notification.notifyType
                            {
                                case .eNotification_Item(let item):

                                    let (sequenceNo, timeOffset) = decodeTimestamp(item, startTime: startTime, expectedDuration: expectedResult[index])

                                    XCTAssertTrue(sequenceNo == index, "Expected timestamp sequence number to correspond")

                                    index += 1

                                    return (sequenceNo: sequenceNo, timeOffset: timeOffset)

                                case .eNotification_Termination:

                                    fatalError("Unexpected code point")
                            }
                        })
                    }

                    let observer = TxTestRigs<String>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.timestamp()

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    let startTime : RxTime = subscription.sourceStartEvalTime!

                    let timeDiffs = getIntervalDiffs(observer.notifications, startTime: startTime).filter({ return $0.timeOffset > t })
                    let maxTimeDiff = getIntervalDiffs(observer.notifications, startTime: startTime).sort({ return $0.timeOffset > $1.timeOffset })

                    trace(2, "\tNote: time interval differences: \(maxTimeDiff)\n")
                    XCTAssertTrue(timeDiffs.count == 0, "\"\(testObservableName):\(runName)\" failure, some items were out of time: \(timeDiffs)\n")
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest(runName)
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testRemoveTimestamp()
    {
        let testName = TxFactory.createTestName(#function)
        let testObservableName = "removeTimestamp"

        func runAll()
        {
            typealias ItemType = Int
            typealias TargetItemType = ItemType
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expectedResult:RxNotificationQueue<ItemType>)


            let t: RxDuration = TxConfig.TestTickUnit

            let source1 = RxSource<ItemType>.fromTimedArray([(t, 0), (t * 2, 1), (t * 3.5, 2), (t * 4.5, 3), (t * 5.5, 4), (t * 6.5, 5), (t * 8, 6)])
            let source2 = RxSource<ItemType>.fromTimedArray([(0, 0)])
            let emptySource = RxSource<ItemType>.empty()
            let sourceWithError = RxSource<ItemType>.fromTimedArray([(t, 0), (t * 2, 1), (t * 3.5, 2), (t * 4.5, 3)], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let run1: RunEntry = (

                    runName: "Test normal item operation",
                    source: source1,
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: [0, 1, 2, 3, 4, 5, 6])
            )

            let run2: RunEntry = (

                    runName: "Test empty source",
                    source: emptySource,
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName: "Test source with items and error",
                    source: sourceWithError,
                    expectedResult:  RxNotificationQueue<ItemType>(items: [0, 1, 2, 3], termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName: "Test empty source error",
                    source: emptySourceWithError,
                    expectedResult: RxNotificationQueue<ItemType>(termination: .eTermination_Error(error))
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
            ]
            {
                let (runName, source, expectedResult) = runSpec

                func runTest(runName : String)
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.timestamp().removeTimestamp({ return ItemType($0)! })
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expectedResult, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expectedResult == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest(runName)
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testDump()
    {
        let testName = TxFactory.createTestName(#function)
        let testObservableName = "dump"

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expectedResult:RxNotificationQueue<ItemType>)



            let emptySource = RxSource<ItemType>.empty()
            let baseItemsList1 = [1, 2, 3, 4]
            let baseItemsList2 = [1, 2, 3]
            let source1 = RxSource<ItemType>.fromArray(baseItemsList1)
            let source1WithError = RxSource<ItemType>.fromArray(baseItemsList2, termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let run1: RunEntry = (

                    runName: "Test normal item operation",
                    source: source1,
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: baseItemsList1)
            )

            let run2: RunEntry = (

                    runName: "Test empty source",
                    source: emptySource,
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName: "Test source with items and error",
                    source: source1WithError,
                    expectedResult: RxNotificationQueue<ItemType>(items: baseItemsList2, termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName: "Test empty source error",
                    source: emptySourceWithError,
                    expectedResult: RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
            ]
            {
                let (runName, source, expectedResult) = runSpec

                func runTest()
                {
                    let dumpTitle = "dumpAction"
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.dump(dumpTitle)
                    let reportFunc = reportGenerator(runName, expectedResult, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expectedResult == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testDumpUsingAction()
    {
        let testName = TxFactory.createTestName(#function)
        let testObservableName = "dumpUsingAction"

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expectedResult:RxNotificationQueue<ItemType>)



            let emptySource = RxSource<ItemType>.empty()
            let baseItemsList1 = [1, 2, 3, 4]
            let baseItemsList2 = [1, 2, 3]
            let source1 = RxSource<ItemType>.fromArray(baseItemsList1)
            let source1WithError = RxSource<ItemType>.fromArray(baseItemsList2, termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let run1: RunEntry = (

                    runName: "Test normal item operation",
                    source: source1,
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: baseItemsList1)
            )

            let run2: RunEntry = (

                    runName: "Test empty source",
                    source: emptySource,
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName: "Test source with items and error",
                    source: source1WithError,
                    expectedResult: RxNotificationQueue<ItemType>(items: baseItemsList2, termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName: "Test empty source error",
                    source: emptySourceWithError,
                    expectedResult: RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
            ]
            {
                let (runName, source, expectedResult) = runSpec

                func runTest()
                {
                    let dumpTitle = source.tag
                    let foundDumpItems = RxNotificationQueue<ItemType>(tag : "\(testObservableName) items")

                    let dumpAction = { (title: String, description: String, item: ItemType?) -> Void in

                        XCTAssertTrue(title == dumpTitle, "Unexpected \(testObservableName) title")

                        switch item
                        {
                            case .None:

                                switch description
                                {
                                    case "completed":
                                        foundDumpItems.queueCompleted(nil)

                                    default:

                                        if description == "error: \(self.error.description)"
                                        {
                                            foundDumpItems.queueCompleted(self.error)
                                        }
                                        else
                                        {
                                            XCTFail("Unexpected \(testObservableName) description")
                                        }

                                }

                            case .Some(let itemValue):

                                foundDumpItems.queueItem(itemValue)
                        }
                    }

                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("dump test observer")
                    let observable = source.dumpUsingAction(dumpAction)
                    let dumpReportFunc = reportGenerator(runName, expectedResult, foundDumpItems)
                    let reportFunc = reportGenerator(runName, expectedResult, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expectedResult == foundDumpItems, dumpReportFunc())
                    XCTAssertTrue(expectedResult == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testMaterialize()
    {
        let testObservableName = "materialize"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias TargetItemType = RxNotification<ItemType>
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expectedResult:RxNotificationQueue<TargetItemType>)

            func mkIt(item : String) -> ItemType
            {
                return ItemType(item)
            }

            func mkTar(item : String) -> TargetItemType
            {
                return TargetItemType(item:ItemType(item))
            }

            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([mkIt("1"), mkIt("2"), mkIt("3"), mkIt("4")])
            let source1WithError = RxSource<ItemType>.fromArray([mkIt("1"), mkIt("2"), mkIt("3")], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let run1: RunEntry = (

                    runName: "Test normal item operation",
                    source: source1,
                    expectedResult: RxNotificationQueue<TargetItemType>(itemsThenCompletion: [mkTar("1"), mkTar("2"), mkTar("3"), mkTar("4"), TargetItemType.completed(nil)])
            )

            let run2: RunEntry = (

                    runName: "Test empty source",
                    source: emptySource,
                    expectedResult: RxNotificationQueue<TargetItemType>(itemsThenCompletion: [TargetItemType.completed(nil)])
            )

            let run3: RunEntry = (

                    runName: "Test source with items and error",
                    source: source1WithError,
                    expectedResult: RxNotificationQueue<TargetItemType>(items: [mkTar("1"), mkTar("2"), mkTar("3"), TargetItemType.completed(error)], termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName: "Test empty source error",
                    source: emptySourceWithError,
                    expectedResult: RxNotificationQueue<TargetItemType>(items: [TargetItemType.completed(error)], termination: .eTermination_Error(error))
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
            ]
            {
                let (runName, source, expectedResult) = runSpec

                func runTest()
                {
                    let observer = RxTestObserver<TargetItemType>(tag: "\(testObservableName) test observer")
                    let observable = source.materialize()
                    let reportFunc = reportGenerator(runName, expectedResult, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expectedResult == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }

        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testDematerialize()
    {
        let testObservableName = "dematerialize"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias DataItemType = String
            typealias ItemType = RxNotification<DataItemType>
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expectedResult:RxNotificationQueue<DataItemType>)

            func mkDa(value : String) -> DataItemType
            {
                return DataItemType(value)
            }

            func mkIt(value : String) -> ItemType
            {
                return ItemType(item:DataItemType(value))
            }

            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([mkIt("1"), mkIt("2:2"), mkIt("data"), mkIt(""), ItemType.completed(nil)])
            let source1WithError = RxSource<ItemType>.fromArray([mkIt("1"), mkIt("2:2"), mkIt("@data"), ItemType.completed(error)], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([ItemType.completed(error)], termination: .eTermination_Error(error))

            let run1: RunEntry = (

                    runName: "Test normal item operation",
                    source: source1,
                    expectedResult: RxNotificationQueue<DataItemType>(itemsThenCompletion: [mkDa("1"), mkDa("2:2"), mkDa("data"), mkDa("")])
            )

            let run2: RunEntry = (

                    runName: "Test empty source",
                    source: emptySource,
                    expectedResult: RxNotificationQueue<DataItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName: "Test source with items and error",
                    source: source1WithError,
                    expectedResult: RxNotificationQueue<DataItemType>(items: [mkDa("1"), mkDa("2:2"), mkDa("@data")], termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName: "Test empty source error",
                    source: emptySourceWithError,
                    expectedResult: RxNotificationQueue<DataItemType>(items: [], termination: .eTermination_Error(error))
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
            ]
            {
                let (runName, source, expectedResult) = runSpec

                func runTest()
                {
                    let observer = RxTestObserver<DataItemType>(tag: "\(testObservableName) test observer")
                    let observable : RxObservableMap<ItemType, DataItemType> = source.dematerialize()
                    let reportFunc = reportGenerator(runName, expectedResult, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expectedResult == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}

class RxObservable_NetFlix_Tests : TxTestCase
{
    func testFilter()
    {
        let testObservableName = "filter"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, predicate : RxTypes<ItemType>.RxPredicate, expected:RxNotificationQueue<ItemType>)

            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let evenFilterFunc = { (item : ItemType) -> Bool in return (item % 2) == 0 }
            let oddFilterFunc =  { (item : ItemType) -> Bool in return (item % 2) == 1 }
            let trueFilterFunc =  { (item : ItemType) -> Bool in return true }
            let falseFilterFunc =  { (item : ItemType) -> Bool in return false }

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    predicate:  evenFilterFunc,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 44, 50])
            )

            let run2: RunEntry = (

                    runName:    "Test filter none",
                    source:     source1,
                    predicate:  trueFilterFunc,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 35, 44, 50])
            )

            let run3: RunEntry = (

                    runName:    "Test filter all",
                    source:     source1,
                    predicate:  falseFilterFunc,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run4: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    predicate:  trueFilterFunc,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run5: RunEntry = (

                    runName:    "Test empty source with error",
                    source:     emptySourceWithError,
                    predicate:  trueFilterFunc,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
            )

            let run6: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    predicate:  oddFilterFunc,
                    expected:   RxNotificationQueue<ItemType>(items: [21, 35], termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, source, filterFunc, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.filter(filterFunc).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testMap()
    {
        let testObservableName = "map"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, mapFunc: RxTypes2<ItemType, ItemType>.RxItemMapOptionalFunc, expected:RxNotificationQueue<ItemType>)



            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let incMapFunc = { (item : ItemType) -> ItemType? in return item + 1 }
            let oddMapFunc =  { (item : ItemType) -> ItemType? in return (item % 2) == 1 ? nil : item + 1 }
            let nilMapFunc =  { (item : ItemType) -> ItemType? in return nil }

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testObservableName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    mapFunc:    incMapFunc,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [11, 22, 36, 45, 51])
            )

            let run2: RunEntry = (

                    runName:    "Test map to nil",
                    source:     source1,
                    mapFunc:    nilMapFunc,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:    "Test intermittent map",
                    source:     source1,
                    mapFunc:    oddMapFunc,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [11, 45, 51])
            )

            let run4: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    mapFunc:  oddMapFunc,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run5: RunEntry = (

                    runName:    "Test empty source with error",
                    source:     emptySourceWithError,
                    mapFunc:    oddMapFunc,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
            )

            let run6: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    mapFunc:    oddMapFunc,
                    expected:   RxNotificationQueue<ItemType>(items: [11, 45, 51], termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ] {
                let (runName, source, mapFunc, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let observable = source.map(mapFunc).appendTag(runName)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testDebounce()
    {
        let testObservableName = "debounce"

        func runAll(iteration : Int)
        {
            typealias ItemType = RxIndexType
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, dueTime:RxDuration, expected:RxNotificationSequence<ItemType>)

            let t: RxDuration = TxConfig.TestTickUnit
            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromTimedArray([(t, 1), (t * 2, 2), (t * 5, 5), (t * 6, 6), (t * 8, 7)])
            let source2 = RxSource<ItemType>.fromTimedArray([(0, 0)])
            let source3 = RxSource<ItemType>.fromTimedArray([(t, 1), (t * 2, 2), (t * 10, 7)])
            let emptySourceWithError = RxSource<ItemType>.fromTimedArray([], termination: .eTermination_Error(error))
            let source1WithError = RxSource<ItemType>.fromTimedArray([(t, 1), (t * 2, 2), (t * 3, 3), (t * 8, 4)], termination: .eTermination_Error(error))

            let run1: RunEntry = (

                    runName: "Test normal operation",
                    source: source1,
                    dueTime : 3 * t,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, (1)), (t * 4, 2), (t * 7, 6)])
            )

            let run2: RunEntry = (

                    runName: "Test empty source",
                    source: emptySource,
                    dueTime : 3 * t,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName: "Test error",
                    source: source1WithError,
                    dueTime : 3 * t,
                    expected: RxNotificationSequence<ItemType>(timedItems: [(t, 1), (t * 6, 3), (t * 8, 4)], termination: etTerminationType.eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName: "Test empty source with error",
                    source: emptySourceWithError,
                    dueTime : 3 * t,
                    expected: RxNotificationSequence<ItemType>(timedItems: [], termination: etTerminationType.eTermination_Error(error))
            )

            let run5: RunEntry = (

                    runName: "Test zero due time",
                    source: source1,
                    dueTime : 0,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, 1), (t * 2, 2), (t * 5, 5), (t * 6, 6), (t * 8, 7)])
            )

            let run6: RunEntry = (

                    runName: "Test timeout",
                    source: source3,
                    dueTime : 5 * t,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t, 1), (t * 7, 2), (t * 10, 7)])
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ]
            {
                let (runName, source, dueTime, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testObservableName) test observer")
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)
                    let observable = source.debounce(dueTime).traceAll(">>", bufferTraceMessages: true)
                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    #if RxMonEnabled

                        if let mon = RxSDK.mon.trace
                        {
                            if expected != observer.notifications
                            {
                                RxLog.log("\n---- error: -----> \(reportFunc())\n")
                            }

                            if expected != observer.notifications
                            {
                                // Output a trace message to console.
                                RxLog.log(mon.traceMessageBuffer)
                            }

                            mon.clearTraceBuffer()
                        }
                    #endif

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll(iteration)

            RxMon.shutdown()
        }
    }
}

