//
// Created by Terry Stillone (http://www.originware.com) on 6/10/2015.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import XCTest

@testable import RxPatternsSDK
@testable import RxPatternsLib

extension RxObservableMap
{
    public func isValidForWithEndWinowAction(duration : RxDuration) -> RxObservableMap<ItemType, ItemType?>
    {
        let tag = "isValidForWithEndWinowAction"

        if duration <= 0
        {
            // Return a Observable that emits an error.
            return throwError(RxLibError(.eInvalidEvalOp)).chainObservable(self)
        }

        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType?>) -> Void in

            let window = RxWindow(tag : tag + "/window")

            // Configure the window.
            window <- .eSetEvalQueue(evalNode.evalQueue) <- .eSetWindowEndAction({ evalNode.syncOutNotifier.notifyItem(nil) })

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType?>) in

                window.stopCurrentWindow()

                notifier.notifyItem(item)

                window.createSingleWindow(RxTime(), duration: duration)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType?>) in

                window.stopCurrentWindow()

                notifier.notifyCompleted(error)
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType?>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalEnd:

                        window.cancelAll()

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservableMap<ItemType, ItemType?>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    public func isValidForWithWindowAction(duration : RxDuration, windowStartItem: ItemType, windowEndItem: ItemType) -> RxObservableMap<ItemType, ItemType?>
    {
        let tag = "isValidForWithWindowAction"

        if duration <= 0
        {
            // Return a Observable that emits an error.
            return throwError(RxLibError(.eInvalidEvalOp)).chainObservable(self)
        }

        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType?>) -> Void in

            let window = RxWindow(tag : tag + "/window")
            let expectedIndex = 0

            // Configure the window.
            window <- .eSetEvalQueue(evalNode.evalQueue) <- .eSetWindowAction({ (window: RxWindow, windowEvent: eWindowEvent) in

                switch windowEvent
                {
                    case .eWindowStart(let(index, _)):
                        XCTAssertTrue(expectedIndex == index, "Unexpected start window index, expected \(expectedIndex), got: \(index)")
                        evalNode.syncOutNotifier.notifyItem(windowStartItem)

                    case .eWindowEnd(let(index, _)):
                        XCTAssertTrue(expectedIndex == index, "Unexpected end window index, expected \(expectedIndex), got: \(index)")
                        evalNode.syncOutNotifier.notifyItem(windowEndItem)
                }
            })

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType?>) in

                window.stopCurrentWindow()

                notifier.notifyItem(item)

                window.createSingleWindow(RxTime(), duration: duration)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType?>) in

                window.stopCurrentWindow()

                notifier.notifyCompleted(error)
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType?>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalEnd:

                        window.cancelAll()

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservableMap<ItemType, ItemType?>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    public func periodicWindowWithWindowEndAction(period: RxDuration, windowEndItem: ItemType) -> RxObservableMap<ItemType, ItemType?>
    {
        let tag = "periodicWindowWithWindowAction"

        if period <= 0
        {
            // Return a Observable that emits an error.
            return throwError(RxLibError(.eInvalidEvalOp)).chainObservable(self)
        }

        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType?>) -> Void in

            let window = RxWindow(tag : tag + "/window")

            // Configure the window.
            window <- .eSetEvalQueue(evalNode.evalQueue) <- .eSetWindowEndAction({ evalNode.syncOutNotifier.notifyItem(windowEndItem) })

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType?>) in

                window.stopCurrentWindow()

                notifier.notifyItem(item)

                window.createPeriodicWindow(RxTime(), startOffset: 0, period: period)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType?>) in

                window.stopCurrentWindow()

                notifier.notifyCompleted(error)
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType?>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalEnd:

                        window.cancelAll()

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservableMap<ItemType, ItemType?>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    public func periodicWindowWithWindowAction(period: RxDuration, windowStartItem: ItemType, windowEndItem: ItemType) -> RxObservableMap<ItemType, ItemType?>
    {
        let tag = "periodicWindowWithWindowAction"

        if period <= 0
        {
            // Return a Observable that emits an error.
            return throwError(RxLibError(.eInvalidEvalOp)).chainObservable(self)
        }

        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType?>) -> Void in

            let window = RxWindow(tag : tag + "/window")
            var expectedIndex = 0

            // Configure the window.
            window <- .eSetEvalQueue(evalNode.evalQueue) <- .eSetWindowAction({ (window: RxWindow, windowEvent: eWindowEvent) in

                switch windowEvent
                {
                    case .eWindowStart(let(index, _)):

                        if expectedIndex != index
                        {

                        }
                        XCTAssertTrue(expectedIndex == index, "Unexpected start window index, expected \(expectedIndex), got: \(index)")
                        evalNode.syncOutNotifier.notifyItem(windowStartItem)

                    case .eWindowEnd(let(index, _)):
                        XCTAssertTrue(expectedIndex == index, "Unexpected end window index, expected \(expectedIndex), got: \(index)")
                        evalNode.syncOutNotifier.notifyItem(windowEndItem)

                        expectedIndex += 1
                }
            })

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType?>) in

                window.stopCurrentWindow()

                expectedIndex = 0

                notifier.notifyItem(item)

                window.createPeriodicWindow(RxTime(), startOffset: 0, period: period)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType?>) in

                window.stopCurrentWindow()

                notifier.notifyCompleted(error)
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType?>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalEnd:

                        window.cancelAll()

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservableMap<ItemType, ItemType?>(tag: tag, evalOp: evalOp).chainObservable(self)
    }
}

func ==(lhs: Int?, rhs: Int?) -> Bool
{
    return true
}

class RxWindowTests  : TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testRxWindowSingleWindow()
    {
        let testSupportName = "SingleWindow"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source: RxSource<ItemType>, period:RxDuration, expected1 :RxNotificationSequence<ItemType?>, expected2 :RxNotificationSequence<ItemType?>)

            let t : RxDuration = TxConfig.TestTickUnit
            let emptyErrorSource = RxSource<ItemType>.throwError(error)

            let run1: RunEntry = (

                    runName: "Test normal operation",
                    source: RxSource.fromTimedArray([(t * 1, 1), (t * 2, 2), (t * 5, 5), (t * 9, 9), (t * 10, 10)]),
                    period: t * 2,
                    expected1: RxNotificationSequence<ItemType?>(timedItemsThenCompletion: [(t, 1), (t * 2, 2), (t * 4, nil), (t * 5, 5), (t * 7, nil), (t * 9, 9), (t * 10, 10)]),
                    expected2: RxNotificationSequence<ItemType?>(timedItemsThenCompletion: [(t, 1), (t, -1), (t * 2, 2), (t * 2, -1), (t * 4, -2), (t * 5, 5), (t * 5, -1), (t * 7, -2), (t * 9, 9), (t * 9, -1), (t * 10, 10)])
            )

            let run2: RunEntry = (

                    runName: "Empty Source",
                    source: RxSource.fromTimedArray([]),
                    period: t * 2,
                    expected1: RxNotificationSequence<ItemType?>(timedItemsThenCompletion: []),
                    expected2: RxNotificationSequence<ItemType?>(timedItemsThenCompletion: [])
            )

            for runSpec in [
                    run1,
                    run2
            ] {
                let (runName, source, period, expected1, expected2) = runSpec

                func runTestForEndWindowAction()
                {
                    let observable = source.isValidForWithEndWinowAction(period)
                    let observer = RxTestObserver<ItemType?>(tag: "window test observer")
                    let subscription = observable.subscribe(observer)

                    subscription.waitForDisposal()

                    let notifications : RxNotificationSequence<ItemType?> = observer.notifications

                    notifications.startTime = subscription.sourceStartEvalTime!

                    trace(2, "window notifications: \(notifications)")

                    let reportFunc = reportGenerator(runName, expected1, observer.notifications)

                    XCTAssertTrue(expected1 == observer.notifications, reportFunc())
                }

                func runTestForWindowAction()
                {
                    let observable = source.isValidForWithWindowAction(period, windowStartItem: -1, windowEndItem: -2)
                    let observer = RxTestObserver<ItemType?>(tag: "window test observer")
                    let subscription = observable.subscribe(observer)

                    subscription.waitForDisposal()

                    let notifications : RxNotificationSequence<ItemType?> = observer.notifications

                    notifications.startTime = subscription.sourceStartEvalTime!

                    trace(2, "window notifications: \(notifications)")

                    let reportFunc = reportGenerator(runName, expected2, observer.notifications)

                    XCTAssertTrue(expected2 == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTestForEndWindowAction()
                runTestForWindowAction()
                trace(2, "= run end   ==> \(runName) <==\n")
            }
        }

        trace(0, "\n")

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSupportName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testRxWindowPeriodicWindow()
    {
        let testSupportName = "PeriodicWindow"
        let testName        = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, period:RxDuration, expected1:RxNotificationSequence<ItemType?>, expected2:RxNotificationSequence<ItemType?>)

            let t: RxDuration    = TxConfig.TestTickUnit
            let emptyErrorSource = RxSource<ItemType>.throwError(error)

            let run1: RunEntry = (

                    runName: "Test normal operation",
                    source: RxSource.fromTimedArray([(t * 1, 1), (t * 2, 2), (t * 9, 9), (t * 10, 10)]),
                    period: t * 2,
                    expected1: RxNotificationSequence<ItemType?>(timedItemsThenCompletion: [(t, 1), (t * 2, 2), (t * 4, -2), (t * 6, -2), (t * 8, -2), (t * 9, 9), (t * 10, 10)]),
                    expected2: RxNotificationSequence<ItemType?>(timedItemsThenCompletion: [(t, 1), (t, -1), (t * 2, 2), (t * 2, -1), (t * 4, -2), (t * 4, -1), (t * 6, -2), (t * 6, -1), (t * 8, -2), (t * 8, -1), (t * 9, 9), (t * 9, -1), (t * 10, 10)])
            )

            let run2: RunEntry = (

                    runName: "Test Zero Period",
                    source: RxSource.fromTimedArray([]),
                    period: 0,
                    expected1: RxNotificationSequence<ItemType?>(termination: .eTermination_Error(RxLibError(.eInvalidEvalOp))),
                    expected2: RxNotificationSequence<ItemType?>(termination: .eTermination_Error(RxLibError(.eInvalidEvalOp)))
            )

            for runSpec in [
                    run1,
                    run2
            ]
            {
                let (runName, source, period, expected1, expected2) = runSpec

                func runTestForEndWindowAction()
                {
                    let observable   = source.periodicWindowWithWindowEndAction(period, windowEndItem: -2)
                    let observer     = RxTestObserver<ItemType?>(tag: "window test observer")
                    let subscription = observable.subscribe(observer)

                    subscription.waitForDisposal()

                    let notifications: RxNotificationSequence<ItemType?> = observer.notifications

                    notifications.startTime = subscription.sourceStartEvalTime!

                    trace(2, "window notifications: \(notifications)")

                    let reportFunc = reportGenerator(runName, expected1, observer.notifications)

                    XCTAssertTrue(expected1 == observer.notifications, reportFunc())
                }

                func runTestForWindowAction()
                {
                    let observable   = source.periodicWindowWithWindowAction(period, windowStartItem: -1, windowEndItem: -2)
                    let observer     = RxTestObserver<ItemType?>(tag: "window test observer")
                    let subscription = observable.subscribe(observer)

                    subscription.waitForDisposal()

                    let notifications: RxNotificationSequence<ItemType?> = observer.notifications

                    notifications.startTime = subscription.sourceStartEvalTime!

                    trace(2, "window notifications: \(notifications)")

                    let reportFunc = reportGenerator(runName, expected2, observer.notifications)

                    XCTAssertTrue(expected2 == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTestForEndWindowAction()
                runTestForWindowAction()
                trace(2, "= run end   ==> \(runName) <==\n")
            }
        }

        trace(0, "\n")

        for iteration in 0 ..< TxConfig.IterationCount
        {
            let percent         = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1, "\n==> test \(testSupportName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}
