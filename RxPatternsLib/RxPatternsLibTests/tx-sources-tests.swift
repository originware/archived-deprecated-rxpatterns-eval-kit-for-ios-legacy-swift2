//
//  Created by Terry Stillone on 19/03/2015.
//  Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import XCTest

@testable import RxPatternsSDK
@testable import RxPatternsLib

class RxMSSources_Primitive_Tests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        RxSDK.control.shutdown()
    }

    func testNever()
    {
        let testSourceName = "never"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = RxIndexType
            typealias RunEntry = (String)

            let t : RxDuration = TxConfig.TestTickUnit
            let emptyErrorSource = RxSource<ItemType>.throwError(error)

            let run1: RunEntry = (

            runName: "Test normal operation"
            )

            for runSpec in [
                    run1
            ] {
                let (runName) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testSourceName) test observer")
                    let source = RxSource<ItemType>.never()
                    let subscription = source.subscribe(observer)

                    if subscription.waitForDisposal(timeout: t)
                    {
                        XCTFail("Unexpected: Never source did complete")
                    }
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testEmpty()
    {
        let testSourceName = "empty"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (String)

            let t : RxDuration = TxConfig.TestTickUnit

            let run1: RunEntry = (

            runName: "Test normal operation"
            )

            for runSpec in [
                    run1
            ] {
                let (runName) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testSourceName) test observer")
                    let source = RxSource<ItemType>.empty()
                    let subscription = source.subscribe(observer)
                    let expected = RxNotificationQueue<ItemType>(itemsThenCompletion: [])
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testReturnItem()
    {
        let testSourceName = "returnItem"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (String)

            let t : RxDuration = TxConfig.TestTickUnit

            let run1: RunEntry = (

            runName: "Test normal operation"
            )

            for runSpec in [
                    run1
            ] {
                let (runName) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testSourceName) test observer")
                    let source = RxSource<ItemType>.returnItem("one")
                    let subscription = source.subscribe(observer)
                    let expected = RxNotificationQueue<ItemType>(itemsThenCompletion: ["one"])
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testRaise()
    {
        let testSourceName = "returnItem"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (String)

            let testSourceName = "raise"

            let t : RxDuration = TxConfig.TestTickUnit

            let run1: RunEntry = (

            runName: "Test normal operation"
            )

            for runSpec in [
                    run1
            ] {
                let (runName) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testSourceName) test observer")
                    let source = RxSource<ItemType>.throwError(error)
                    let subscription = source.subscribe(observer)
                    let expected = RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}

class RxMSSources_Create_Tests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testCreate()
    {
        let testSourceName = "create"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expected:RxNotificationQueue<ItemType>)

            let emptySource = RxSource<ItemType>.empty()
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testSourceName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 35, 44, 50])
            )

            let run2: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    expected:   RxNotificationQueue<ItemType>(items: [10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName:    "Test empty source with error",
                    source:     emptySourceWithError,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4
            ] {
                let (runName, source, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testSourceName) test observer")
                    let observable = RxSource.create({ (consumer : ARxConsumer<ItemType>) -> RxSubscription in

                        return source.subscribe(consumer)

                    }).appendTag(runName)

                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testInterval()
    {
        let testSourceName = "interval"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = RxIndexType
            typealias RunEntry = (runName:String, period:RxDuration, waitCount:RxIndexType, expected:RxNotificationSequence<ItemType>)

            let t : RxDuration = TxConfig.TestTickUnit
            let emptyErrorSource = RxSource<ItemType>.throwError(error)

            let run1: RunEntry = (

                    runName: "Test normal operation",
                    period: t,
                    waitCount: 5,
                    expected: RxNotificationSequence<ItemType>(timedItems: [(t, 0), (t * 2, 1), (t * 3, 2), (t * 4, 3), (t * 5, 4)])
            )

            let run2: RunEntry = (

                    runName: "Test zero period",
                    period: 0,
                    waitCount: 3,
                    expected: RxNotificationSequence<ItemType>(timedItems: [(0, 0), (0, 1), (0, 2)])
            )

            let run3: RunEntry = (

                    runName: "Test negative period",
                    period: -1,
                    waitCount: 20,
                    expected: RxNotificationSequence<ItemType>(timedItems: [], termination: etTerminationType.eTermination_Error(RxLibError(.eInvalidTimePeriod)))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3
            ] {
                let (runName, period, waitCount, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserverManagingSubscription("subscription test observer", maxItemCount : waitCount)

                    let map = { (index : RxIndexType) -> ItemType in

                        return ItemType(index)
                    }
                    let source = RxSource<ItemType>.interval(period, map : map)
                    let subscription = source.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testRepeatForever()
    {
        let testSourceName = "repeatForever"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName: String, expected: RxNotificationQueue<ItemType>)

            let run1: RunEntry = (

            runName: "Test normal operation",
                    expected:   RxNotificationQueue<ItemType>(count: 10, repeatingItem:"One")
            )

            for runSpec in [
                    run1
            ] {
                let (runName, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserverManagingSubscription("\(testSourceName) test observer", maxItemCount : 10)
                    let source = RxSource<ItemType>.repeatForever("One").appendTag(runName)
                    let subscription = source.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testRepeatWithCount()
    {
        let testSourceName = "repeatWithCount"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, count:RxIndexType, expected:RxNotificationQueue<ItemType>)

            let testName = TxFactory.createTestName(#function)
            let result = RxNotificationQueue<ItemType>(tag: "expected \(testSourceName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 10])
            )

            let run2: RunEntry = (

                    runName:    "Test zero count",
                    count:      0,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            for runSpec in [
                    run1,
                    run2
            ] {
                let (runName, count, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testSourceName) test observer")
                    let source = RxSource<ItemType>.repeatWithCount(10, count: count).appendTag(runName)
                    let subscription = source.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testRepeatSourceForever()
    {
        let testSourceName = "repeatSourceForever"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, count:RxIndexType, expected:RxNotificationQueue<ItemType>)

            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44], termination: .eTermination_Error(error))

            let t : RxTimeOffset = TxConfig.TestTickUnit

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testSourceName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    count:      12,
                    expected:   RxNotificationQueue<ItemType>(items: [10, 21, 35, 44, 10, 21, 35, 44, 10, 21, 35, 44])
            )

            let run2: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    count:      0,
                    expected:   RxNotificationQueue<ItemType>(items: [])
            )

            let run3: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    count:      5,
                    expected:   RxNotificationQueue<ItemType>(items: [10, 21, 35, 44], termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3
            ] {
                let (runName, source, count, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserverManagingSubscription("\(runName) test observer", maxItemCount : count)
                    let observable = RxSource.repeatSourceForever(source)
                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    if count > 0
                    {
                        TxDisposalTestRigs.waitForDisposal(subscription)
                    }
                    else
                    {
                        NSThread.sleepForTimeInterval(t / 100)

                        subscription.unsubscribe()
                    }

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace("\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace("= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testGenerate()
    {
        let testSourceName = "generate"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = (Int)
            typealias GenerateFunc = (ItemType) -> ItemType?
            typealias RunEntry = (runName:String, generateFunc:GenerateFunc, expected:RxNotificationQueue<ItemType>)

            let testName = TxFactory.createTestName(#function)
            let result = RxNotificationQueue<ItemType>(tag: "expected \(testSourceName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    generateFunc: { (index : RxIndexType) -> ItemType? in
                                        let items : [ItemType?] = [(0), (3), nil]
                                        return items[index]
                                    },
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [(0), (3)])
            )

            let run2: RunEntry = (

                    runName:    "Test zero count",
                                    generateFunc:  { (index : RxIndexType) -> ItemType? in
                                        return nil
                                    },
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            for runSpec in [
                    run1,
                    run2
            ] {
                let (runName, generateFunc, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testSourceName) test observer")
                    let source = RxSource<ItemType>.generate(generateFunc).appendTag(runName)
                    let subscription = source.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testRange()
    {
        let testSourceName = "range"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, start:RxIndexType, count:RxIndexType, expected:RxNotificationQueue<ItemType>)


            let testName = TxFactory.createTestName(#function)
            let result = RxNotificationQueue<ItemType>(tag: "expected \(testSourceName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    start:      0,
                    count:      2,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [0, 1])
            )

            let run2: RunEntry = (

                    runName:    "Test zero count",
                    start:      1,
                    count:      0,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            for runSpec in [
                    run1,
                    run2
            ] {
                let (runName, start, count, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testSourceName) test observer")
                    let source = RxSource<ItemType>.range(start, count: count).appendTag(runName)
                    let subscription = source.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testStartWith()
    {
        let testSourceName = "startsWith"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias ItemFactoryType = () -> ItemType
            typealias RunEntry = (runName:String, itemFactory:ItemFactoryType, expected:RxNotificationQueue<ItemType>)

            let testName = TxFactory.createTestName(#function)
            let result = RxNotificationQueue<ItemType>(tag: "expected \(testSourceName) result")

            let run1: RunEntry = (

            runName:        "Test normal operation",
                    itemFactory:    {() -> ItemType in return 44},
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [44])
            )

            for runSpec in [
                    run1
            ] {
                let (runName, itemFactory, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testSourceName) test observer")
                    let source = RxSource<ItemType>.startWith(itemFactory).appendTag(runName)
                    let subscription = source.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testTimerByTimeOffset()
    {
        let testSourceName = "timerWithTimeOffset"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = RxIndexType
            typealias RunEntry = (runName:String, item: ItemType, timeOffset:RxDuration, expected:RxNotificationSequence<ItemType>)

            let t : RxDuration = TxConfig.TestTickUnit

            let run1: RunEntry = (

                    runName: "Test normal operation",
                    item: 77,
                    timeOffset: t * 5,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t * 5, 77)])
            )

            let run2: RunEntry = (

                    runName: "Test zero timeOffset",
                    item: 77,
                    timeOffset: 0,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(0, 77)])
            )

            let run3: RunEntry = (

                    runName: "Test negative timeOffset",
                    item: 77,
                    timeOffset: -1,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(0, 77)])
            )

            for runSpec in [
                    run1,
                    run2,
                    run3
            ] {
                let (runName, item, timeOffset, expected) = runSpec

                func runTest() {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testSourceName) test observer")
                    let source = RxSource<ItemType>.timer(item, dueTimeOffset:timeOffset)
                    let subscription = source.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    subscription.waitForDisposal()

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testTimerByDueTime()
    {
        let testSourceName = "timerByDueTime"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = RxIndexType
            typealias RunEntry = (runName:String, item: ItemType, timeOffset:RxDuration, expected:RxNotificationSequence<ItemType>)

            let t : RxDuration = TxConfig.TestTickUnit

            let run1: RunEntry = (

                    runName: "Test normal operation",
                    item: 77,
                    timeOffset: t * 5,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(t * 5, 77)])
            )

            let run2: RunEntry = (

                    runName: "Test now",
                    item: 77,
                    timeOffset: 0,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(0, 77)])
            )

            let run3: RunEntry = (

                    runName: "Test past",
                    item: 77,
                    timeOffset: -1,
                    expected: RxNotificationSequence<ItemType>(timedItemsThenCompletion: [(0, 77)])
            )

            for runSpec in [
                    run1,
                    run2,
                    run3
            ] {
                let (runName, item, timeOffset, expected) = runSpec

                func runTest() {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testSourceName) test observer")
                    let source = RxSource<ItemType>.timer(item, dueTimeOffset:timeOffset)
                    let subscription = source.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    subscription.waitForDisposal()

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testPeriodicTimerByDueTime()
    {
        let testSourceName = "periodicTimerByDueTime"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = RxIndexType
            typealias RunEntry = (runName:String, item: ItemType, timeOffset:RxTimeOffset, period:RxDuration, waitCount:RxIndexType, expected:RxNotificationSequence<ItemType>)

            let t : RxDuration = TxConfig.TestTickUnit

            let run1: RunEntry = (

                    runName: "Test normal operation",
                    item: 77,
                    timeOffset: t * 5,
                    period: t,
                    waitCount: 3,
                    expected: RxNotificationSequence<ItemType>(timedItems: [(t * 5, 77), (t * 6, 78), (t * 7, 79)])
            )

            let run2: RunEntry = (

                    runName: "Test now timeOffset",
                    item: 77,
                    timeOffset: 0,
                    period: t,
                    waitCount: 3,
                    expected: RxNotificationSequence<ItemType>(timedItems: [(0, 77), (t * 2, 78), (t * 3, 79)])
            )

            let run3: RunEntry = (

                    runName: "Test negative period",
                    item: 77,
                    timeOffset: 2 * t,
                    period: -t,
                    waitCount: 4,
                    expected: RxNotificationSequence<ItemType>(timedItems: [], termination: .eTermination_Error(RxLibError(.eInvalidTimePeriod)))
            )

            let run4: RunEntry = (

                    runName: "Test zero period",
                    item: 77,
                    timeOffset: -1,
                    period: 0,
                    waitCount: 4,
                    expected: RxNotificationSequence<ItemType>(timedItems: [], termination: .eTermination_Error(RxLibError(.eInvalidTimePeriod)))
            )

            let run5: RunEntry = (

                    runName: "Test past time",
                    item: 77,
                    timeOffset: -1,
                    period: t,
                    waitCount: 2,
                    expected: RxNotificationSequence<ItemType>(timedItems: [(0, 77), (t * 2, 78)])
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5
            ] {
                let (runName, item, timeOffset, period, waitCount, expected) = runSpec

                func runTest() {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserverManagingSubscription("\(testSourceName) test observer", maxItemCount : waitCount)
                    let time = RxTime(timeIntervalSinceNow: timeOffset)
                    let source = RxSource<ItemType>.timer(item, dueTime:time, period: period)
                    let subscription = source.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testPeriodicTimerByTimeOffset()
    {
        let testSourceName = "periodicTimerByTimeOffset"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = RxIndexType
            typealias RunEntry = (runName:String, item: ItemType, timeOffset:RxTimeOffset, period:RxDuration, waitCount:RxIndexType, expected:RxNotificationSequence<ItemType>)

            let t : RxDuration = TxConfig.TestTickUnit

            let run1: RunEntry = (

                    runName: "Test normal operation",
                    item: 77,
                    timeOffset: t * 5,
                    period: t,
                    waitCount: 3,
                    expected: RxNotificationSequence<ItemType>(timedItems: [(t * 5, 77), (t * 6, 78), (t * 7, 79)])
            )

            let run2: RunEntry = (

                    runName: "Test zero timeOffset",
                    item: 77,
                    timeOffset: 0,
                    period: t,
                    waitCount: 3,
                    expected: RxNotificationSequence<ItemType>(timedItems: [(0, 77), (t * 2, 78), (t * 3, 79)])
            )

            let run3: RunEntry = (

                    runName: "Test negative period",
                    item: 77,
                    timeOffset: 2 * t,
                    period: -t,
                    waitCount: 4,
                    expected: RxNotificationSequence<ItemType>(timedItems: [], termination: .eTermination_Error(RxLibError(.eInvalidTimePeriod)))
            )

            let run4: RunEntry = (

                    runName: "Test zero period",
                    item: 77,
                    timeOffset: -t,
                    period: 0,
                    waitCount: 4,
                    expected: RxNotificationSequence<ItemType>(timedItems: [], termination: .eTermination_Error(RxLibError(.eInvalidTimePeriod)))
            )

            let run5: RunEntry = (

                    runName: "Test negative timeoffset",
                    item: 77,
                    timeOffset: -t,
                    period: t,
                    waitCount: 2,
                    expected: RxNotificationSequence<ItemType>(timedItems: [(0, 77), (t * 2, 78)])
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
                    run5
            ] {
                let (runName, item, timeOffset, period, waitCount, expected) = runSpec

                func runTest() {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserverManagingSubscription("\(testSourceName) test observer", maxItemCount : waitCount)
                    let source = RxSource<ItemType>.timer(item, dueTimeOffset:timeOffset, period: period)
                    let subscription = source.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testDeferObservable()
    {
        let testSourceName = "deferObservable"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expected:RxNotificationQueue<ItemType>)

            let emptySource = RxSource<ItemType>.empty()
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))
            let source1 = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50])
            let source1WithError = RxSource<ItemType>.fromArray([10, 21, 35, 44, 50], termination: .eTermination_Error(error))

            let result = RxNotificationQueue<ItemType>(tag: "expected \(testSourceName) result")

            let run1: RunEntry = (

                    runName:    "Test normal operation",
                    source:     source1,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [10, 21, 35, 44, 50])
            )

            let run2: RunEntry = (

                    runName:    "Test empty source",
                    source:     emptySource,
                    expected:   RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName:    "Test source with error",
                    source:     source1WithError,
                    expected:   RxNotificationQueue<ItemType>(items: [10, 21, 35, 44, 50], termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName:    "Test empty source with error",
                    source:     emptySourceWithError,
                    expected:   RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4
            ] {
                let (runName, source, expected) = runSpec

                func runTest()
                {
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testSourceName) test observer")
                    let observable = RxSource.deferObservable({ () -> ARxProducer<ItemType> in

                        return source

                    }).appendTag(runName)

                    let subscription = observable.subscribe(observer)
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}

class RxMSSources_Conversion_Tests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testFromArray()
    {
        let testSourceName = "fromArray"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = Int
            typealias ItemFactoryType = () -> ItemType
            typealias RunEntry = (runName:String, items:[ItemType], etTerminationType, expected:RxNotificationQueue<ItemType>)

            let testName = TxFactory.createTestName(#function)

            let run1: RunEntry = (

                    runName:        "Test item operation",
                    items:          [0, 1],
                    termination:    .eTermination_Completed,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [0, 1])
            )

            let run2: RunEntry = (

                    runName:        "Test empty stream",
                    items:          [],
                    termination:    .eTermination_Completed,
                    expected:       RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )


            let run3: RunEntry = (

                    runName:        "Test empty stream with error",
                    items:          [],
                    termination:    .eTermination_Error(error),
                    expected:       RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
            )


            for runSpec in [
                    run1,
                    run2,
                    run3
            ] {
                let (runName, items, termination, expected) = runSpec

                func runTest()
                {
                    let observable = RxSource.fromArray(items, termination : termination, tag: "source")
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testSourceName) test observer")
                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func fromTimedArray_test<ItemType>(timingBoard : TxTimingBoard<ItemType>)
    {
        let testSourceName = "fromTimedArray"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias RunEntry = (runName:String, matchTags:Set<RxIndexType>)

            let run1: RunEntry = (

                    runName:    "Test basic one stream operation",
                    matchTags:  [1]
            )

            let run2: RunEntry = (

                    runName:    "Test empty stream",
                    matchTags:  [3]
            )


            let run3: RunEntry = (

                    runName:    "Test error stream",
                    matchTags:  [2]
            )

            let run4: RunEntry = (

                    runName:    "Test empty error stream",
                    matchTags:  [4]
            )

            for runSpec in [
                    run1,
                    run2,
                    run3,
                    run4,
            ] {
                let (runName, matchTags) = runSpec

                func runTest()
                {
                    let (items, termination) = timingBoard.extractTimingArrayItems(matchTags)
                    let observable = RxSource.fromTimedArray(items, termination : termination, tag: "source")
                    let observer = TxTestRigs<ItemType>.fullCheckingTestObserver("\(testSourceName) test observer")
                    let expected = timingBoard.extractTimedNotifications(matchTags)

                    let reportFunc = reportGenerator(runName, expected, observer.notifications)

                    let subscription = observable.subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    if expected != observer.notifications { RxLog.log(reportFunc()) }

                    XCTAssertTrue(expected == observer.notifications, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for _ in 0..<1
        {
            runAll()
        }
    }

    func testFromTimingArray()
    {
        let testSourceName = "fromTimedArray"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias TickType = ItemType

            let t : RxTimeOffset = TxConfig.TestShortTickUnit
            let timingArray : TxTimingBoard<ItemType>.TimingArrayType = [

                    (t,         1,  .eItem("1-0")),
                    (t * 2,     2,  .eItem("2-0")),
                    (t * 3,     1,  .eItem("1-1")),
                    (t * 5,     2,  .eItem("2-1")),
                    (t * 6,     2,  .eItem("2-2")),
                    (t * 7,     1,  .eItem("1-2")),
                    (t * 8,     1,  .eCompleted(.eTermination_Completed)),
                    (t * 9,     2,  .eCompleted(.eTermination_Error(error))),
                    (t * 3,     3,  .eCompleted(.eTermination_Completed)),
                    (t * 2,     4,  .eCompleted(.eTermination_Error(error)))
            ]

            let testMatrix = TxTestMatrix<ItemType>(tag : testName, timingArray : timingArray)

            for test in testMatrix.allTests
            {
                fromTimedArray_test(test.timingBoard)
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testSourceName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}