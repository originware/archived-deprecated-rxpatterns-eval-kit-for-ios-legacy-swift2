//
// Created by Terry Stillone (http://www.originware.com) on 28/11/2015.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import XCTest

@testable import RxPatternsSDK
@testable import RxPatternsLib

public class StreamReader : RxObject
{
    var index : Int = 0
    let destructDelegate : () -> Void

    public init(filePath : String, destructDelegate : () -> Void)
    {
        self.destructDelegate = destructDelegate

        super.init(tag: "StreamReader")
    }

    deinit
    {
        destructDelegate()
    }

    public func nextLine() -> String?
    {
        let currentIndex = index

        index += 1

        return (currentIndex < 10) ? "line\n" : nil
    }
}

func MyCustomDataDecoder<DataType : StringLiteralConvertible>(line: String, type: DataType.Type) -> (timeOffset: RxTimeOffset, dataItem: DataType?)
{
    return (TxConfig.TestTickUnit, "Line data")
}

extension RxSource where ItemType : StringLiteralConvertible
{
    public class func fileReaderSourceAsync(filePath: String, timesAreStrict: Bool, subscriptionType: eRxSubscriptionType, destructDelegate : () -> Void) -> RxSource<ItemType>
    {
        let evalOp: ((RxEvalNode<ItemType, ItemType>) -> Void) = { (evalNode: RxEvalNode<ItemType, ItemType>) -> Void in

            var streamReader: StreamReader? = nil
            var lastItem: ItemType? = nil
            var testObject : RxTestMonitoredObject? = nil

            let generator = { (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxAsyncGenCommand in

                switch index
                {
                    case 0:
                        // Create the stream reader.
                        streamReader = StreamReader(filePath: filePath, destructDelegate: destructDelegate)
                        testObject = RxTestMonitoredObject(tag: "test object", traceTag:nil)

                        if let traceTag = evalNode.traceTag
                        {
                            streamReader!.trace(traceTag)
                        }

                        // Handler compiler non-read warning.
                        assert(testObject != nil)
                        return .eNextTickAt(0)

                    default:
                        // If there was an item scheduled for this time, emit it.
                        if let lastItem = lastItem
                        {
                            notifier.notifyItem(lastItem)
                        }

                        // Read the next text line.
                        if let line = streamReader!.nextLine()
                        {
                            // Decode the next item and time-offset from the file.
                            let (timeOffset, dataItem) = MyCustomDataDecoder(line, type: ItemType.self)

                            // This item is to be run in the next generator call, save it.
                            lastItem = dataItem

                            // Direct the handler to call us again at this time-offset.
                            return .eNextTickAt(timeOffset)
                        }
                        else
                        {
                            // Close the stream reader and emit a completed.
                            //streamReader = nil
                            lastItem = nil
                            notifier.notifyCompleted()
                            return .eStopTicking
                        }
                }
            }

            let evalOp2 : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.asyncGenPat(timesAreStrict, generator: generator)

            evalOp2(evalNode)
        }

        return RxSource(tag: "fromFileDataSource", subscriptionType: subscriptionType, evalOp: evalOp)
    }

    public class func fileReaderSourceSync(filePath: String, timesAreStrict: Bool, subscriptionType: eRxSubscriptionType, destructDelegate : () -> Void) -> RxSource<ItemType>
    {
        let evalOp: ((RxEvalNode<ItemType, ItemType>) -> Void) = { (evalNode: RxEvalNode<ItemType, ItemType>) -> Void in

            var streamReader: StreamReader? = nil
            var lastItem: ItemType? = nil
            var testObject : RxTestMonitoredObject? = nil

            let generator = { (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxSyncGenCommand in

                switch index
                {
                    case 0:
                        // Create the stream reader.
                        streamReader = StreamReader(filePath: filePath, destructDelegate: destructDelegate)
                        testObject = RxTestMonitoredObject(tag: "test object", traceTag:nil)
                        assert(testObject != nil)
                        return .eNextTick

                    default:
                        // If there was an item scheduled for this time, emit it.
                        if let lastItem = lastItem
                        {
                            notifier.notifyItem(lastItem)
                        }

                        // Read the next text line.
                        if let line = streamReader!.nextLine()
                        {
                            // Decode the next item and time-offset from the file.
                            let (_, dataItem) = MyCustomDataDecoder(line, type: ItemType.self)

                            // This item is to be run in the next generator call, save it.
                            lastItem = dataItem

                            // Direct the handler to call us again at this time-offset.
                            return .eNextTick
                        }
                        else
                        {
                            // Do not nil the streamReader, as we want timer subscription destruction to do that itself.
                            //streamReader = nil
                            lastItem = nil
                            notifier.notifyCompleted()
                            return .eStopTicking
                        }
                }
            }

            let evalOp2 : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.syncGenPat(generator)

            evalOp2(evalNode)
        }

        return RxSource(tag: "fromFileDataSource", subscriptionType: subscriptionType, evalOp: evalOp)
    }
}

class RxDeallocationTests: TxTestCase
{
    typealias ItemType = Int
    typealias RunEntry = (runName:String, observable:ARxProducer<ItemType>, expectedEvalQueueCount:Int, expected:RxNotificationQueue<ItemType>)
    typealias SourceRunEntry = (runName:String, source:RxSource<ItemType>, expected:RxNotificationQueue<ItemType>)

    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testAsyncGenPatEvalOpDeallocation()
    {
        let testName = "asyncGenPat Deallocation"

        func runAll(iteration : Int)
        {
            var hasDestructed = false

            let instanceMon = RxMonInstances()
            let reportFunc = reportGenerator(testName, instanceMon)

            RxMon.addMonitor("instanceMon", monitor: instanceMon)

            do
            {
                let destructDelegate = {
                    hasDestructed = true
                }

                let source: RxSource<String> = RxSource<String>.fileReaderSourceAsync("dummy test path", timesAreStrict: true, subscriptionType: .eHot, destructDelegate: destructDelegate)

                do
                {
                    let subscription = source.traceAll(">>", bufferTraceMessages : true).subscribe()

                    TxDisposalTestRigs.waitForDisposal(subscription)
                }

                #if RxMonEnabled

                    if let mon = RxSDK.mon.trace
                    {
                        if !hasDestructed
                        {
                            RxLog.log("\n- test object not destructed --------------------------------------\n")
                        }

                        if !hasDestructed
                        {
                            // Output a trace message to console.
                            RxLog.log(mon.traceMessageBuffer)
                        }
                    }

                #endif

                XCTAssertTrue(hasDestructed, "Expected StreamReader to be destructed.")
            }

            RxSDK.evalQueue.stopAll()

            let objectCount = instanceMon.objectCount
            let message = reportFunc()

            if let mon = RxSDK.mon.trace where objectCount != 0
            {
                RxLog.log(message)

                // Output a trace message to console.
                RxLog.log(mon.traceMessageBuffer)

                // Check that all objects were deallocated, including the source.
                XCTAssertTrue(false, message)
           }

            if let mon = RxSDK.mon.trace
            {
                mon.clearTraceBuffer()
            }

            RxMon.shutdown()
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            RxSDK.control.start()

            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll(iteration)

            RxMon.shutdown()
        }
    }

    func testSyncGenPatEvalOpDeallocation()
    {
        let testName = "syncGenPat Deallocation"

        func runAll(iteration : Int)
        {
            var hasDestructed = false

            let destructDelegate = {
                hasDestructed = true
            }

            let source: RxSource<String> = RxSource<String>.fileReaderSourceSync("test", timesAreStrict: true, subscriptionType: .eHot, destructDelegate: destructDelegate)

            do
            {
                do
                {
                    let subscription = source.traceAll(">>", bufferTraceMessages : true).subscribe()

                    TxDisposalTestRigs.waitForDisposal(subscription)
                }

                #if RxMonEnabled

                    if let mon = RxSDK.mon.trace
                    {
                        if !hasDestructed
                        {
                            RxLog.log("\n- test object not destructed --------------------------------------\n")
                        }

                        if !hasDestructed
                        {
                            // Output a trace message to console.
                            RxLog.log(mon.traceMessageBuffer)
                        }

                        mon.clearTraceBuffer()
                    }
                #endif
                XCTAssertTrue(hasDestructed, "Expected StreamReader to be destructed.")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll(iteration)

            RxMon.shutdown()
        }
    }
}