//
//  Created by Terry Stillone on 19/03/2015.
//  Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import XCTest

@testable import RxPatternsSDK
@testable import RxPatternsLib


class RxEnumerationTests: TxTestCase
{
    override func setUp()
    {
        super.setUp()

        RxSDK.control.start()
    }

    override func tearDown()
    {
        super.tearDown()

        RxSDK.control.shutdown()
    }

    func testToEnumerable()
    {
        let testName = TxFactory.createTestName(#function)
        let testObservableName = "toEnumerable"

        func runAll()
        {
            typealias ItemType = Int16
            typealias RunEntry = (runName:String, source:RxSource<ItemType>, expectedResult:RxNotificationQueue<ItemType>)

            let emptySource = RxSource<ItemType>.empty()
            let source1 = RxSource<ItemType>.fromArray([1, 4, 2, 0, 223])
            let source1WithError = RxSource<ItemType>.fromArray([1, 4, 2, 0, 223], termination: .eTermination_Error(error))
            let emptySourceWithError = RxSource<ItemType>.fromArray([], termination: .eTermination_Error(error))

            let run1: RunEntry = (

                    runName: "Test normal item operation",
                    source: source1,
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: [1, 4, 2, 0, 223])
            )

            let run2: RunEntry = (

                    runName: "Test empty source",
                    source: emptySource,
                    expectedResult: RxNotificationQueue<ItemType>(itemsThenCompletion: [])
            )

            let run3: RunEntry = (

                    runName: "Test source with items and error",
                    source: source1WithError,
                    expectedResult: RxNotificationQueue<ItemType>(items: [1, 4, 2, 0, 223], termination: .eTermination_Error(error))
            )

            let run4: RunEntry = (

                    runName: "Test empty source error",
                    source: emptySourceWithError,
                    expectedResult: RxNotificationQueue<ItemType>(items: [], termination: .eTermination_Error(error))
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
            ]
            {
                let (runName, source, expectedResult) = runSpec

                func runTest()
                {
                    let resultQueue: TRxEnumerable<ItemType> = source.toEnumerable()
                    let reportFunc = reportGenerator(runName, expectedResult, resultQueue)

                    XCTAssertTrue(expectedResult == resultQueue, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testLatest()
    {
        let testObservableName = "latest"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, sourceTags: Set<Int>, delay: RxDuration, expected: RxNotificationQueue<ItemType>)
            typealias TimingArrayType = RxTypes<ItemType>.TimingArrayType

            let t : RxTimeOffset = TxConfig.TestShortTickUnit

            let timingBoard = TxTimingBoard<ItemType>([
                      (t,     1, .eItem("1-0")),
                      (t * 2, 4, .eCompleted(.eTermination_Completed)),
                      (t * 2, 2, .eItem("2-0")),
                      (t * 3, 1, .eItem("1-1")),
                      (t * 4, 5, .eCompleted(.eTermination_Error(error))),
                      (t * 4, 3, .eItem("3-0")),
                      (t * 5, 2, .eItem("2-1")),
                      (t * 6, 3, .eItem("3-1")),
                      (t * 7, 3, .eCompleted(.eTermination_Error(error))),
                      (t * 8, 2, .eItem("2-2")),
                      (t * 8, 1, .eItem("1-2")),
                      (t * 9, 2, .eCompleted(.eTermination_Completed)),
                      (t * 11, 1, .eCompleted(.eTermination_Completed))
              ])

            let run1: RunEntry = (

                    runName:        "Test basic operation",
                    sourceTags:     [1],
                    delay:          t * 4,
                    expected:       RxNotificationQueue<ItemType>(items: ["1-0", "1-1", "1-2"])
            )

            let run2: RunEntry = (

                    runName:        "Test no delay",
                    sourceTags:     [1],
                    delay:          0,
                    expected:       RxNotificationQueue<ItemType>(items: ["1-0", "1-1", "1-2"])
            )

            let run3: RunEntry = (

                    runName:        "Test stream empty",
                    sourceTags:     [4],
                    delay:          t * 2,
                    expected:       RxNotificationQueue<ItemType>(items: [])
            )

            let run4: RunEntry = (

                    runName:        "Test stream with error",
                    sourceTags:     [3],
                    delay:          t * 4,
                    expected:       RxNotificationQueue<ItemType>(items: ["3-0", "3-1"])
            )

            let run5: RunEntry = (

            runName:        "Test empty stream with error",
                    sourceTags:     [5],
                    delay:          t,
                    expected:       RxNotificationQueue<ItemType>(items: [])
            )

            let run6: RunEntry = (

                    runName:        "Test no delay",
                    sourceTags:     [1],
                    delay:          0,
                    expected:       RxNotificationQueue<ItemType>(items: ["1-0", "1-1", "1-2"])
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5,
                    run6
            ]
            {
                let (runName, sourceTags, delay, expected) = runSpec

                func runTest()
                {
                    let subscriptionType                    = eRxSubscriptionType.eHot
                    let source                              = timingBoard.createTimedSource(sourceTags, name: runName + "/Source", subscriptionType: subscriptionType)
                    let enumerator: TRxEnumerable<ItemType> = source.latest()
                    let resultQueue                         = RxNotificationQueue<ItemType>(tag: "result")
                    let reportFunc                          = reportGenerator(runName, expected, resultQueue)

                    usleep(useconds_t(delay * 1000000.0))

                    for item in enumerator
                    {
                        resultQueue.queueItem(item)
                    }

                    XCTAssertTrue(expected == resultQueue, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testMostRecent()
    {
        let testObservableName = "mostRecent"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, sourceTags:Set<Int>, sequenceTags:Set<Int>, expected:RxNotificationQueue<ItemType>)
            typealias TimingArrayType = RxTypes<ItemType>.TimingArrayType

            let t: RxTimeOffset = TxConfig.TestTickUnit

            let timingBoard = TxTimingBoard<ItemType>([
                      (t, 1, .eItem("1-0")),
                      (t, 7, .eItem("run")),
                      (t * 3, 1, .eItem("1-1")),
                      (t * 4, 4, .eCompleted(.eTermination_Completed)),
                      (t * 4, 5, .eCompleted(.eTermination_Error(error))),
                      (t * 4, 2, .eItem("2-0")),
                      (t * 4, 3, .eItem("3-0")),
                      (t * 5, 2, .eItem("2-1")),
                      (t * 6, 3, .eItem("3-1")),
                      (t * 7, 3, .eCompleted(.eTermination_Error(error))),
                      (t * 8, 2, .eItem("2-2")),
                      (t * 9, 2, .eCompleted(.eTermination_Completed)),
                      (t * 10, 1, .eItem("1-2")),
                      (t * 11, 1, .eCompleted(.eTermination_Completed)),
                      (t * 12, 6, .eItem("run")),
                      (t * 13, 6, .eCompleted(.eTermination_Completed)),
                      (t * 13, 7, .eCompleted(.eTermination_Completed)),
              ])

            let run1: RunEntry = (

                    runName: "Test basic operation",
                    sourceTags: [1],
                    sequenceTags: [6],
                    expected: RxNotificationQueue<ItemType>(items: ["1-2"])
            )

            let run2: RunEntry = (

                    runName: "Test default value",
                    sourceTags: [2],
                    sequenceTags: [7],
                    expected: RxNotificationQueue<ItemType>(items: ["defaultItem", "2-0", "2-1", "2-2"])
            )

            let run3: RunEntry = (

                    runName: "Test stream empty",
                    sourceTags: [4],
                    sequenceTags: [7],
                    expected: RxNotificationQueue<ItemType>(items: ["defaultItem"])
            )

            let run4: RunEntry = (

                    runName: "Test stream with error",
                    sourceTags: [3],
                    sequenceTags: [6],
                    expected: RxNotificationQueue<ItemType>(items: ["3-1"])
            )

            let run5: RunEntry = (

                    runName: "Test empty stream with error",
                    sourceTags: [5],
                    sequenceTags: [6],
                    expected: RxNotificationQueue<ItemType>(items: [])
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5
            ]
            {
                let (runName, sourceTags, sequenceTags, expected) = runSpec

                func runTest()
                {
                    let resultQueue                         = RxNotificationQueue<ItemType>(tag: "result")
                    let reportFunc                          = reportGenerator(runName, expected, resultQueue)

                    let subscriptionType                    = eRxSubscriptionType.eHot
                    let runQueue                            = RxEvalQueue(sourceTag: "run queue", evalQueueType: .eConcurrent)
                    let source                              = timingBoard.createTimedSource(sourceTags, name: runName + "/Source", subscriptionType: subscriptionType)
                    let sequencer                           = timingBoard.createTimedSource(sequenceTags, name: runName + "/Sequence", subscriptionType: subscriptionType)
                    let enumerator: TRxEnumerable<ItemType> = source.mostRecent("defaultItem")

                    let sequenceSubscription = sequencer.subscribe({ (command : String) in

                        switch command
                        {
                            case "run":

                                runQueue.dispatchAsync({

                                    for item in enumerator
                                    {
                                        resultQueue.queueItem(item)
                                    }
                                })

                            default:
                                break
                        }
                    })

                    sequenceSubscription.waitForDisposal()

                    XCTAssertTrue(expected == resultQueue, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testNext()
    {
        let testObservableName = "next"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = String
            typealias RunEntry = (runName:String, sourceTags:Set<Int>, expected:RxNotificationQueue<ItemType>)
            typealias TimingArrayType = RxTypes<ItemType>.TimingArrayType

            let t: RxTimeOffset = TxConfig.TestTickUnit

            let timingBoard = TxTimingBoard<ItemType>([
                      (t, 1, .eItem("1-0")),
                      (t * 2, 4, .eCompleted(.eTermination_Completed)),
                      (t * 2, 2, .eItem("2-0")),
                      (t * 3, 1, .eItem("1-1")),
                      (t * 4, 5, .eCompleted(.eTermination_Error(error))),
                      (t * 4, 3, .eItem("3-0")),
                      (t * 5, 2, .eItem("2-1")),
                      (t * 6, 3, .eItem("3-1")),
                      (t * 7, 3, .eCompleted(.eTermination_Error(error))),
                      (t * 8, 2, .eItem("2-2")),
                      (t * 9, 2, .eCompleted(.eTermination_Completed)),
                      (t * 10, 1, .eItem("1-2")),
                      (t * 11, 1, .eCompleted(.eTermination_Completed))
              ])

            let run1: RunEntry = (

                    runName: "Test basic operation",
                    sourceTags: [1],
                    expected: RxNotificationQueue<ItemType>(items: ["1-0", "1-1", "1-2"])
            )

            let run2: RunEntry = (

                    runName: "Test no delay",
                    sourceTags: [1],
                    expected: RxNotificationQueue<ItemType>(items: ["1-0", "1-1", "1-2"])
            )

            let run3: RunEntry = (

                    runName: "Test stream empty",
                    sourceTags: [4],
                    expected: RxNotificationQueue<ItemType>(items: [])
            )

            let run4: RunEntry = (

                    runName: "Test stream with error",
                    sourceTags: [3],
                    expected: RxNotificationQueue<ItemType>(items: ["3-0", "3-1"])
            )

            let run5: RunEntry = (

                    runName: "Test empty stream with error",
                    sourceTags: [5],
                    expected: RxNotificationQueue<ItemType>(items: [])
            )

            for runSpec in [

                    run1,
                    run2,
                    run3,
                    run4,
                    run5
            ]
            {
                let (runName, sourceTags, expected) = runSpec

                func runTest()
                {
                    let subscriptionType                    = eRxSubscriptionType.eHot
                    let source                              = timingBoard.createTimedSource(sourceTags, name: runName + "/Source", subscriptionType: subscriptionType)
                    let resultQueue                         = RxNotificationQueue<ItemType>(tag: "result")
                    let reportFunc                          = reportGenerator(runName, expected, resultQueue)
                    let enumerator: TRxEnumerable<ItemType> = source.next()

                    for item in enumerator
                    {
                        resultQueue.queueItem(item)
                    }

                    XCTAssertTrue(expected == resultQueue, reportFunc())
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}
