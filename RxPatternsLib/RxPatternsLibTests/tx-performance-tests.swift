//
// Created by Terry Stillone (http://www.originware.com) on 19/05/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import XCTest

@testable import RxPatternsSDK
@testable import RxPatternsLib

class RxPerformanceTests: TxTestCase
{
    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testSyncGenPatPerformance()
    {
        let testObservableName = "syncGenPat"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = RxIndexType
            typealias RunEntry = (runName:String, observable: RxObservable<ItemType>, expectedResult:RxNotificationQueue<ItemType>)

            let iterationCount = 50000

            let syncGenEvalOp = RxEvalOps.syncGenPat({ (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxSyncGenCommand in

                if index < iterationCount
                {
                    notifier.notifyItem(index)

                    return eRxSyncGenCommand.eNextTick
                }

                return eRxSyncGenCommand.eTerminate
            })

            let source1 = RxSource<ItemType>(tag:  testName, subscriptionType : .eCold, evalOp: syncGenEvalOp)

            let run1: RunEntry = (

            runName:  "Test sequence generation performance, no completion",
                    observable : source1,
                    expectedResult: RxNotificationQueue<ItemType>(items: [])
            )

            let run2: RunEntry = (

            runName:  "Test sequence generation with two takes performance, no completion",
                    observable : source1.take(iterationCount).take(iterationCount),
                    expectedResult: RxNotificationQueue<ItemType>(items: [])
            )

            for runSpec in [

                    run1,
                    run2
            ]
            {
                let (runName, observable, expectedResult) = runSpec

                func runTest()
                {
                    let observer = ARxObserver<ItemType>(tag:  "syncGenPat performance test observer")
                    let stats = RxSubscriptionStats()
                    let subscription = observable.setProperty(.eSubscriptionStatsEnable(stats)).subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    let buildDuration = stats.buildDuration
                    let evalDuration = stats.totalDuration

                    trace(2, "\n= run duration ==> \(testName) : \(runName): build:\(buildDuration) eval:\(evalDuration) \(evalDuration / Double(iterationCount)) <==")
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testZipPerformance()
    {
        let testObservableName = "zip"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            typealias ItemType = RxIndexType
            typealias ZipType = (item1 : ItemType, item2 : ItemType)
            typealias RunEntry = (runName:String, observable: RxSource<ZipType>, expectedResult:RxNotificationQueue<ZipType>)

            let iterationCount = 50000

            let syncGenEvalOp = RxEvalOps.syncGenPat({ (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxSyncGenCommand in

                if index < iterationCount
                {
                    notifier.notifyItem(index)

                    return eRxSyncGenCommand.eNextTick
                }

                notifier.notifyCompleted()
                return eRxSyncGenCommand.eStopTicking
            })

            let source1 : ARxProducer<ItemType> = RxSource<ItemType>(tag:  "Source1", subscriptionType : .eCold, evalOp: syncGenEvalOp)
            let source2 : ARxProducer<ItemType> = RxSource<ItemType>(tag:  "Source2", subscriptionType : .eCold, evalOp: syncGenEvalOp)
            let zipSelector = { (item1 : ItemType, item2 : ItemType) -> ZipType in

                return (item1, item2)
            }

            let run1: RunEntry = (

                    runName:  "Test empty source zip performance",
                    observable : RxSource<ZipType>.zip([RxSource<RxIndexType>.empty(), RxSource<RxIndexType>.empty()], selector : zipSelector),
                    expectedResult: RxNotificationQueue<ZipType>(items: [])
            )

            let run2: RunEntry = (

                    runName:  "Test sequence generation with zip performance",
                    observable : RxSource<ZipType>.zip([source1, source2], selector : zipSelector),
                    expectedResult: RxNotificationQueue<ZipType>(items: [])
            )

            for runSpec in [

                    run1,
                    run2
            ]
            {
                let (runName, observable, expectedResult) = runSpec

                func runTest()
                {
                    let observer = ARxObserver<ZipType>(tag:  "syncGenPat performance test observer")
                    let stats = RxSubscriptionStats()
                    let subscription = observable.setProperty(.eSubscriptionStatsEnable(stats)).subscribe(observer)

                    TxDisposalTestRigs.waitForDisposal(subscription)

                    let duration = stats.totalDuration

                    trace(2, "\n= run duration ==> \(testName) : \(runName): \(duration) \(duration / Double(iterationCount)) <==")
                    trace(2, "\n= run duration ==> \(testName) : \(runName): \(duration) \(duration / Double(iterationCount)) <==\n")
                }

                trace(2, "\n= run begin ==> \(testName) : \(runName) <==\n")
                runTest()
                trace(2, "= run end   ==> \(testName) : \(runName) <==\n")
            }
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }

    func testRxSignableBarrierPerformance()
    {
        let testObservableName = "RxSignableBarrier"
        let testName = TxFactory.createTestName(#function)

        func runAll()
        {
            let barrier = RxBarrierSignalable()
            let iterationCount = 50000

            barrier.signal()

            let startTime = RxTime()

            for _ in 0...iterationCount
            {
                barrier.wait()
            }

            let duration = -startTime.timeIntervalSinceNow

            trace(2, "\n= run duration ==> \(testName) : \(name): \(duration) \(duration / Double(iterationCount)) <==\n")
        }

        for iteration in 0..<TxConfig.IterationCount
        {
            let percent = percentage(iteration, TxConfig.IterationCount)
            let isTenPercentile = isInTenPercentile(iteration, TxConfig.IterationCount)

            trace(isTenPercentile ? 0 : 1 , "\n==> test \(testObservableName) : iteration: \(iteration)/\(TxConfig.IterationCount) (\(percent)%%)\n")
            runAll()

            RxMon.shutdown()
        }
    }
}