//
// Created by Terry Stillone on 1/03/2016.
// Copyright (c) 2016 Originware. All rights reserved.
//

import Foundation
import RxPatternsSDK

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ADxDevice: The abstract base class for devices. All devices supported by the platform should inhertit from this class.
///

public class ADxDevice : ADxObject, IDxDevice
{
    /// Reference to a device, optionally refers to an error in getting the reference.
    public enum eDeviceReference
    {
        /// The reference is invalid.
        case eDeviceError(DxError)

        /// The reference to a valid device.
        case eDevice(ADxDevice)
    }

    /// The status of the device.
    public var status :    eDeviceStatus { return m_status }

    /// Indicate whether the device is enabled.
    public var isEnabled : Bool { return m_status.isEnabled }

    /// The backing var for the status property.
    internal var m_status = eDeviceStatus.eEnabled

    /// Initialise with device URI.
    /// - Parameter objectURI: The uri of the device.
    /// - Parameter platform: Reference to the platform for logging, tracing and auditing.
    /// - Parameter status: The initial device status.
    public init(objectURI : String, platform: DxPlatform, status : eDeviceStatus = .eEnabled)
    {
        self.m_status = status

        super.init(objectURI: objectURI, platform: platform)
    }

    /// Initialise with device URI.
    /// - Parameter deviceURI: The uri of the device.
    /// - Parameter platform: Reference to the platform for logging, tracing and auditing.
    public convenience init(deviceURI : String, platform: DxPlatform)
    {
        self.init(objectURI: deviceURI, platform: platform)
    }

    /// Enable/Disable the device.
    /// - Parameter enableValue: the enable value to be set.
    public func setEnable(enableValue: Bool)
    {
        switch m_status
        {
            case .eEnabled:    if !enableValue  { m_status = eDeviceStatus.eEnabled }
            case .eDisabled:   if enableValue   { m_status = eDeviceStatus.eDisabled }
            case .eFault:      break
        }
    }
}
