//
// Created by Terry Stillone on 20/03/2016.
// Copyright (c) 2016 Originware. All rights reserved.
//

import Foundation
import RxPatternsSDK

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IDxObject: The platform base object interface.
///

public protocol IDxObject : IDxPlatformReferenceable
{
    /// The descriptive tag of the object.
    var objectURI:    String  { get }

    /// The instance ID of the object.
    var instanceID :  RxInstanceID { get }
}

public extension IDxObject
{
    /// Log an informative message.
    /// - Parameter title:  The log title.
    /// - Parameter message: The tracing message.
    @inline(__always) public func dxLog(title: String, _ message : String)
    {
        #if !DxDisableLog
                platform?.dxLog(title, message)
        #endif
    }

    /// Trace an informative message.
    /// - Parameter title:  The trace title.
    /// - Parameter message: The tracing message.
    @inline(__always) public func dxTrace(title: String, _ message : String)
    {
        #if !DxDisableTrace
                platform?.dxTrace(title, message)
        #endif
    }
}

/// The device status.
public enum eDeviceStatus
{
    /// Device enabled.
    case eEnabled

    /// Device disabled.
    case eDisabled

    /// Device has faulted.
    case eFault(DxError)

    /// Indicate whether the status indicates the device is enabled.
    public var isEnabled : Bool
    {
        switch self
        {
            case .eEnabled: return true
            default:        return false
        }
    }

    /// Indicate whether the status has an error.
    public var error : DxError?
    {
        switch self
        {
            case .eEnabled:
                return nil

            case .eDisabled:
                return nil

            case .eFault(let error):
                return error
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IDxDevice: The platform base device object interface.
///

public protocol IDxDevice : IDxObject
{
    /// The status of the device.
    var status : eDeviceStatus { get }
}

public extension IDxDevice
{
    /// The device URI (equivalent to the objectURI).
    public var deviceURI : String { return objectURI }

    /// Indicate whether the device has an error.
    public var error : DxError?  { return status.error }

    /// Indicate whether the device is enabled.
    public var isEnabled : Bool { return status.isEnabled }

    /// Perform a self test on the devices. To be overridden.
    /// - Parameter timeout: The timeout for the self test.
    /// - Parameter deviceWorkSet: Additional devices that maybe required for the self test.
    /// - Returns: The device status, determined by the self test.
    public func performSelfTest<DeviceWorkSetType: IDxDeviceWorkSet>(timeout : DxDuration, deviceWorkSet : DeviceWorkSetType) -> eDeviceStatus
    {
        return .eEnabled
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IDxDeviceBusInterconnect: Mediates between the device and the bus. The interconnect is associated with a device as given by the device URI. All interconnects should inherit from the base class.
///

public protocol IDxDeviceBusInterconnect: IDxObject
{
    /// The interconnect URI, equivalent to the object URI.
    var interconnectURI: String { get }

    /// Create a device given by the deviceURI.
    /// - Parameter deviceURI: The URI of the device to be created.
    /// - Returns: A reference to the device.
    func createDevice(deviceURI : String, deviceInterconnectURI : String) -> ADxDevice.eDeviceReference
}

public extension IDxDeviceBusInterconnect
{
    var interconnectURI: String { return objectURI }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IDxDeviceWorkSet: A workset collection of devices.
///
public protocol IDxDeviceWorkSet : IDxObject, SequenceType
{
    associatedtype GeneratorType = IDxDevice

    var devices : [IDxDevice] { get }

    func generate() -> IndexingGenerator<[IDxDevice]>
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// DxInstanceIDCounter: A sequential cardinal number generator for instance counting.
///

internal protocol IDxInstanceIDGenerator
{
    func getNextInstanceIDByTag(tag: String) -> RxInstanceID
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IDxInstanceTracker: Track DxObject instances.
///

internal protocol IDxInstanceTrackable
{
    mutating func notifyInstanceCreated(uri: String, instanceID : RxInstanceID)
    mutating func notifyInstanceDestroyed(instanceID : RxInstanceID)
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IDxPlatformReferenceable: Objects that have reference to the platform.
///

public protocol IDxPlatformReferenceable: AnyObject
{
    weak var platform : DxPlatform? { get }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// IDxPlatformFactory: The factory to create the platform and platform devices.
///

public protocol IDxPlatformFactory
{
    /// Create the platform.
    /// - Parameter shutdownAction: The action to perform on shutdown.
    /// - Returns: The platform.
    func createPlatform(shutdownAction: (() -> Void)?) -> DxPlatform.ePlatformReference

    /// Create a device given by the URI.
    /// - Parameter deviceURI: The device URI.
    /// - Parameter deviceInterconnectURI: The device interconnect URI.
    /// - Returns: A reference to the device or error.
    func createDevice(deviceURI : String, deviceInterconnectURI: String) -> ADxDevice.eDeviceReference
}
