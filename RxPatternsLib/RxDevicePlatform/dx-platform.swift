//
//  sdx-factory.swift
//  Experimental
//
//  Created by Terry Stillone on 29/12/2015.
//  Copyright © 2015 Originware. All rights reserved.
//

import Foundation
import RxPatternsSDK

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// The types for the Device Interconnect Platform
//

/// The Time type for the platform as a Double indicating the number of seconds from the reference date of the start of 2001.
public typealias DxTime = RxRelTime

/// The Time Duration for the platform in seconds.
public typealias DxDuration = RxDuration

/// The Time Interval for the platform in seconds.
public typealias DxTimeOffset = RxTimeOffset


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ADxObject: The platform abstract base object.
///

public class ADxObject : IDxObject, Hashable, Equatable
{
    /// RxInstanceCounter:  A sequential cardinal number generator for object instance IDs
    public struct InstanceIDCounter
    {
        /// The current instance counter value.
        private static var counter = RxAtomic(1)

        /// Get the next Instance ID
        static var nextInstanceID: RxInstanceID
        {
            return RxInstanceID(counter.barrierInc())
        }
    }

    /// The string reference of the object as an URI.
    public let objectURI:    String

    /// The instance ID of the object.
    public let instanceID :  RxInstanceID

    /// The platform reference for tracing, logging and auditing of the object.
    public var platform :    DxPlatform? { return m_platform }

    /// The backing var for the platform property.
    private var m_platform :  DxPlatform? = nil

    /// Initialise instanceID.
    /// - Parameter objectURI: The descriptive URI of the object.
    /// - Parameter platform: The reference to the platform.
    public init(objectURI: String, platform: DxPlatform)
    {
        let assignedInstanceID = InstanceIDCounter.nextInstanceID

        self.objectURI = objectURI
        self.instanceID = assignedInstanceID
        self.m_platform = platform

        #if !DxDisableInstanceTracking
            m_platform!.notifyInstanceCreated(objectURI, instanceID: assignedInstanceID)
        #endif
    }

    deinit
    {
        #if !DxDisableInstanceTracking
            m_platform!.notifyInstanceDestroyed(instanceID)
        #endif

        m_platform = nil
    }

    /// Hashable conformance.
    public var hashValue : Int
    {
        return objectURI.hashValue
    }
}

public func==(lhs: ADxObject, rhs: ADxObject) -> Bool
{
    return lhs.objectURI == rhs.objectURI
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// DxError: The error class for the platform. Persists a collection of errors each described by a String.
///

public class DxError: IRxError
{
    public let descriptions : [String]

    /// Indicator of whether the error instance is holding any errors.
    public final var isEmpty : Bool { return descriptions.count == 0 }

    /// Initialise with an empty set of descriptions.
    public init()
    {
        descriptions = [String]()
    }

    /// Initialise with a list of description.
    public init(descriptions : [String])
    {
        self.descriptions = descriptions
    }

    /// Initialise with a single description.
    public init(_ description : String)
    {
        self.descriptions = [description]
    }

    /// CustomDebugStringConvertible protocol conformance.
    public final var description: String
    {
        return descriptions.description
    }

    /// Compare the instance with another error.
    /// - Parameter other: The other error instance to compare with.
    @inline(__always) public func isEqual(other : IRxError) -> Bool
    {
        if let otherISError = other as? DxError
        {
            return descriptions == otherISError.descriptions
        }

        return false
    }
}

/// Operator to form the union of two errors.
public func+(lhs : DxError, rhs : DxError) -> DxError
{
    return DxError(descriptions: lhs.descriptions + rhs.descriptions)
}

/// Operator to add an error to an existing error.
public func+=(inout lhs : DxError, rhs : DxError)
{
    lhs = DxError(descriptions: lhs.descriptions + rhs.descriptions)
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// DxPlatform: The Platform interface for accessing supported devices (which have been discovered by the Bus).
///

public class DxPlatform: IDxInstanceIDGenerator, IDxInstanceTrackable
{
    public enum ePlatformReference
    {
        /// The reference is invalid.
        case ePlatformError(DxError)

        /// The reference to a valid device.
        case ePlatform(DxPlatform)
    }

    /// State tracking of the tracing, logging and auditing state.
    public struct LogState
    {
        /// Tracing enabler.
        public var enableTrace : Bool = false

        /// Logging enabler.
        public var enableLogging : Bool = true

        /// Auditing enabler.
        public var enableAudit : Bool = false

        /// The audit dictionary.
        public var audit = [String : [String]]()

        /// Previous Log Title
        private var prevLogTitle : String  = ""

        /// Log an informative message.
        /// - Parameter title: The log message title.
        /// - Parameter message: The tracing message.
        @inline(__always) mutating public func dxLog(title : String, _ message : String)
        {
            #if !DxDisableLog

                if enableLogging
                {
                    let titleLength = 30
                    let stringLength : Int = title.characters.count
                    let paddingCount = (stringLength < titleLength) ? titleLength - stringLength : 0
                    let padding = String(count : paddingCount, repeatedValue: Character(" "))

                    if prevLogTitle != title
                    {
                        print("")

                        prevLogTitle = title
                    }

                    print("log >> " + title + padding + message)
                }

            #endif
        }

        /// Trace an informative message.
        /// - Parameter title: The tracing message title.
        /// - Parameter message: The tracing message.
        @inline(__always) mutating public func dxTrace(title : String, _ message : String)
        {
            #if !DxDisableTrace

                if enableTrace
                {
                    let titleLength = 40
                    let stringLength : Int = title.characters.count
                    let paddingCount = (stringLength < titleLength) ? titleLength - stringLength : 0
                    let padding = String(count : paddingCount, repeatedValue: Character(" "))

                    print("trace >> " + title + padding + message)
                }

            #endif
        }

        /// Audit an key value pairs.
        /// - Parameter key: The audit key.
        /// - Parameter value: The audit value.
        @inline(__always) mutating public func dxAudit(key: String, _ value : String)
        {
            #if !DxDisableAudit

                if enableAudit
                {
                    if var oldValue = audit[key]
                    {
                        oldValue.append(value)

                        audit[key] = oldValue
                    }
                    else
                    {
                        audit[key] = [value]
                    }
                }

            #endif
        }
    }

    /// State tracking of the instance counter state.
    private struct InstanceCounterState
    {
        /// Instance counters by their URI.
        var counterByTag = [String: RxAtomicObject]()

        /// Get the next counter ID for the given tag.
        /// - Parameter tag: The tag asscociated with the instance type.
        /// - Returns: The next instance ID.
        @inline(__always) mutating func getNextInstanceIDByTag(tag : String) -> RxInstanceID
        {
            if let counter = counterByTag[tag]
            {
                return RxInstanceID(counter.barrierInc())
            }
            else
            {
                counterByTag[tag] = RxAtomicObject(0)

                return 0
            }
        }
    }

    /// The state for tracing, logging and auditing.
    public var logState = LogState()

    /// The state for instance counters.
    private var instanceCounterState = InstanceCounterState()

    /// The platform completion action.
    private let m_shutdownAction: (() -> Void)?

    /// Initialise with the platform completion action.
    public init(shutdownAction: (() -> Void)? = nil)
    {
        self.m_shutdownAction = shutdownAction
    }

    deinit
    {
        m_shutdownAction?()
    }

    /// Log an informative message.
    /// - Parameter title: The log message title.
    /// - Parameter message: The tracing message.
    @inline(__always) public func dxLog(title : String, _ message : String)
    {
        logState.dxLog(title, message)
    }

    /// Trace an informative message.
    /// - Parameter title: The tracing message title.
    /// - Parameter message: The tracing message.
    @inline(__always) public func dxTrace(title : String, _ message : String)
    {
        logState.dxTrace(title, message)
    }

    /// Audit an key value pairs.
    /// - Parameter key: The audit key.
    /// - Parameter value: The audit value.
    @inline(__always) public func dxAudit(key: String, _ value : String)
    {
        logState.dxAudit(key, value)
    }

    /// Get the next counter ID for the given tag.
    /// - Parameter tag: The tag asscociated with the instance type.
    /// - Returns: The next instance ID.
    @inline(__always) public func getNextInstanceIDByTag(tag : String) -> RxInstanceID
    {
        return instanceCounterState.getNextInstanceIDByTag(tag)
    }

#if !DxDisableInstanceTracking

    /// State tracking of the instance tracker.
    private class InstanceTrackingState
    {
        /// Instance Tracking.
        var objectURIByInstanceID = [RxInstanceID : String]()

        /// The queuer for synchronising internal platform operations.
        let queuer = RxEvalQueue(sourceTag: "/platform/queuer", evalQueueType: .eSerial)

        /// Mark the instance as created.
        /// - Parameter objectURI: The uri of the object being created.
        /// - Parameter instanceID: The instanceID of the created object.
        func notifyInstanceCreated(objectURI: String, instanceID : RxInstanceID)
        {
            queuer.dispatchSync({

                if self.objectURIByInstanceID[instanceID] != nil
                {
                    assert(false, "inconsistency: instance \(instanceID) already exists")
                }
                else
                {
                    self.objectURIByInstanceID[instanceID] = objectURI
                }
            })
        }

        /// Mark the instance as destroyed.
        /// - Parameter instanceID: The instanceID of the destroyed object.
        func notifyInstanceDestroyed(instanceID : RxInstanceID)
        {
            queuer.dispatchSync({

                if self.objectURIByInstanceID[instanceID] != nil
                {
                    self.objectURIByInstanceID.removeValueForKey(instanceID)
                }
                else
                {
                    assert(false, "inconsistency: instance \(instanceID) does not exist")
                }
            })
        }

        /// Indicate if there are any active instances.
        /// - Returns: An indicator of remaining active instances.
        func hasActiveInstances() -> Bool
        {
            var result = true

            queuer.dispatchSync({

                result = self.objectURIByInstanceID.count == 0
            })

            return result
        }
    }

    private var trackingState = InstanceTrackingState()

    /// Mark the instance as created.
    /// - Parameter uri: The objectURI of the object being created.
    /// - Parameter instanceID: The instanceID of the created object.
    @inline(__always) public func notifyInstanceCreated(objectURI: String, instanceID : RxInstanceID)
    {
        trackingState.notifyInstanceCreated(objectURI, instanceID: instanceID)
    }

    /// Mark the instance as destroyed.
    /// - Parameter instanceID: The instanceID of the destroyed object.
    @inline(__always) public func notifyInstanceDestroyed(instanceID : RxInstanceID)
    {
        trackingState.notifyInstanceDestroyed(instanceID)
    }

    /// Indicate if there are any active instances.
    /// - Returns: An indicator of remaining active instances.
    @inline(__always) public func hasActiveInstances() -> Bool
    {
        return trackingState.hasActiveInstances()
    }

#endif
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// DxReplyQueue: The reply communication path from interconnect to the paired device. Manages the replies by queueing if the device is not ready for the reply.
///

public final class DxReplyQueue<DataType> : ADxObject
{
    public typealias ReplyActionType = (DataType) -> Bool
    public typealias CompletedActionType = (IRxError?) -> Void

    /// The queue of reply data items.
    private let m_dataItemQueue:  RxNotificationQueue<DataType>

    /// The collection of subscriptions.
    private var m_subscriptions:  Array<DxReplySubscription<DataType>>

    /// The eval queue for synchronisation of operations.
    private let m_replyEvalQueue: RxEvalQueue

    /// Initialise by tag and platform.
    /// - Parameter objectURI: A URI associated with the Obkect.
    /// - Parameter platform: The platform reference for looging and tracing.
    public override init(objectURI: String, platform: DxPlatform)
    {
        m_dataItemQueue = RxNotificationQueue<DataType>(tag: objectURI + "/queue")
        m_subscriptions = Array<DxReplySubscription<DataType>>()
        m_replyEvalQueue = RxEvalQueue(sourceTag: objectURI + "/evalQueue", evalQueueType: .eSerial)

        super.init(objectURI: objectURI, platform: platform)
    }

    /// Queue a reply data item.
    /// - Parameter replyItem: The item to queue.
    public func queue(replyItem: DataType)
    {
        m_replyEvalQueue.dispatchAsync({ [unowned self] in

            // Process pending data items
            while self.m_dataItemQueue.itemCount > 0
            {
                let pendingtItem = self.m_dataItemQueue.unqueueItem()

                if !self.processReply(pendingtItem!)
                {
                    self.m_dataItemQueue.queueItem(replyItem)

                    return
                }
            }

            // Process the given data item.
            if !self.processReply(replyItem)
            {
                self.m_dataItemQueue.queueItem(replyItem)
            }
        })
    }

    /// Queue an interconnect completed notification.
    /// - Parameter error: The optional error associated with the completion.
    public func queueCompleted(error : IRxError?)
    {
        m_dataItemQueue.queueCompleted(error)
    }

    /// Register a subscription action and completed action to the reply queue.
    /// - Parameter action: A subscription action of for replies..
    /// - Parameter completedAction: A completed action for replies.
    /// - Returns: The Reply Subscription.
    public func subscribe(completedAction : CompletedActionType? = nil, barrier : RxBarrierSignalable? = nil, action : ReplyActionType) -> DxReplySubscription<DataType>
    {
        let subscription = DxReplySubscription<DataType>(replyAction: action, completedAction: completedAction, barrier: barrier)

        m_replyEvalQueue.dispatchSync({ [unowned self] in

            self.m_subscriptions.append(subscription)
        })

        return subscription
    }

    /// Process a data item reply.
    /// - Parameter replyItem: A reply data item.
    /// - Return: An indicator of whether there are remaining actions for handling the replies.
    @inline(__always) private func processReply(replyItem: DataType) -> Bool
    {
        var result = false
        var removalIndexes = [Int]()

        for index in 0..<self.m_subscriptions.count
        {
            let subscription = self.m_subscriptions[index]

            if !subscription.replyAction(replyItem)
            {
                subscription.completedAction?(nil)

                removalIndexes.append(index)

                subscription.signalCompleted()
            }
            else
            {
                result = true
            }
        }

        for index in removalIndexes
        {
            self.m_subscriptions.removeAtIndex(index)
        }

        return result
    }

    /// Clear state.
    @inline(__always) public func clear()
    {
        m_dataItemQueue.removeAll()
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// DxReplySubscription: The subscription class for DxReplyQueue subscriptions.
///

public final class DxReplySubscription<DataType>
{
    public typealias ReplyActionType = (DataType) -> Bool
    public typealias CompletedActionType = (IRxError?) -> Void

    /// The subscription reply action.
    public var  replyAction :               ReplyActionType

    /// The subscription completed action.
    public var  completedAction :           CompletedActionType?

    /// The waitForCompletion barrier.
    private var m_waitForCompletionBarrier: RxBarrierSignalable? = nil

    /// The NOP subscription for NOP operation.
    public static var nopSubscription : DxReplySubscription<DataType>  {

        return DxReplySubscription<DataType>(replyAction: {(_) in return false })
    }

    /// Initialise by reply action, completed action and barrier.
    /// - Parameter replyAction: The subscription reply action.
    /// - Parameter completedAction: The subscription completed action.
    /// - Parameter barrier: The wait for completion blocking barrier, used in the waitForCompletion function.
    public init(replyAction: ReplyActionType, completedAction: CompletedActionType? = nil, barrier : RxBarrierSignalable? = nil)
    {
        self.replyAction = replyAction
        self.completedAction = completedAction
        self.m_waitForCompletionBarrier = barrier
    }

    /// Signal that the subscription is completed.
    @inline(__always) public func signalCompleted()
    {
        m_waitForCompletionBarrier?.signal()
    }

    /// Wait (block) until subscription completion.
    @inline(__always) public func waitForCompletion()
    {
        assert(m_waitForCompletionBarrier != nil, "Expected barrier to be valid")

        m_waitForCompletionBarrier?.wait()
    }

    /// Clear delegates.
    @inline(__always) public func clearDelegates()
    {
        replyAction = { (_) in return false }
        completedAction = nil
    }
}

