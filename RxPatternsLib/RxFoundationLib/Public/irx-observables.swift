///
///Created by Terry Stillone on 30/04/15.
///Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///   RxObservableSync:
///
/// - runSync:               Perform expression evaluation synchronously, with variants.
///

public protocol RxObservableSync
{
    associatedtype ItemOutType

    func runSync()
    func runSync(consumer: ARxConsumer<ItemOutType>)
    func runSync(itemAction: RxTypes<ItemOutType>.RxItemActionDelegate, completedAction : RxTypes<ItemOutType>.RxCompletedActionDelegate?)
    func runSync<TargetItemType>(itemNotifier : RxTypes2<ItemOutType, TargetItemType>.RxItemNotifierAction, completedNotifier : RxTypes2<ItemOutType, TargetItemType>.RxCompletedNotifierAction?)
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///   RxMSObservables_Primitive:
///
/// - raise:               An Observable that immediately throws a given error.
///

public protocol RxMSObservables_Primitive
{
    associatedtype ItemOutType
    associatedtype ItemInType

    func throwError(error: IRxError) -> RxObservableMap<ItemInType, ItemOutType>
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///   RxMSObservables_Boolean:
///
/// - all:                  Determine whether all items emitted by the Observable meet the given predicate.
/// - contains:             Determine whether the Observable emits a particular item.
/// - any:                  Determine whether the Observable emits any items.
/// - isEmpty:              Determine whether the Observable emits no items.
/// - sequenceEqual:        Determine whether two Observables emit the same sequence of items.

public protocol RxMSObservables_Boolean
{
    associatedtype ItemOutType

    associatedtype BoolItemType = Bool
    associatedtype RxObservableType = RxObservable<ItemOutType>
    associatedtype RxBoolObservableType = RxObservableMap<ItemOutType, Bool>

    func all(predicate : RxTypes<ItemOutType>.RxPredicate)                                                  -> RxBoolObservableType
    func contains(containsItem: ItemOutType, equateOp: (lhs : ItemOutType, rhs : ItemOutType) -> Bool)      -> RxBoolObservableType
    // See Equatable extensions for:   func contains(containsItem: ItemType)                                -> RxBoolObservableType
    func any()                                                                                              -> RxBoolObservableType
    func isEmpty()                                                                                          -> RxBoolObservableType

    func sequenceEqual(other : ARxProducer<ItemOutType>, equateOp: (lhs : ItemOutType, rhs : ItemOutType) -> Bool)  -> RxBoolObservableType
    // See Equatable extensions for:   func sequenceEqual(other : ARxProducer<ItemType>)                   -> RxBoolObservableType
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///   RxMSObservables_Filter:
///
/// - distinct:               Suppress all duplicate item notifications.
/// - distinctUntilChanged:   Only emit items that differ from their previous item.
/// - ignoreElements:         Do not emit any items, only pass over termination notifications.
/// - skip:                   Discard the first given count item notifications.
/// - skipLast:               Discard the last given count item notifications.
/// - skipWhile:              Discard item notifications until a given predicate becomes false
/// - skipUntil:              Discard item notifications until a second Observable emits an item.
/// - take:                   Emit only the first given count item notifications.
/// - takeLast:               Emit only the final given count item notifications.
/// - takeLastBuffer:         Emit the last given item notifications, as a single list item.
/// - takeWhile:              Emit item notifications until a given predicate becomes false.
/// - takeUntil:              Emit item notifications until a second Observable emits an item
/// - elementAt:              Emit only the item notifications of the given index.
/// - elementAtOrDefault:     Emit only the item notifications of the given index, if none exist then emit the given default.
/// - whereByPredicate:       Emit only those item notifications that pass a given predicate.
/// - defaultIfEmpty:         Emit item notifications, if none exist then emit the given default item.
///

public protocol RxMSObservables_Filter
{
    associatedtype ItemOutType

    associatedtype RxObservableType = RxObservable<ItemOutType>

    func distinct(hashOp : RxTypes<ItemOutType>.RxHashOp)                       ->RxObservableType
    // See Hashable extensions for:  func distinct()
    func distinctUntilChanged(equateOp: RxTypes<ItemOutType>.RxEquateOp)        ->RxObservableType
    // See Equatable extensions for:  func distinctUntilChanged()

    func ignoreElements()                                                       -> RxObservableType

    func skip(count : RxCountType)                                              -> RxObservableType
    func skipLast(count : RxCountType)                                          -> RxObservableType
    func skipWhile(predicate : RxTypes<ItemOutType>.RxPredicate)                -> RxObservableType
    func skipUntil<OtherOutType>(stream : ARxProducer<OtherOutType>)            -> RxObservableType

    func take(count : RxCountType)                                              -> RxObservableType
    func takeLast(count : RxCountType)                                          -> RxObservableType
    func takeLastBuffer(count : RxCountType)                                    -> RxObservableMap<ItemOutType, [ItemOutType]>
    func takeWhile(predicate : RxTypes<ItemOutType>.RxPredicate)                -> RxObservableType
    func takeUntil<OtherItemType>(stream : ARxProducer<OtherItemType>)          -> RxObservable<ItemOutType>

    func elementAt(index : Int)                                                 -> RxObservableType
    func elementAtOrDefault(index : Int, defaultItem : ItemOutType)             -> RxObservableType

    func whereByPredicate(predicate : RxTypes<ItemOutType>.RxPredicate)         -> RxObservableType

    func defaultIfEmpty(defaultItem : ItemOutType)                              -> RxObservableType
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///   RxMSObservables_Blocking:
///
/// - first:                    Emit only the first item notification or nil on error.
/// - firstThrowable            Emit only the first item notification, throw errors.
/// - last:                     Emit only the last item notification, or nil on error.
/// - lastThrowable:            Emit only the last item notification, throw errors.
/// - firstOrDefault:           Emit only the first item, if none exists then emit the given default item.
/// - firstOrDefaultThrowable:  Emit only the first item, if none exists then emit the given default item. Throw errors.
/// - lastOrDefault:            Emit only the last item emitted, if none exists then emit the given default item.
/// - lastOrDefaultThrowable:   Emit only the last item emitted, if none exists then emit the given default item. Throw errors.
/// - single:                   Emit the first item, if there are more then emit an error.
/// - singleThrowable:          Emit the first item, if there are more then emit an error. Throw errors.
/// - singleOrDefault:          Emit the first item, if there are more then emit and error, if none exists emit the given default.
/// - singleOrDefaultThrowable  Emit the first item, if there are more then emit and error, if none exists emit the given default. Throw errors.
///
/// - latest:                   Return an enumerable that blocks until the Observable emits another item, then returns that item.
/// - mostRecent:               Return an enumerable that emits the most recently received item. If there has been no item notifications, emit the given default item.
/// - next:                     Return an enumerable that blocks until the Observable emits another item, then returns that item. If items are received faster than the enumeration, then they are discarded.
///

public protocol RxMSObservables_Blocking
{
    associatedtype ItemOutType

    associatedtype RxEnumerableType = TRxEnumerable<ItemOutType>

    func first()                                                                -> ItemOutType?
    func firstThrowable() throws                                                -> ItemOutType
    func last(ignoreErrors : Bool)                                              -> ItemOutType?
    func lastThrowable() throws                                                 -> ItemOutType
    func firstOrDefault(defaultItem : ItemOutType, ignoreErrors : Bool)         -> ItemOutType
    func firstOrDefaultThrowable(defaultItem : ItemOutType) throws              -> ItemOutType
    func lastOrDefault(defaultItem : ItemOutType, ignoreErrors : Bool)          -> ItemOutType
    func lastOrDefaultThrowable(defaultItem : ItemOutType) throws               -> ItemOutType
    func single()                                                               -> ItemOutType?
    func singleThrowable() throws                                               -> ItemOutType
    func singleOrDefault(defaultItem : ItemOutType)                             -> ItemOutType
    func singleOrDefaultThrowable(defaultItem : ItemOutType) throws             -> ItemOutType

    func toEnumerable()                                                         -> RxEnumerableType
    func latest()                                                               -> RxEnumerableType
    func next()                                                                 -> RxEnumerableType
    func mostRecent(defaultItem : ItemOutType)                                  -> RxEnumerableType
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///   RxMSObservables_EventHooks:
///
/// - do - doOnItem:          Perform a given item or completed action for each item notification.
/// - do - doOnNotify:        Notify a given notifier of received notifications.
/// - finally:                Perform an action upon the completion or error notification.

public protocol RxMSObservables_EventHooks
{
    associatedtype ItemOutType

    associatedtype RxObservableType = RxObservable<ItemOutType>

    func doOnNotify(onItem: RxTypes<ItemOutType>.RxItemActionDelegate?, onCompleted: RxTypes<ItemOutType>.RxCompletedActionDelegate?) -> RxObservableType
    func doOnNotify(notifier : ARxNotifier<ItemOutType>)                        -> RxObservableType
    func finally(action : RxAction)                                             -> RxObservableType
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///   RxMSObservables_Transform:
///
/// - select :               Given an item mapping function, emit the mapped items.
/// - scan:                  Given a aggregation function, emit items that are the results of the aggregation function.

public protocol RxMSObservables_Transform
{
    associatedtype ItemOutType

    func select<TargetItemType>(mapAction : RxTypes2<ItemOutType, TargetItemType>.RxItemMapOptionalFunc) -> RxObservableMap<ItemOutType, TargetItemType>
    //func selectMany(evalAction : RxEvalAction)                                                -> RxObservable<ItemOutType>

    func scan(startItem: ItemOutType, accumulator : RxTypes<ItemOutType>.RxItemMapFunc)         -> RxObservable<ItemOutType>
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///   RxMSObservables_Combine:
///
/// - combineLatest:          Given another stream and selector func, combine the latest items and emit an item based on the selector function.
/// - merge:                  Given another stream combine the streams by emitting the items as they are notified.
/// - concat:                 Given another stream concatenate the items by emitting all items of the first to emit and then the other.
/// - zip:                    Given another stream and a selector function, emit the results of the selector on notified items in sequence.

public protocol RxMSObservables_Combine
{
    associatedtype ItemOutType

    func merge(stream: ARxProducer<ItemOutType>)    -> RxObservable<ItemOutType>
    func concat(stream: ARxProducer<ItemOutType>)   -> RxObservable<ItemOutType>

    func combineLatest<TargetItemType>(stream : ARxProducer<ItemOutType>, selector: (ItemOutType, ItemOutType) -> TargetItemType?)  -> RxObservableMap<ItemOutType, TargetItemType>
    func zip<TargetItemType>(stream : ARxProducer<ItemOutType>, selector : (ItemOutType, ItemOutType) -> TargetItemType?)           -> RxObservableMap<ItemOutType, TargetItemType>
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///   RxMSObservables_Timing:
///
/// - buffer:         Periodically gather item notifications into bundles and emit these bundles rather than emitting the items one at a time.
/// - delay:          Shift item notifications forward in time by an time offset or specific time.
/// - sample:         Emit the most recent item notifications within periodic time intervals.
/// - timeout:        Emit item notifications, but issue an error if none are received after a given elapsed time period.
/// - timeout with source: Emit item notifications, if none are received after a given elapsed time period, substitute the given sequence.
/// - throttle:       Emit all item notifications after a particular timespan has passed without receiving a notification.
/// - window:         Periodically subdivide item notifications into Observable windows and emit those windows rather than emitting the items one at a time.
///

public protocol RxMSObservables_Timing
{
    associatedtype ItemOutType

    associatedtype RxObservableType = RxObservable<ItemOutType>
    associatedtype RxObservableListType = RxObservable<[ItemOutType]>
    associatedtype RxObservableOfRxObservableType = RxObservable<RxObservable<ItemOutType>>

    func buffer(count : RxCountType, skip : RxCountType)                        -> RxObservableListType
    func buffer(duration : RxDuration)                                          -> RxObservableListType
    func buffer(count : RxCountType, duration : RxDuration)                     -> RxObservableListType
    func buffer(duration : RxDuration, timeShift : RxTimeOffset)                -> RxObservableListType

    func delay(duration : RxDuration)                                           -> RxObservableType
    func delayTo(time : RxTime)                                                 -> RxObservableType

    func sample(duration : RxDuration)                                          -> RxObservableType

    func timeout(timeout : RxTimeOffset)                                        -> RxObservableType
    func timeout(timeout : RxTimeOffset, other : ARxProducer<ItemOutType>)      -> RxObservableType

    func throttle(dueTime : RxTimeOffset)                                       -> RxObservableType

    func window(count : RxCountType, skip : RxCountType)                        -> RxObservableOfRxObservableType
    func window(duration : RxDuration)                                          -> RxObservableOfRxObservableType
    func window(count : RxCountType, duration : RxDuration)                     -> RxObservableOfRxObservableType
    func window(duration : RxDuration, timeShift : RxTimeOffset)                -> RxObservableOfRxObservableType
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///   RxMSObservables_Generation:
///
/// - startWith:            Emit a given sequence of items before beginning to emit received item notifications.
/// - repeat:               On completion, repeatedly emit all the received item notifications forever.
/// - repeatWithCount:      On completion, Repeatedly emit the received item notifications for count times.

public protocol RxMSObservables_Generation
{
    associatedtype ItemOutType
    associatedtype RxObservableType = RxObservable<ItemOutType>

    func startWith(items: [ItemOutType])                                        -> RxObservableType
    func repeatForever()                                                        -> RxObservableType
    func repeatWithCount(count : RxCountType)                                   -> RxObservableType
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///   RxMSObservables_Group:
///
/// - groupBy:            Given a partition function, convert an Observable into a set of Observables which each emits items chosen with the same partition mapping result.
///

public protocol RxMSObservables_Group
{
    associatedtype ItemOutType
    associatedtype RxTargetObservableType = RxObservable<RxObservable<ItemOutType>>

    func groupBy<GroupType:Hashable, ObservableType>(groupSelector: RxTypes2<ItemOutType, GroupType>.RxMapItemAction, mapItemToObservableType: RxTypes2<ItemOutType, ObservableType>.RxMapItemAction) -> RxObservableMap<ItemOutType, RxObservable<ObservableType>>

    func groupBy<GroupType:Hashable>(groupSelector: RxTypes2<ItemOutType, GroupType>.RxMapItemAction) -> RxObservableMap<ItemOutType, RxObservable<ItemOutType>>


    func groupByUntil<GroupType:Hashable, DurationItemType>(groupSelector: RxTypes2<ItemOutType, GroupType>.RxMapItemAction, durationObservable: RxObservable<DurationItemType>) -> RxObservableMap<ItemOutType, RxSource<ItemOutType>>

    func groupByUntil<GroupType:Hashable, ObservableType, DurationItemType>(groupSelector: RxTypes2<ItemOutType, GroupType>.RxMapItemAction, mapItemToObservableType: RxTypes2<ItemOutType, ObservableType>.RxMapItemAction, durationObservable: RxObservable<DurationItemType>) -> RxObservableMap<ItemOutType, RxSource<ObservableType>>
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///   RxMSObservables_ErrorHandling:
///
/// - retry:              If an error notification is issued, resubscribe in the hope that it will complete without error a second time.
/// - retry (count):      If an error notification is issued, resubscribe up to a maximium of count times.
/// - catch:              Recover from an error condition by ignoring error notifications.
/// - onErrorResumeNext:  If an error notification is encountered, switch over to the given stream.
/// - onErrorReturn:      If an error notification is encountered, emit the given item and a completed notification.
///

public protocol RxMSObservables_ErrorHandling
{
    associatedtype ItemOutType

    associatedtype RxObservableType = RxObservable<ItemOutType>

    func retry()                                                                -> RxObservableType
    func retry(count : RxCountType)                                             -> RxObservableType
    func catchError(stream : ARxProducer<ItemOutType>)                          -> RxObservableType
    func onErrorResumeNext(streams : [ARxProducer<ItemOutType>])                -> RxObservableType
    func onErrorResumeNext(streams : ARxProducer<RxSource<ItemOutType>>)        -> RxObservableType
    func onErrorReturn(item : ItemOutType)                                      -> RxObservableType
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///   RxMSObservables_Connectable:
///
/// - replay:             Create an observable that always replays all notifications to subscriptions.
/// - publish:            Create an observable that will act as a hot source, ensuring all its subscriptions, will only observe current notifications.
///

public protocol RxMSObservables_Connectable
{
    associatedtype ItemOutType

    associatedtype RxObservableType = RxObservable<ItemOutType>

    func replay(inout replayNotifications : RxNotificationConstrainedSequence<ItemOutType>)     -> RxObservableType
    func publish()                                                                              -> RxObservableType
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///   RxMSObservables_Aggregation:
///
/// - count:              Emit the count the number of item notifications received and then complete.
/// - sum:                Emit the calculation the sum of number based item notifications received and then terminate.
/// - average:            Emit the calculation the average of number based item notifications received and then terminate.
/// - min:                Emit the calculation the minimum of number based item notifications received and then terminate.
/// - max:                Emit the calculation the maximum of number based item notifications received and then terminate.
/// - aggregate:          Apply a function to each number based item notification received, then emit the final value and terminate.
///

public protocol RxMSObservables_Aggregation
{
    associatedtype ItemOutType

    associatedtype RxObservableType = RxObservable<ItemOutType>

    func count()                                                                            -> RxObservableMap<ItemOutType, RxCountType>
    func aggregate(startItem: ItemOutType, accumulator: RxTypes<ItemOutType>.RxItemMapFunc) -> RxObservableType

    func min(compareOp: RxTypes<ItemOutType>.RxCompareOp)                                   -> RxObservableType
    // See Comparable extension for:    func min()                                          -> RxObservableType
    func max(compareOp: RxTypes<ItemOutType>.RxCompareOp)                                   -> RxObservableType
    // See Comparable extension for:    func max()                                          -> RxObservableType
    func sum(summer : (item : ItemOutType?) -> ItemOutType)                                 -> RxObservableType
    func average(averager : (item : ItemOutType?) -> ItemOutType?)                          -> RxObservableType
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///   RxMSObservables_Utility:
///
/// - using:              Given a disposable factory, create a disposable that has the same lifespan as the Observable.
/// - timeInterval:       Convert item notifications into one that emits the time elapsed between the previous emission.
/// - timestamp:          Emit a timestamp of item notifications received together with a string form of the item itself.
/// - removeTimestamp:    Remove timestamps on item notifications and emit the string form of the item extracted from the notification.
/// - dump:               Print to console a string form of the notifications, together with the given title. All Notifications are passed on.
/// - dumpUsingAction:    Pass received item notifications to a given dump function. All Notifications are passed on.
/// - materialize:        Emit the conversion of item notifications to formal RxNotification representation of the item.
/// - dematerialize:      Unwrap item notifications in RxNotification form to their native item form.
///

public protocol RxMSObservables_Utility
{
    associatedtype ItemOutType

    associatedtype RxObservableType = RxObservable<ItemOutType>

    func using(disposableFactory : () -> RxDisposable)                              -> RxObservableType

    func timeInterval()                                                             -> RxObservableMap<ItemOutType, String>
    func timestamp()                                                                -> RxObservableMap<ItemOutType, String>
    func removeTimestamp<TargetItemType>(mapItemFunc : (String) -> TargetItemType)  -> RxObservableMap<ItemOutType, TargetItemType>

    func dump(title : String)                                                       -> RxObservableType
    func dumpUsingAction(dumpAction : RxTypes<ItemOutType>.RxDumpAction)            -> RxObservableType

    func materialize()                                                              -> RxObservableMap<ItemOutType, RxNotification<ItemOutType>>
    func dematerialize<DataItemType>()                                              -> RxObservableMap<ItemOutType, DataItemType>
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///   RxNetflixObservables:
///
/// - map :               Equivalent to MS select: Given a mapping function, emit items that are the result of the mapping of the notified items
/// - filter:             Equivalent to MS where: Given a predicate function, only emit those items that pass the predicate.
/// - debounce:           Equivalent to MS throttle: Emit all notifications after a particular timespan has passed without receiving a notification.
///

public protocol RxNetflixObservables
{
    associatedtype ItemOutType

    associatedtype RxObservableType = RxObservable<ItemOutType>

    func map<TargetItemType>(mapAction : RxTypes2<ItemOutType, TargetItemType>.RxItemMapOptionalFunc)    -> RxObservableMap<ItemOutType, TargetItemType>
    func filter(predicate : RxTypes<ItemOutType>.RxPredicate)                                            -> RxObservableType
    func debounce(dueTime : RxDuration)                                                                  -> RxObservableType
}
