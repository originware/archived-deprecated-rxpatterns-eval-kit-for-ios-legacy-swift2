//
//  irx-types.swift
//  RxPatternsLib
//
//  Created by Terry Stillone on 9/09/2015.
//  Copyright © 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK

extension RxTypes
{
    /// Index to Item mapping func.
    public typealias RxIndexTypeMap = (RxIndexType) -> ItemType
    
    /// Item predicate func.
    public typealias RxPredicate = (ItemType) -> Bool
    
    /// Item to Item mapping func.
    public typealias RxItemMapFunc = (ItemType, ItemType) -> ItemType

    /// Item Equate func.
    public typealias RxEquateOp = (lhs : ItemType, rhs : ItemType) -> Bool

    /// Item Compare func.
    public typealias RxCompareOp = (lhs : ItemType, rhs : ItemType) -> Bool
    
    /// Item Hash func.
    public typealias RxHashOp = (item: ItemType) -> Int

    /// Item dump func.
    public typealias RxDumpAction = (title : String, description: String, item : ItemType?) -> Void
}

extension RxTypes2
{
    /// Item to optional Item mapping func.
    public typealias RxItemMapOptionalFunc = (ItemInType) -> ItemOutType?
}