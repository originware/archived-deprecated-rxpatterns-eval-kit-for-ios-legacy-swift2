//
// Created by Terry Stillone (http://www.originware.com) on 8/04/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxSubject: The classic RxSubject class. Performs as both an Observer and Observable (of the same generic ItemType).
///
/// - Useful for unit testing expressions.
/// - The notify methods are synchronous and return only after the notification has been delivered.
///

public class RxSubject<ItemType> : RxSource<ItemType>, IRxSubject
{
    public typealias ObserverItemType = ItemType

    /// Indicates that there are active subscriptions.
    public var isActive : Bool          { return outNotifier != nil }

    /// The notifier to emit from the subject.
    public var outNotifier: ARxNotifier<ItemType>? = nil

    /// The barrier to delay any notifications being received, to accommodate pending subscriptions.
    private let m_pendingSubscriptionBarrier = RxBarrier()

    /// The RxObject type for this class.
    public override var objectType : eRxObjectType               { return .eRxObject_RxSubject }

    /// Initialise with tag.
    /// - Parameter tag: The RxObject tag for the class.
    /// - Parameter subscriptionType: The subscription type (Hot/Cold).
    public init(tag: String, subscriptionType : eRxSubscriptionType = .eHot)
    {
        // Note: Takes the evalOp from the createEvalOpDelegate() func.
        super.init(tag: tag, subscriptionType: subscriptionType, evalOp: nil)
    }

    /// Notify reception of Item.
    /// - Parameter item: The item being notified.
    public func notifyItem(item: ItemType)
    {
        m_pendingSubscriptionBarrier.wait()

        if isActive
        {
            #if RxMonEnabled
                RxMon.monObservable(.eObservable_notifyItem(self, item))
            #endif

            // Emit the given item notification.
            self.outNotifier!.notifyItem(item)
        }
    }

    /// Notify reception of Completed/Error.
    /// - Parameter error: The error or nil if completed.
    public func notifyCompleted(error : IRxError? = nil)
    {
        // Wait for any pending subscriptions.
        m_pendingSubscriptionBarrier.wait()

        if isActive
        {
            #if RxMonEnabled
                RxMon.monObservable(.eObservable_notifyCompleted(self, error))
            #endif

            // Emit the given completion notification.
            self.outNotifier!.notifyCompleted(error)
        }
    }

    /// Notify reception of Eval State Change.
    /// - Parameter stateChange: The new eval state.
    public func notifyStateChange(stateChange : eRxEvalStateChange)
    {
        // Wait for any pending subscriptions.
        m_pendingSubscriptionBarrier.wait()

        if isActive
        {
            #if RxMonEnabled
                RxMon.monObservable(.eObservable_notifyStateChange(self, stateChange))
            #endif

            // Emit the given state change notification.
            self.outNotifier!.notifyStateChange(stateChange)
        }
    }

    /// Create the RxEvalNode eval op as its self dependencies constrain it from being defined in the init.
    public override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode : RxEvalNode<ItemType, ItemType>) in

            evalNode.stateChangeDelegate = { (stateChange : eRxEvalStateChange, notifier : ARxNotifier<ItemType>) in

                switch self.subscriptionType
                {
                    case .eHot:
                        self.hotSubjectStateChangeHandler(evalNode, stateChange:stateChange, notifier:notifier)

                    case .eCold:
                        self.coldSubjectStateChangeHandler(evalNode, stateChange:stateChange, notifier:notifier)
                }
            }
        }
    }

    /// The eval state change handler made access able to classes that inherit from RxSubject.
    public func hotSubjectStateChangeHandler(evalNode: RxEvalNode<ItemType, ItemType>, stateChange : eRxEvalStateChange, notifier : ARxNotifier<ItemType>)
    {
        switch stateChange
        {
            case eRxEvalStateChange.eNewSubscriptionInSubscriptionThread:

                // Increment subscription barrier pending count
                self.m_pendingSubscriptionBarrier.enterBlock()

            case eRxEvalStateChange.eSubscriptionBegin:

                if self.outNotifier == nil
                {
                    self.outNotifier = evalNode.syncOutNotifier
                }

                if evalNode.subscriptionCount > 1
                {
                    // Decrement subscription barrier pending count.
                    self.m_pendingSubscriptionBarrier.exitBlock()
                }

            case eRxEvalStateChange.eEvalBegin:

                // Decrement subscription barrier pending count.
                self.m_pendingSubscriptionBarrier.exitBlock()

            case eRxEvalStateChange.eEvalEnd:

                self.outNotifier = nil

            default:
                break
        }
    }

    /// The eval state change handler made access able to classes that inherit from RxSubject.
    public func coldSubjectStateChangeHandler(evalNode: RxEvalNode<ItemType, ItemType>, stateChange : eRxEvalStateChange, notifier : ARxNotifier<ItemType>)
    {
        switch stateChange
        {
            case eRxEvalStateChange.eNewSubscriptionInSubscriptionThread:

                // Increment subscription barrier pending count
                self.m_pendingSubscriptionBarrier.enterBlock()

            case eRxEvalStateChange.eSubscriptionBegin:

                // As a cold subscription will create a separate Eval Engine per subscription, the evalNode.syncOutNotifier will be
                //   different for each subscription we must append each notifier in order to emit to each subscription eval node.
                if let outNotifier = self.outNotifier as? RxNotifier_NotifyConsumers
                {
                    outNotifier.appendConsumer(evalNode.syncOutNotifier)
                }
                else
                {
                    let outNotifier = RxNotifier_NotifyConsumers<ItemType>(tag: self.tag + "/notifier")

                    outNotifier.appendConsumer(evalNode.syncOutNotifier)

                    self.outNotifier = outNotifier
                }

            case eRxEvalStateChange.eEvalBegin:

                // Decrement subscription barrier pending count.
                self.m_pendingSubscriptionBarrier.exitBlock()

            case eRxEvalStateChange.eEvalEnd:

                // On the last subscription, nil out the reference to the RxEvalNode emit notifier.
                if (self.outNotifier != nil) && (self.outNotifier!.count == 0)
                {
                    self.outNotifier = nil
                }

            case eRxEvalStateChange.eSubscriptionEnd:

                // Remove notifier:
                if let outNotifier = self.outNotifier as? RxNotifier_NotifyConsumers
                {
                    outNotifier.removeConsumer(evalNode.syncOutNotifier)
                }
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ReplaySubject: Replay saved notifications to any subsequent subscriptions.
///
/// - [Intro Doc Link]( http://www.introtorx.com/uat/content/v1.0.10621.0/02_KeyTypes.html#ReplaySubject )
/// - [Microsoft Doc Link]( https://msdn.microsoft.com/en-us/library/hh211810(v=vs.103).aspx )
/// - [Netflix Doc Link]( https://github.com/ReactiveX/RxJava/wiki/Subject )
///

public class RxReplaySubject<DataItemType> : RxSubject<DataItemType>
{
    /// The notifications to be replayed.
    public let replayNotifications: RxNotificationConstrainedSequence<DataItemType>
    private var m_queuer : RxEvalQueue? = nil

    /// Initialise with tag.
    /// - Parameter tag: The RxObject tag for the class.
    public init(tag: String)
    {
        self.replayNotifications = RxNotificationConstrainedSequence<DataItemType>(tag: tag + "/replayNotifications")

        super.init(tag: tag)
    }

    /// Initialise with tag and max count of items.
    /// - Parameter tag: The RxObject tag for the class.
    /// - Parameter maxItemCount: The maximum number of items to replay.
    public convenience init(tag: String, maxItemCount: RxCountType)
    {
        self.init(tag: tag)

        self.replayNotifications.maxItemCount = maxItemCount
    }

    /// Initialise with tag and duration of items.
    /// - Parameter tag: The RxObject tag for the class.
    /// - Parameter duration: The maximum duration back in time of notifications to replay.
    public convenience init(tag: String, duration: RxDuration)
    {
        self.init(tag: tag)

        self.replayNotifications.maxItemTimeSpan = duration
    }

    /// Initialise with tag, max count of items and duration of items.
    /// - Parameter tag: The RxObject tag for the class.
    /// - Parameter maxItemCount: The maximum number of items to replay.
    /// - Parameter duration: The maximum duration back in time of notifications to replay.
    public convenience init(tag: String, maxItemCount: RxCountType, duration: RxDuration)
    {
        self.init(tag: tag)

        self.replayNotifications.maxItemTimeSpan = duration
        self.replayNotifications.maxItemCount = maxItemCount
    }

    /// Notify reception of Item
    /// - Parameter item: The item being notified.
    public override func notifyItem(item: DataItemType)
    {
        // Wait for any pending subscriptions.
        m_pendingSubscriptionBarrier.wait()

        if isActive && (m_queuer != nil)
        {
            #if RxMonEnabled
                RxMon.monObservable(.eObservable_notifyItem(self, item))
            #endif

            m_queuer!.dispatchSync({
                // queue the item to replay to future subscriptions.
                self.replayNotifications.queueItem(item)
            })

            // Emit the given item notification.
            self.outNotifier!.notifyItem(item)
        }
    }

    /// Notify reception of Completed/Error.
    /// - Parameter error: The error or nil if completed.
    public override func notifyCompleted(error: IRxError? = nil)
    {
        // Wait for any pending subscriptions.
        m_pendingSubscriptionBarrier.wait()

        if isActive
        {
            #if RxMonEnabled
                RxMon.monObservable(.eObservable_notifyCompleted(self, error))
            #endif

            // Emit the given completion notification.
            outNotifier!.notifyCompleted(error)
        }
    }

    /// Create the RxEvalNode eval op as its self dependencies constrain it from being defined in the init.
    public override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode: RxEvalNode<DataItemType, DataItemType>) in

            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange: eRxEvalStateChange, notifier: ARxNotifier<DataItemType>) in

                self.hotSubjectStateChangeHandler(evalNode, stateChange:stateChange, notifier:notifier)

                switch stateChange
                {
                    case eRxEvalStateChange.eNewSubscriptionInSubscriptionThread:

                        if self.m_queuer == nil
                        {
                            self.m_queuer = evalNode.evalQueue
                        }

                    case eRxEvalStateChange.eSubscriptionBegin:

                        // Replay the previous items to the new subscription.
                        if (self.replayNotifications.itemCount > 0) && self.isActive
                        {
                            self.replayNotifications.sendItemsToNotifyHandlerNonDestructive(notifier)
                        }

                    case eRxEvalStateChange.eEvalEnd:

                        self.m_queuer = nil

                    default:
                        break
                }
            }
        }
    }

    /// Clear the replay notifications buffer.
    public func clear()
    {
        self.replayNotifications.removeAll()
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// BehaviourSubject: Replay the last notification to subsequent subscriptions.
///
/// - [Intro Doc Link]( http://www.introtorx.com/uat/content/v1.0.10621.0/02_KeyTypes.html#BehaviorSubject )
/// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh211949(v=vs.103).aspx )
/// - [Netflix Doc Link]( https://github.com/ReactiveX/RxJava/wiki/Subject )
///

public class RxBehaviourSubject<ItemType> : RxSubject<ItemType>
{
    private var m_lastItem: ItemType? = nil

    /// Initialise with tag.
    /// - Parameter tag: The RxObject tag for the class.
    public init(tag : String, defaultItem: ItemType)
    {
        self.m_lastItem = defaultItem

        super.init(tag: tag)
    }

    /// Notify reception of Item
    /// - Parameter item: The item being notified.
    public override func notifyItem(item: ItemType)
    {
        // Wait for any pending subscriptions.
        m_pendingSubscriptionBarrier.wait()

        if isActive
        {
            #if RxMonEnabled
                RxMon.monObservable(.eObservable_notifyItem(self, item))
            #endif

            m_lastItem = item

            // Emit the given item notification.
            outNotifier!.notifyItem(item)
        }
    }

    /// Notify reception of Completed/Error.
    /// - Parameter error: The error or nil if completed.
    public override func notifyCompleted(error : IRxError? = nil)
    {
        // Wait for any pending subscriptions.
        m_pendingSubscriptionBarrier.wait()

        m_lastItem = nil

        if isActive
        {
            #if RxMonEnabled
                RxMon.monObservable(.eObservable_notifyCompleted(self, error))
            #endif

            // Emit the given completion notification.
            outNotifier!.notifyCompleted(error)
        }
    }

    /// Create the RxEvalNode eval op as its self dependencies constrain it from being defined in the init.
    public override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode : RxEvalNode<ItemType, ItemType>) in

            evalNode.stateChangeDelegate = { (stateChange : eRxEvalStateChange, notifier : ARxNotifier<ItemType>) in

                self.hotSubjectStateChangeHandler(evalNode, stateChange: stateChange, notifier: notifier)

                switch stateChange
                {
                    case eRxEvalStateChange.eSubscriptionBegin:

                        if let item = self.m_lastItem
                        {
                            notifier.notifyItem(item)
                        }

                    default:
                        break
                }
            }
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// AsyncSubject: Notify methods are asynchronous and return before notification finishes.
///
/// - [Intro Doc Link]( http://www.introtorx.com/uat/content/v1.0.10621.0/02_KeyTypes.html#AsyncSubject )
/// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh229363(v=vs.103).aspx )
/// - [Netflix Doc Link]( https://github.com/ReactiveX/RxJava/wiki/Subject )
///

public class RxAsyncSubject<ItemType> : RxSubject<ItemType>
{
    private var m_lastItem : ItemType? = nil

    /// Initialise with tag.
    /// - Parameter tag: The RxObject tag for the class.
    public init(tag : String)
    {
        super.init(tag: tag)
    }

    /// Notify reception of Item
    /// - Parameter item: The item being notified.
    public override func notifyItem(item: ItemType)
    {
        #if RxMonEnabled
            RxMon.monObservable(.eObservable_notifyItem(self, item))
        #endif

        if outNotifier != nil
        {
            // Save the last consumed item.
            m_lastItem = item
        }
    }

    /// Notify reception of Completed/Error.
    /// - Parameter error: The error or nil if completed.
    public override func notifyCompleted(error : IRxError? = nil)
    {
        #if RxMonEnabled
            RxMon.monObservable(.eObservable_notifyCompleted(self, error))
        #endif

        if let outNotifier = outNotifier
        {
            if let item = m_lastItem
            {
                // Emit the last consumed item.
                outNotifier.notifyItem(item)
            }

            // Emit completion.
            outNotifier.notifyCompleted(error)
        }

        m_lastItem = nil
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxRelaySubjectMap: An asynchronous notifications mapping relay. Relays from its ARxConsumer style interface to subscribers.
///

public class RxRelaySubjectMap<ItemInType, ItemOutType> : RxObservableMap<ItemInType, ItemOutType>
{
    /// Indicates that there are active subscriptions.
    public var isActive : Bool          { return outNotifier != nil }

    /// The notifier to emit from the subject.
    public var outNotifier: ARxNotifier<ItemOutType>? = nil

    /// The RxObject type for this class.
    public override var objectType : eRxObjectType               { return .eRxObject_RxSubject }

    /// Initialise with a tag.
    /// - Parameter tag: The RxObject tag.
    public init(tag : String)
    {
        super.init(sourceTag: tag, subscriptionType: eRxSubscriptionType.eHot, evalOp: nil)
    }

    /// Notify reception of an item notification.
    /// - Parameter item: The item of the notification.
    public func notifyItem(item : ItemInType)
    {
        if isActive
        {
            if let outItem = item as? ItemOutType
            {
                self.outNotifier?.notifyItem(outItem)
            }
            else
            {
                assert(false, "Could not convert item to type \(ItemOutType.self)")
            }
        }
    }

    /// Notify reception of a completed notification.
    /// - Parameter error: Optional that indicates whether the sequence completed with an error.
    public func notifyCompleted(error : IRxError? = nil)
    {
        outNotifier?.notifyCompleted(error)
    }

    /// Notify reception of a eval state change notification.
    /// - Parameter stateChange: The new eval state.
    public func notifyStateChange(stateChange : eRxEvalStateChange)
    {
        outNotifier?.notifyStateChange(stateChange)
    }

    /// Create the RxEvalNode eval op as its self dependencies constrain it from being defined in the init.
    public override func createEvalOpDelegate() -> RxEvalOp
    {
        return { (evalNode: RxEvalNode<ItemInType, ItemOutType>) in

            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        self.outNotifier = evalNode.asyncOutNotifier

                    case eRxEvalStateChange.eEvalEnd:

                        self.outNotifier = nil

                    default:
                        break
                }
            }
        }
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxRelaySubject: An asynchronous notifications relay. Relays from a consumer interface to subscribers.
///

public class RxRelaySubject<ItemType> : RxSource<ItemType>, IRxConsumer
{
    var m_notifier: ARxNotifier<ItemType>? = nil

    /// The RxObject type for this class.
    public override var objectType : eRxObjectType               { return .eRxObject_RxSubject }

    /// Initialise with a tag.
    /// - Parameter tag: The RxObject tag.
    public init(tag : String)
    {
        super.init(tag: tag, subscriptionType: eRxSubscriptionType.eHot, evalOp: nil)
    }

    /// Notify reception of an item notification.
    /// - Parameter item: The item of the notification.
    public func notifyItem(item : ItemType)
    {
        self.m_notifier?.notifyItem(item)
    }

    /// Notify reception of a completed notification.
    /// - Parameter error: Optional that indicates whether the sequence completed with an error.
    public func notifyCompleted(error : IRxError? = nil)
    {
        m_notifier?.notifyCompleted(error)
    }

    /// Notify reception of a eval state change notification.
    /// - Parameter stateChange: The new eval state.
    public func notifyStateChange(stateChange : eRxEvalStateChange)
    {
        m_notifier?.notifyStateChange(stateChange)
    }

    /// Create the RxEvalNode eval op as its self dependencies constrain it from being defined in the init.
    public override func createEvalOpDelegate() -> RxEvalOp
    {
        return { (evalNode: RxEvalNode<ItemType, ItemType>) in

            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        self.m_notifier = evalNode.asyncOutNotifier

                    case eRxEvalStateChange.eEvalEnd:

                        self.m_notifier = nil

                    default:
                        break
                }
            }
        }
    }
}
