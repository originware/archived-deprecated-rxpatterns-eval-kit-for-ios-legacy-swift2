//
// Created by Terry Stillone (http://www.originware.com) on 29/04/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxSource: The base class for all Source Observables. All Sources should inherit directly or indirectly from this class.
///

public class RxSource<ItemType> : RxObservable<ItemType>, IRxSource
{
    /// The RxObject type for this class. (See The SDK RxObject)
    public override var objectType : eRxObjectType               { return .eRxObject_RxSource }

    /// The subscription type for the source (Hot/Cold). (See The SDK eRxSubscriptionType documentation.)
    public var subscriptionType : eRxSubscriptionType            { return getSubscriptionType()! }

    /// Initialise with tag, subscriptionType and eval op.
    /// - Parameter tag: The RxObject tag for this instance. (See The SDK RxObject)
    /// - Parameter subscriptionType: The type of subscription (Hot/Cold). (See The SDK eRxSubscriptionType)
    /// - Parameter evalOp: The behaviour of the source encasulated as an RxEvalOp.
    public required init(tag: String, subscriptionType : eRxSubscriptionType, evalOp: RxEvalOp? = nil)
    {
        super.init(sourceTag: tag, subscriptionType: subscriptionType, evalOp: evalOp)
    }
}

extension RxSource: RxSourcePatterns
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    ///  AsyncGenPat: A Source that generates notifications from an async (time based) generator function.
    ///
    ///  The notifications times and values are calculated by the given generator function.
    ///
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter timesAreStrict: Indicator that the generated times should be strict or subject to timer coalescing.
    /// - Parameter subscriptionType: The type of subscription (Hot, Cold).
    /// - Parameter generator: Generator of item times and values to emit.
    /// - Returns: The RxSource that generates the async notifications.
    ///
    public final class func asyncGenPat(tag: String, timesAreStrict: Bool, subscriptionType: eRxSubscriptionType, generator: (index:RxIndexType, notifier:ARxNotifier<ItemType>) -> eRxAsyncGenCommand) -> RxSource<ItemType>
    {
        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.asyncGenPat(true, generator: generator)

        return RxSource<ItemType>(tag: tag, subscriptionType: .eHot, evalOp: evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    ///  syncGenPat: A Source that generates notifications from an synchronous (non-time) based generator function.
    ///
    ///   The notification values are calculated by the given generator functions.
    ///
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter subscriptionType: The type of subscription (Hot, Cold).
    /// - Parameter generator: Generator of item values to emit.
    /// - Returns: The RxSource that generates the sync notifications.
    ///
    public final class func syncGenPat(tag: String, subscriptionType: eRxSubscriptionType, generator: (index:RxIndexType, notifier:ARxNotifier<ItemType>) -> eRxSyncGenCommand) -> RxSource<ItemType>
    {
        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.syncGenPat(generator)

        return RxSource<ItemType>(tag: tag, subscriptionType: .eHot, evalOp: evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    ///  GateStreamsPat: A Source that collects notifications from given, separate streams.
    ///
    ///   Collects the items from separate streams together as a tuple and apply a mapping function on the tuples.
    ///
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter subscriptionType: The type of subscription (Hot, Cold).
    /// - Parameter gateType: The type of gate operation to be performed.
    /// - Parameter streams: The streams to gate.
    /// - Parameter mapEvalOp: The map of gate output items to the TargetItemType.
    /// - Returns: The RxSource that performs the gating.
    ///
    public final class func gateStreamsPat<TargetItemType>(tag: String, subscriptionType: eRxSubscriptionType, gateType: eGateType, streams: [ARxProducer<ItemType>], mapEvalOp: (RxGateMapType<ItemType, TargetItemType>) -> Void) -> RxSource<TargetItemType>
    {
        let evalOp : (RxEvalNode<TargetItemType, TargetItemType>) -> Void = RxEvalOps.gateStreamsPat(gateType, streams : streams, mapEvalOp: mapEvalOp)

        return RxSource<TargetItemType>(tag: tag, subscriptionType: subscriptionType, evalOp: evalOp)
    }
}

extension RxSource: RxTickableSourcePatterns
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    ///  A Source which emits notifications redirected from external notifier.
    ///
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter subscriptionType: The type of subscription (Hot, Cold).
    /// - Parameter notifier: The notifier to send the notifications to.
    /// - Returns: The source that performs the redirection.
    ///
    public final class func redirectPat(tag: String, subscriptionType: eRxSubscriptionType, notifier : RxNotifier_NotifyConsumers<ItemType>) -> RxSource<ItemType>
    {
        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.redirectPat(notifier)

        return RxSource<ItemType>(tag: tag, subscriptionType: .eHot, evalOp: evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    ///  A Source which emits generated notifications, triggered by an external notifier.
    ///
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter subscriptionType: The type of subscription (Hot, Cold).
    /// - Parameter tickNotifier: The notifier that determines when to emit items.
    /// - Parameter generator: The generator that generates the item values to be emitted.
    /// - Returns: The RxSource that performs the tick emission.
    ///
    public final class func tickGenPat<TickType>(tag: String, subscriptionType: eRxSubscriptionType, tickNotifier : RxNotifier_NotifyConsumers<TickType>, generator: (tick : TickType, notifier : ARxNotifier<ItemType>) -> Bool) -> RxObservableMap<TickType, ItemType>
    {
        let evalOp : ((RxEvalNode<TickType, ItemType>) throws -> Void) = RxEvalOps.tickGenPat(tickNotifier, generator : generator)

        return RxObservableMap<TickType, ItemType>(sourceTag: tag, subscriptionType: .eHot, evalOp: evalOp)
    }
}

extension RxSource: RxMSSources_Primitive
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// throwError: Create a Source that emits and error immediately.
    ///
    /// - [Microsoft Doc Link]( https://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.throw(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/empty-never-throw.htm )
    ///

    /// - Parameter error: The error to emit.
    /// - Returns: The RxObservable that emits the error.

    public final class func throwError(error: IRxError) -> RxSource<ItemType>
    {
        let tag = "throwError"

        let evalOp = { (evalNode: RxEvalNode<ItemType, ItemType>) -> Void in

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        notifier.notifyCompleted(error)

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxSource<ItemType>(tag: tag, subscriptionType: .eCold, evalOp: evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Never: Create a Source that never emits anything (item and completed notifications).
    ///
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh211979(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/empty-never-throw.htm )
    ///

    public final class func never() -> RxSource<ItemType>
    {
        let tag = "never"

        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOpsConvenience<ItemType, ItemType>.noOpPat()

        return RxSource<ItemType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Empty: Create a Source that emits nothing and then completes.
    ///
    /// - [Intro Doc Link]( https://github.com/Netflix/RxJava/wiki/Creating-Observables#empty-error-and-never )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.empty(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/empty-never-throw.html )
    ///

    public final class func empty() -> RxSource<ItemType>
    {
        let tag = "empty"
        let evalOp = RxEvalOpsConvenience<ItemType, ItemType>.completedOpPat(nil)

        return RxSource<ItemType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// EmitSingleItem: Create a Source that emits a given item and then completes.
    ///
    ///     This is called "Return" in formal Documentation.
    ///
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.return(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/just.html )
    ///

    public final class func returnItem(item: ItemType) -> RxSource<ItemType>
    {
        let tag = "returnItem"

        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.syncGenPat( { (index: RxIndexType, notifier: ARxNotifier<ItemType>)  -> eRxSyncGenCommand in

            // Emit the item and then a complete notification.

            notifier.notifyItem(item)
            notifier.notifyCompleted()

            return eRxSyncGenCommand.eStopTicking
        })

        return RxSource<ItemType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Raise: Create a Source that emits no items and then an error.
    ///
    /// - [Intro Doc Link]( https://github.com/Netflix/RxJava/wiki/Creating-Observables#empty-error-and-never )
    /// - [Microsoft Doc Link]( Similar concept to throw:   http://msdn.microsoft.com/en-us/library/hh211979(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/empty-never-throw.html )
    ///

    public final class func raise(error : IRxError) -> RxSource<ItemType>
    {
        let tag = "raise"
        let evalOp = { (evalNode: RxEvalNode<ItemType, ItemType>) -> Void in

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemType>) in

                switch stateChange
                {
                    case .eSubscriptionBegin:

                        notifier.notifyCompleted(error)

                    default:
                        break
                }
            }
        }

        return RxSource<ItemType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }
}


extension RxSource: RxMSSources_Create
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Create: Creates a source which will call the subscribe delegate anytime a subscription is made.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/content/v1.0.10621.0/04_CreatingObservableSequences.html )
    /// - [Microsoft Doc Link]( https://msdn.microsoft.com/en-us/library/hh229114(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/create.html )
    ///

    public final class func create(subscribe: (ARxConsumer<ItemType>) -> RxSubscription) -> RxSource<ItemType>
    {
        let tag = "create"

        let evalOp = { (evalNode : RxEvalNode<ItemType, ItemType>) -> Void in

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemType>) in

                switch stateChange
                {
                    case .eEvalBegin(let subscription):

                        // Run the given subscribe func in the assigned eval queue.
                        let surrogateSubscription = subscribe(notifier)

                        surrogateSubscription.appendDisposeAction({
                            subscription.unsubscribe()
                        })

                    default:
                        break
                }
            }
        }

        return RxSource<ItemType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Interval: Create a Source that emits a sequence of integers periodically.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/04_CreatingObservableSequences.html#ObservableInterval )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.interval(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/interval.html )
    ///

    public final class func interval(period : RxDuration, timesAreStrict: Bool = true) -> RxSource<RxIndexType>
    {
        let tag = "interval"

        let evalOp : ((RxEvalNode<RxIndexType, RxIndexType>) -> Void) = {

            switch period
            {
                case _ where period > 0:

                    return RxEvalOps.asyncGenPat(timesAreStrict, generator: { (index: RxIndexType, notifier: ARxNotifier<RxIndexType>) -> eRxAsyncGenCommand in

                        if index > 0
                        {
                            // Subsequent index cycles emit an item corresponding to the index - 1.
                            notifier.notifyItem(index - 1)
                        }

                        // All index cycles wait for the next period.
                        return .eNextTickAt(period + (period * RxTimeOffset(index)))
                    })

                case _ where period == 0:

                    // create a non-timed sequence interval source.
                    return RxEvalOps.syncGenPat({ (index: RxIndexType, notifier: ARxNotifier<RxIndexType>) -> eRxSyncGenCommand in

                        // Emit an item corresponding to the index, forever.

                        notifier.notifyItem(index)

                        return eRxSyncGenCommand.eNextTick
                    })

                default:   // case _ where period < 0:

                    // Emit an error.
                    return RxEvalOpsConvenience.completedOpPat(RxLibError(.eInvalidTimePeriod))
            }
        }()

        return RxSource<RxIndexType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Interval: Create a Source that emits a sequence of items periodically. Items are generated by mapping the sequence index to the ItemType.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/04_CreatingObservableSequences.html#ObservableInterval )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.interval(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/interval.html )
    ///

    public final class func interval(period : RxDuration, timesAreStrict: Bool = true, map : RxTypes<ItemType>.RxIndexTypeMap) -> RxSource<ItemType>
    {
        let tag = "interval"

        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = {

            switch period
            {
                case _ where period > 0:

                    return RxEvalOps.asyncGenPat(timesAreStrict, generator: { (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxAsyncGenCommand in

                        if index > 0
                        {
                            // Subsequent index cycles emit an item corresponding to the index.
                            notifier.notifyItem(map(index - 1))
                        }

                        // All index cycles wait for the period, forever.
                        return .eNextTickAt(period + (period * RxTimeOffset(index)))
                    })

                case _ where period == 0:

                    // create a non-timed sequence interval source.
                    return RxEvalOps.syncGenPat({ (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxSyncGenCommand in

                        // Emit an item corresponding to the index, forever.

                        notifier.notifyItem(map(index))

                        return eRxSyncGenCommand.eNextTick
                    })

                default:   // case _ where period < 0:

                    // Emit an error.
                    return RxEvalOpsConvenience.completedOpPat(RxLibError(.eInvalidTimePeriod))
            }
        }()

        return RxSource<ItemType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// repeatForever: Create a Source that emits a given item or sequence of items repeatedly.
    ///
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.repeat(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/repeat.html )
    ///

    public final class func repeatForever(item: ItemType) -> RxSource<ItemType>
    {
        let tag = "repeatForever"

        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.syncGenPat( { (index: RxIndexType, notifier: ARxNotifier<ItemType>)  -> eRxSyncGenCommand in

            notifier.notifyItem(item)

            return eRxSyncGenCommand.eNextTick
        })

        return RxSource<ItemType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }

    ///
    /// RepeatWithCount: Create a Source that emits a given item or sequence of items for a given count.
    ///
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.repeat(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/repeat.html )
    ///

    public final class func repeatWithCount(item: ItemType, count : RxCountType) -> RxSource<ItemType>
    {
        let tag = "repeatWithCount"

        if count == 0
        {
            return RxSource<ItemType>(tag: tag, subscriptionType: .eCold, evalOp : RxEvalOpsConvenience.completedOpPat())
        }

        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.syncGenPat( { (index: RxIndexType, notifier: ARxNotifier<ItemType>)  -> eRxSyncGenCommand in

            let endRepeat = index >= count - 1

            // emit the given item only count times.
            notifier.notifyItem(item)

            if endRepeat
            {
                notifier.notifyCompleted(nil)
            }

            return endRepeat ? eRxSyncGenCommand.eStopTicking : eRxSyncGenCommand.eNextTick
        })

        return RxSource<ItemType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// repeatSourceForever: Create a Source that emits the items of a given source repeatedly.
    ///
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.repeat(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/repeat.html )
    ///

    public final class func repeatSourceForever(source: ARxProducer<ItemType>) -> RxSource<ItemType>
    {
        let tag = "repeatSourceForever"

        let evalOp = { (evalNode: RxEvalNode<ItemType, ItemType>) -> Void in

            var hasEnded                            = false
            var streamSubscription: RxSubscription? = nil
            let buffer                              = RxNotificationQueue<ItemType>(tag: tag + "/buffer")

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemType>) in

                switch stateChange
                {
                    case .eEvalBegin:

                        let streamNotifier = RxNotifier_NotifyDelegates<ItemType>(tag: tag + "/notifier")

                        streamNotifier.onItem = { (item : ItemType) in

                            evalNode.evalQueue.dispatchSync({ notifier.notifyItem(item) })
                            buffer.queueItem(item)
                        }

                        streamNotifier.onCompleted = { (error : IRxError?) in

                            if error != nil
                            {
                                evalNode.evalQueue.dispatchAsync({ notifier.notifyCompleted(error) })
                                return
                            }

                            while !hasEnded
                            {
                                buffer.sendItemsToNotifierOnEvalQueue(notifier, evalQueue: evalNode.evalQueue, terminate: &hasEnded)
                            }
                        }

                        hasEnded = false

                        streamSubscription = source.subscribe(streamNotifier)
                        assert(streamSubscription != nil, "Remove compiler warning")

                    case .eEvalEnd:

                        hasEnded = true
                        streamSubscription = nil

                    default:
                        break
                }
            }
        }

        return RxSource<ItemType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Generate: Create a Source using a generation function.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/04_CreatingObservableSequences.html#ObservableGenerate )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.generate(v=vs.103).aspx )
    /// - [Netflix Doc Link]( https://github.com/Netflix/RxJava/wiki/Phantom-Operators#generate-and-generateabsolutetime )
    ///

    public final class func generate(generateAction : (RxIndexType) -> ItemType?) -> RxSource<ItemType>
    {
        let tag = "generate"

        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.syncGenPat( { (index: RxIndexType, notifier: ARxNotifier<ItemType>)  -> eRxSyncGenCommand in

            // perform the generate to obtain the next items.
            let item : ItemType? = generateAction(index)

            if let item = item
            {
                // item emit case.
                notifier.notifyItem(item)
                return eRxSyncGenCommand.eNextTick
            }
            else
            {
                // termination case.
                notifier.notifyCompleted(nil)
                return eRxSyncGenCommand.eStopTicking
            }
        })

        return RxSource<ItemType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Range: Create a Source that emits a given range of sequential integers.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/04_CreatingObservableSequences.html#ObservableRange )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.range(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/range.html )
    ///

    public final class func range(start : RxIndexType, count : RxCountType) -> RxSource<RxIndexType>
    {
        let tag = "range"

        let evalOp : ((RxEvalNode<RxIndexType, RxIndexType>) -> Void) = RxEvalOps.syncGenPat( { (index: RxIndexType, notifier: ARxNotifier<RxIndexType>)  -> eRxSyncGenCommand in

            switch index
            {
                case 0..<count:

                    notifier.notifyItem(start + index)

                    return eRxSyncGenCommand.eNextTick

                default:

                    notifier.notifyCompleted(nil)

                    return eRxSyncGenCommand.eStopTicking
            }
        })

        return RxSource<RxIndexType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// StartWith: Create a Source that emits items from a generation function.
    ///
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.start(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/start.html )
    ///

    public final class func startWith(itemFactory : () -> ItemType) -> RxSource<ItemType>
    {
        let tag = "startWith"

        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.syncGenPat( { (index: RxIndexType, notifier: ARxNotifier<ItemType>)  -> eRxSyncGenCommand in

            // obtain the first item by factory.
            let item = itemFactory()

            // emit the item and then terminate.
            notifier.notifyItem(item)
            notifier.notifyCompleted()

            return eRxSyncGenCommand.eStopTicking
        })

        return RxSource<ItemType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Timer: Create a Source that emits a particular item after a given delay.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/04_CreatingObservableSequences.html#ObservableTimer )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.timer(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/timer.html )
    ///

    public final class func timer(item : ItemType, dueTime : RxTime, timesAreStrict: Bool = true) -> RxSource<ItemType>
    {
        let tag = "timer"

        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.asyncGenPat(timesAreStrict, generator: { (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxAsyncGenCommand in

            if index == 0
            {
                // do not emit anything yet, in this index cycle calculate the timer and wait that time.

                let timeOffset = dueTime.timeIntervalSinceDate(RxTime())

                return eRxAsyncGenCommand.eNextTickAt(timeOffset > 0 ? timeOffset : 0)
            }

            // emit the item, completion and terminate.
            notifier.notifyItem(item)
            notifier.notifyCompleted()

            return eRxAsyncGenCommand.eStopTicking
        })

        return RxSource<ItemType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }

    ///
    /// Timer: Create a Source that emits a particular item after a given delay.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/04_CreatingObservableSequences.html#ObservableTimer )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.timer(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/timer.html )
    ///

    public final class func timer(item : ItemType, dueTimeOffset : RxTimeOffset, timesAreStrict: Bool = true) -> RxSource<ItemType>
    {
        let tag = "timer"
        let generator = { (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxAsyncGenCommand in

            if index == 0
            {
                // do not emit anything yet, in this index cycle calculate the timer period and wait that time.
                return eRxAsyncGenCommand.eNextTickAt(dueTimeOffset > 0 ? dueTimeOffset : 0)
            }

            // emit the item, completion and terminate.
            notifier.notifyItem(item)
            notifier.notifyCompleted()

            return eRxAsyncGenCommand.eStopTicking
        }

        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.asyncGenPat(timesAreStrict, generator:generator)

        return RxSource<ItemType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }

    ///
    /// Timer: Create a Source that emits a particular item after a given delay.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/04_CreatingObservableSequences.html#ObservableTimer )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.timer(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/timer.html )
    ///

    public final class func timer(startValue: RxIndexType, dueTime : RxTime, period : RxDuration, timesAreStrict: Bool = true) -> RxSource<RxIndexType>
    {
        let tag = "timer"

        if period <= 0
        {
            // Error case.
            return RxSource<RxIndexType>.throwError(RxLibError(.eInvalidTimePeriod))
        }

        let evalOp: ((RxEvalNode<RxIndexType, RxIndexType>) -> Void) = { (evalNode: RxEvalNode<RxIndexType, RxIndexType>) -> Void in

            let startTimeOffset = dueTime.timeIntervalSinceDate(RxTime())

            let generator = { (index: RxIndexType, notifier: ARxNotifier<RxIndexType>) -> eRxAsyncGenCommand in

                if index == 0
                {
                    // do not emit anything yet, in this index cycle calculate the timer period and wait that time.
                    return eRxAsyncGenCommand.eNextTickAt(startTimeOffset > 0 ? startTimeOffset : 0)
                }

                // emit the item and wait the period. This is a non terminating action.
                notifier.notifyItem(startValue + index - 1)

                return eRxAsyncGenCommand.eNextTickAt(startTimeOffset + period * RxDuration(index))
            }

            let evalOp2 : ((RxEvalNode<RxIndexType, RxIndexType>) -> Void) = RxEvalOps.asyncGenPat(timesAreStrict, generator: generator)

            // Get the asyncGenPat to install the delegates into the evalnode.
            evalOp2(evalNode)
        }

        return RxSource<RxIndexType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }

    ///
    /// Timer: Create a Source that emits a particular item after a given delay.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/04_CreatingObservableSequences.html#ObservableTimer )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.timer(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/timer.html )
    ///

    public final class func timer(startValue : RxIndexType, dueTimeOffset : RxTimeOffset, period : RxDuration, timesAreStrict: Bool = true) -> RxSource<RxIndexType>
    {
        let tag = "timer"

        if period <= 0
        {
            // Error case.
            return RxSource<RxIndexType>.throwError(RxLibError(.eInvalidTimePeriod))
        }

        let generator = { (index: RxIndexType, notifier: ARxNotifier<RxIndexType>) -> eRxAsyncGenCommand in

            if index == 0
            {
                // do not emit anything yet, in this index cycle calculate the timer period and wait that time.

                return eRxAsyncGenCommand.eNextTickAt(dueTimeOffset > 0 ? dueTimeOffset : 0)
            }

            // emit the item and wait the period. This is a non terminating action.
            notifier.notifyItem(startValue + index - 1)

            return eRxAsyncGenCommand.eNextTickAt(dueTimeOffset + period * RxDuration(index))
        }

        let evalOp : ((RxEvalNode<RxIndexType, RxIndexType>) -> Void) = RxEvalOps.asyncGenPat(timesAreStrict, generator: generator)

        return RxSource<RxIndexType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Defer: Create a Source that emits a given streams items only when the observable is subscribed to.
    ///
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh229160(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/defer.html )
    ///

    public final class func deferObservable(observableFactory : () -> ARxProducer<ItemType>) -> RxSource<ItemType>
    {
        let tag = "deferObservable"

        let evalOp = { (evalNode: RxEvalNode<ItemType, ItemType>) -> Void in

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin(let subscription):

                        let observable = observableFactory()

                        #if RxMonEnabled
                            if let traceTag = evalNode.traceTag { observable.traceAll(traceTag) }
                        #endif

                        let factorySubscription = observable.subscribe(notifier)

                        factorySubscription.appendDisposeAction({
                            subscription.unsubscribe()
                        })

                    default:
                        break
                }
            }

        }

        return RxSource<ItemType>(tag: tag, subscriptionType: .eCold, evalOp : evalOp)
    }
}

extension RxSource: RxMSSources_Combine
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// CombineLatest: Given a collection of Sources, create a Source that emits the latest item of those stream that is the result of a given mapping function.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/12_CombiningSequences.html#CombineLatest )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh211991(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/combinelatest.html )
    ///

    public final class func combineLatest<StreamItemType>(stream1 : ARxProducer<StreamItemType>, stream2 : ARxProducer<StreamItemType>, selector : (StreamItemType, StreamItemType) -> ItemType?) -> RxSource<ItemType>
    {
        let tag = "combineLatest"

        let evalOp : (RxEvalNode<ItemType, ItemType>) -> Void = RxEvalOps.gateStreamsPat(eGateType.eGate_OnLatestItem, streams : [stream1, stream2], mapEvalOp: { (map: RxGateMapType<StreamItemType, ItemType>) in

            map.mapItemsDelegate = { (gate: RxNotificationGate<StreamItemType, ItemType>, notifier: ARxNotifier<ItemType>) in

                gate.popAllQueuesWithExtraItems()

                let (item1, item2) = (gate.itemAtQueueIndex(0), gate.itemAtQueueIndex(1))

                if let combinedItem = selector(item1, item2)
                {
                    notifier.notifyItem(combinedItem)
                }
            }

            map.mapCompletedDelegate = { (index : RxIndexType, error: IRxError?, gate : RxNotificationGate<StreamItemType, ItemType>, notifier: ARxNotifier<ItemType>) in

                notifier.notifyCompleted(error)
            }
        })

        return RxSource<ItemType>(tag: tag, subscriptionType: .eHot, evalOp: evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Concat: Given a collection of Sources, create a Source that emits all of the items of the first to emit, then all of the second to emit and so on.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/12_CombiningSequences.html#Concat )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.concat(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/concat.html )
    ///

    public final class func concat(streams : [ARxProducer<ItemType>]) -> RxSource<ItemType>
    {
        let tag = "concat"
        let evalOp = { (evalNode : RxEvalNode<ItemType, ItemType>) -> Void in

            var streamEnumerable : RxObservableEnumerable<ItemType>? = nil

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        streamEnumerable = RxObservableEnumerable<ItemType>(streams : streams)

                        streamEnumerable!.sendToNotifier(notifier, sendCompleted: true)

                    case eRxEvalStateChange.eEvalEnd:

                        streamEnumerable?.terminate(.eTermination_Completed)

                    default:
                        break
                }
            }
        }

        return RxSource<ItemType>(tag: tag, subscriptionType: .eHot, evalOp : evalOp)
    }

    ///
    /// Concat: Given a collection of Sources, create a Source that emits all of the items of the first to emit, then all of the second to emit and so on.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/12_CombiningSequences.html#Concat )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.concat(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/concat.html )
    ///

    public final class func concat(streams : ARxProducer<ARxProducer<ItemType>>) -> RxSource<ItemType>
    {
        let tag = "concat"

        let evalOp = { (evalNode : RxEvalNode<ItemType, ItemType>) -> Void in

            var streamEnumerable : RxObservableEnumerable<ItemType>? = nil

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        streamEnumerable = RxObservableEnumerable<ItemType>(streams : streams)

                        streamEnumerable!.sendToNotifier(notifier, sendCompleted: true)

                    case eRxEvalStateChange.eEvalEnd:

                        streamEnumerable?.terminate(.eTermination_Completed)

                    default:
                        break
                }
            }
        }

        return RxSource<ItemType>(tag: tag, subscriptionType: .eHot, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Merge: Given a collection of Sources, create a Source that emits the items of those streams as they are emitted.
    ///
    /// - [Intro Doc Link]( https://github.com/Netflix/RxJava/wiki/Combining-Observables#merge )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.merge(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/merge.html )
    ///

    public final class func merge(streams : [ARxProducer<ItemType>]) -> RxSource<ItemType>
    {
        let tag = "merge"
        var activeStreamCount = streams.count

        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.switchStreamsPat(streams, mapEvalOp: { (map: RxSwitchMapType<ItemType, ItemType>) in

            map.mapItemDelegate = { (index : RxIndexType, item : ItemType, switcher: RxNotificationSwitch<ItemType, ItemType>, notifier: ARxNotifier<ItemType>) in

                notifier.notifyItem(item)
            }

            map.mapCompletedDelegate = { (index : RxIndexType, error: IRxError?, switcher: RxNotificationSwitch<ItemType, ItemType>, notifier: ARxNotifier<ItemType>) in

                activeStreamCount -= 1

                if (error != nil) || (activeStreamCount == 0)
                {
                    notifier.notifyCompleted(error)
                }
            }
        })

        return RxSource<ItemType>(tag: tag, subscriptionType: .eHot, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Zip: Given two sources a create a Source that emits the tuple of items from those two streams.
    ///
    /// - [Intro Doc Link]( https://github.com/Netflix/RxJava/wiki/Combining-Observables#zip )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.zip(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/zip.html )
    ///

    public final class func zip<StreamItemType>(streams : [ARxProducer<StreamItemType>], selector : (StreamItemType, StreamItemType) -> ItemType?) -> RxSource<ItemType>
    {
        let tag = "zip"

        let evalOp : (RxEvalNode<ItemType, ItemType>) -> Void = RxEvalOps.gateStreamsPat(.eGate_OnAllQueuesHaveAtLeastOneItem, streams : streams, mapEvalOp: { (map: RxGateMapType<StreamItemType, ItemType>) in

            map.mapItemsDelegate = { (gate: RxNotificationGate<StreamItemType, ItemType>, notifier: ARxNotifier<ItemType>) in

                assert(gate.itemCount(0) > 0 && gate.itemCount(1) > 0, "Expected all gate queues to have items")

                if let zippedItem = selector(gate.itemAtQueueIndex(0), gate.itemAtQueueIndex(1))
                {
                    notifier.notifyItem(zippedItem)
                }

                gate.popAllQueues()
            }

            map.mapCompletedDelegate = { (index : RxIndexType, error: IRxError?, gate : RxNotificationGate<StreamItemType, ItemType>, notifier: ARxNotifier<ItemType>) in

                notifier.notifyCompleted(error)
            }
        })

        return RxSource<ItemType>(tag: tag, subscriptionType: .eHot, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// SwitchOnNext: Given a collection of Sources, create a Source that emits the most-recently emitted items from those sources.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/12_CombiningSequences.html#Switch )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh229197(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/switch.html )
    ///

    public final class func switchOnNext(streams : [ARxProducer<ItemType>]) -> RxSource<ItemType>
    {
        let tag = "switchOnNext"

        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.switchStreamsPat(streams, mapEvalOp: { (map: RxSwitchMapType<ItemType, ItemType>) in

            var currentActiveStreamIndex = -1

            map.mapItemDelegate = { (index : RxIndexType, item : ItemType, gate: RxNotificationSwitch<ItemType, ItemType>, notifier: ARxNotifier<ItemType>) in

                if index > currentActiveStreamIndex
                {
                    notifier.notifyItem(item)

                    currentActiveStreamIndex = index
                }
                else if index == currentActiveStreamIndex
                {
                    notifier.notifyItem(item)
                }
            }

            map.mapCompletedDelegate = { (index : RxIndexType, error: IRxError?, gate : RxNotificationSwitch<ItemType, ItemType>, notifier: ARxNotifier<ItemType>) in

                if index >= currentActiveStreamIndex
                {
                    notifier.notifyCompleted(error)
                }
            }
        })

        return RxSource<ItemType>(tag: tag, subscriptionType: .eHot, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Amb: Given a collection of Sources, create a Source that emits all of the items from only the first to emit an item.
    ///
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.amb(v=vs.103).aspx )
    /// - [Netflix Doc Link]( https://github.com/Netflix/RxJava/wiki/Conditional-and-Boolean-Operators#amb )
    ///

    public final class func amb(streams : [ARxProducer<ItemType>]) -> RxSource<ItemType>
    {
        let tag = "amb"

        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.switchStreamsPat(streams, mapEvalOp: { (map: RxSwitchMapType<ItemType, ItemType>) in

            var firstActiveStreamIndex = -1

            map.mapItemDelegate = { (index : RxIndexType, item : ItemType, gate: RxNotificationSwitch<ItemType, ItemType>, notifier: ARxNotifier<ItemType>) in

                if firstActiveStreamIndex == -1
                {
                    notifier.notifyItem(item)

                    firstActiveStreamIndex = index
                }
                else if firstActiveStreamIndex == index
                {
                    notifier.notifyItem(item)
                }
                else
                {
                    notifier.notifyCompleted(nil)
                }
            }

            map.mapCompletedDelegate = { (index : RxIndexType, error: IRxError?, gate : RxNotificationSwitch<ItemType, ItemType>, notifier: ARxNotifier<ItemType>) in

                if (firstActiveStreamIndex == -1) || (firstActiveStreamIndex == index)
                {
                    notifier.notifyCompleted(error)
                }
            }
        })

        return RxSource<ItemType>(tag: tag, subscriptionType: .eHot, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Join: Given two streams, create a Source that combines the items selectively based on time windows.
    ///
    /// - [Intro Doc Link](http://www.introtorx.com/uat/content/v1.0.10621.0/17_SequencesOfCoincidence.html#Join)
    /// - [Microsoft Doc Link](http://msdn.microsoft.com/en-us/library/hh229750(v=vs.103).aspx)
    /// - [Netflix Doc Link](http://reactivex.io/documentation/operators/join.html)
    ///

    public final class func join<StreamItemType>(

            stream1 : ARxProducer<StreamItemType>, stream2 : ARxProducer<StreamItemType>,
            stream1DurationSelector : (StreamItemType) -> ARxProducer<StreamItemType>, stream2DurationSelector : (StreamItemType) -> ARxProducer<StreamItemType>,
            pairValueSelector : (StreamItemType, StreamItemType) -> ItemType

    ) -> RxSource<ItemType>
    {
        let tag = "join"

        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.gateStreamsPat(.eGate_OnAllQueuesHaveAtLeastOneItem, streams : [stream1, stream2], mapEvalOp: { (map: RxGateMapType<StreamItemType, ItemType>) in

            // To be implemented
        })

        return RxSource<ItemType>(tag: tag, subscriptionType: .eHot, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// GroupJoin Documentation:
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/uat/content/v1.0.10621.0/17_SequencesOfCoincidence.html#Join )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh244235(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/join.html )
    ///

    public final class func groupJoin<StreamItemType>(
            stream1: ARxProducer<StreamItemType>, stream2: ARxProducer<StreamItemType>,
            stream1DurationSelector: (StreamItemType) -> ARxProducer<StreamItemType>, stream2DurationSelector: (StreamItemType) -> ARxProducer<StreamItemType>,
            pairValueSelector: (StreamItemType, StreamItemType) -> ItemType
    ) -> RxSource<ItemType>
    {
        let tag = "groupJoin"

        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.gateStreamsPat(.eGate_OnAllQueuesHaveAtLeastOneItem, streams : [stream1, stream2], mapEvalOp: { (map: RxGateMapType<StreamItemType, ItemType>) in

            // To be implemented
        })

        return RxSource<ItemType>(tag: tag, subscriptionType: .eHot, evalOp : evalOp)
    }
}


extension RxSource: RxMSSources_Compare
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// SequenceEqual: Given two streams, create a Bool Source that emits the result of the comparison of those two streams.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/06_Inspection.html#SequenceEqual )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.sequenceequal(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/sequenceequal.html )
    ///

    public final class func sequenceEqual(stream1 : ARxProducer<ItemType>, stream2 : ARxProducer<ItemType>, equateOp: RxTypes<ItemType>.RxCompareOp) -> RxSource<Bool>
    {
        let tag = "sequenceEqual"
        let evalOp : (RxEvalNode<Bool, Bool>) -> Void = RxEvalOps.gateStreamsPat(.eGate_OnAllQueuesHaveAtLeastOneItem, streams : [stream1, stream2], mapEvalOp: { (map: RxGateMapType<ItemType, Bool>) in

            map.mapItemsDelegate = { (gate: RxNotificationGate<ItemType, Bool>, notifier: ARxNotifier<Bool>) in

                assert(gate.itemCount(0) > 0 && gate.itemCount(1) > 0, "Expected all gate queues to have items")

                if !equateOp(lhs: gate.itemAtQueueIndex(0), rhs: gate.itemAtQueueIndex(1))
                {
                    notifier.notifyItem(false)
                    notifier.notifyCompleted()

                    gate.closeGate()
                }

                gate.popAllQueues()
            }

            map.mapCompletedDelegate = { (index : RxIndexType, error: IRxError?, gate : RxNotificationGate<ItemType, Bool>, notifier: ARxNotifier<Bool>) in

                if error == nil
                {
                    notifier.notifyItem(gate.countOfInputQueuesWithSomeItems == 0)
                }

                notifier.notifyCompleted(error)

                gate.closeGate()
            }
        })

        return RxSource<Bool>(tag: tag, subscriptionType: .eHot, evalOp: evalOp)
    }
}

extension RxSource: RxMSSources_Conversion
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// FromArray: Given an array of items, create a Source that emits those items and an optional terminator.
    ///
    /// - Parameter items: The items to be emitted by the created RxSources.
    /// - Parameter termination: The optional termination notification for the RxSource.
    /// - Parameter subscriptionType: The type of subscription (Hot/Cold).
    /// - Parameter tag: The RxObject tag for the RxSource.
    /// - Returns: The created RxSource.
    ///
    public final class func fromArray(items: [ItemType], termination: etTerminationType = .eTermination_Completed, subscriptionType : eRxSubscriptionType = .eCold, tag : String = "fromArray") -> RxSource<ItemType>
    {
        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.syncGenPat( { (index: RxIndexType, notifier: ARxNotifier<ItemType>)  -> eRxSyncGenCommand in

            if index >= RxIndexType(items.count)
            {
                // Termination case.
                notifier.notifyTermination(termination)

                // Inform the generator caller to finish.
                return eRxSyncGenCommand.eStopTicking
            }

            // Item emit case: Emit the item from the items array coresponding to the current index.
            notifier.notifyItem(items[Int(index)])

            // Inform the generator caller to recall the generator for the next item.
            return eRxSyncGenCommand.eNextTick
        })

        return RxSource<ItemType>(tag: tag, subscriptionType: subscriptionType, evalOp : evalOp)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// FromTimedArray: Given an array of a tuple of (time-offsets, items), create a Source that emits those items at those times and an optional terminator.
    ///
    /// - Parameter timedItems: The array of tuples of items and time-offsets to be emitted by the created RxSources.
    /// - Parameter termination: The termination notification for the RxSource.
    /// - Parameter timesAreStrict: Indicator that timers should not be subject to coalescing.
    /// - Parameter subscriptionType: The type of subscription (Hot/Cold).
    /// - Parameter tag: The RxObject tag for the RxSource.
    /// - Returns: The created RxSource.
    ///
    public final class func fromTimedArray(timedItems: RxTypes<ItemType>.TimingArrayType, termination: etTerminationType = .eTermination_Completed, timesAreStrict: Bool = true, subscriptionType : eRxSubscriptionType = .eCold, tag : String = "fromTimedArray") -> RxSource<ItemType>
    {
        let count = timedItems.count

        if count == 0
        {
            return RxSource<ItemType>(tag: tag, subscriptionType: .eHot, evalOp : RxEvalOpsConvenience<ItemType, ItemType>.terminationOpPat(termination))
        }

        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = RxEvalOps.asyncGenPat(timesAreStrict, generator: { (index: RxIndexType, notifier: ARxNotifier<ItemType>) -> eRxAsyncGenCommand in

            switch index
            {
                case 0:                         // Setup first notification.

                    // Direct the handler to call the generator at the first tick time.
                    return eRxAsyncGenCommand.eNextTickAt(timedItems[0].timeOffset)

                case 1..<count:                 // Emit items.

                    // Emit the item from the items array corresponding to the previous index
                    let item = timedItems[Int(index - 1)].item

                    notifier.notifyItem(item)

                    // Direct the handler to call the generator at the next tick time.
                    return eRxAsyncGenCommand.eNextTickAt(timedItems[Int(index)].timeOffset)

                case count:                     // Emit last item and Completed.

                    // Emit the last item.
                    let item = timedItems[Int(count - 1)].item

                    notifier.notifyItem(item)

                    // Emit termination.
                    notifier.notifyTermination(termination ?? .eTermination_Completed)

                    // Direct the handler to finish.
                    return eRxAsyncGenCommand.eStopTicking

                default:                       // Internal error case.

                    fatalError("Unexpected tick index")
            }
        })

        return RxSource<ItemType>(tag: tag, subscriptionType: subscriptionType, evalOp : evalOp)
    }
}

extension RxSource: RxMiscSources
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// toHotSource: Given a SequenceType, create a Hot Source that emits the items from the SequenceType.
    ///
    /// - Parameter tag: The RxObject tag for the RxSource.
    /// - Parameter sequence: The sequence to use to generate item notifications.
    /// - Returns: The created RxSource.
    ///
    /// As SequenceTypes are not restartable, this creates a non restartable hot source.
    ///

    public final class func toHotSource<ItemType, S:SequenceType where S.Generator.Element == ItemType>(tag: String, sequence: S) -> RxSource<ItemType>
    {
        let evalOp : ((RxEvalNode<ItemType, ItemType>) -> Void) = { (evalNode: RxEvalNode<ItemType, ItemType>) -> Void in

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        if evalNode.performedEvaluationCount > 1
                        {
                            notifier.notifyCompleted()
                            return
                        }

                        for item in sequence
                        {
                            notifier.notifyItem(item)
                        }

                        notifier.notifyCompleted()

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxSource<ItemType>(tag: tag, subscriptionType: .eHot, evalOp : evalOp)
    }
}