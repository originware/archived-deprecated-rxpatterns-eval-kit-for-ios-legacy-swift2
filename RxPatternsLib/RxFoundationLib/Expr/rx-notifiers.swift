//
// Created by Terry Stillone (http://www.originware.com) on 29/10/2015.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxNotifier_NotifyTestDelegates:  Notifies targets of notifications and their RxEvalNode by means of delegates.
/// This is used for monitoring or testing RxEvalNode behaviour.
///
public class RxNotifier_NotifyTestDelegates<ItemType>
{
    // Item notification action delegate type.
    public typealias RxTestItemActionDelegate = (ItemType, RxEvalNode<ItemType, ItemType>) -> Void

    // Completed/Error notification action delegate type.
    public typealias RxTestCompletedActionDelegate = (IRxError?, RxEvalNode<ItemType, ItemType>) -> Void

    // Eval State Change notification action delegate type.
    public typealias RxTestStateChangeActionDelegate = (eRxEvalStateChange, RxEvalNode<ItemType, ItemType>) -> Void

    /// The onItem item notification delegate.
    public final var onItem: RxTestItemActionDelegate = { (item: ItemType, evalNode : RxEvalNode<ItemType, ItemType>) in

    }

    /// The onCompleted completed/error notification delegate.
    public final var onCompleted : RxTestCompletedActionDelegate = { (error : IRxError?, evalNode : RxEvalNode<ItemType, ItemType>) in

    }

    /// The onStateChange eval State Change notification delegate.
    public final var onStateChange : RxTestStateChangeActionDelegate  = { (stateChange : eRxEvalStateChange, evalNode : RxEvalNode<ItemType, ItemType>) in

    }

    /// Notifier operation enabler.
    public var enabled: Bool = true

    /// Initialise with tag and description.
    /// - Parameter tag: The tag identifier of the RxObject.
    /// - Parameter description: The description of the RxObject.
    public init(tag : String, description : String? = nil)
    {
    }

    /// Notify an Item Notification.
    /// - Parameter item: The item associated with the notification.
    /// - Parameter evalNode: The EvalNode that handled the notification.
    @inline(__always) public func notifyItem(item: ItemType, evalNode : RxEvalNode<ItemType, ItemType>)
    {
        if enabled
        {
            onItem(item, evalNode)
        }
    }

    /// Notify a completed/error Notification.
    /// - Parameter error: The optional error associated with the completion.
    /// - Parameter evalNode: The EvalNode that handled the notification.
    @inline(__always) public func notifyCompleted(error : IRxError? = nil, evalNode : RxEvalNode<ItemType, ItemType>)
    {
        if enabled
        {
            onCompleted(error, evalNode)
        }
    }

    /// Notify an eval state change Notification.
    /// - Parameter stateChange: The eval state change with the completion.
    /// - Parameter evalNode: The EvalNode that handled the notification.
    @inline(__always) public func notifyStateChange(stateChange : eRxEvalStateChange, evalNode : RxEvalNode<ItemType, ItemType>)
    {
        if enabled
        {
            onStateChange(stateChange, evalNode)
        }
    }

    /// Clear the delegates.
    public func clear()
    {
        onItem = { _ in }
        onCompleted = { _ in }
        onStateChange = { _ in }
    }
}