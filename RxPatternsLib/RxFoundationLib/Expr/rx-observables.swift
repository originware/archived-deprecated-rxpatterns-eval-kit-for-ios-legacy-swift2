//
// Created by Terry Stillone (http://www.originware.com) on 29/04/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0.
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxObservable: The base class for all Observables. All Observables should inherit directly or indirectly from this class.
///

public class RxObservableMap<ItemInType, ItemOutType> : RxObservableNode<ItemInType, ItemOutType>
{
    /// Initialise for Observable operation with tag, the previous node in the composition and the evalOp with the Observables behaviour.
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter evalOp: The behaviour of the Observable encapsulated in an RxEvalOp
    public override init(tag : String, evalOp : ((RxEvalNode<ItemInType, ItemOutType>) throws -> Void)? = nil)
    {
        super.init(tag: tag, evalOp : evalOp)
    }

    /// Initialise for source operation.
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter subscriptionType: The type of subscription.
    /// - Parameter evalOp: The behaviour of the Observable encapsulated in an RxEvalOp.
    public override init(sourceTag: String, subscriptionType : eRxSubscriptionType, evalOp: ((RxEvalNode<ItemInType, ItemOutType>) throws -> Void)? = nil)
    {
        super.init(sourceTag: sourceTag, subscriptionType : subscriptionType, evalOp : evalOp)
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxObservable: The base class for all Observables
///
/// - Note:     All Observables should inherit directly or indirectly from this class.
/// - See:     The RxObservablePatterns extension in irx-patterns.swift
///                                       IRxMonInstanceDataProvider

public class RxObservable<ItemType> : RxObservableMap<ItemType, ItemType>
{
    /// Initialise for Observable operation with tag, the previous node in the composition and the evalOp with the Observables behaviour.
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter evalOp: The behaviour of the Observable encapsulated in an RxEvalOp
    public override init(tag : String, evalOp : RxEvalOp? = nil)
    {
        super.init(tag: tag, evalOp : evalOp)
    }

    /// Initialise for source operation.
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter subscriptionType: The type of subscription.
    /// - Parameter evalOp: The behaviour of the Observable encapsulated in an RxEvalOp.
    public override init(sourceTag: String, subscriptionType : eRxSubscriptionType, evalOp: RxTypes2<ItemType, ItemType>.RxEvalOp? = nil)
    {
        super.init(sourceTag: sourceTag, subscriptionType : subscriptionType, evalOp : evalOp)
    }
}

/// Equatable operator for RxObservableMap class.
public func ==<ItemInType, ItemOutType>(lhs: RxObservableMap<ItemInType, ItemOutType>, rhs: RxObservableMap<ItemInType, ItemOutType>) -> Bool
{
    return lhs.hashValue == rhs.hashValue
}

extension RxObservableMap : RxObservablePatterns
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    ///  relayPat:  Employ a given evaluation function to process in-coming notifications and emit the resulting notification.
    ///
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter evalOp: The RxEvalOp which defines the behaviour of the Observable.
    public final func relayPat<ItemTargetType>(tag: String, evalOp: (RxEvalNode<ItemOutType, ItemTargetType>) throws -> Void) -> RxObservableMap<ItemOutType, ItemTargetType>
    {
        return RxObservableMap<ItemOutType, ItemTargetType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    ///  extractPat:  Extract notifications to a given consumer for the purposes of external processing.
    ///

    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter extractConsumer: The given notifier to send notifications to.
    /// - Returns: The RxObservable that performs the extraction.
    public final func extractPat(tag: String, extractConsumer: ARxConsumer<ItemOutType>) -> RxObservable<ItemOutType>
    {
        let evalOp = RxEvalOps.extractPat(extractConsumer)

        return RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    ///  gateStreamsPat: Collect the items from separate streams together as a tuple and apply a mapping function on the tuples to generate a target notification.
    ///
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter gateType: The type of gate operation to be performed.
    /// - Parameter streams: The streams to gate.
    /// - Parameter mapEvalOp: The map of gate output items to the TargetItemType.
    /// - Returns: The RxObservableMap that performs the gating.
    public final func gateStreamsMapPat<TargetItemType>(tag: String, gateType: eGateType, streams: [ARxProducer<ItemOutType>], mapEvalOp: (RxGateMapType<ItemOutType, TargetItemType>) -> Void) -> RxObservableMap<ItemOutType, TargetItemType>
    {
        let evalOp : (RxEvalNode<ItemOutType, TargetItemType>) -> Void = RxEvalOps.gateStreamsMapPat(gateType, streams: streams, mapEvalOp: mapEvalOp)

        return RxObservableMap<ItemOutType, TargetItemType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }
}

extension RxObservableMap : RxObservableSync
{
    /// Perform expression evaluation with nothing required to observe the result (sync operation).
    public final func runSync()
    {
        let subscription = subscribe()

        subscription.waitForDisposal()
    }

    /// Perform expression evaluation with notifier and observable (sync operation).
    /// - Parameter consumer: The consumer that is to observe the Notifications from the Observable.
    public final func runSync(consumer: ARxConsumer<ItemOutType>)
    {
        let subscription = subscribe(consumer)

        subscription.waitForDisposal()
    }

    /// Perform expression evaluation to actions (sync operation).
    /// - Parameter itemAction: The action to perform on Item notification.
    /// - Parameter completedAction: The action to perform on completed notification.
    public final func runSync(itemAction: RxTypes<ItemOutType>.RxItemActionDelegate, completedAction : RxTypes<ItemOutType>.RxCompletedActionDelegate? = nil)
    {
        let subscription = subscribe(itemAction, completedAction : completedAction)

        subscription.waitForDisposal()
    }

    /// Perform expression evaluation to notifiers (sync operation).
    /// - Parameter itemNotifier: The notifier be notified on Item notification.
    /// - Parameter completedNotifier: The notifier be notified on completed notification.
    public final func runSync<TargetItemType>(itemNotifier : RxTypes2<ItemOutType, TargetItemType>.RxItemNotifierAction, completedNotifier : RxTypes2<ItemOutType, TargetItemType>.RxCompletedNotifierAction? = nil)
    {
        let subscription = subscribe(itemNotifier, completedNotifier : completedNotifier)

        subscription.waitForDisposal()
    }
}

extension RxObservableMap: RxMSObservables_Primitive
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// throwError: Create a Source that emits and error immediately.
    ///
    /// - [Microsoft Doc Link]( https://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.throw(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/empty-never-throw.htm )
    ///

    /// - Parameter error: The error to emit.
    /// - Returns: The RxObservable that emits the error.

    public final func throwError<TargetItemType>(error: IRxError) -> RxObservableMap<ItemOutType, TargetItemType>
    {
        let tag = "throwError"

        let evalOp = { (evalNode: RxEvalNode<ItemOutType, TargetItemType>) -> Void in

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<TargetItemType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        notifier.notifyCompleted(error)

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservableMap<ItemOutType, TargetItemType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    /// - Parameter error: The error to emit.
    /// - Returns: The RxObservable that emits the error.
    public final func throwError(error: IRxError) -> RxObservable<ItemOutType>
    {
        let tag = "throwError"

        let evalOp : (RxEvalNode<ItemOutType, ItemOutType>) throws -> Void = { (evalNode: RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        notifier.notifyCompleted(error)

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }
}

extension RxObservableMap: RxMSObservables_Boolean
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// All: Determine whether all items emitted by the Observable meet the given predicate.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/06_Inspection.html#All )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh229537(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/all.html )
    ///

    public final func all(predicate : RxTypes<ItemOutType>.RxPredicate) -> RxObservableMap<ItemOutType, Bool>
    {
        typealias TargetItemType = Bool

        let evalOp = { (evalNode : RxEvalNode<ItemOutType, Bool>) -> Void in

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<Bool>) in

                if !predicate(item)
                {
                    notifier.notifyItem(false)
                    notifier.notifyCompleted()
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<Bool>) in

                if error == nil
                {
                    notifier.notifyItem(true)
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservableMap<ItemOutType, Bool>(tag: "all", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Any: Determine whether the Observable emits any items.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/06_Inspection.html#Any )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.any.aspx )
    ///

    public final func any() -> RxObservableMap<ItemOutType, Bool>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, Bool>) -> Void in

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<Bool>) in

                notifier.notifyItem(true)
                notifier.notifyCompleted()
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<Bool>) in

                if error == nil
                {
                    notifier.notifyItem(false)
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservableMap<ItemOutType, Bool>(tag: "any", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// IsEmpty: Determine whether the Observable emits no items.
    ///
    /// - [Microsoft Doc Link]( Use all:(PredicateAction)predicateAction with a predicate that returns false. )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/contains.html )
    ///

    public final func isEmpty() -> RxObservableMap<ItemOutType, Bool>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, Bool>) -> Void in

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<Bool>) in

                notifier.notifyItem(false)
                notifier.notifyCompleted()
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<Bool>) in

                if error == nil
                {
                    notifier.notifyItem(true)
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservableMap<ItemOutType, Bool>(tag: "isEmpty", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// SequenceEqual: Determine whether two Observables emit the same sequence of items.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/06_Inspection.html#SequenceEqual )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.sequenceequal(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/sequenceequal.html )
    ///

    public final func sequenceEqual(other : ARxProducer<ItemOutType>, equateOp: (lhs : ItemOutType, rhs : ItemOutType) -> Bool) -> RxObservableMap<ItemOutType, Bool>
    {
        let tag = "sequenceEqual"
        let evalOp : (RxEvalNode<ItemOutType, Bool>) -> Void = RxEvalOps.gateStreamsMapPat(.eGate_OnAllQueuesHaveAtLeastOneItem, streams : [other], mapEvalOp: { (map: RxGateMapType<ItemOutType, Bool>) in

            map.mapItemsDelegate = { (gate: RxNotificationGate<ItemOutType, Bool>, notifier: ARxNotifier<Bool>) in

                assert(gate.itemCount(0) > 0 && gate.itemCount(1) > 0, "Expected all gate queues to have items")

                if !equateOp(lhs: gate.itemAtQueueIndex(0), rhs: gate.itemAtQueueIndex(1))
                {
                    notifier.notifyItem(false)
                    notifier.notifyCompleted()

                    gate.closeGate()
                }

                gate.popAllQueues()
            }

            map.mapCompletedDelegate = { (index : RxIndexType, error: IRxError?, gate : RxNotificationGate<ItemOutType, Bool>, notifier: ARxNotifier<Bool>) in

                if error == nil
                {
                    notifier.notifyItem(gate.countOfInputQueuesWithSomeItems == 0)
                }

                notifier.notifyCompleted(error)

                gate.closeGate()
            }
        })

        return RxObservableMap<ItemOutType, Bool>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Contains: Determine whether the Observable emits a particular item.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/06_Inspection.html#Contains )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.contains(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/contains.html )
    ///

    public final func contains(containsItem: ItemOutType, equateOp: (lhs : ItemOutType, rhs : ItemOutType) -> Bool) -> RxObservableMap<ItemOutType, Bool>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, Bool>) -> Void in

            var haveEmittedResult = false

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<Bool>) in

                if equateOp(lhs: containsItem, rhs: item)
                {
                    haveEmittedResult = true
                    notifier.notifyItem(true)
                    notifier.notifyCompleted()
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<Bool>) in

                if error == nil
                {
                    if !haveEmittedResult
                    {
                        haveEmittedResult = true
                        notifier.notifyItem(false)
                    }
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservableMap<ItemOutType, Bool>(tag: "contains", evalOp: evalOp).chainObservable(self)
    }
}

extension RxObservableMap: RxMSObservables_Filter
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Distinct: Suppress duplicate item notifications.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/05_Filtering.html#Distinct )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.distinct(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/distinct.html )
    ///

    public final func distinct(hashOp : RxTypes<ItemOutType>.RxHashOp) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var hashedItems = [Int : Bool]()

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                let hash = hashOp(item: item)

                if hashedItems[hash] == nil
                {
                    hashedItems[hash] = true
                    notifier.notifyItem(item)
                }
            }
        }

        return RxObservable<ItemOutType>(tag: "distinct", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// DistinctUntilChanged: Only emit items that differ from their previous item.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/05_Filtering.html#Distinct )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.distinctuntilchanged(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/distinct.html )
    ///

    public final func distinctUntilChanged(equateOp: RxTypes<ItemOutType>.RxEquateOp) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var previousItem : ItemOutType? = nil

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                if (previousItem == nil) || !equateOp(lhs: previousItem!, rhs: item)
                {
                    previousItem = item
                    notifier.notifyItem(item)
                }
            }
        }

        return RxObservable<ItemOutType>(tag: "distinctUntilChanged", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// IgnoreElements: Do not emit any items, only pass over termination notifications.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/05_Filtering.html#IgnoreElements )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh229242(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/ignoreelements.html )
    ///

    public final func ignoreElements() -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                // do nothing.
            }
        }

        return RxObservable<ItemOutType>(tag: "ignoreElements", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Skip: Discard the first given count item notifications.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/05_Filtering.html#SkipAndTake )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh229847(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/skip.html )
    ///

    public final func skip(count : RxCountType) -> RxObservable<ItemOutType>
    {
        assert(count >= 0, "Expected count to be non negative")

        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            if count > 0
            {
                var skippedCount : RxCountType = 0

                evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                    if skippedCount < count
                    {
                        skippedCount += 1
                        return
                    }

                    notifier.notifyItem(item)
                }
            }
        }

        return RxObservable<ItemOutType>(tag: "skip",  evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// SkipLast: Discard the last given count item notifications.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/05_Filtering.html#SkipAndTake )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh211750(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/skiplast.html )
    ///

    public final func skipLast(count : RxCountType) -> RxObservable<ItemOutType>
    {
        assert(count >= 0, "Expected count to be non negative")

        let tag = "skipLast"
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            let buffer = RxNotificationQueue<ItemOutType>(tag: tag)

            if count > 0
            {
                evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                    buffer.queueItem(item)

                    while RxCountType(buffer.itemCount) > count
                    {
                        notifier.notifyItem(buffer.unqueueItem()!)
                    }
                }
            }
        }

        return RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// SkipWhile: Discard item notifications until a given predicate becomes false
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/05_Filtering.html#SkipAndTake )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.skipwhile(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/skipwhile.html )
    ///

    public final func skipWhile(predicate : RxTypes<ItemOutType>.RxPredicate)  -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var inSkipMode = true

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                if !inSkipMode
                {
                    notifier.notifyItem(item)
                }
                else if !predicate(item)
                {
                    inSkipMode = false
                    notifier.notifyItem(item)
                }
            }
        }

        return RxObservable<ItemOutType>(tag: "skipWhile", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// SkipUntil: Discard item notifications until a second Observable emits an item.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/05_Filtering.html#SkipAndTake )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh229358(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/skipuntil.html )
    ///

    public final func skipUntil<OtherOutType>(stream : ARxProducer<OtherOutType>) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var subscription : RxSubscription? = nil
            var amSkipping = true

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                if !amSkipping
                {
                    notifier.notifyItem(item)
                }
            }

            evalNode.stateChangeDelegate = { (stateChange : eRxEvalStateChange, notifier : ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        let streamNotifier = RxNotifier_NotifyDelegates<OtherOutType>(tag: self.tag)

                        streamNotifier.onItem = { (item: OtherOutType) in

                            amSkipping = false

                            subscription?.unsubscribe()

                            subscription = nil
                        }

                        subscription = stream.subscribe(streamNotifier)

                    case eRxEvalStateChange.eEvalEnd:

                        subscription?.unsubscribe()
                        subscription = nil

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservable<ItemOutType>(tag: "skipUntil", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Take: Emit only the first given count item notifications.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/05_Filtering.html#SkipAndTake )
    /// - [Microsoft Doc Link]( https://msdn.microsoft.com/en-us/library/hh212114(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/take.html )
    ///

    public final func take(count : RxCountType) -> RxObservable<ItemOutType>
    {
        assert(count >= 0, "Expected count to be non negative")

        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            switch count
            {
                case _ where count > 0:

                    var itemCount : RxCountType = 0

                    evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                        notifier.notifyItem(item)

                        itemCount += 1

                        if itemCount >= count
                        {
                            notifier.notifyCompleted()
                        }
                    }

                case 0:

                    evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                        switch stateChange
                        {
                            case eRxEvalStateChange.eEvalBegin:
                                notifier.notifyCompleted()

                            default:
                                // do nothing.

                                break
                        }
                    }

                default:
                    assert(false, "Expected count to be zero or positive")
            }
        }

        return RxObservable<ItemOutType>(tag: "take", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// TakeLast: Emit only the final given count item notifications.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/05_Filtering.html#SkipAndTake )
    /// - [Microsoft Doc Link]( https://msdn.microsoft.com/en-us/library/hh212114(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/takelast.html )
    ///

    public final func takeLast(count : RxCountType) -> RxObservable<ItemOutType>
    {
        assert(count >= 0, "Expected count to be non negative")

        let tag = "takeLast"
        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            switch count
            {
                case _ where count > 0:

                    let buffer = RxNotificationQueue<ItemOutType>(tag: tag)

                    evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType>) in

                        buffer.queueItem(item)

                        if buffer.itemCount > count
                        {
                            buffer.popCount(buffer.itemCount - count)
                        }
                    }

                    evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                        if error == nil
                        {
                            buffer.sendItemsToNotifier(notifier)
                        }

                        notifier.notifyCompleted(error)
                    }

                case 0:

                    evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType>) in

                        notifier.notifyCompleted()
                    }

                default:
                    assert(false, "Expected count to be zero or positive")
            }
        }

        return RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// TakeULasteBuffer: Emit the last given item notifications, as a single list item.
    ///
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/takelast.html )
    ///

    public final func takeLastBuffer(count : RxCountType) -> RxObservableMap<ItemOutType, [ItemOutType]>
    {
        assert(count >= 0, "Expected count to be non negative")

        typealias TargetItemType = [ItemOutType]

        let tag = "takeLastBuffer"
        let evalOp = { (evalNode: RxEvalNode<ItemOutType, TargetItemType>) -> Void in

            let notificationQueue = RxNotificationQueue<ItemOutType>(tag : tag)

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<TargetItemType>) in

                if notificationQueue.itemCount >= count
                {
                    notificationQueue.popItem()
                }

                notificationQueue.queueItem(item)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<TargetItemType>) in

                if error != nil
                {
                    notifier.notifyCompleted(error)
                }
                else
                {
                    notificationQueue.sendItemsAsListToNotifier(notifier, count: notificationQueue.itemCount, skip : 0)
                    notifier.notifyCompleted(nil)
                }
            }
        }

        return RxObservableMap<ItemOutType, TargetItemType>(tag: "takeLastBuffer", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// TakeWhile: Emit item notifications until a given predicate becomes false.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/05_Filtering.html#SkipAndTake )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.takewhile(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/takewhile.html )
    ///

    public final func takeWhile(predicate : RxTypes<ItemOutType>.RxPredicate) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in
            
            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in
                
                if predicate(item)
                {
                    notifier.notifyItem(item)
                }
                else
                {
                    notifier.notifyCompleted()
                }
            }
        }
        
        return RxObservable<ItemOutType>(tag: "takeWhile", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// TakeUntil: Emit item notifications until a second Observable emits an item
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/05_Filtering.html#SkipAndTake )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh229530(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/takeuntil.html )
    ///

    public final func takeUntil<OtherItemType>(stream : ARxProducer<OtherItemType>) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemType,ItemType>) -> Void in
            
            var streamSubscription: RxSubscription? = nil

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemType>) in

                notifier.notifyCompleted(error)
            }

            evalNode.stateChangeDelegate = { (stateChange : eRxEvalStateChange, notifier : ARxNotifier<ItemType>) in
                
                switch stateChange
                {
                case eRxEvalStateChange.eEvalBegin:

                    // Subscribe to the other stream.
                    // Note: errors on the other stream are ignored.
                    let streamNotifier = RxNotifier_NotifyDelegates<OtherItemType>(tag: self.tag)
                    
                    streamNotifier.onItem = { (item: OtherItemType) in

                        evalNode.evalQueue.dispatchAsync({
                            // When an item is notified on the other stream, complete the in-line subscription.
                            notifier.notifyCompleted()
                        })
                    }

                    streamSubscription = stream.subscribe(streamNotifier)

                case eRxEvalStateChange.eEvalEnd:

                    streamSubscription?.unsubscribe()
                    streamSubscription = nil

                default:
                    // do nothing.
                    break
                }
            }
        }
        
        return RxObservable<ItemType>(tag: "takeUntil", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// ElementAt: Emit only the item notifications of the given index.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/06_Inspection.html#ElementAt )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh229725(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/elementat.html )
    ///

    public final func elementAt(index : Int) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var curentIndex = 0

            if index >= 0
            {
                evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                    if curentIndex == index
                    {
                        notifier.notifyItem(item)
                        notifier.notifyCompleted()
                    }

                    curentIndex += 1
                }
            }
        }

        return RxObservable<ItemOutType>(tag: "elementAt", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// ElementAtOrDefault: Emit only the item notifications of the given index, if none exist then emit the given default.
    ///
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh229845(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/elementat.html )
    ///

    public final func elementAtOrDefault(index : Int, defaultItem: ItemOutType) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var currentIndex = 0

            if index >= 0
            {
                evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                    if currentIndex == index
                    {
                        notifier.notifyItem(item)
                        notifier.notifyCompleted()
                    }

                    currentIndex += 1
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if error != nil
                {
                    notifier.notifyCompleted(error)
                }
                else
                {
                    notifier.notifyItem(defaultItem)
                    notifier.notifyCompleted(nil)
                }
            }
        }

        return RxObservable<ItemOutType>(tag: "elementAtOrDefault", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Where: Emit only those item notifications that pass a given predicate.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/05_Filtering.html#Where )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.where(v=vs.103).aspx )
    /// - [Netflix Doc Link]( Netflix call this filter:  http://reactivex.io/documentation/operators/filter.html )
    ///

    public final func whereByPredicate(predicate : RxTypes<ItemOutType>.RxPredicate) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                if predicate(item)
                {
                    notifier.notifyItem(item)
                }
            }
        }

        return RxObservable<ItemOutType>(tag: "whereByPredicate", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// DefaultIfEmpty: Emit item notifications, if none exist then emit the given default item.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/06_Inspection.html#DefaultIfEmpty )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.defaultifempty(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/defaultifempty.html )
    ///

    public final func defaultIfEmpty(defaultItem: ItemOutType) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var hasItems = false

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                hasItems = true
                notifier.notifyItem(item)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if !hasItems && (error == nil)
                {
                    notifier.notifyItem(defaultItem)
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservable<ItemOutType>(tag: "defaultIfEmpty", evalOp: evalOp).chainObservable(self)
    }
}

extension RxObservableMap: RxMSObservables_EventHooks
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Do: Perform a given item or completed action for each item notification.
    ///
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.do(v=vs.103).aspx )
    /// - [Netflix Doc Link]( Netflix calls this do http://reactivex.io/documentation/operators/do.html )
    ///

    public final func doOnNotify(onItem: RxTypes<ItemOutType>.RxItemActionDelegate?, onCompleted: RxTypes<ItemOutType>.RxCompletedActionDelegate? = nil) -> RxObservable<ItemOutType>
    {
        let extractNotifier = RxNotifier_NotifyDelegates<ItemOutType>(tag: "doOnNotify")
        let evalOp = RxEvalOps.extractPat(extractNotifier)

        if onItem != nil
        {
            extractNotifier.onItem = onItem!
        }

        if onCompleted != nil
        {
            extractNotifier.onCompleted = onCompleted!
        }

        return RxObservable<ItemOutType>(tag: "doOnNotify", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Do:  Notify a given notifier of received notifications.
    ///
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.do(v=vs.103).aspx )
    /// - [Netflix Doc Link]( Netflix calls this doOnEach: http://reactivex.io/documentation/operators/do.html )
    ///

    public final func doOnNotify(doNotifier : ARxNotifier<ItemOutType>) -> RxObservable<ItemOutType>
    {
        let evalOp = RxEvalOps.extractPat(doNotifier)

        return RxObservable<ItemOutType>(tag: "doOnNotify", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Finally: Perform an action upon the completion or error notification.
    ///
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh212133(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/do.html )
    ///

    public final func finally(action : RxAction) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if error == nil
                {
                    action()
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservable<ItemOutType>(tag: "finally",  evalOp: evalOp).chainObservable(self)
    }
}

extension RxObservableMap: RxMSObservables_Transform
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Select: Given an item mapping function, emit the mapped items.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/08_Transformation.html#Select )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.select(v=vs.103).aspx )
    /// - [Netflix Doc Link]( Netflix call this map:  http://reactivex.io/documentation/operators/map.html )
    ///

    public final func select<TargetItemType>(mapAction : RxTypes2<ItemOutType, TargetItemType>.RxItemMapOptionalFunc) -> RxObservableMap<ItemOutType, TargetItemType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, TargetItemType>) -> Void in

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<TargetItemType>) in

                if let mappedItem = mapAction(item)
                {
                    notifier.notifyItem(mappedItem)
                }
            }
        }

        return RxObservableMap<ItemOutType, TargetItemType>(tag: "select", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// SelectMany Documentation:
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/08_Transformation.html#SelectMany )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.selectmany(v=vs.103).aspx )
    /// - [Netflix Doc Link]( Netflix call this flatmap:  http://reactivex.io/documentation/operators/flatmap.html )
    ///

    /*
    public final func selectMany(evalAction : RxEvalAction) -> RxObservable<ItemOutType>
    {
        let evalOp : ((RxEvalNode<ItemOutType, ItemOutType>) -> Void) = RxEvalOps.noOpPat()

        // To be implemented.

        return RxObservable<ItemOutType>(tag: "selectMany", evalOp: evalOp).chainObservable(self)
    }
    */

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Scan:  Given a aggregation function, emit items that are the results of the aggregation function.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#Scan )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.scan(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/scan.html )
    ///

    public final func scan(startItem: ItemOutType, accumulator : RxTypes<ItemOutType>.RxItemMapFunc) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var currentAccumulator = startItem

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                currentAccumulator = accumulator(currentAccumulator, item)

                notifier.notifyItem(currentAccumulator)
            }
        }

        return RxObservable<ItemOutType>(tag: "scan", evalOp: evalOp).chainObservable(self)
    }
}

extension RxObservableMap: RxMSObservables_Combine
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Merge: Given another stream combine the streams by emitting the items as they are notified.
    ///
    /// Given another stream combine the streams by emitting the items as they are notified.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/12_CombiningSequences.html#Merge )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.merge(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/merge.html )
    ///

    public final func merge(stream : ARxProducer<ItemOutType>) -> RxObservable<ItemOutType>
    {
        let streams = [stream]
        var activeStreamCount = streams.count + 1

        let evalOp : ((RxEvalNode<ItemOutType, ItemOutType>) -> Void) = RxEvalOps.switchStreamsMapPat(streams, mapEvalOp: { (map: RxSwitchMapType<ItemOutType, ItemOutType>) in

            map.mapItemDelegate = { (index : RxIndexType, item : ItemType, switcher: RxNotificationSwitch<ItemOutType, ItemOutType>, notifier: ARxNotifier<ItemOutType>) in

                notifier.notifyItem(item)
            }

            map.mapCompletedDelegate = { (index : RxIndexType, error: IRxError?, switcher: RxNotificationSwitch<ItemOutType, ItemOutType>, notifier: ARxNotifier<ItemOutType>) in

                activeStreamCount -= 1

                if (error != nil) || (activeStreamCount == 0)
                {
                    notifier.notifyCompleted(error)
                }
            }
        })

        return RxObservable<ItemOutType>(tag: "merge", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Concat: Given another stream concatenate the items by emitting all items of the first to emit and then the other.
    ///
    /// Given another stream concatenate the items by emitting all items of the first to emit and then the other.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/12_CombiningSequences.html#Concat )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.concat(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/concat.html )
    ///

    public final func concat(stream : ARxProducer<ItemOutType>) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            // Hold onto the subscription.
            var subscription : RxSubscription? = nil

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if error == nil
                {
                    subscription = stream.subscribe(notifier)

                    // Remove the compiler warning that subscription is never read.
                    assert(subscription != nil)
                }
                else
                {
                    notifier.notifyCompleted(error)
                }
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalEnd:

                        subscription = nil

                    default:
                        // do nothing.
                        break
                }
            }

            #if RxMonEnabled
                if let traceTag = evalNode.traceTag
                {
                    stream.traceAll(traceTag)
                }
            #endif
        }

        return RxObservable<ItemOutType>(tag: "concat", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// CombineLatest: Given another stream and selector func, combine the latest items and emit an item based on the selector function.
    ///
    /// Given another stream and selector func, combine the latest items and emit an item based on the selector function.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/12_CombiningSequences.html#CombineLatest )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh211991(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/combinelatest.html )
    ///

    public final func combineLatest<TargetItemType>(stream : ARxProducer<ItemOutType>, selector: (ItemOutType, ItemOutType) -> TargetItemType?) -> RxObservableMap<ItemOutType, TargetItemType>
    {
        let evalOp : ((RxEvalNode<ItemOutType, TargetItemType>) -> Void) = RxEvalOps.gateStreamsMapPat(.eGate_OnLatestItem, streams : [stream], mapEvalOp: { (map: RxGateMapType<ItemOutType, TargetItemType>) in

            map.mapItemsDelegate = { (gate: RxNotificationGate<ItemOutType, TargetItemType>, notifier: ARxNotifier<TargetItemType>) in

                gate.popAllQueuesWithExtraItems()

                let (item1, item2) = (gate.itemAtQueueIndex(0), gate.itemAtQueueIndex(1))

                if let combinedItem = selector(item1, item2)
                {
                    notifier.notifyItem(combinedItem)
                }
            }

            map.mapCompletedDelegate = { (index : RxIndexType, error: IRxError?, gate : RxNotificationGate<ItemOutType, TargetItemType>, notifier: ARxNotifier<TargetItemType>) in

                notifier.notifyCompleted(error)
            }
        })

        return RxObservableMap<ItemOutType, TargetItemType>(tag: "combineLatest", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Zip: Given another stream and a selector function, emit the results of the selector on notified items in sequence.
    ///
    /// Given another stream and a selector function, emit the results of the selector on notified items in sequence.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/12_CombiningSequences.html#Zip )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.zip(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/zip.html )
    ///

    public final func zip<TargetItemType>(stream : ARxProducer<ItemOutType>, selector : (ItemOutType, ItemOutType) -> TargetItemType?) -> RxObservableMap<ItemOutType, TargetItemType>
    {
        let evalOp : (RxEvalNode<ItemOutType, TargetItemType>) -> Void = RxEvalOps.gateStreamsMapPat(.eGate_OnAllQueuesHaveAtLeastOneItem, streams : [stream], mapEvalOp: { (map: RxGateMapType<ItemOutType, TargetItemType>) in

            map.mapItemsDelegate = { (gate: RxNotificationGate<ItemOutType, TargetItemType>, notifier: ARxNotifier<TargetItemType>) in

                assert(gate.itemCount(0) > 0 && gate.itemCount(1) > 0, "Expected all gate queues to have items")

                if let zippedItem = selector(gate.itemAtQueueIndex(0), gate.itemAtQueueIndex(1))
                {
                    notifier.notifyItem(zippedItem)
                }

                gate.popAllQueues()
            }

            map.mapCompletedDelegate = { (index : RxIndexType, error: IRxError?, gate : RxNotificationGate<ItemOutType, TargetItemType>, notifier: ARxNotifier<TargetItemType>) in

                notifier.notifyCompleted(error)
            }
        })

        return RxObservableMap<ItemOutType, TargetItemType>(tag: "zip", evalOp: evalOp).chainObservable(self)
    }
}

extension RxObservableMap: RxMSObservables_Timing
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Buffer: Periodically gather item notifications into bundles and emit these bundles rather than emitting the items one at a time.
    ///         Bundles are defined by count and or time windows.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/13_TimeShiftedSequences.html#Buffer )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.buffer(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/buffer.html )
    ///
    public final func buffer(count : RxCountType) -> RxObservableMap<ItemOutType, [ItemOutType]>
    {
        return buffer(count, skip: count)
    }

    ///
    /// Buffer: Periodically gather item notifications into bundles and emit these bundles rather than emitting the items one at a time.
    ///         Bundles are defined by count and or time windows.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/13_TimeShiftedSequences.html#Buffer )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.buffer(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/buffer.html )
    ///

    public final func buffer(duration : RxDuration) -> RxObservableMap<ItemOutType, [ItemOutType]>
    {
        return buffer(duration, timeShift: duration)
    }

    ///
    /// Buffer-count: Periodically gather item notifications into bundles defined by count.
    ///

    public final func buffer(count : RxCountType, skip : RxCountType) -> RxObservableMap<ItemOutType, [ItemOutType]>
    {
        let tag = "buffer"

        let evalOp = { (evalNode : RxEvalNode<ItemOutType, [ItemOutType]>) -> Void in

            let notifications = RxNotificationQueue<ItemOutType>(tag : tag)
            var effectiveSkip = 0

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<[ItemOutType]>) in

                notifications.queueItem(item)

                if notifications.itemCount >= count + effectiveSkip
                {
                    notifications.sendItemsAsListToNotifier(notifier, count : count, skip : effectiveSkip)

                    effectiveSkip = skip
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<[ItemOutType]>) in

                if error == nil
                {
                    let remainingCount = notifications.itemCount - effectiveSkip

                    if remainingCount > 0
                    {
                        notifications.sendItemsAsListToNotifier(notifier, count : remainingCount, skip : effectiveSkip)
                    }
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservableMap<ItemOutType, [ItemOutType]>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    ///
    /// Buffer-duration: Periodically gather item notifications into bundles defined by time windows time-shifted.
    ///
    public final func buffer(duration : RxDuration, timeShift : RxTimeOffset) -> RxObservableMap<ItemOutType, [ItemOutType]>
    {
        let tag = "buffer"

        switch duration
        {
            case _ where duration < 0:
                // Error case.
                return throwError(RxLibError(.eInvalidTimePeriod))

            default:
                break
        }

        let evalOp = { (evalNode : RxEvalNode<ItemOutType, [ItemOutType]>) -> Void in

            var startTime : RxTime? = nil
            let emitNotifier = evalNode.syncOutNotifier
            let notifications = RxNotificationSequence<ItemOutType>(tag : tag)
            let window = RxWindow(tag : tag + "/window")
            var effectiveTimeShift : RxTimeOffset = duration
            var iteration = 0

            let windowAction : (RxWindow, eWindowEvent) -> Void = { (currentWindow : RxWindow, windowEvent : eWindowEvent) in

                switch windowEvent
                {
                    case .eWindowStart:
                        // do nothing
                        break

                    case .eWindowEnd(let (_, timeError)):

                        #if RxMonEnabled
                            // Check if the window is out of time.
                            if abs(timeError) > duration
                            {
                                if let mon = RxSDK.mon.trace
                                {
                                    // Output a trace message to console.
                                    RxLog.log(mon.traceMessageBuffer)

                                    mon.clearTraceBuffer()
                                }
                            }
                        #endif

                        let baseTimeOffset = RxTimeOffset(iteration) * duration
                        let itemsToEmit = notifications.timeSlice(startTime!, duration: baseTimeOffset + duration, timeShift: baseTimeOffset + duration - effectiveTimeShift)

                        emitNotifier.notifyItem(itemsToEmit)

                        effectiveTimeShift = timeShift

                        iteration += 1
                }
            }

            // Configure the window.
            window <- .eSetEvalQueue(evalNode.evalQueue) <- .eSetWindowAction(windowAction)

            // Request the source start time, as a reference time base for stamping.
            evalNode.supportOptionRequests = [oRxEvalNodeSupport_Options.oSourceStartTime]

            evalNode.supportOptionReplyDelegate = { (reply: eRxEvalNodeSupport_Reply) in

                switch reply
                {
                    case .eSourceStartTime(let sourceStartTime):
                        startTime = sourceStartTime

                    default:
                        // do nothing.
                        break
                }
            }

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<[ItemOutType]>) in

                if duration > 0
                {
                    notifications.queueItem(item)
                }
                else
                {
                    notifier.notifyItem([item])
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<[ItemOutType]>) in

                window.stopCurrentWindow()

                if (error == nil) && (duration > 0)
                {
                    let baseTimeOffset = RxTimeOffset(iteration) * duration
                    let itemsToEmit = notifications.timeSlice(startTime!, duration: baseTimeOffset + duration, timeShift: baseTimeOffset + duration - effectiveTimeShift)

                    if itemsToEmit.count > 0
                    {
                        notifier.notifyItem(itemsToEmit)
                    }

                    iteration += 1
                }

                notifier.notifyCompleted(error)
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<[ItemOutType]>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        notifications.startTime = startTime!

                        if duration > 0
                        {
                            window.createPeriodicWindow(startTime!, startOffset: 0, period: duration)
                        }

                    case eRxEvalStateChange.eEvalEnd:

                        window.cancelAll()

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservableMap<ItemOutType, [ItemOutType]>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    ///
    /// Buffer-count-duration: Periodically gather item notifications into bundles defined by time windows and count.
    ///
    public final func buffer(count : RxCountType, duration : RxDuration) -> RxObservableMap<ItemOutType, [ItemOutType]>
    {
        let tag = "buffer"

        switch duration
        {
            case _ where duration < 0:
                // Error case.
                return throwError(RxLibError(.eInvalidTimePeriod))

            default:
                break
        }

        let evalOp = { (evalNode : RxEvalNode<ItemOutType, [ItemOutType]>) -> Void in

            var windowIteration = 0
            var startTime : RxTime? = nil
            let emitNotifier = evalNode.syncOutNotifier
            let notifications = RxNotificationSequence<ItemOutType>(tag : tag)
            let window = RxWindow(tag : tag + "/window")
            let windowAction : (RxWindow, eWindowEvent) -> Void = { (currentWindow : RxWindow, windowEvent : eWindowEvent) in

                evalNode.evalQueue.dispatchAsync({

                    switch windowEvent
                    {
                        case .eWindowStart:
                            // do nothing
                            break

                        case .eWindowEnd:
                            let baseTimeOffset = RxTimeOffset(windowIteration) * duration
                            let itemsToEmit = notifications.timeSlice(startTime!, duration: baseTimeOffset + duration)

                            emitNotifier.notifyItem(itemsToEmit)
                            windowIteration += 1
                    }
                })
            }

            // Configure the window.
            window <- .eSetEvalQueue(evalNode.evalQueue) <- .eSetWindowAction(windowAction)

            // Request the source start time, as a reference time base for stamping.
            evalNode.supportOptionRequests = [oRxEvalNodeSupport_Options.oSourceStartTime]

            evalNode.supportOptionReplyDelegate = { (reply: eRxEvalNodeSupport_Reply) in

                switch reply
                {
                    case .eSourceStartTime(let sourceStartTime):
                        startTime = sourceStartTime

                    default:
                        // do nothing.
                        break
                }
            }

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<[ItemOutType]>) in

                if count > 0
                {
                    notifications.queueItem(item)

                    if notifications.itemCount >= count
                    {
                        notifications.sendItemsAsListToNotifier(notifier, count : count)
                    }
                }
                else if duration > 0
                {
                    notifications.queueItem(item)
                }
                else
                {
                    notifier.notifyItem([item])
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<[ItemOutType]>) in

                window.stopCurrentWindow()

                if error == nil
                {
                    let remainingCount = notifications.itemCount

                    if remainingCount > 0
                    {
                        notifications.sendItemsAsListToNotifier(notifier, count : remainingCount)
                    }
                }

                notifier.notifyCompleted(error)
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<[ItemOutType]>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        if duration > 0
                        {
                            window.createPeriodicWindow(startTime!, startOffset: 0, period: duration)
                        }

                    case eRxEvalStateChange.eEvalEnd:

                        window.cancelAll()

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservableMap<ItemOutType, [ItemOutType]>(tag: tag, evalOp: evalOp).chainObservable(self)
    }


    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Delay: Shift item notifications forward in time by an time offset.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/13_TimeShiftedSequences.html#Delay )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.delay(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/delay.html )
    ///
    ///
    public final func delay(duration : RxDuration) -> RxObservable<ItemOutType>
    {
        let tag = "delay"

        switch duration
        {
            case _ where duration < 0:
                // Error case.
                return throwError(RxLibError(.eInvalidTimePeriod))

            case 0:
                // Default behaviour case.
                return RxObservable<ItemOutType>(tag: tag, evalOp: nil).chainObservable(self)

            default:
                break
        }

        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            let timer = RxTimer(tag: tag + "/timer")

            func delayAction(action: RxAction)
            {
                timer.addTimer(RxTime(timeIntervalSinceNow: duration), timerAction: { [unowned evalNode] (index: RxIndexType) in

                    evalNode.evalQueue.dispatchAsync(action)
                })
            }

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType>) in

                delayAction({ notifier.notifyItem(item) })
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if !timer.isEmpty
                {
                    delayAction({ notifier.notifyCompleted(error) })
                }
                else
                {
                    notifier.notifyCompleted(error)
                }
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalEnd:

                        timer.cancelAll()

                    default:
                        // do nothing.
                        break
                }
            }

            #if RxMonEnabled
                if evalNode.traceTag != nil
                {
                    evalNode.traceDependant(timer)
                }
            #endif
        }

        return RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Delay: Shift item notifications forward in time to a specified time.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/13_TimeShiftedSequences.html#Delay )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.delay(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/delay.html )
    ///

    public final func delayTo(dueTime: RxTime) -> RxObservable<ItemOutType>
    {
        let tag = "delayTo"
        let timeOffset = dueTime.timeIntervalSinceNow

        switch timeOffset
        {
            case _ where timeOffset <= 0:
                // In the past, so perform default behaviour.
                return RxObservable<ItemOutType>(tag: tag, evalOp: nil).chainObservable(self)

            default:
                break
        }

        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var effectiveDelay : RxDuration? = nil
            var startTime = RxTime()
            let timer = RxTimer(tag: tag + "/timer")

            func delayAction(action : RxAction)
            {
                if effectiveDelay == nil
                {
                    effectiveDelay = dueTime.timeIntervalSinceDate(startTime)
                }

                timer.addTimer(RxTime(timeIntervalSinceNow: effectiveDelay!), timerAction: { (index: RxIndexType) in

                    evalNode.evalQueue.dispatchAsync({ action() })
                })
            }

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType>) in

                delayAction({ notifier.notifyItem(item) })
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if !timer.isEmpty
                {
                    delayAction({ notifier.notifyCompleted(error) })
                }
                else
                {
                    notifier.notifyCompleted(error)
                }
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalEnd:

                        timer.cancelAll()

                    default:
                        // do nothing.
                        break
                }
            }

            #if RxMonEnabled
                if evalNode.traceTag != nil
                {
                    evalNode.traceDependant(timer)
                }
            #endif
        }

        return RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Sample: Emit the most recent item notifications within periodic time intervals.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/13_TimeShiftedSequences.html#Sample )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.sample(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/sample.html )
    ///

    public final func sample(duration : RxDuration) -> RxObservable<ItemOutType>
    {
        let tag = "sample"

        switch duration
        {
            case _ where duration < 0:
                // Error case.
                return throwError(RxLibError(.eInvalidTimePeriod))

            case 0:
                // Default behaviour case.
                return RxObservable<ItemOutType>(tag: tag, evalOp: nil).chainObservable(self)

            default:
                break
        }

        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var startTime : RxTime? = nil
            var lastItem : ItemOutType? = nil
            var termination : etTerminationType? = nil
            let emitNotifier = evalNode.syncOutNotifier

            let window = RxWindow(tag : tag + "/window")

            let windowAction : () -> Void = {

                if let lastItem = lastItem
                {
                    emitNotifier.notifyItem(lastItem)
                }

                if let termination = termination
                {
                    emitNotifier.notifyTermination(termination)
                    window.stopCurrentWindow()
                }

                lastItem = nil
            }

            // Configure the window.
            window <- .eSetEvalQueue(evalNode.evalQueue) <- .eSetWindowEndAction(windowAction)

            // Request the source start time, as a reference time base for sampling.
            evalNode.supportOptionRequests = [oRxEvalNodeSupport_Options.oSourceStartTime]

            evalNode.supportOptionReplyDelegate = { (reply: eRxEvalNodeSupport_Reply) in

                switch reply
                {
                    case .eSourceStartTime(let sourceStartTime):
                        startTime = sourceStartTime

                    default:
                        // do nothing.
                        break
                }
            }

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType>) in

                lastItem = item
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<ItemOutType>) in

                if window.isActive
                {
                    termination = etTerminationType(error: error)
                }
                else
                {
                    notifier.notifyCompleted(error)
                }
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        window.createPeriodicWindow(startTime!, startOffset:0, period: duration)

                    case eRxEvalStateChange.eEvalEnd:

                        window.cancelAll()

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Timeout: Emit item notifications, but issue an error if none are received after a given elapsed time period.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/13_TimeShiftedSequences.html#Timeout )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.timeout(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/timeout.html )
    ///

    public final func timeout(timeout : RxTimeOffset) -> RxObservable<ItemOutType>
    {
        let tag = "timeout"

        switch timeout
        {
            case _ where timeout < 0:
                // Error case.
                return throwError(RxLibError(.eInvalidTimePeriod))

            case 0:
                // Default behaviour case.
                return RxObservable<ItemOutType>(tag: tag, evalOp: nil).chainObservable(self)

            default:
                break
        }

        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            let emitNotifier = evalNode.syncOutNotifier
            let window = RxWindow(tag : tag + "/window")

            var hasCompleted = false

            let windowAction : (RxWindow, eWindowEvent) -> Void = { (currentWindow : RxWindow, windowEvent : eWindowEvent) in

                switch windowEvent
                {
                    case .eWindowStart:
                        // do nothing
                        break

                    case .eWindowEnd:

                        currentWindow.enabled = false

                        if !hasCompleted
                        {
                            hasCompleted = true
                            emitNotifier.notifyCompleted(RxLibError(.eTimeout))
                        }
                }
            }

            // Configure the window.
            window <- .eSetWindowAction(windowAction) <- .eSetEvalQueue(evalNode.evalQueue)

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType>) in

                window.stopCurrentWindow()
                window.createSingleWindow(RxTime(), duration: timeout)

                notifier.notifyItem(item)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<ItemOutType>) in

                window.stopCurrentWindow()

                if !hasCompleted
                {
                    hasCompleted = true
                    notifier.notifyCompleted(error)
                }
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        window.createSingleWindow(RxTime(), duration: timeout)

                    case eRxEvalStateChange.eEvalEnd:

                        window.cancelAll()

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Timeout: Emit item notifications, if none are received after a given elapsed time period, substitute the given sequence.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/13_TimeShiftedSequences.html#Timeout )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.timeout(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/timeout.html )
    ///

    public final func timeout(timeout : RxTimeOffset, other : ARxProducer<ItemOutType>) -> RxObservable<ItemOutType>
    {
        let tag = "timeout"

        switch timeout
        {
            case _ where timeout < 0:
                // Error case.
                return throwError(RxLibError(.eInvalidTimePeriod))

            case 0:
                // Default behaviour case.
                return RxObservable<ItemOutType>(tag: tag, evalOp: nil).chainObservable(self)

            default:
                break
        }

        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            let emitNotifier = evalNode.syncOutNotifier

            var acceptInlineNotifications = true
            var subscription : RxSubscription? = nil
            let window = RxWindow(tag : tag + "/window")
            let windowAction : (RxWindow, eWindowEvent) -> Void = { (currentWindow : RxWindow, windowEvent : eWindowEvent) in

                switch windowEvent
                {
                    case .eWindowStart:
                        // do nothing
                        break

                    case .eWindowEnd:

                        acceptInlineNotifications = false

                        subscription = other.subscribe(emitNotifier)
                        assert(subscription != nil)
                }
            }

            // Configure the window.
            window <- .eSetWindowAction(windowAction) <- .eSetEvalQueue(evalNode.evalQueue)

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType>) in

                if acceptInlineNotifications
                {
                    window.stopCurrentWindow()
                    window.createSingleWindow(RxTime(), duration: timeout)

                    notifier.notifyItem(item)
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<ItemOutType>) in

                if acceptInlineNotifications
                {
                    window.stopCurrentWindow()

                    notifier.notifyCompleted(error)
                }
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        window.createSingleWindow(RxTime(), duration: timeout)

                    case eRxEvalStateChange.eEvalEnd:

                        window.cancelAll()
                        subscription = nil

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Throttle: Emit all item notifications after a particular timespan has passed without receiving a notification.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/13_TimeShiftedSequences.html#Throttle )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.throttle(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/debounce.html )
    ///

    public final func throttle(dueTimeOffset: RxTimeOffset) -> RxObservable<ItemOutType>
    {
        let tag = "throttle"

        if dueTimeOffset <= 0
        {
            // If the dueTimeOffset is less than or equal to zero just relay the notifications.
            return RxObservable<ItemOutType>(tag: tag, evalOp: nil).chainObservable(self)
        }

        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var lastItemTimestamp : RxTime? = nil
            var scheduledItem : ItemOutType? = nil

            var timer = RxTimer(tag: tag + "/timer")
            var timerSubscription : RxTimerSubscription? = nil

            @inline(__always) func notifyLastItem(notifier: ARxNotifier<ItemOutType>)
            {
                if scheduledItem != nil
                {
                    notifier.notifyItem(scheduledItem!)

                    scheduledItem = nil
                    lastItemTimestamp = RxTime()
                }
            }

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType>) in

                if lastItemTimestamp == nil
                {
                    // Case: Emit the first item.

                    lastItemTimestamp = RxTime()

                    notifier.notifyItem(item)
                }
                else if timerSubscription == nil
                {
                    // Case: Schedule a timer request to perform notification of this item.

                    let evalQueue = evalNode.evalQueue

                    scheduledItem = item

                    timerSubscription = timer.addTimer(RxTime(timeIntervalSinceNow: dueTimeOffset), timerAction: { [weak evalQueue, weak notifier] (index: RxIndexType) in

                        if let evalQueue = evalQueue, notifier = notifier
                        {
                            evalQueue.dispatchAsync({notifyLastItem(notifier) })
                        }

                        timerSubscription = nil
                    })
                }
                else
                {
                    // Case: An item was scheduled to be emitted at the timeout and a new one was received before that occured. So update the scheduled item.

                    scheduledItem = item
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                timer.cancelAll()

                if scheduledItem != nil { notifyLastItem(notifier) }

                notifier.notifyCompleted(error)
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        scheduledItem = nil
                        lastItemTimestamp = nil

                    case eRxEvalStateChange.eEvalEnd:

                        scheduledItem = nil
                        timerSubscription = nil
                        timer.cancelAll()

                    default:
                        // do nothing.
                        break
                }
            }

            #if RxMonEnabled
                if evalNode.traceTag != nil
                {
                    evalNode.traceDependant(timer)
                }
            #endif
        }

        return RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Window: Periodically subdivide item notifications into Observable windows and emit those windows rather than emitting the items one at a time.
    ///         Windows are defined by item counts.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/17_SequencesOfCoincidence.html#Window )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.window(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/window.html )
    ///

    public final func window(count : RxCountType, skip : RxCountType) -> RxObservableMap<ItemOutType, RxSource<ItemOutType>>
    {
        typealias TargetItemType = RxSource<ItemOutType>

        let tag = "window"
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, TargetItemType>) -> Void in

            var notifications = RxNotificationQueue<ItemOutType>(tag : tag)
            var effectiveSkip = 0

            func emit(notifier : ARxNotifier<TargetItemType>, inout notifications : RxNotificationQueue<ItemOutType>, count : RxCountType, skip : RxCountType)
            {
                let itemsToEmit = notifications.rangeSlice(Range<RxIndexType>(skip...count - 1 + skip))
                let notificationsToEmit = RxNotificationQueue<ItemOutType>(tag:  notifications.tag, itemsThenCompletion : itemsToEmit)

                notifications.popCount(count)
                notifier.notifyItem(notificationsToEmit.toObservable(RxSource<ItemOutType>.self))
            }

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<TargetItemType>) in

                notifications.queueItem(item)

                if notifications.itemCount >= count + effectiveSkip
                {
                    emit(notifier, notifications: &notifications, count: count, skip: effectiveSkip)
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<TargetItemType>) in

                let remainingCount = notifications.itemCount - effectiveSkip

                if remainingCount > 0
                {
                    emit(notifier, notifications: &notifications, count: remainingCount, skip: effectiveSkip)
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservableMap<ItemOutType, TargetItemType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Window: Periodically subdivide item notifications into Observable windows and emit those windows rather than emitting the items one at a time.
    ///         Windows are defined by item duration.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/17_SequencesOfCoincidence.html#Window )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.window(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/window.html )
    ///

    public final func window(duration : RxDuration) -> RxObservableMap<ItemOutType, RxSource<ItemOutType>>
    {
        typealias TargetItemType = RxSource<ItemOutType>

        let tag = "window"

        switch duration
        {
            case _ where duration < 0:
                // Error case.
                return throwError(RxLibError(.eInvalidTimePeriod))

            default:
                break
        }

        let evalOp = { (evalNode : RxEvalNode<ItemOutType, RxSource<ItemOutType>>) -> Void in

            func emit(notifier : ARxNotifier<TargetItemType>, inout notifications : RxNotificationQueue<ItemOutType>)
            {
                let count = notifications.itemCount
                let itemsToEmit = notifications.rangeSlice(0..<count)
                let notificationsToEmit = RxNotificationQueue<ItemOutType>(tag:  notifications.tag, itemsThenCompletion : itemsToEmit)

                notifications.popCount(count)
                notifier.notifyItem(notificationsToEmit.toObservable(RxSource<ItemOutType>.self))
            }

            let emitNotifier = evalNode.syncOutNotifier
            var startTime: RxTime? = nil
            var notifications = RxNotificationQueue<ItemOutType>(tag : tag)
            let window = RxWindow(tag : tag + "/window")
            let windowAction : (RxWindow, eWindowEvent) -> Void = { (currentWindow : RxWindow, windowEvent : eWindowEvent) in

                switch windowEvent
                {
                    case .eWindowStart:
                        // do nothing
                        break

                    case .eWindowEnd:
                        emit(emitNotifier, notifications: &notifications)
                }
            }

            // Configure the window.
            window <- .eSetEvalQueue(evalNode.evalQueue) <- .eSetWindowAction(windowAction)

            // Request the source start time, as a reference time base for stamping.
            evalNode.supportOptionRequests = [oRxEvalNodeSupport_Options.oSourceStartTime]

            evalNode.supportOptionReplyDelegate = { (reply: eRxEvalNodeSupport_Reply) in

                switch reply
                {
                    case .eSourceStartTime(let sourceStartTime):
                        startTime = sourceStartTime

                    default:
                        // do nothing.
                        break
                }
            }

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<TargetItemType>) in

                notifications.queueItem(item)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<TargetItemType>) in

                window.stopCurrentWindow()

                emit(emitNotifier, notifications: &notifications)
                notifier.notifyCompleted(error)
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<TargetItemType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        window.createPeriodicWindow(startTime!, startOffset: 0, period: duration)

                    case eRxEvalStateChange.eEvalEnd:

                        window.cancelAll()

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservableMap<ItemOutType, TargetItemType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Window: Periodically subdivide item notifications into Observable windows and emit those windows rather than emitting the items one at a time.
    ///         Windows are defined by item count and duration.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/17_SequencesOfCoincidence.html#Window )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.window(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/window.html )
    ///

    public final func window(count : RxCountType, duration : RxDuration) -> RxObservableMap<ItemOutType, RxSource<ItemOutType>>
    {
        typealias TargetItemType = RxSource<ItemOutType>

        let tag = "window"

        switch duration
        {
            case _ where duration < 0:
                // Error case.
                return throwError(RxLibError(.eInvalidTimePeriod))

            default:
                break
        }

        let evalOp = { (evalNode : RxEvalNode<ItemOutType, RxSource<ItemOutType>>) -> Void in

            func emit(notifier : ARxNotifier<TargetItemType>, inout notifications : RxNotificationQueue<ItemOutType>, count : RxCountType)
            {
                let itemsToEmit = notifications.rangeSlice(Range<RxIndexType>(0..<count))
                let notificationsToEmit = RxNotificationQueue<ItemOutType>(tag:  notifications.tag, itemsThenCompletion : itemsToEmit)

                notifications.popCount(count)
                notifier.notifyItem(notificationsToEmit.toObservable(RxSource<ItemOutType>.self))
            }

            let emitNotifier = evalNode.syncOutNotifier
            var notifications = RxNotificationQueue<ItemOutType>(tag : tag)
            var startTime: RxTime? = nil
            let window = RxWindow(tag : tag + "/window")
            let windowAction : (RxWindow, eWindowEvent) -> Void = { (currentWindow : RxWindow, windowEvent : eWindowEvent) in

                switch windowEvent
                {
                    case .eWindowStart:
                        // do nothing
                        break

                    case .eWindowEnd:
                        emit(emitNotifier, notifications: &notifications, count: notifications.itemCount)
                }
            }

            // Configure the window.
            window <- .eSetEvalQueue(evalNode.evalQueue) <- .eSetWindowAction(windowAction)

            // Request the source start time, as a reference time base for stamping.
            evalNode.supportOptionRequests = [oRxEvalNodeSupport_Options.oSourceStartTime]

            evalNode.supportOptionReplyDelegate = { (reply: eRxEvalNodeSupport_Reply) in

                switch reply
                {
                    case .eSourceStartTime(let sourceStartTime):
                        startTime = sourceStartTime

                    default:
                        // do nothing.
                        break
                }
            }

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<TargetItemType>) in

                notifications.queueItem(item)

                if notifications.itemCount >= count
                {
                    emit(emitNotifier, notifications: &notifications, count: count)
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<TargetItemType>) in

                let remainingCount = notifications.itemCount

                if remainingCount > 0
                {
                    emit(emitNotifier, notifications: &notifications, count: remainingCount)
                }

                notifier.notifyCompleted(error)
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<TargetItemType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        window.createPeriodicWindow(startTime!, startOffset: 0, period: duration)

                    case eRxEvalStateChange.eEvalEnd:

                        window.cancelAll()

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservableMap<ItemOutType, TargetItemType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Window: Periodically subdivide item notifications into Observable windows and emit those windows rather than emitting the items one at a time.
    ///         Windows are defined by duration and timeshift.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/17_SequencesOfCoincidence.html#Window )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.window(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/window.html )
    ///

    public final func window(duration : RxDuration, timeShift : RxTimeOffset) -> RxObservableMap<ItemOutType, RxSource<ItemOutType>>
    {
        typealias TargetItemType = RxSource<ItemOutType>

        let tag = "window"

        switch duration
        {
            case _ where duration < 0:
                // Error case.
                return throwError(RxLibError(.eInvalidTimePeriod))

            default:
                break
        }

        let evalOp = { (evalNode : RxEvalNode<ItemOutType, RxSource<ItemOutType>>) -> Void in

            var startTime : RxTime? = nil

            func emit(notifier : ARxNotifier<TargetItemType>, inout notifications : RxNotificationSequence<ItemOutType>, timeShift : RxTimeOffset)
            {
                let count = notifications.itemCount
                let itemsToEmit = notifications.timeSlice(startTime!, duration: duration, timeShift: timeShift)
                let notificationsToEmit = RxNotificationQueue<ItemOutType>(tag:  notifications.tag, itemsThenCompletion : itemsToEmit)

                notifications.popCount(count)
                notifier.notifyItem(notificationsToEmit.toObservable(RxSource<ItemOutType>.self))
            }

            let emitNotifier = evalNode.syncOutNotifier
            var notifications = RxNotificationSequence<ItemOutType>(tag : tag)
            let window = RxWindow(tag : tag + "/window")
            let windowAction : (RxWindow, eWindowEvent) -> Void = { (currentWindow : RxWindow, windowEvent : eWindowEvent) in

                switch windowEvent
                {
                    case .eWindowStart:
                        // do nothing
                        break

                    case .eWindowEnd:
                        emit(emitNotifier, notifications: &notifications, timeShift: timeShift)
                }
            }

            // Configure the window.
            window <- .eSetEvalQueue(evalNode.evalQueue) <- .eSetWindowAction(windowAction)

            // Request the source start time, as a reference time base for stamping.
            evalNode.supportOptionRequests = [oRxEvalNodeSupport_Options.oSourceStartTime]

            evalNode.supportOptionReplyDelegate = { (reply: eRxEvalNodeSupport_Reply) in

                switch reply
                {
                    case .eSourceStartTime(let sourceStartTime):
                        startTime = sourceStartTime

                    default:
                        // do nothing.
                        break
                }
            }

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<TargetItemType>) in

                notifications.queueItem(item)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<TargetItemType>) in

                window.stopCurrentWindow()

                emit(emitNotifier, notifications: &notifications, timeShift: timeShift)
                notifier.notifyCompleted(error)
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<TargetItemType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        window.createPeriodicWindow(startTime!, startOffset: 0, period: duration)

                    case eRxEvalStateChange.eEvalEnd:

                        window.cancelAll()

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservableMap<ItemOutType, TargetItemType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }
}

extension RxObservableMap: RxMSObservables_Generation
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// StartWith: Emit a given sequence of items before beginning to emit received item notifications.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/12_CombiningSequences.html#StartWith )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.startwith(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/startwith.html )
    ///

    public final func startWith(items: [ItemOutType]) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var hasTerminated = false

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        let queue = RxNotificationQueue<ItemOutType>(tag: "startWith", items: items, termination: nil)
                        let evalQueue : RxEvalQueue = evalNode.evalQueue

                        queue.sendItemsToNotifierOnEvalQueue(notifier, evalQueue: evalQueue, terminate: &hasTerminated)

                    case eRxEvalStateChange.eEvalEnd:

                        hasTerminated = true

                    default:
                        break
                }
            }
        }

        return RxObservable<ItemOutType>(tag: "startWith", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Repeat: On completion, repeatedly emit all the received item notifications forever.
    ///
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.repeat(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/repeat.html )
    ///

    public final func repeatForever() -> RxObservable<ItemOutType>
    {
        let tag = "repeatForever"
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            let buffer        = RxNotificationQueue<ItemOutType>(tag: tag)
            var hasTerminated = false

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType>) in

                buffer.queueItem(item)
                notifier.notifyItem(item)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<ItemOutType>) in

                if (error != nil) || (buffer.itemCount == 0)
                {
                    notifier.notifyCompleted(error)
                }
                else
                {
                    while !hasTerminated && evalNode.evalQueue.isActive
                    {
                        buffer.sendItemsToNotifierOnEvalQueue(notifier, evalQueue: evalNode.evalQueue, terminate: &hasTerminated)
                    }
                }
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalEnd:

                        hasTerminated = true

                    default:
                        break
                }
            }
        }

        return RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Repeat: On completion, Repeatedly emit the received item notifications for count times.
    ///
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.repeat(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/repeat.html )
    ///

    public final func repeatWithCount(count : RxCountType) -> RxObservable<ItemOutType>
    {
        let tag = "repeat"
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            let buffer        = RxNotificationQueue<ItemOutType>(tag: tag)
            var hasTerminated = false

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType>) in

                buffer.queueItem(item)
                notifier.notifyItem(item)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<ItemOutType>) in

                if (error != nil) || (buffer.itemCount == 0)
                {
                    notifier.notifyCompleted(error)
                }
                else
                {
                    for _ in 0..<count
                    {
                        buffer.sendItemsToNotifierOnEvalQueue(notifier, evalQueue: evalNode.evalQueue, terminate: &hasTerminated)
                    }

                    notifier.notifyCompleted()
                }
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalEnd:

                        hasTerminated = true

                    default:
                        break
                }
            }
        }

        return RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }
}

extension RxObservableMap: RxMSObservables_Group
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// GroupBy: Given a partition function, convert an Observable into a set of Observables which each emits items chosen with the same partition mapping result.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#GroupBy )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.groupby(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/groupby.html )
    ///

    public final func groupBy<GroupType : Hashable, ObservableType>(groupSelector : RxTypes2<ItemOutType, GroupType>.RxMapItemAction, mapItemToObservableType:RxTypes2<ItemOutType, ObservableType>.RxMapItemAction) -> RxObservableMap<ItemOutType, RxObservable<ObservableType>>
    {
        typealias TargetItemType = RxObservable<ObservableType>

        let evalOp = { (evalNode : RxEvalNode<ItemOutType, TargetItemType>) -> Void in

            var notifierByGroup = [GroupType : ARxNotifier<ObservableType>]()
            var subscriptionByGroup = [GroupType : RxSubscription]()

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<TargetItemType>) in

                let group = groupSelector(item)

                if let notifierForGroup = notifierByGroup[group]
                {
                    notifierForGroup.notifyItem(mapItemToObservableType(item))
                }
                else
                {
                    let notifierForGroup = RxNotifier_NotifyConsumers<ObservableType>(tag : self.tag)
                    let source = RxSource<ObservableType>.redirectPat(self.tag, subscriptionType: .eHot, notifier: notifierForGroup)

                    notifierByGroup[group] = notifierForGroup
                    subscriptionByGroup[group] = source.subscribe(notifierForGroup)

                    notifier.notifyItem(source)
                    notifierForGroup.notifyItem(mapItemToObservableType(item))
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<TargetItemType>) in

                for notifierForGroup in notifierByGroup.values
                {
                    notifierForGroup.notifyCompleted(nil)
                }

                notifier.notifyCompleted(error)
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<TargetItemType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalEnd:

                        for subscription in subscriptionByGroup.values
                        {
                            if subscription.subscribed
                            {
                                subscription.unsubscribe()
                            }
                        }

                        notifierByGroup.removeAll()
                        subscriptionByGroup.removeAll()

                    default:
                        break
                }
            }
        }

        return RxObservableMap<ItemOutType, TargetItemType>(tag: "groupBy", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// GroupBy: Given a partition function, convert an Observable into a set of Observables which each emits items chosen with the same partition mapping result.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#GroupBy )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.groupby(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/groupby.html )
    ///

    public final func groupBy<GroupType : Hashable>(groupSelector : RxTypes2<ItemOutType, GroupType>.RxMapItemAction) -> RxObservableMap<ItemOutType, RxObservable<ItemOutType>>
    {
        let mapItemToObservableType = { (item : ItemOutType) -> ItemOutType in

            return item
        }

        return groupBy(groupSelector, mapItemToObservableType: mapItemToObservableType)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// GroupUntil: Group item notifications in batches.
    ///
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.groupbyuntil(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/groupby.html )
    ///

    public final func groupByUntil<GroupType : Hashable, ObservableType, DurationItemType>(groupSelector : RxTypes2<ItemOutType, GroupType>.RxMapItemAction, mapItemToObservableType:RxTypes2<ItemOutType, ObservableType>.RxMapItemAction, durationObservable: RxObservable<DurationItemType>) -> RxObservableMap<ItemOutType, RxSource<ObservableType>>
    {
        typealias TargetItemType = RxSource<ObservableType>

        let evalOp = { (evalNode: RxEvalNode<ItemOutType, TargetItemType>) -> Void in

            var durationSubscription: RxSubscription? = nil
            var notifierByGroup = [GroupType: ARxNotifier<ObservableType>]()
            var subscriptionByGroup = [GroupType: RxSubscription]()

            evalNode.itemDelegate = {
                (item: ItemOutType, notifier: ARxNotifier<TargetItemType>) in

                let group = groupSelector(item)

                if let notifierForGroup = notifierByGroup[group]
                {
                    notifierForGroup.notifyItem(mapItemToObservableType(item))
                }
                else
                {
                    let notifierForGroup = RxNotifier_NotifyConsumers<ObservableType>(tag: self.tag)
                    let source = RxSource<ObservableType>.redirectPat(self.tag, subscriptionType: .eHot, notifier: notifierForGroup)

                    notifierByGroup[group] = notifierForGroup
                    subscriptionByGroup[group] = source.subscribe(notifierForGroup)

                    notifier.notifyItem(source)
                    notifierForGroup.notifyItem(mapItemToObservableType(item))
                }
            }

            evalNode.completedDelegate = {
                (error: IRxError?, notifier: ARxNotifier<TargetItemType>) in

                for notifierForGroup in notifierByGroup.values
                {
                    notifierForGroup.notifyCompleted(nil)
                }

                notifier.notifyCompleted(error)
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<TargetItemType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        durationSubscription = durationObservable.subscribe({
                            (durationItem: DurationItemType) in

                            for notifierForGroup in notifierByGroup.values
                            {
                                notifierForGroup.notifyCompleted(nil)
                            }

                            notifierByGroup.removeAll()
                            subscriptionByGroup.removeAll()
                        })

                    case eRxEvalStateChange.eEvalEnd:

                        for subscription in subscriptionByGroup.values
                        {
                            if subscription.subscribed
                            {
                                subscription.unsubscribe()
                            }
                        }

                        durationSubscription?.unsubscribe()
                        durationSubscription = nil

                        notifierByGroup.removeAll()
                        subscriptionByGroup.removeAll()

                    default:
                        break
                }
            }
        }

        return RxObservableMap<ItemOutType, TargetItemType>(tag: "groupBy", evalOp: evalOp).chainObservable(self)
    }

    ///
    /// GroupUntil: Group item notifications in batches.
    ///
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.groupbyuntil(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/groupby.html )
    ///

    public final func groupByUntil<GroupType : Hashable, DurationItemType>(groupSelector : RxTypes2<ItemOutType, GroupType>.RxMapItemAction, durationObservable: RxObservable<DurationItemType>) -> RxObservableMap<ItemOutType, RxSource<ItemOutType>>
    {
        let mapItemToObservableType = { (item : ItemOutType) -> ItemOutType in

            return item
        }

        return groupByUntil(groupSelector, mapItemToObservableType: mapItemToObservableType, durationObservable:durationObservable)
    }
}

extension RxObservableMap: RxMSObservables_ErrorHandling
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Retry : If an error notification is issued, resubscribe in the hope that it will complete without error a second time.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/11_AdvancedErrorHandling.html#Retry )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.retry(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/retry.html )
    ///

    public final func retry() -> RxObservable<ItemOutType>
    {
        let tag = "retry"
        var retrySelf : RxObservable<ItemOutType>? = nil
        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            let subscriptionType = self.getSubscriptionType()

            if subscriptionType != .eHot
            {
                // The subscription type must be hot.
                let errorEvalOp: (evalNode:RxEvalNode<ItemOutType, ItemOutType>) -> Void = RxEvalOpsConvenience.completedOpPat(RxLibError(eRxLibErrorType.eSubscriptionTypeIsNotHot))

                errorEvalOp(evalNode: evalNode)

                return
            }

            var reSubscription: RxSubscription? = nil
            var iteration                       = 0

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if error == nil
                {
                    // If the stream has completed then we complete.
                    notifier.notifyCompleted(nil)
                }
                else
                {
                    // If the stream has error-ed, then we re-subscribe.
                    let outNotifier           = evalNode.asyncOutNotifier
                    let resubscribingObserver = ResubscribingObserver<ItemOutType>(observable: retrySelf!, iteration: iteration, count: nil, outNotifier: outNotifier)

                    iteration += 1
                    reSubscription = retrySelf?.subscribe(resubscribingObserver)
                    assert(reSubscription != nil, "Remove compiler warning")
                }
            }
        }

        retrySelf = RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)

        return retrySelf!
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Retry: If an error notification is issued, resubscribe up to a maximum of count times.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/11_AdvancedErrorHandling.html#Retry )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.retry(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/retry.html )
    ///

    public final func retry(count : RxCountType) -> RxObservable<ItemOutType>
    {
        let tag = "retry"
        var retrySelf : RxObservable<ItemOutType>? = nil
        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            let subscriptionType = self.getSubscriptionType()

            if subscriptionType != .eHot
            {
                // The subscription type must be hot.
                let errorEvalOp: (evalNode:RxEvalNode<ItemOutType, ItemOutType>) -> Void = RxEvalOpsConvenience.completedOpPat(RxLibError(eRxLibErrorType.eSubscriptionTypeIsNotHot))

                errorEvalOp(evalNode: evalNode)

                return
            }

            var reSubscription : RxSubscription? = nil
            var iteration = 0

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                func unsubscribePrevious()
                {
                    if let prevSubscription = reSubscription where prevSubscription.subscribed
                    {
                        prevSubscription.unsubscribe()
                    }
                }

                if error == nil
                {
                    // If the stream has completed then we complete.
                    notifier.notifyCompleted(nil)
                }
                else if iteration < count
                {
                    unsubscribePrevious()

                    // If the stream has error-ed, then we re-subscribe.
                    let outNotifier           = evalNode.asyncOutNotifier
                    let resubscribingObserver = ResubscribingObserver<ItemOutType>(observable: retrySelf!, iteration: iteration, count: count, outNotifier: outNotifier)

                    iteration += 1
                    reSubscription = retrySelf?.subscribe(resubscribingObserver)
                    assert(reSubscription != nil, "Remove compiler warning")
                }
                else
                {
                    unsubscribePrevious()

                    notifier.notifyCompleted(error)
                }
            }
        }

        retrySelf = RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)

        return retrySelf!
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Catch: Recover from an error condition by ignoring error notifications.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/content/v1.0.10621.0/11_AdvancedErrorHandling.html#Catch )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.catch(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/catch.html )
    ///

    public final func catchError(stream : ARxProducer<ItemOutType>) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var subscription : RxSubscription? = nil

            evalNode.completedDelegate = { (error : IRxError?, notifier : ARxNotifier<ItemOutType>) in

                if error == nil
                {
                    notifier.notifyCompleted()
                }
                else
                {
                    subscription = stream.subscribe(notifier)
                    
                    // Remove compiler warning.
                    assert(subscription != nil)
                }
            }

            evalNode.stateChangeDelegate = { (stateChange : eRxEvalStateChange, notifier : ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalEnd:

                        subscription = nil

                    default:
                        break
                }
            }
        }

        return RxObservable<ItemOutType>(tag: "catchError", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// OnErrorResumeNext: If an error notification is encountered, switch over to the given stream.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/11_AdvancedErrorHandling.html#OnErrorResumeNext )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.onerrorresumenext(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/catch.html )
    ///

    public final func onErrorResumeNext(streams : [ARxProducer<ItemOutType>]) -> RxObservable<ItemOutType>
    {
        let tag = "onErrorResumeNext"
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            let evalQueue = evalNode.evalQueue
            var hasEnded = false
            
            evalNode.completedDelegate = { (error : IRxError?, notifier : ARxNotifier<ItemOutType>) in
                
                if error == nil
                {
                    notifier.notifyCompleted()
                    return
                }

                var terminationError : IRxError? = nil

                for stream in streams
                {
                    if (hasEnded || !evalQueue.isActive) { break }

                    let redirectNotifier = RxNotifier_NotifyDelegates<ItemOutType>(directedToConsumer: notifier, tag: tag + "/redirect")

                    redirectNotifier.onItem = { (item : ItemOutType) in
                        
                        notifier.notifyItem(item)
                        
                    }
                    
                    redirectNotifier.onCompleted = { ( error : IRxError?) in

                        terminationError = error
                        
                        if error == nil
                        {
                            hasEnded = true
                        }
                    }

                    let subscription = stream.subscribe(redirectNotifier)

                    subscription.waitForDisposal()
                }
                
                notifier.notifyCompleted(terminationError)
            }
            
            evalNode.stateChangeDelegate = { (stateChange : eRxEvalStateChange, notifier : ARxNotifier<ItemOutType>) in
                
                switch stateChange
                {
                case eRxEvalStateChange.eEvalEnd:

                    hasEnded = true
                    
                default:
                    break
                }
            }
        }

        return RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// OnErrorResumeNext: If an error notification is encountered, switch over to the given stream.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/11_AdvancedErrorHandling.html#OnErrorResumeNext )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.onerrorresumenext(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/catch.html )
    ///

    public final func onErrorResumeNext(streams : ARxProducer<RxSource<ItemOutType>>)  -> RxObservable<ItemOutType>
    {
        let tag = "onErrorResumeNext"
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            let evalQueue = evalNode.evalQueue
            var hasEnded = false

            evalNode.completedDelegate = { (error : IRxError?, notifier : ARxNotifier<ItemOutType>) in

                if error == nil
                {
                    notifier.notifyCompleted()
                    return
                }

                let streamCollection = streams.toEnumerable()
                var terminationError : IRxError? = nil

                for stream in streamCollection
                {
                    if (hasEnded || !evalQueue.isActive) { break }

                    let redirectNotifier = RxNotifier_NotifyDelegates<ItemOutType>(directedToConsumer: notifier, tag: tag + "/redirect")

                    redirectNotifier.onItem = { (item : ItemOutType) in

                        notifier.notifyItem(item)

                    }

                    redirectNotifier.onCompleted = { ( error : IRxError?) in

                        terminationError = error

                        if error == nil
                        {
                            hasEnded = true
                        }
                    }

                    let subscription = stream.subscribe(redirectNotifier)

                    subscription.waitForDisposal()
                }

                notifier.notifyCompleted(terminationError)
            }
        }

        return RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// OnErrorReturn  If an error notification is encountered, emit the given item and a completed notification.
    ///
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/catch.html )
    ///

    public final func onErrorReturn(item: ItemOutType)  -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if error != nil
                {
                    notifier.notifyItem(item)
                }
                
                notifier.notifyCompleted()
            }
        }

        return RxObservable<ItemOutType>(tag: "onErrorReturn", evalOp: evalOp).chainObservable(self)
    }
}

extension RxObservableMap: RxMSObservables_Connectable
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Replay: Create an observable that always replays the last count notifications within time window to subscriptions.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/content/v1.0.10621.0/14_HotAndColdObservables.html#Replay )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.replay(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/replay.html )
    ///

    public final func replay(inout replayNotifications : RxNotificationConstrainedSequence<ItemOutType>) -> RxObservable<ItemOutType>
    {
        let tag = "replay"
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            let subscriptionType = self.getSubscriptionType()

            if subscriptionType != .eHot
            {
                // The subscription type must be hot.
                let errorEvalOp: (evalNode:RxEvalNode<ItemOutType, ItemOutType>) -> Void  = RxEvalOpsConvenience.completedOpPat(RxLibError(eRxLibErrorType.eSubscriptionTypeIsNotHot))

                errorEvalOp(evalNode: evalNode)

                return
            }

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                replayNotifications.queueItem(item)
                notifier.notifyItem(item)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                replayNotifications.queueCompleted(error, timeStamp : nil)
                notifier.notifyCompleted(error)
            }

            evalNode.stateChangeDelegate = { (stateChange : eRxEvalStateChange, notifier : ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eSubscriptionBegin:

                        if replayNotifications.totalCount > 0
                        {
                            replayNotifications.sendItemsToNotifyHandlerNonDestructive(notifier)

                            if let termination = replayNotifications.termination
                            {
                                notifier.notifyTermination(termination)
                            }
                        }

                    default:
                        break
                }
            }
        }

        return RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Publish: Create an observable that will act as a hot source, ensuring all its subscriptions, will only observe current notifications.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/14_HotAndColdObservables.html#PublishAndConnect )
    /// - [Microsoft Doc Link]( https://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.publish(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/publish.html )
    ///

    public final func publish() -> RxObservable<ItemOutType>
    {
        let tag = "publish"

        guard let subscriptionType = getSubscriptionType() else {
            RxSDK.error.handler(RxError(.eSubscriptionFailure("Could not get subscription type.")))

            return RxSource.raise(RxError(.eSubscriptionFailure("Could not get subscription type.")))
        }

        if subscriptionType == .eHot
        {
            // Fail if the subscription is already hot.
            assert(false, "The subscription is already hot")

            return RxObservable<ItemOutType>(tag: tag).chainObservable(self)
        }

        var source : RxSource<ItemOutType>? = nil
        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var publishSubscription  : RxSubscription? = nil
            let publishNotifier      = evalNode.syncInNotifier

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                notifier.notifyItem(item)
            }

            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case .eNewSubscriptionInSubscriptionThread:

                        if evalNode.subscriptionCount == 0
                        {
                            // We want the eval build engine fully built in the subscription thread.
                            publishSubscription = self.subscribe(publishNotifier)

                            #if RxMonEnabled
                                if source?.traceTag != nil
                                {
                                    evalNode.traceDependant(publishSubscription!)
                                }
                            #endif
                        }

                    case eRxEvalStateChange.eEvalEnd:

                        publishSubscription?.unsubscribe()
                        publishSubscription = nil

                    default:
                        // do nothing.
                        break
                }
            }

            #if RxMonEnabled
                if source?.traceTag != nil
                {
                    evalNode.traceDependant(publishNotifier)
                }
            #endif
        }

        source = RxSource<ItemOutType>(tag: tag, subscriptionType : .eHot, evalOp: evalOp).chainPublish(self)

        return source!
    }
}

extension RxObservableMap: RxMSObservables_Aggregation
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Count: Emit the count the number of item notifications received and then complete.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#Count )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh229470(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/count.html )
    ///

    public final func count() -> RxObservableMap<ItemOutType, RxCountType>
    {
        typealias TargetItemType = RxCountType

        let evalOp = { (evalNode : RxEvalNode<ItemOutType, RxCountType>) -> Void in

            var count : RxCountType = 0

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<RxCountType>) in

                count = count + 1
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<RxCountType>) in

                if error == nil
                {
                    // Emit the cont an the end of the sequence, if there was no error.
                    notifier.notifyItem(count)
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservableMap<ItemOutType, RxCountType>(tag: "count", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Aggregate: Apply a function to each number based item notification received, then emit the final value and terminate.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#Aggregate )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.aggregate(v=vs.103).aspx )
    /// - [Netflix Doc Link]( Netflix calls this reduce: http://reactivex.io/documentation/operators/reduce.html )
    ///

    public final func aggregate(startItem: ItemOutType, accumulator: RxTypes<ItemOutType>.RxItemMapFunc) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var aggregatedValue: ItemOutType = startItem

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                aggregatedValue = accumulator(aggregatedValue, item)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if error == nil
                {
                    notifier.notifyItem(aggregatedValue)
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservable<ItemOutType>(tag: "aggregate", evalOp: evalOp).chainObservable(self)
    }


    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Min: Emit the calculation the minimum of number based item notifications received and then terminate.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#MaxAndMin )
    /// - [Microsoft Doc Link]( https://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.min(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/min.html )
    ///

    public final func min(compareOp: RxTypes<ItemOutType>.RxCompareOp) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var minItem: ItemOutType? = nil

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                if (minItem == nil) || compareOp(lhs : item, rhs : minItem!)
                {
                    minItem = item
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if error == nil
                {
                    if minItem != nil
                    {
                        notifier.notifyItem(minItem!)
                    }
                    else
                    {
                        notifier.notifyCompleted(RxLibError(.eNoSuchElement))

                        return
                    }
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservable<ItemOutType>(tag: "min", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Max: Emit the calculation the maximum of number based item notifications received and then terminate.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#MaxAndMin )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.max(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/max.html )
    ///

    public final func max(compareOp: RxTypes<ItemOutType>.RxCompareOp) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var maxItem: ItemOutType? = nil

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType>) in

                if (maxItem == nil) || compareOp(lhs : maxItem!, rhs : item)
                {
                    maxItem = item
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if error == nil
                {
                    if maxItem != nil
                    {
                        notifier.notifyItem(maxItem!)
                    }
                    else
                    {
                        notifier.notifyCompleted(RxLibError(.eNoSuchElement))

                        return
                    }
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservable<ItemOutType>(tag: "max", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Sum: Emit the calculation the sum of number based item notifications received and then terminate.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#MaxAndMin )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.sum(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/sum.html )
    ///

    public final func sum(sumOp: (item : ItemOutType?) -> ItemOutType) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var sum : ItemOutType = sumOp(item : nil)

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                sum = sumOp(item : item)

            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if error == nil
                {
                    notifier.notifyItem(sum)
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservable<ItemOutType>(tag: "sum", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Average: Emit the calculation the average of number based item notifications received and then terminate.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#MaxAndMin )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.average(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/average.html )
    ///

    public final func average(averager : (item : ItemOutType?) -> ItemOutType?) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var average : ItemOutType? =  averager(item : nil)

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                average = averager(item : item)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if error == nil
                {
                    if average != nil
                    {
                        notifier.notifyItem(average!)
                    }
                    else
                    {
                        notifier.notifyCompleted(RxLibError(.eNoSuchElement))
                        return
                    }
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservable<ItemOutType>(tag: "average", evalOp: evalOp).chainObservable(self)
    }
}

extension RxObservableMap: RxMSObservables_Utility
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Using: Given a disposable factory, create a disposable that has the same lifespan as the Observable.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/11_AdvancedErrorHandling.html#Using )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh229585(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/using.html )
    ///

    public final func using(disposableFactory : () -> RxDisposable) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                var disposable : RxDisposable? = nil

                switch stateChange
                {
                    case eRxEvalStateChange.eSubscriptionBegin:

                        disposable = disposableFactory()

                    case eRxEvalStateChange.eEvalEnd:

                        disposable?.dispose()

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservable<ItemOutType>(tag: "using", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// TimeInterval: Convert item notifications into one that emits the time elapsed between the previous emission.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/08_Transformation.html#TimeStampAndTimeInterval )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.timeinterval(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/timeinterval.html )
    ///

    public final func timeInterval() -> RxObservableMap<ItemOutType, String>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, String>) -> Void in

            var lastTimestamp = RxTime()

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<String>) in

                let durationSinceLastNotification = -lastTimestamp.timeIntervalSinceNow
                let durationstamp = RxDateFormatter.durationFormatter(durationSinceLastNotification)
                let itemAsString = String(item)

                lastTimestamp = RxTime()

                notifier.notifyItem("\(itemAsString)@\(durationstamp)")
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<String>) in

                notifier.notifyCompleted(error)
            }
        }

        return RxObservableMap<ItemOutType, String>(tag: "timeInterval", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Timestamp: Emit a timestamp of item notifications received together with a string form of the item itself.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/08_Transformation.html#TimeStampAndTimeInterval )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.timestamp(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/timestamp.html )
    ///

    public final func timestamp() -> RxObservableMap<ItemOutType, String>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, String>) -> Void in

            var timerStartTime : RxTime? = nil
            let dateFormatter = NSDateFormatter()

            // Request the source start time, as a reference time base for stamping.
            evalNode.supportOptionRequests = [oRxEvalNodeSupport_Options.oSourceStartTime]

            evalNode.supportOptionReplyDelegate = { (reply: eRxEvalNodeSupport_Reply) in

                switch reply
                {
                    case .eSourceStartTime(let sourceStartTime):
                        timerStartTime = sourceStartTime

                    default:
                        // do nothing.
                        break
                }
            }

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<String>) in

                assert(timerStartTime != nil, "Expected the source start time to be valid.")

                if let startTime = timerStartTime
                {
                    let timestamp = RxDateFormatter.timeFormatterMicroSec(startTime, dateFormatter: dateFormatter)
                    let itemAsString = String(item)

                    notifier.notifyItem("\(itemAsString)@\(timestamp)")
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<String>) in

                notifier.notifyCompleted(error)
            }
        }

        return RxObservableMap<ItemOutType, String>(tag: "timestamp", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Timestamp: Remove timestamps on item notifications and emit the string form of the item extracted from the notification.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/08_Transformation.html#TimeStampAndTimeInterval )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.timestamp(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/timestamp.html )
    ///

    public final func removeTimestamp<TargetItemType>(mapItemFunc : (String) -> TargetItemType) -> RxObservableMap<ItemOutType, TargetItemType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, TargetItemType>) -> Void in

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<TargetItemType>) in

                if let stringItemType = item as? String
                {
                    if let range = stringItemType.rangeOfString("@")
                    {
                        let extractedItem = stringItemType.substringToIndex(range.startIndex)
                        let targetItem : TargetItemType = mapItemFunc(extractedItem)

                        notifier.notifyItem(targetItem)

                        return
                    }
                }

                notifier.notifyCompleted(RxLibError(.eInvalidTimestamp))
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<TargetItemType>) in

                notifier.notifyCompleted(error)
            }
        }

        return RxObservableMap<ItemOutType, TargetItemType>(tag: "removeTimestamp", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Dump: Print to console a string form of the notifications, together with the given title. All Notifications are passed on.
    ///

    public final func dump(title : String) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                let itemDescription = String(item)

                print("\(title)-->\(itemDescription)")

                notifier.notifyItem(item)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if error == nil
                {
                    print("completed")
                }
                else
                {
                    print("failed with error-->\(error!.description)")
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservable<ItemOutType>(tag: "dump", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// DumpUsingAction: Pass received item notifications to a given dump function. All Notifications are passed on.
    ///

    public final func dumpUsingAction(dumpAction : RxTypes<ItemOutType>.RxDumpAction) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            let title = self.tag

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                let itemDescription = String(item)

                dumpAction(title : title, description : itemDescription, item : item)

                notifier.notifyItem(item)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if error == nil
                {
                    dumpAction(title : title, description : "completed", item : nil)
                }
                else
                {
                    dumpAction(title : title, description : "error: \(error!.description)", item : nil)
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservable<ItemOutType>(tag: "dumpUsingAction", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Materialise: Emit the conversion of item notifications to formal RxNotification representation of the item.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/08_Transformation.html#MaterializeAndDematerialize )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh229453(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/materialize-dematerialize.html )
    ///

    public final func materialize() -> RxObservableMap<ItemOutType, RxNotification<ItemOutType>>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, RxNotification<ItemOutType>>) -> Void in

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<RxNotification<ItemOutType>>) in

                let notification = RxNotification<ItemOutType>(item: item)

                notifier.notifyItem(notification)
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<RxNotification<ItemOutType>>) in

                let notification = RxNotification<ItemOutType>.completed(error)

                notifier.notifyItem(notification)
                notifier.notifyCompleted(error)
            }
        }

        return RxObservableMap<ItemOutType, RxNotification<ItemOutType>>(tag: "materialize", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Materialise: Unwrap item notifications in RxNotification form to their native item form.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/08_Transformation.html#MaterializeAndDematerialize )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/hh229453(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/materialize-dematerialize.html )
    ///

    public final func dematerialize<DataItemType>() -> RxObservableMap<ItemOutType, DataItemType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, DataItemType>) -> Void in

            var hasTerminated = false

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<DataItemType>) in

                if let itemAsNotification = item as? RxNotification<DataItemType>
                {
                    switch itemAsNotification.notifyType
                    {
                    case .eNotification_Item(let item):
                            notifier.notifyItem(item)

                        case .eNotification_Termination(let termination):
                            notifier.notifyTermination(termination)
                            hasTerminated = true
                    }
                }
                else
                {
                    notifier.notifyCompleted(RxLibError(.eInvalidNotificationItem))
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<DataItemType>) in

                if !hasTerminated
                {
                    notifier.notifyCompleted(error)
                }
            }
        }

        return RxObservableMap<ItemOutType, DataItemType>(tag: "dematerialize", evalOp: evalOp).chainObservable(self)
    }
}

extension RxObservableMap: RxMSObservables_Blocking
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// First: Emit only the first item notification or nil on error.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#First )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.first(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/first.html )
    ///

    public final func first() -> ItemOutType?
    {
        var result : ItemOutType? = nil

        self.runSync({ (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

            result = item
            notifier.notifyCompleted()
        })

        return result
    }

    ///
    /// firstThrowable : Emit only the first item notification. Throw an error if there are no item notifications.
    ///     This function throws an error on receipt of an error notification.
    ///

    public final func firstThrowable() throws -> ItemOutType
    {
        var result : ItemOutType? = nil
        var handoverError : IRxError? = nil

        self.runSync({ (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

            result = item
            notifier.notifyCompleted()

        }, completedNotifier: {(error : IRxError?, notifier : ARxNotifier<ItemOutType>) in

            handoverError = error
            notifier.notifyCompleted(error)
        })

        if let handoverError = handoverError
        {
            throw handoverError
        }

        if result == nil
        {
            throw RxLibError(eRxLibErrorType.eNoSuchElement)
        }

        return result!
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Last: Emit only the first item notification, throw errors.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#Last )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.firstordefault(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/last.html )
    ///

    public final func last(ignoreErrors : Bool = true) -> ItemOutType?
    {
        var result : ItemOutType? = nil

        self.runSync({ (item: ItemOutType) in

            result = item

        }, completedAction: { (error : IRxError?) in

            if !ignoreErrors
            {
                result = nil
            }
        })

        return result
    }

    ///
    /// LastThrowable : Emit only the first item notification. Throw an error if there are no item notifications.
    ///     This function throws an error on receipt of an error notification.
    ///

    public final func lastThrowable() throws -> ItemOutType
    {
        var result : ItemOutType? = nil
        var handoverError : IRxError? = nil

        self.runSync({ (item: ItemOutType) in

            result = item

        }, completedAction: { (error : IRxError?) in

            handoverError = error
        })

        if let handoverError = handoverError
        {
            throw handoverError
        }

        if result == nil
        {
            throw RxLibError(eRxLibErrorType.eNoSuchElement)
        }

        return result!
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// FirstOrDefault: Emit only the last item notification, or nil on error.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#First )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.firstordefault(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/first.html )
    ///

    public final func firstOrDefault(defaultItem: ItemOutType, ignoreErrors : Bool = true) -> ItemOutType
    {
        var result : ItemOutType? = nil

        self.runSync({ (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

            result = item
            notifier.notifyCompleted()

        }, completedNotifier: {(error : IRxError?, notifier : ARxNotifier<ItemOutType>) in

            if !ignoreErrors
            {
                result = nil
            }

            notifier.notifyCompleted(error)
        })

        return result ?? defaultItem
    }

    ///
    /// Emit only the first item, if none exists then emit the given default item.
    ///     This function throws an error on receipt of an error notification.
    ///

    public final func firstOrDefaultThrowable(defaultItem: ItemOutType) throws -> ItemOutType
    {
        var result : ItemOutType? = nil
        var handoverError : IRxError? = nil

        self.runSync({ (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

             result = item
             notifier.notifyCompleted()

         }, completedNotifier: {(error : IRxError?, notifier : ARxNotifier<ItemOutType>) in

            handoverError = error
            notifier.notifyCompleted(error)
        })

        if let handoverError = handoverError
        {
            throw handoverError
        }

        return result ?? defaultItem
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// LastOrDefault: Emit only the last item emitted, if none exists then emit the given default item.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#Last )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.firstordefault(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/last.html )
    ///

    public final func lastOrDefault(defaultItem: ItemOutType, ignoreErrors : Bool = true) -> ItemOutType
    {
        var result : ItemOutType? = nil

        self.runSync({ (item: ItemOutType) in

            result = item

        }, completedAction: { (error : IRxError?) in

            if !ignoreErrors
            {
                result = nil
            }
        })

        return result ?? defaultItem
    }

    ///
    /// lastOrDefaultThrowable: Emit only the last item emitted, if none exists then emit the given default item.
    ///     This function throws an error on receipt of an error notification.
    ///

    public final func lastOrDefaultThrowable(defaultItem: ItemOutType) throws -> ItemOutType
    {
        var result : ItemOutType? = nil
        var handoverError : IRxError? = nil

        self.runSync({ (item: ItemOutType) in

            result = item

        }, completedAction: { (error : IRxError?) in

            handoverError = error
        })

        if let handoverError = handoverError
        {
            throw handoverError
        }

        return result ?? defaultItem
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Single: Emit the first item notification, if there is more than one item notification then emit an error.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#Single )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.single(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/first.html )
    ///

    public final func single() -> ItemOutType?
    {
        var result : ItemOutType? = nil

        self.runSync({ (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

            if result == nil
            {
                result = item
            }
            else
            {
                result = nil
                notifier.notifyCompleted()
            }
        })

        return result
    }

    ///
    /// SingleThrowable:          Emit the first item, if there are more then emit an error. Throw errors.
    ///

    public final func singleThrowable() throws -> ItemOutType
    {
        var result : ItemOutType? = nil
        var handoverError : IRxError? = nil

        self.runSync({ (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                if result == nil
                {
                    result = item
                }
                else
                {
                    result = nil
                    notifier.notifyCompleted()
                }

            }, completedNotifier: {(error : IRxError?, notifier : ARxNotifier<ItemOutType>) in

                handoverError = error
                notifier.notifyCompleted(error)
        })

        if let handoverError = handoverError
        {
            throw handoverError
        }

        if result == nil
        {
            throw RxLibError(eRxLibErrorType.eNoSuchElement)
        }

        return result!
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// SingleOrDefault: Emit the first item, if there are more then emit and error, if none exists emit the given default
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#Single )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.singleordefault(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/first.html )
    ///

    public final func singleOrDefault(defaultItem: ItemOutType) -> ItemOutType
    {
        var result : ItemOutType? = nil

        self.runSync({ (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

            if result == nil
            {
                result = item
            }
            else
            {
                notifier.notifyCompleted(RxLibError(.eInvalidOperation))
            }
        })

        return result ?? defaultItem
    }

    ///
    /// SingleOrDefaultThrowable  Emit the first item, if there are more then emit and error, if none exists emit the given default. Throw errors.
    ///

    public final func singleOrDefaultThrowable(defaultItem : ItemOutType) throws -> ItemOutType
    {
        var result : ItemOutType? = nil
        var handoverError : IRxError? = nil

        self.runSync({ (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

            if result == nil
            {
                result = item
            }
            else
            {
                notifier.notifyCompleted(RxLibError(.eInvalidOperation))
            }

        }, completedNotifier: {(error : IRxError?, notifier : ARxNotifier<ItemOutType>) in

            handoverError = error
            notifier.notifyCompleted(error)
        })

        if let handoverError = handoverError
        {
            throw handoverError
        }

        return result ?? defaultItem
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// MostRecent: Return an enumerable that emits the most recently received item. If there has been no item notifications, emit the given default item.
    ///

    public final func mostRecent(defaultItem : ItemOutType) -> TRxEnumerable<ItemOutType>
    {
        let tag                   = "mostRecent"
        let enumeratorBarrier     = RxBarrierSignalable()
        var queuer : RxEvalQueue? = nil
        let notificationQueue     = RxNotificationConstrainedQueue<ItemOutType>(tag : tag)
        var emittedDefault        = false
        
        let notifier = RxNotifier_NotifyDelegates<ItemOutType>(tag: tag)
        
        notifier.onItem = { (item : ItemOutType) in

            notificationQueue.queueItem(item)

            enumeratorBarrier.signal()
        }
        
        notifier.onCompleted = { (error : IRxError?) in

            notificationQueue.queueCompleted(error)

            enumeratorBarrier.signal()
        }

        let enumerator : AnyGenerator<ItemOutType> = AnyGenerator
        {
            while true
            {
                if let item = notificationQueue.unqueueItemSync(queuer!)
                {
                    emittedDefault = true

                    return item
                }
                else if notificationQueue.hasTerminatedSync(queuer!)
                {
                    return nil
                }
                else if !emittedDefault
                {
                    emittedDefault = true
                    
                    return defaultItem
                }
 
                enumeratorBarrier.wait()
            }
        }

        notificationQueue.maxItemCount = 1
        notificationQueue.enumerator = enumerator

        let subscription = subscribe(notifier)

        queuer = subscription.evalQueue!

        return TRxEnumerable(notificationQueue)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Latest: Return an enumerable that blocks until the Observable emits another item, then returns that item.
    ///

    public final func latest() -> TRxEnumerable<ItemOutType>
    {
        let tag                   = "latest"
        var queuer : RxEvalQueue? = nil
        let enumeratorBarrier     = RxBarrierSignalable()
        let notificationQueue     = RxNotificationQueue<ItemOutType>(tag : tag)
        let notifier              = RxNotifier_NotifyDelegates<ItemOutType>(tag: tag)

        notifier.onItem = { (item : ItemOutType) in

            notificationQueue.queueItem(item)

            enumeratorBarrier.signal()
        }

        notifier.onCompleted = { (error : IRxError?) in

            notificationQueue.queueCompleted(error)

            enumeratorBarrier.signal()
        }

        notificationQueue.enumerator = AnyGenerator
        {
            while true
            {
                if let item = notificationQueue.unqueueItemSync(queuer!)
                {
                    return item
                }
                else if notificationQueue.hasTerminatedSync(queuer!)
                {
                    return nil
                }
                else
                {
                    enumeratorBarrier.wait()
                }
            }
        }

        let subscription = subscribe(notifier)

        queuer = subscription.evalQueue!

        return TRxEnumerable(notificationQueue)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Next: Return an enumerable that blocks until the Observable emits another item, then returns that item.
    ///         If items are received faster than the enumeration, then they are discarded.
    ///

    public final func next() -> TRxEnumerable<ItemOutType>
    {
        let tag                   = "next"
        var queuer : RxEvalQueue? = nil
        let enumeratorBarrier     = RxBarrierSignalable()
        let notificationQueue     = RxNotificationQueue<ItemOutType>(tag : tag)
        let notifier              = RxNotifier_NotifyDelegates<ItemOutType>(tag: tag)
        var isActive              = false

        notifier.onItem = { (item : ItemOutType) in

            if isActive
            {
                notificationQueue.queueItem(item)
            }

            enumeratorBarrier.signal()
        }

        notifier.onCompleted = { (error : IRxError?) in

            notificationQueue.queueCompleted(error)

            isActive = false

            enumeratorBarrier.signal()
        }

        notificationQueue.enumerator = AnyGenerator
        {
            isActive = true

            while true
            {
                if let item = notificationQueue.unqueueItemSync(queuer!)
                {
                    return item
                }
                else if notificationQueue.hasTerminatedSync(queuer!)
                {
                    return nil
                }
                else
                {
                    enumeratorBarrier.wait()
                }
            }
        }

        let subscription = subscribe(notifier)

        queuer = subscription.evalQueue

        return TRxEnumerable(notificationQueue)
    }
}

extension RxObservableMap: RxNetflixObservables
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Map Documentation:
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/08_Transformation.html#Select )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.select(v=vs.103).aspx )
    /// - [Netflix Doc Link]( Netflix call this map:  http://reactivex.io/documentation/operators/map.html )
    ///

    public final func map<TargetItemType>(mapAction : RxTypes2<ItemOutType, TargetItemType>.RxItemMapOptionalFunc) -> RxObservableMap<ItemOutType, TargetItemType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, TargetItemType>) -> Void in

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<TargetItemType>) in

                if let mappedItem = mapAction(item)
                {
                    notifier.notifyItem(mappedItem)
                }
            }
        }

        return RxObservableMap<ItemOutType, TargetItemType>(tag: "map", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Filter Documentation:
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/05_Filtering.html#Where )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.where(v=vs.103).aspx )
    /// - [Netflix Doc Link]( Netflix call this filter:  http://reactivex.io/documentation/operators/filter.html )
    ///

    public final func filter(predicate : RxTypes<ItemOutType>.RxPredicate) -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                if predicate(item)
                {
                    notifier.notifyItem(item)
                }
            }
        }

        return RxObservable<ItemOutType>(tag: "filter", evalOp: evalOp).chainObservable(self)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Debounce Documentation:
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/13_TimeShiftedSequences.html#Throttle )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.throttle(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/debounce.html )
    ///

    public final func debounce(dueTimeOffset: RxDuration) -> RxObservable<ItemOutType>
    {
        let tag = "debounce"

        if dueTimeOffset <= 0
        {
            // If the dueTimeOffset is less than or equal to zero just relay the notifications.
            return RxObservable<ItemOutType>(tag: tag, evalOp: nil).chainObservable(self)
        }

        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var lastItemTimestamp : RxTime? = nil
            var scheduledItem : ItemOutType? = nil

            var timer = RxTimer(tag: tag + "/timer")
            var timerSubscription : RxTimerSubscription? = nil

            @inline(__always) func notifyLastItem(notifier: ARxNotifier<ItemOutType>)
            {
                if scheduledItem != nil
                {
                    notifier.notifyItem(scheduledItem!)

                    scheduledItem = nil
                    lastItemTimestamp = RxTime()
                }
            }

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType>) in

                if lastItemTimestamp == nil
                {
                    // Case: Emit the first item.

                    lastItemTimestamp = RxTime()

                    notifier.notifyItem(item)
                }
                else if timerSubscription == nil
                {
                    // Case: Schedule a timer request to perform notification of this item.

                    let evalQueue = evalNode.evalQueue

                    scheduledItem = item

                    timerSubscription = timer.addTimer(RxTime(timeIntervalSinceNow: dueTimeOffset), timerAction: { [weak evalQueue, weak notifier] (index: RxIndexType) in

                        if let evalQueue = evalQueue, notifier = notifier
                        {
                            evalQueue.dispatchAsync({notifyLastItem(notifier) })
                        }

                        timerSubscription = nil
                    })
                }
                else
                {
                    // Case: An item was scheduled to be emitted at the timeout and a new one was received before that occured. So update the scheduled item.

                    scheduledItem = item
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                timer.cancelAll()

                if scheduledItem != nil { notifyLastItem(notifier) }

                notifier.notifyCompleted(error)
            }

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        scheduledItem = nil
                        lastItemTimestamp = nil

                    case eRxEvalStateChange.eEvalEnd:

                        scheduledItem = nil
                        timerSubscription = nil
                        timer.cancelAll()

                    default:
                        // do nothing.
                        break
                }
            }

            #if RxMonEnabled
                if evalNode.traceTag != nil
                {
                    evalNode.traceDependant(timer)
                }
            #endif
        }

        return RxObservable<ItemOutType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///  RxObservable extension: Where ItemOutType is Hashable.
///

extension RxObservableMap where ItemOutType : Hashable
{
    ///
    /// Distinct:  Suppress all duplicate item notifications.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/05_Filtering.html#Distinct )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.distinct(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/distinct.html )
    ///

    public final func distinct() -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var hashedItemSet = Set<ItemOutType>()

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                if !hashedItemSet.contains(item)
                {
                    hashedItemSet.insert(item)
                    notifier.notifyItem(item)
                }
            }
        }

        return RxObservable<ItemOutType>(tag: "distinct", evalOp: evalOp).chainObservable(self)
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///  RxObservable extension:  Where ItemOutType is Equatable.
///

extension RxObservableMap where ItemOutType : Equatable
{
    ///
    /// DistinctUntilChanged: Only emit items that differ from their previous item.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/05_Filtering.html#Distinct )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.distinctuntilchanged(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/distinct.html )
    ///

    public final func distinctUntilChanged() -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var previousItem : ItemOutType? = nil

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                if (previousItem == nil) || (item != previousItem!)
                {
                    previousItem = item
                    notifier.notifyItem(item)
                }
            }
        }

        return RxObservable<ItemOutType>(tag: "distinctUntilChanged", evalOp: evalOp).chainObservable(self)
    }

    ///
    /// SequenceEqual: Determine whether two Observables emit the same sequence of items.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/06_Inspection.html#SequenceEqual )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.sequenceequal(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/sequenceequal.html )
    ///

    public final func sequenceEqual(other : ARxProducer<ItemOutType>) -> RxObservableMap<ItemOutType, Bool>
    {
        let evalOp : (RxEvalNode<ItemOutType, Bool>) -> Void = RxEvalOps.gateStreamsMapPat(.eGate_OnAllQueuesHaveAtLeastOneItem, streams : [other], mapEvalOp: { (map: RxGateMapType<ItemOutType, Bool>) in

            map.mapItemsDelegate = { (gate: RxNotificationGate<ItemOutType, Bool>, notifier: ARxNotifier<Bool>) in

                assert(gate.itemCount(0) > 0 && gate.itemCount(1) > 0, "Expected all gate queues to have items")

                if gate.itemAtQueueIndex(0) != gate.itemAtQueueIndex(1)
                {
                    notifier.notifyItem(false)
                    notifier.notifyCompleted()

                    gate.closeGate()
                }

                gate.popAllQueues()
            }

            map.mapCompletedDelegate = { (index : RxIndexType, error: IRxError?, gate : RxNotificationGate<ItemOutType, Bool>, notifier: ARxNotifier<Bool>) in

                if error == nil
                {
                    notifier.notifyItem(gate.countOfInputQueuesWithSomeItems == 0)
                }

                notifier.notifyCompleted(error)

                gate.closeGate()
            }
        })

        return RxObservableMap<ItemOutType, Bool>(tag: "sequenceEqual", evalOp: evalOp).chainObservable(self)
    }

    ///
    /// Contains: Determine whether the Observable emits a particular item.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/06_Inspection.html#Contains )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.contains(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/contains.html )
    ///

    public final func contains(containsItem: ItemOutType) -> RxObservableMap<ItemOutType, Bool>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, Bool>) -> Void in

            var haveEmittedResult = false

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<Bool>) in

                if item == containsItem
                {
                    haveEmittedResult = true
                    notifier.notifyItem(true)
                    notifier.notifyCompleted()
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier : ARxNotifier<Bool>) in

                if error == nil
                {
                    if !haveEmittedResult
                    {
                        haveEmittedResult = true
                        notifier.notifyItem(false)
                    }
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservableMap<ItemOutType, Bool>(tag: "contains", evalOp: evalOp).chainObservable(self)
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///  RxObservable extension:  Where ItemOutType is Comparable.
///

extension RxObservableMap where ItemOutType : Comparable
{
///
    /// Min: Emit the calculation the minimum of number based item notifications received and then terminate.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#MaxAndMin )
    /// - [Microsoft Doc Link]( https://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.min(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/min.html )
    ///

    public final func min() -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var minItem: ItemOutType? = nil

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                if (minItem == nil) || (item < minItem!)
                {
                    minItem = item
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if error == nil
                {
                    if minItem != nil
                    {
                        notifier.notifyItem(minItem!)
                    }
                    else
                    {
                        notifier.notifyCompleted(RxLibError(.eNoSuchElement))

                        return
                    }
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservable<ItemOutType>(tag: "min", evalOp: evalOp).chainObservable(self)
    }

    ///
    /// Max: Emit the calculation the maximum of number based item notifications received and then terminate.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#MaxAndMin )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.max(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/max.html )
    ///

    public final func max() -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var maxItem: ItemOutType? = nil

            evalNode.itemDelegate = { (item: ItemOutType, notifier: ARxNotifier<ItemOutType>) in

                if (maxItem == nil) || (maxItem! < item)
                {
                    maxItem = item
                }
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if error == nil
                {
                    if maxItem != nil
                    {
                        notifier.notifyItem(maxItem!)
                    }
                    else
                    {
                        notifier.notifyCompleted(RxLibError(.eNoSuchElement))

                        return
                    }
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservable<ItemOutType>(tag: "max", evalOp: evalOp).chainObservable(self)
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///  RxObservable extension: Where ItemOutType is RxSummable.
///

public protocol RxSummable
{
    /// RxSummable + operator.
    func +(lhs: Self, rhs: Self) -> Self

    /// Initialise RxSummable.
    init()
}

extension Int : RxSummable {}
extension UInt : RxSummable {}
extension Int32 : RxSummable {}
extension Int64 : RxSummable {}
extension UInt32 : RxSummable {}
extension UInt64 : RxSummable {}
extension Float32 : RxSummable {}
extension Float64 : RxSummable {}

extension RxObservableMap where ItemOutType : RxSummable
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Sum: Emit the calculation the sum of number based item notifications received and then terminate.
    ///
    /// - [Intro Doc Link]( http://www.introtorx.com/Content/v1.0.10621.0/07_Aggregation.html#MaxAndMin )
    /// - [Microsoft Doc Link]( http://msdn.microsoft.com/en-us/library/system.reactive.linq.observable.sum(v=vs.103).aspx )
    /// - [Netflix Doc Link]( http://reactivex.io/documentation/operators/sum.html )
    ///

    public final func sum() -> RxObservable<ItemOutType>
    {
        let evalOp = { (evalNode : RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            var sum = ItemOutType()

            evalNode.itemDelegate = { (item: ItemOutType, notifier : ARxNotifier<ItemOutType>) in

                sum = sum + item
            }

            evalNode.completedDelegate = { (error: IRxError?, notifier: ARxNotifier<ItemOutType>) in

                if error == nil
                {
                    notifier.notifyItem(sum)
                }

                notifier.notifyCompleted(error)
            }
        }

        return RxObservable<ItemOutType>(tag: "sum", evalOp: evalOp).chainObservable(self)
    }
}


extension RxObservableMap
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// complete: Convenience RxObservableMap, only emits a completed/error notification.
    ///
    /// - Parameter tag: The RxObject tag for the instance.
    /// - Parameter error: The error to emit. Nil for no error.
    ///
    public class func complete(tag: String = "complete", error : IRxError? = nil) -> RxObservableMap<ItemOutType, ItemOutType>
    {
        let evalOp = { (evalNode: RxEvalNode<ItemOutType, ItemOutType>) -> Void in

            evalNode.stateChangeDelegate = { (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        notifier.notifyCompleted(error)

                    default:
                        // do nothing.
                        break
                }
            }
        }

        return RxObservableMap<ItemOutType, ItemOutType>(tag: tag, evalOp: evalOp)
    }
}


extension RxObservableMap
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// testObservable: An RxObservable that can be used to monitor EvalNode operation. This is useful for unit testing.
    ///
    /// - Parameter tag: The RxObject tag for the instance.
    /// - Parameter testNotifier: The notifier that is notified of notifications and the working RxEvalNode handling these notifications.
    ///

    func testObservable(tag: String = "testObservable", testNotifier : RxNotifier_NotifyTestDelegates<ItemType>) -> RxObservableMap<ItemType, ItemType>
    {
        let evalOp = { (evalNode: RxEvalNode<ItemType, ItemType>) -> Void in

            evalNode.stateChangeDelegate = { [unowned evalNode]  (stateChange: eRxEvalStateChange, notifier: ARxNotifier<ItemOutType>) in

                testNotifier.notifyStateChange(stateChange, evalNode: evalNode)
                notifier.notifyStateChange(stateChange)
            }

            evalNode.completedDelegate = { (error : IRxError?, notifier: ARxNotifier<ItemType>) in

                testNotifier.notifyCompleted(error, evalNode: evalNode)
                notifier.notifyCompleted(error)
            }

            evalNode.stateChangeDelegate = { (stateChange : eRxEvalStateChange, notifier: ARxNotifier<ItemType>) in

                testNotifier.notifyStateChange(stateChange, evalNode: evalNode)
            }
        }

        return RxObservableMap<ItemType, ItemType>(tag: tag, evalOp: evalOp).chainObservable(self)
    }
}
