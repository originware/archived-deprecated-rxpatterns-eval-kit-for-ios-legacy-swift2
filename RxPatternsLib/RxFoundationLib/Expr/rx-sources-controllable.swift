//
// Created by Terry Stillone (http://www.originware.com) on 29/04/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eRxSwitchSourceControl: Control commands for the RxSwitchableSource.
///
/// - eSwitchSourceControl_SwitchOff:       Switch off, source is inactive.
/// - eSwitchSourceControl_SwitchToSource:  Switch to the given source.
/// - eSwitchSourceControl_SendCompleted:   Send a completed/error message to the sources.
///

public enum eRxSwitchSourceControl<ItemType>
{
    case eSwitchSourceControl_SwitchOff
    case eSwitchSourceControl_SwitchToSource(source : RxSource<ItemType>)
    case eSwitchSourceControl_SendCompleted(error : IRxError?)
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxSwitchableSource: A source which subscribes to a control stream that directs it to switch between data sources.
///
/// - Templates: <DataItemType>:   The data ItemType of the sources.

public class RxSwitchableSource<ItemType> : RxSource<ItemType>, IRxSubject
{
    public typealias ControlItemType = eRxSwitchSourceControl<ItemType>
    public typealias ObserverItemType = ControlItemType

    /// The source of data notifications for the sources being controller.
    private let m_dataNotifier: RxNotifier_NotifyConsumers<ItemType>
    
    /// Collection of the subscriptions to sources being controlled.
    private var m_subscriptions = [RxSubscription]()

    /// Initialise with tag and  the control source
    /// - Parameter tag: The RxObject tag for this source.
    /// - Parameter subscriptionType: The subscription type.
    /// - Parameter evalOp: evaluation operation of the source.
    public required init(tag: String, subscriptionType: eRxSubscriptionType, evalOp: RxEvalOp? = nil)
    {
        self.m_dataNotifier = RxNotifier_NotifyConsumers<ItemType>(tag: tag)

        super.init(tag: tag, subscriptionType : subscriptionType, evalOp: evalOp)
    }

    /// Handle the control notification.
    /// - Parameter control: The control Item being notified.
    public func notifyItem(control: ControlItemType)
    {
        switch control
        {
        case .eSwitchSourceControl_SwitchOff:
            
            m_dataNotifier.clear()
            
        case .eSwitchSourceControl_SwitchToSource(let source):
            
            source.subscribe(m_dataNotifier).addToContainer(&m_subscriptions, tag : source.tag)
            
        case .eSwitchSourceControl_SendCompleted(let error):
            
            m_dataNotifier.notifyCompleted(error)
        }
    }
    
    /// Handle completion/error of the control source.
    /// - Parameter error: The error or nil if completed.
    public func notifyCompleted(error : IRxError? = nil)
    {
        m_dataNotifier.notifyCompleted(error)
    }

    public func notifyStateChange(stateChange: eRxEvalStateChange)
    {
        // do nothing.
    }

    /// Handle Eval state change of the control source.
    public override func createEvalOpDelegate() -> RxEvalOp
    {
        return { (evalNode : RxEvalNode<ItemType, ItemType>) in

            evalNode.stateChangeDelegate = { (stateChange : eRxEvalStateChange, notifier : ARxNotifier<ItemType>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eSubscriptionBegin:

                        self.m_dataNotifier.appendConsumer(notifier)

                    case eRxEvalStateChange.eEvalEnd:

                        self.m_dataNotifier.removeConsumer(notifier)

                    default:
                        break
                }
            }
        }
    }
}
