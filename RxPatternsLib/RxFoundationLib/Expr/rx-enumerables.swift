//
// Created by Terry Stillone (http://www.originware.com) on 4/07/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxObservableEnumerable: An enumerator of a collection of Observables.
///

public class RxObservableEnumerable<DataItemType> : IRxEnumerable, CustomStringConvertible
{
    public typealias EnumeratorGeneratorType = AnyGenerator<ARxProducer<DataItemType>>

    /// CustomStringConvertible compliance.
    public var description : String                 { return "RxObservableEnumerable" }
    
    /// The generator of RxObservables.
    private var m_generator : EnumeratorGeneratorType

    /// Indicator of termination state.
    private var m_termination : etTerminationType? = nil

    /// The current stream subscription.
    private var m_currentSubscription: RxSubscription? = nil

    /// Initialise with the streams to enumerate over.
    /// - Parameter streams: The ARxProducer to enumerate over.
    public init(streams : [ARxProducer<DataItemType>])
    {
        var nextIndex : Int = 0
        let count = streams.count

        m_generator = AnyGenerator
        {
            if nextIndex < count
            {
                let currentIndex = nextIndex

                nextIndex += 1

                return streams[currentIndex]
            }
            else
            {
                return nil
            }
        }
    }

    /// Initialise with an Observable which will emit the ARxProducer-s to enumerate over.
    /// - Parameter streams: The RxObservables to enumerate over.
    public init(streams : ARxProducer<ARxProducer<DataItemType>>)
    {
        m_generator = streams.toEnumerable().generate()
    }

    /// SequenceType conformance.
    public func generate() -> EnumeratorGeneratorType
    {
        return m_generator
    }

    /// Get the next RxObservable.
    /// - Returns: The next RxObservable.
    public func next() -> ARxProducer<DataItemType>?
    {
        return m_generator.next()
    }

    /// Set terminate the enumerable.
    public func terminate(termination : etTerminationType)
    {
        m_termination = termination
        m_currentSubscription?.unsubscribe()
    }

    /// Send the notifications sent to the enumerator to the notifier.
    /// - Parameter sendCompleted: emit a completed notification to the notifier.
    public func sendToNotifier(notifier : ARxNotifier<DataItemType>, sendCompleted : Bool)
    {
        while let stream = next()
        {
            if m_termination != nil { break }

            let streamNotifier = RxNotifier_NotifyDelegates<DataItemType>(tag : notifier.tag + "/sendToNotifier")

            streamNotifier.onItem = { (item : DataItemType) in

                notifier.notifyItem(item)
            }

            streamNotifier.onCompleted = { (error : IRxError?) in

                if let error = error
                {
                    self.m_termination = .eTermination_Error(error)
                }
            }

            let subscription = stream.subscribe(streamNotifier)

            m_currentSubscription = subscription

            subscription.waitForDisposal()
        }

        if let termination = m_termination
        {
            notifier.notifyTermination(termination)
        }
        else if sendCompleted
        {
            notifier.notifyCompleted()
        }
    }
}