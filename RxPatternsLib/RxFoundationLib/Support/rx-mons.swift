//
// Created by Terry Stillone (http://www.originware.com) on 12/05/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The InstanceMon data provider interface, giving the instance data being tracked.
///
public protocol IRxMonInstanceDataProvider
{
    /// The active object descriptions by their instance ID
    var activeObjectDescriptionByInstanceID: [RxInstanceID : String]            { get }

    /// The active object tags by their instance IDs.
    var activeObjectTagByInstanceID: [RxInstanceID : String]                    { get }

    /// The allocated object-counts by their object types.
    var allocatedObjectypeCountByObjectType: [eRxObjectType : Int]              { get }
}

/// Support extension.
extension IRxMonInstanceDataProvider
{
    var objectCount : Int                                       { return activeObjectDescriptionByInstanceID.count }

    /// The total count of objects created.
    public var objectsCreatedCount : Int
    {
        var total = 0

        for (_, count) in allocatedObjectypeCountByObjectType
        {
            total += count
        }

        return total
    }
}

///
/// The InstanceMon data store (stack)
///
public struct RxMonInstanceStack
{
    /// The InstanceMon stack entry data model.
    public class RxMonInstanceDataItem : IRxMonInstanceDataProvider
    {
        /// Dictionary of object names by their object instance IDs
        public var activeObjectDescriptionByInstanceID = [RxInstanceID : String]()

        /// Dictionary of object tags by their object instance IDs.
        public var activeObjectTagByInstanceID = [RxInstanceID : String]()

        /// Dictionary of object type count by their object type.
        public var allocatedObjectypeCountByObjectType = [eRxObjectType : Int]()

        /// Inc count for RxObject.
        func incObjecTypeCount(objectType : eRxObjectType)
        {
            if let count = allocatedObjectypeCountByObjectType[objectType]
            {
                allocatedObjectypeCountByObjectType[objectType] = count + 1
            }
            else
            {
                allocatedObjectypeCountByObjectType[objectType] = 1
            }
        }
    }

    /// The current data set on the stack.
    public var current : RxMonInstanceDataItem                  { return stack.last! }

    /// Stack of count results.
    public var stack = [RxMonInstanceDataItem]()

    /// Initialise.
    public init()
    {
        // Populate stack with an initial entry.
        save()
    }

    /// Save (push) a new entry onto the stack.
    public mutating func save()                                 { stack.append(RxMonInstanceDataItem()) }

    /// Restore (pop) the current entry from the stack.
    public mutating func restore()                              { stack.removeAtIndex(stack.count - 1) }

    /// Indicate if the object as given by its Instance ID is active.
    public func objectIsAlive(instanceID: RxInstanceID) -> Bool
    {
        for level in stack
        {
            if level.activeObjectDescriptionByInstanceID[instanceID] != nil
            {
                return true
            }
        }

        return false
    }
}

///
/// RxMonInstances: Monitor RxObject allocations and deallocations.
///
public class RxMonInstances: IRxMon
{
    /// Evalqueue types to ignore.
    private static let evalQueuesToIgnore : Set<eRxEvalQueueType> = [.eMonitor, .eEvalQueueManagerQueuer, .eQueuer, .eTimer]

    /// The RxObject tag for this instance.
    public var tag : String

    /// The stats for instance monitoring.
    public var stack = RxMonInstanceStack()

    /// Switch for remarking on tracking inconsistencies observed during processing.
    public var remarkOnInconsistencies = false
    
    /// The queue used to perform monitor processing.
    private let m_monEvalQueue = RxEvalQueue(sourceTag : "RxMonInstances", evalQueueType : .eMonitor)

    /// Switch for locking the Monitor tracking state.
    private var m_locked = false

    /// Indicator of whether the monitor is locked from further object monitoring.
    public var locked : Bool
    {
        get                 { return m_locked }
        set(newValue)       { m_monEvalQueue.dispatchStrictSync({ self.m_locked = newValue }) }
    }

    /// Initialise.
    public init()
    {
        tag = "RxMonInstances"
    }

    /// Handle non-item instance event.
    public func onInstanceEvent(monType: eRxMonType_Instance)
    {
        if m_locked
        {
            //RxLog.log("\n>>>> RxMonInstances Note: while locked, received dealloction/allocation event: \(monType.description)\n")
            return
        }

        switch monType
        {
            case .eCreateObject(let object):
                onInstanceCreate(object)

            case .eDestroyObject(let object):
                onInstanceDestroy(object)

            case .eStopEvalQueue:
                // do nothing
                break
        }
    }

    /// Handle an object tag event.
    /// - Parameter monType: The monitored event type.
    public func onOpAction(monType : eRxMonType_Action)
    {
        switch monType
        {
            case .eTagObject(let object):
                m_monEvalQueue.dispatchStrictSync({

                    self.stack.current.activeObjectTagByInstanceID[object.instanceID] = object.tag
                })

            default:
                // do nothing.
                break
        }
    }

    /// Handle instance Creation.
    /// - Parameter object: The object that was created.
    public func onInstanceCreate(object : RxObject)
    {
        let instanceID = object.instanceID
        let objectTag = object.tag
        let description = object.description
        let className = object.dynamicType
        let objectType = object.objectType

        m_monEvalQueue.dispatchStrictSync({

            let data = self.stack.current

            if data.activeObjectDescriptionByInstanceID[instanceID] != nil
            {
                RxLog.log("RxMonInstances: error: duplicate object instanceID for \(objectTag)\n")
            }
            else if objectType != .eRxObject_RxEvalQueue
            {
                data.incObjecTypeCount(objectType)
                data.activeObjectDescriptionByInstanceID[instanceID] = "\(className)(\(description))"
                data.activeObjectTagByInstanceID[instanceID] = objectTag
            }
            else if let evalQueue = object as? RxEvalQueue
            {
                if !RxMonInstances.evalQueuesToIgnore.contains(evalQueue.evalQueueType)
                {
                    data.incObjecTypeCount(objectType)
                    data.activeObjectDescriptionByInstanceID[instanceID] = "\(className)(\(description))"
                    data.activeObjectTagByInstanceID[instanceID] = objectTag
                }
            }
        })
    }

    /// Handle instance Destruction.
    /// - Parameter object: The object that was destroyed.
    public func onInstanceDestroy(object : RxObject)
    {
        let instanceID = object.instanceID
        let objectTag = object.tag
        let objectType = object.objectType

        m_monEvalQueue.dispatchStrictSync({

            switch object.objectType
            {
                case .eRxObject_RxDisposable:

                    if let disposable = object as? RxDisposable
                    {
                        if !disposable.disposed
                        {
                            RxLog.log("RxDisposable: error: not disposed name \(disposable.tag)\n")
                        }
                    }

                default:
                    break
            }

            let data = self.stack.current

            if data.activeObjectDescriptionByInstanceID[instanceID] != nil
            {
                if objectType != .eRxObject_RxEvalQueue
                {
                    data.activeObjectDescriptionByInstanceID.removeValueForKey(instanceID)
                    data.activeObjectTagByInstanceID.removeValueForKey(instanceID)
                }
                else if let evalQueue = object as? RxEvalQueue
                {
                    if !RxMonInstances.evalQueuesToIgnore.contains(evalQueue.evalQueueType)
                    {
                        data.activeObjectDescriptionByInstanceID.removeValueForKey(instanceID)
                        data.activeObjectTagByInstanceID.removeValueForKey(instanceID)
                    }
                }
            }
            else if self.remarkOnInconsistencies
            {
                RxLog.log("RxMonInstances.onInstanceDestroy: error: missing object for \(objectTag) instanceID :\(instanceID)\n")
            }
        })
    }

    /// The current instance object count.
    public var objectCount : RxCountType                        { return stack.current.objectCount }

    /// Save (push) a new entry onto the stack.
    public func save()                                          { m_monEvalQueue.dispatchStrictSync({ self.stack.save() }) }

    /// Restore (pop) the current entry from the stack.
    public func restore()                                       { m_monEvalQueue.dispatchStrictSync({ self.stack.restore() }) }

    /// Report on object stats.
    /// - Returns: The report.
    public func reportObjectStats() -> String                   { return reportObjectStats(stack.current) }

    /// Report on selective non deallocated objects.
    /// - Parameter reportFunc: The event selection func.
    /// - Returns: The report.
    public func reportNonDeallocatedObjects() -> String         { return reportNonDeallocatedObjects(stack.current) }

    /// Indicate if the object as given by InstanceID is still alive
    public func objectIsAlive(instanceID: RxInstanceID) -> Bool
    {
        var result = false
        
        m_monEvalQueue.dispatchStrictSync({
            
            result = self.stack.objectIsAlive(instanceID)
        })
        
        return result
    }
}

extension RxMonInstances
{
    /// The current count of user (non-system objects)
    public var userObjectCount : RxCountType                    { return getUserObjectCount(stack.current) }

    /// Get the count of non system objects.
    private func getUserObjectCount(data : IRxMonInstanceDataProvider) -> RxCountType
    {
        var count = 0

        m_monEvalQueue.dispatchStrictSync({

            if data.objectCount > 0
            {
                for (_, description) in data.activeObjectDescriptionByInstanceID
                {
                    // Do not include system eval queues.
                    if description.rangeOfString("RxSystem/.*/evalQueue", options: .RegularExpressionSearch) == nil
                    {
                        count += 1
                    }
                }
            }
        })

        return count
    }

    /// Report on object stats.
    /// - Parameter data: The current stack data entry.
    /// - Returns: The report.
    private func reportObjectStats(data : IRxMonInstanceDataProvider) -> String
    {
        var result = ""

        m_monEvalQueue.dispatchStrictSync({

            /// Pad a string (used for reports).
            func padString(string : String, _ padLength : Int) -> String
            {
                let stringLength = string.characters.count
                let paddingCount = (stringLength < padLength) ? padLength - stringLength : 0

                let padding = String(count: paddingCount, repeatedValue:Character(" "))

                return string + padding
            }

            if data.allocatedObjectypeCountByObjectType.count > 0
            {
                result += "\nRxMonInstance Report:    Total RxObjects Created: \(data.objectsCreatedCount)\n\n"

                for objectType in data.allocatedObjectypeCountByObjectType.keys.sort({$0.rawValue < $1.rawValue})
                {
                    result += String(format:"\t%@%d\n", padString("ObjectType(\(objectType.description)):", 40), data.allocatedObjectypeCountByObjectType[objectType]!)
                }
            }
        })

        return result
    }

    /// Report on non deallocated objects.
    /// - Parameter data: The current stack data entry.
    /// - Returns: The report.
    private func reportNonDeallocatedObjects(data : IRxMonInstanceDataProvider) -> String
    {
        var result = ""

        m_monEvalQueue.dispatchStrictSync({

            if data.objectCount > 0
            {
                var userReportContent = ""
                var sdkReportContent = ""
                var unallocatedUserCount = 0

                for (instanceID, description) in data.activeObjectDescriptionByInstanceID
                {
                    let tag = data.activeObjectTagByInstanceID[instanceID]!

                    // Do not include system eval queues.
                    if description.rangeOfString("RxSystem/.*/evalQueue", options: .RegularExpressionSearch) == nil
                    {
                        userReportContent += "\t\(description) tag[\(tag)]\n"
                        unallocatedUserCount += 1
                    }
                    else
                    {
                        sdkReportContent += "\t\(description) tag[\(tag)]\n"
                    }
                }

                if (userReportContent.characters.count > 0) || (sdkReportContent.characters.count > 0)
                {
                    let formattedUserReport = (userReportContent.characters.count > 0) ? "\(userReportContent)\n\n" : ""
                    let formattedSDKReport = (sdkReportContent.characters.count > 0) ? "\(sdkReportContent)\n" : ""

                    result = "\nRxMonInstances: Unallocated User RxObjects: \(unallocatedUserCount), Unallocated SDK Internal RxObjects: \(data.objectCount - unallocatedUserCount)\n\n" + formattedUserReport + formattedSDKReport
                }
            }
        })

        return result
    }

}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxMonEvalQueue: Monitor RxEvalQueue Allocations and Deallocations.
///

public class RxMonEvalQueue: IRxMon
{
    /// The RxObject tag for this instance.
    public var tag : String

    /// Dictionary of eval queue tags by their object instance IDs
    public var evalQueueCreatedTagsByByEvalQueueType = [eRxEvalQueueType : [String]]()
    
    /// Dictionary of eval queue count by their object type.
    public var evalQueueTypeCreatedCountByEvalQueueType = [eRxEvalQueueType : Int]()

    /// Switch for remarking on tracking inconsistencies observed during processing.
    public var remarkOnInconsistencies = false
    
    /// The queue used to perform monitor processing.
    private let m_monEvalQueue = RxEvalQueue(sourceTag : "RxMonEvalQueue", evalQueueType : .eMonitor)
    
    /// Switch for locking the Monitor tracking state.
    private var m_locked = false

    /// Indicator of whether the Monitor tracking state is locked.
    public var locked : Bool
    {
        get                 { return m_locked }
        set(newValue)       { m_monEvalQueue.dispatchStrictSync({ self.m_locked = newValue }) }
    }
    
    /// The total count of RxEvalQueues Created
    public var createdTotalQueueCount : Int
    {
        var total = 0
        
        for count in evalQueueTypeCreatedCountByEvalQueueType.values
        {
            total += count
        }
        
        return total
    }
    
    /// The count of Evaluation RxEvalQueues Created
    public var createdEvaluationRxEvalQueueCount : Int
    {
        if let count = evalQueueTypeCreatedCountByEvalQueueType[.eEval]
        {
            return count
        }
        
        return 0
    }

    /// Initialise.
    public init()
    {
        tag = "RxSystem/RxMonEvalQueue"
    }
    
    /// Handle non-item instance event.
    public func onInstanceEvent(monType: eRxMonType_Instance)
    {
        if m_locked
        {
            RxLog.log(">>>> RxMonEvalQueue locked and received: \(monType.description)\n")
            return
        }
        
        switch monType
        {
        case .eCreateObject(let object):
            
            if let evalQueue = object as? RxEvalQueue
            {
                onInstanceCreate(evalQueue)
            }

        case .eDestroyObject:
            /// do nothing.
            break
            
        case .eStopEvalQueue:
            // do nothing.
            break
        }
    }
    
    /// Handle instance Creation.
    /// - Parameter object: The object that was created.
    public func onInstanceCreate(evalQueue : RxEvalQueue)
    {
        m_monEvalQueue.dispatchStrictSync({
            
            /// Inc count for RxObject.
            func incEvalQueueTypeCount(evalQueue : RxEvalQueue)
            {
                if let count = self.evalQueueTypeCreatedCountByEvalQueueType[evalQueue.evalQueueType]
                {
                    self.evalQueueTypeCreatedCountByEvalQueueType[evalQueue.evalQueueType] = count + 1
                }
                else
                {
                    self.evalQueueTypeCreatedCountByEvalQueueType[evalQueue.evalQueueType] = 1
                }
            }
            
            func addTagEvalQueueType(evalQueue : RxEvalQueue)
            {
                if var tags = self.evalQueueCreatedTagsByByEvalQueueType[evalQueue.evalQueueType]
                {
                    tags.append(evalQueue.tag)
                    
                    self.evalQueueCreatedTagsByByEvalQueueType[evalQueue.evalQueueType] = tags
                }
                
                self.evalQueueCreatedTagsByByEvalQueueType[evalQueue.evalQueueType] = [evalQueue.tag]
            }
            
            incEvalQueueTypeCount(evalQueue)
            addTagEvalQueueType(evalQueue)
        })
    }
    
    /// Report on created RxEvalQueues.
    /// - Returns: The report.
    public func reportRxEvalQueueUsage() -> String
    {
        var result = ""
        
        m_monEvalQueue.dispatchStrictSync({
            
            if self.evalQueueCreatedTagsByByEvalQueueType.count > 0
            {
                result += "\nRxMonEvalQueue: RxEvalQueues: \(self.createdTotalQueueCount)\n\n"
                
                for (evalQueueType, count) in self.evalQueueCreatedTagsByByEvalQueueType
                {
                    result += "\tRxEvalQueue: \(evalQueueType.description)\t\(count)\n"
                }
            }
        })
        
        return result
    }

    /// Report on created RxEvalQueues used for RxEvalNode evaluation.
    /// - Returns: The report.
    public func reportEvaluationRxEvalQueueUsage() -> String
    {
        var result = ""
        
        m_monEvalQueue.dispatchStrictSync({
            
            if let tags = self.evalQueueCreatedTagsByByEvalQueueType[.eEval]
            {
                result += "\nRxMonEvalQueue: Evaluation EvalQueues: \(tags.count)\n\n"
                
                for tag in tags
                {
                    result += "\tEvaluation Queue: \(tag)\n"
                }
                
                result += "\n"
            }
        })
        
        return result
    }

    /// Clear all tracked objects.
    public func clear()
    {
        m_monEvalQueue.dispatchStrictSync({

            self.evalQueueCreatedTagsByByEvalQueueType.removeAll()
            self.evalQueueTypeCreatedCountByEvalQueueType.removeAll()
        })
    }
    
    /// Pad a string (used for reports).
    private final func padString(string : String, _ padLength : Int) -> String
    {
        let stringLength = string.characters.count
        let paddingCount = (stringLength < padLength) ? padLength - stringLength : 0
        
        let padding = String(count: paddingCount, repeatedValue:Character(" "))
        
        return string + padding
    }
}
