//
// Created by Terry Stillone (http://www.originware.com) on 12/08/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxDateFormatter: Convert between RxDuration/RxTime and String.
///

class RxDateFormatter
{
    /// The time format.
    static let TimeFormat = "yyyy-MM-dd HH:mm:ss zzz"
    
    /// The time format with micro sec.
    static let TimeFormatMicroSec = "yyyy-MM-dd HH:mm:ss.SSSSSS"

    /// Format duration to String.
    /// - Parameter duration: The duration to be formatted.
    /// - Returns: The formatted result.
    class func durationFormatter(duration : RxDuration) -> String
    {
        let sign = duration < 0 ? "-" : ""
        let absDuration = abs(duration)

        let ti = Int(absDuration)
        let (seconds, minutes, hours, microSecs) = (ti % 60, (ti / 60) % 60, ti / 3600, Int((absDuration - Double(ti)) * 1000000))

        return String(format: "%@%02d:%02d:%02d:%06d", sign, hours, minutes, seconds, microSecs)
    }

    /// Decode a formatted duration to RxDuration.
    /// - Parameter formattedDuration: The formatted duration.
    /// - Returns: The decoded duration.
    class func durationDecoder(formattedDuration : String) -> RxDuration?
    {
        let tokens = formattedDuration.componentsSeparatedByString(":")

        if tokens.count != 4
        {
            return nil
        }

        let (hours, minutes, seconds, microSecs) = (Int(tokens[0]), Int(tokens[1]), Int(tokens[2]), Int(tokens[3]))

        if (hours == nil) || (minutes == nil) || (seconds == nil) || (microSecs == nil)
        {
            return nil
        }

        return RxDuration((hours! * 3600) + (minutes! * 60) + seconds!) + RxDuration(microSecs!) / 1000000.0
    }

    /// Format a time to String.
    /// - Parameter time: The time to be formatted.
    /// - Returns: The formatted result.
    class func timeFormatter(time : RxTime) -> String
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = TimeFormat

        return dateFormatter.stringFromDate(RxTime())
    }

    /// Format a time to String with micro secs.
    /// - Parameter time: The time to be formatted.
    /// - Returns: The formatted result.
    class func timeFormatterMicroSec(time : RxTime, dateFormatter : NSDateFormatter) -> String
    {
        dateFormatter.dateFormat = TimeFormatMicroSec

        return dateFormatter.stringFromDate(RxTime())
    }

    /// Decode a formatted time to RxTime.
    /// - Parameter time: The formatted time.
    /// - Returns: The decoded time.

    class func timeDecoder(time : String, dateFormatter : NSDateFormatter) -> RxTime?
    {
        dateFormatter.dateFormat = TimeFormat

        return dateFormatter.dateFromString(time)
    }

    /// Decode a formatted time to RxTime with micro secs.
    /// - Parameter time: The formatted time.
    /// - Returns: The decoded time.
    class func timeDecoderMicroSec(time : String, dateFormatter : NSDateFormatter) -> RxTime?
    {
        dateFormatter.dateFormat = TimeFormatMicroSec

        return dateFormatter.dateFromString(time)
    }
}
