//
// Created by Terry Stillone (http://www.originware.com) on 24/04/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eWindowEvent: Time window events: start time window and end time window.
///

public enum eWindowEvent
{
    /// The start of the Time Window event.
    case eWindowStart(RxIndexType, RxTimeOffset)

    /// The end of the Time Window event.
    case eWindowEnd(RxIndexType, RxTimeOffset)
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eWindowCommand: The RxWindowCommander manipulator commands. For use with the <- operator on the window.
///

public enum eWindowCommand
{
    /// Set the window action as which provides the RxWindow and the eWindowEvent.
    case eSetWindowAction((RxWindow, eWindowEvent) -> Void)

    /// Set the window action as a simple action to be run at the window end.
    case eSetWindowEndAction(() -> Void)

    /// Set the window action as a action which provides a context.
    case eSetWindowEndActionWithContext(([String : Any]) -> Void)

    /// Set the eval queue.
    case eSetEvalQueue(RxEvalQueue)

    /// Set a context key.
    case eSetContextKeyValue(String, Any)
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxWindow: The manager of time windows.
///

public class RxWindow: RxObject
{
    /// Operation enabler.
    public var enabled :        Bool                                  = true

    /// Indicator of active windows.
    public var isActive :       Bool                                            { return m_isActive }

    /// The window action to be run when a window is triggered.
    public var windowAction :   ((RxWindow, eWindowEvent) -> Void)?   = nil

    /// The simple action that is triggered at the end of a window.
    public var windowEndAction: (() -> Void)?                         = nil

    /// The action with a context parameter.
    public var windowEndActionWithContext: (([String: Any]) -> Void)? = nil

    /// The context that can be made available to the window actions.
    public lazy var context : [String: Any] = [String: Any]()

    /// The time window timer.
    private lazy var m_timer : RxTimer = RxTimer(tag: self.tag)

    /// The timer subscription.
    private var timerSubscription : RxTimerSubscription? = nil

    /// Backing var for isActive.
    private var m_isActive = false

    /// The role of the class.
    public override var objectType : eRxObjectType              { return .eRxObject_Window }

    /// Initialise with instance tag.
    public init(tag : String)
    {
        super.init(tag : tag)
    }

    /// Create a new single shot window.
    /// - Parameter startTime: The reference starting time of the window.
    /// - Parameter duration: The window time duration.
    /// - Parameter action: The actions to be performed by window event.
    public func createSingleWindow(startTime: RxTime, duration : RxDuration)
    {
        let clampedDuration = duration > 0 ? duration : 0

        enabled = true
        m_isActive = true

        if isActive { timerSubscription?.terminate() }

        if windowAction != nil
        {
            timerSubscription = m_timer.addTickGenerator(startTime, generator: { (index : RxIndexType) -> RxTimeOffset? in

                guard self.enabled else { return nil }

                let timeError = -startTime.timeIntervalSinceNow - Double(index) * clampedDuration

                switch index
                {
                    case 0:
                        // Window begin tick.
                        self.windowAction?(self, eWindowEvent.eWindowStart(index / 2, timeError))
                        return clampedDuration

                    case 1:
                        // Window end tick.
                        self.windowAction?(self, eWindowEvent.eWindowEnd(index / 2, timeError))
                        self.m_isActive = false
                        return nil

                    default:
                        assert(false, "Unexpected code point")
                        self.m_isActive = false
                        return nil
                }
            })
        }
        else if windowEndAction != nil
        {
            timerSubscription = m_timer.addTimer(startTime.dateByAddingTimeInterval(duration), timerAction: { (index : RxIndexType) -> Void in

                guard self.enabled else { return }

                self.windowEndAction?()

                self.m_isActive = false
            })
        }
        else if windowEndActionWithContext != nil
        {
            timerSubscription = m_timer.addTimer(startTime.dateByAddingTimeInterval(duration), timerAction: { (index : RxIndexType) -> Void in

                guard self.enabled else { return }

                self.windowEndActionWithContext?(self.context)

                self.m_isActive = false
            })
        }
    }

    /// Create a new periodic window.
    /// - Parameter startTime: The reference starting time of the window.
    /// - Parameter startOffset: The window start time.
    /// - Parameter period: The window period duration.
    public func createPeriodicWindow(startTime: RxTime, startOffset: RxDuration = 0, period : RxDuration)
    {
        let clampedStart = startOffset > 0 ? startOffset : 0
        let clampedPeriod = period > 0 ? period : 0

        if isActive { timerSubscription?.terminate() }

        enabled = true
        m_isActive = true

        if windowAction != nil
        {
            timerSubscription = m_timer.addTickGenerator(startTime, generator: { (index : RxIndexType) -> RxTimeOffset? in

                guard self.enabled else { return nil }

                let timeError = -startTime.timeIntervalSinceNow - Double(index) * clampedPeriod

                switch index
                {
                    case 0:
                        break

                    case 1:
                        // Window begin tick.
                        self.windowAction?(self, .eWindowStart(0, timeError))

                    default:
                        // Window end tick
                        let windowIndex = index - 2

                        self.windowAction?(self, .eWindowEnd(windowIndex, timeError))
                        self.windowAction?(self, .eWindowStart(windowIndex + 1, timeError))
                }

                return clampedStart + RxDuration(index) * clampedPeriod
            })
        }
        else if windowEndAction != nil
        {
            timerSubscription = m_timer.addTickGenerator(startTime, generator: { (index : RxIndexType) -> RxTimeOffset? in

                guard self.enabled else { return nil }

                switch index
                {
                    case 0:
                        break

                    default:
                        // Window tick
                        self.windowEndAction?()
                }

                return clampedStart + RxDuration(index + 1) * clampedPeriod
            })
        }
        else if windowEndActionWithContext != nil
        {
            timerSubscription = m_timer.addTickGenerator(startTime, generator: { (index : RxIndexType) -> RxTimeOffset? in

                guard self.enabled else { return nil }

                switch index
                {
                    case 0:
                        break

                    default:
                        // Window tick
                        self.windowEndActionWithContext?(self.context)
                }

                return clampedStart + RxDuration(index + 1) * clampedPeriod
            })
        }
    }

    /// Cancel running window.
    @inline(__always) public func stopCurrentWindow()
    {
        enabled = false
        m_isActive = false

        if let timerSubscriptionReference = timerSubscription
        {
            timerSubscriptionReference.terminate()

            timerSubscription = nil
        }
    }

    /// Cancel all running windows.
    @inline(__always)  public func cancelAll()
    {
        enabled = false
        m_isActive = false
        timerSubscription = nil
        m_timer.cancelAll()
    }
}

infix operator <- { associativity left }

public func <-(window: RxWindow, command: eWindowCommand) -> RxWindow
{
    switch command
    {
        case .eSetWindowAction(let windowAction):

            window.windowAction = windowAction
            window.windowEndAction = nil
            window.windowEndActionWithContext = nil

        case .eSetWindowEndAction(let action):

            window.windowEndAction = action
            window.windowAction = nil
            window.windowEndActionWithContext = nil

        case .eSetWindowEndActionWithContext(let action):

            window.windowEndActionWithContext = action
            window.windowEndAction = nil
            window.windowAction = nil

        case .eSetEvalQueue(let evalQueue):

            window.m_timer.evalQueue = evalQueue

        case .eSetContextKeyValue(let (key, object)):

            window.context[key] = object
    }

    return window
}
