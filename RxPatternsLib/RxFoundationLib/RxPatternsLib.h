//
//  RxPatternsLib.h
//  RxPatternsLib
//
//  Created by Terry Stillone on 13/08/15.
//  Copyright (c) 2015 Originware. All rights reserved.
//


#import <UIKit/UIKit.h>
//#import "RxPatternsSDK.h"

//! Project version number for RxPatternsLib.
FOUNDATION_EXPORT double RxPatternsLibVersionNumber;

//! Project version string for RxPatternsLib.
FOUNDATION_EXPORT const unsigned char RxPatternsLibVersionString[];

