//
// Created by Terry Stillone (http://www.originware.com) on 29/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// String extensions
//

extension String // CustomStringConvertible
{
    public var description: String {

        return self
    }

    public var toFloat : Float? {

        let formatter = NSNumberFormatter()
        formatter.numberStyle = .DecimalStyle

        let number = formatter.numberFromString(self)

        return number != nil ? number!.floatValue : nil
    }

    public var toDouble : Double? {

        let formatter = NSNumberFormatter()
        formatter.numberStyle = .DecimalStyle

        let number = formatter.numberFromString(self)

        return number != nil ? number!.doubleValue : nil
    }

    public var toNSURL : NSURL? {

        return NSURL(string: self)
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// NSURL extensions
//

public extension NSURL
{
    public func toURLRequestReply(requestData : Any?) -> URLRequestReply
    {
        return URLRequestReply(url: self, requestData: requestData, replyData: nil)
    }
}

