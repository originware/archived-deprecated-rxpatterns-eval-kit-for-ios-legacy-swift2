//
// Created by Terry Stillone (http://www.originware.com) on 20/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

struct Helpers
{
    private static let LowThreadhold : CGFloat = 0.3
    private static let HighThreashold : CGFloat = 0.8

    static func clamptoZeroOne(value : CGFloat ) -> CGFloat
    {
        switch value
        {
            case _ where value < 0.0:
                return 0.0

            case _ where value > 1.0:
                return 1.0

            default:
                return value
        }
    }

    static func clamptoNegPos1(value : CGFloat ) -> CGFloat
    {
        switch value
        {
            case _ where value < -1.0:
                return -1.0

            case _ where value > 1.0:
                return 1.0

            default:
                return value
        }
    }

    static func threshold(value : CGFloat) -> CGFloat
    {
        switch value
        {
            case _ where value < LowThreadhold:
                return 0.0

            case _ where value > HighThreashold:
                return 1.0

            default:
                return value
        }
    }

    static func rangeToNSRange(string : String, _ range : Range<String.Index>) -> NSRange
    {
        let length = range.startIndex.distanceTo(range.endIndex)
        let start = string.startIndex.distanceTo(range.startIndex)
        let nsRange = NSMakeRange(start, length)

        return nsRange
    }

    static func doRangesIntersect(string : String, _ range1 : Range<String.Index>, _ range2 : Range<String.Index>) -> Bool
    {
        let nsRange1 = Helpers.rangeToNSRange(string, range1)
        let nsRange2 = Helpers.rangeToNSRange(string, range2)

        return NSIntersectionRange(nsRange1, nsRange2).length > 0
    }

    static func padString(string : String, _ length : Int) -> String
    {
        let stringLength : Int = string.characters.count
        let paddingCount = (stringLength < length) ? length - stringLength : 0
        let padding = String(count : paddingCount, repeatedValue: Character(" "))

        return string + padding
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// SubstringMatcher: A Utility class that performs substring matching by hashing sub-strings.
///

public class SubstringMatcher
{
    private var m_string : String
    private let m_minSubstringLength : Int
    private var m_substringHashToMatchRange = [Int : Range<String.Index>]()
    private var m_hashInDescendingSubstringLength = [Int]()

    public init(string : String, minSubstringLength : Int)
    {
        self.m_string = string
        self.m_minSubstringLength = minSubstringLength

        generateSubstringHashes()
    }

    private func generateSubstringHashes()
    {
        func addSubstring(range: Range<String.Index>)
        {
            let substring = m_string.substringWithRange(range)
            let hash = substring.hash

            if m_substringHashToMatchRange[hash] == nil
            {
                m_substringHashToMatchRange[hash] = range
                m_hashInDescendingSubstringLength.append(hash)
            }
        }

        let stringLength = m_string.startIndex.distanceTo(m_string.endIndex)
        let maxSubstringLength = stringLength
        let minSubstringLength = m_minSubstringLength

        for substringLength in maxSubstringLength.stride(to: minSubstringLength, by: -1)
        {
            let endStartIndex = m_string.startIndex.advancedBy(stringLength - substringLength)

            for startIndex in m_string.startIndex...endStartIndex
            {
                let range = Range<String.Index>(startIndex..<startIndex.advancedBy(substringLength))

                addSubstring(range)
            }
        }
    }

    public func getCommonSubstringMatches(other : SubstringMatcher) -> POIKeywordMatch
    {
        let matches = POIKeywordMatch(poiKeyword: m_string)

        for hash in m_hashInDescendingSubstringLength
        {
            if other.m_substringHashToMatchRange[hash] != nil
            {
                matches.addMatchForPOIKeyword(m_substringHashToMatchRange[hash]!)
            }
        }

        return matches
    }
}

