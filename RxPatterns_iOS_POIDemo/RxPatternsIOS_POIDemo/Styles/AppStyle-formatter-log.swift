//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

public class AppStyle_Log
{
    public class func getAppEventLogParagraphStyle() -> NSMutableParagraphStyle
    {
        let terminator = NSTextTab.columnTerminatorsForLocale(NSLocale.currentLocale())
        let tabs = [
                NSTextTab(textAlignment: NSTextAlignment.Left, location:30, options:[NSTabColumnTerminatorsAttributeName : terminator]),
                NSTextTab(textAlignment: NSTextAlignment.Left, location:100, options:[NSTabColumnTerminatorsAttributeName : terminator]),
                NSTextTab(textAlignment: NSTextAlignment.Left, location:250, options:[NSTabColumnTerminatorsAttributeName : terminator]),
                NSTextTab(textAlignment: NSTextAlignment.Left, location:450, options:[NSTabColumnTerminatorsAttributeName : terminator])
        ]

        let paraStyle = NSMutableParagraphStyle()

        paraStyle.tabStops = tabs
        paraStyle.headIndent = 0
        paraStyle.alignment = NSTextAlignment.Left

        return paraStyle
    }
}

class AppStyle_LogFormatter
{
    private let TimeFormat = "HH:mm:ss"
    private let Separator = "\t"
    private static let BackgroundColor1 = UIColor.clearColor()
    private static let BackgroundColor2 = UIColor.blueColor().colorWithAlphaComponent(0.1)

    private let m_datestampFormatter = NSDateFormatter()
    private var m_logLineNumber : Int  = 1
    private let m_paragraphStyle = AppStyle_Log.getAppEventLogParagraphStyle()
    private var m_currentLogText = NSMutableAttributedString()
    private var m_currentLineBackgroundColor = AppStyle_LogFormatter.BackgroundColor2

    private var m_currentLogLine = NSMutableAttributedString()


    var currentLogText : NSMutableAttributedString
    {
        return m_currentLogText
    }

    init()
    {
        self.m_datestampFormatter.dateFormat = TimeFormat
    }

    func nextLine()
    {
        m_currentLineBackgroundColor = (m_logLineNumber % 2 == 1) ? AppStyle_LogFormatter.BackgroundColor1 : AppStyle_LogFormatter.BackgroundColor2

        m_currentLogText.appendAttributedString(m_currentLogLine)

        m_currentLogLine = NSMutableAttributedString()

        m_logLineNumber += 1
    }

    func clearLog()
    {
        m_currentLogText = NSMutableAttributedString()
        m_logLineNumber = 1
    }

    func appendLogText(text: String, textColor : UIColor = UIColor.blackColor())
    {
        let atributedText = NSMutableAttributedString(string: text, attributes:[NSForegroundColorAttributeName : textColor, NSBackgroundColorAttributeName : m_currentLineBackgroundColor])

        m_currentLogLine.appendAttributedString(atributedText)
    }

    func appendTimestampText(timestamp : NSDate)
    {
        let formattedimestamp = m_datestampFormatter.stringFromDate(timestamp) + Separator
        let textColor = AppStyle.color(.eColor_LogTimeStamp)
        let atributedText = NSMutableAttributedString(string:formattedimestamp, attributes:[NSForegroundColorAttributeName : textColor, NSBackgroundColorAttributeName : m_currentLineBackgroundColor])

        m_currentLogLine.appendAttributedString(atributedText)
    }

    func appendMarkerText(showMarker : Bool, isPlayedEvent: Bool)
    {
        let markerText = showMarker ? "  \u{25C6}\(Separator)" : Separator
        let markerColor = isPlayedEvent ? AppStyle.color(.eColor_statusTextColor) : AppStyle.color(.eColor_errorTextColor)
        let attributes = showMarker ? [NSForegroundColorAttributeName : markerColor] : [NSBackgroundColorAttributeName : m_currentLineBackgroundColor]
        let atributedText = NSMutableAttributedString(string:markerText, attributes:attributes)

        m_currentLogLine.appendAttributedString(atributedText)
    }

    func appendLineEnd()
    {
        let atributedText = NSMutableAttributedString(string:"\n", attributes:[NSBackgroundColorAttributeName : m_currentLineBackgroundColor])

        m_currentLogLine.appendAttributedString(atributedText)
    }

    func formatLogLineEntry(appEvent : AppEvent, _ text : String, _ textColor : UIColor) -> NSAttributedString
    {
        let showMarker = appEvent.isSimulatedEvent
        let timestamp = appEvent.timestamp

        appendMarkerText(showMarker, isPlayedEvent : appEvent.isSimulatedEvent)
        appendTimestampText(timestamp)
        appendLogText(text, textColor : textColor)
        appendLineEnd()

        nextLine()

        return currentLogText
    }
}

extension AppStyle_LogFormatter
{
    func formatPOILocateResultForLog(appEvent : AppEvent, poilocateResult : POILocateEvent_Reply) -> NSAttributedString
    {
        formatLogLineEntry(appEvent, poilocateResult.description, AppStyle.color(.eColor_LogHighLightedEventText))

        return currentLogText
    }

    func formatPOIKeywordMatchLogText(appEvent : AppEvent) -> NSAttributedString
    {
        formatLogLineEntry(appEvent, appEvent.description, AppStyle.color(.eColor_LogEventText))

        return currentLogText
    }
}
