//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

extension AppStyle
{
    class func formatLocationChangeForUILabel(locationChange : eDeviceEvent_Location, isSimulated : Bool) -> NSAttributedString
    {
        switch locationChange
        {
            case .eLocation_Invalid:
                return NSAttributedString(string:"Unknown", attributes:[NSForegroundColorAttributeName : AppStyle.color(.eColor_NotAvailable)])

            case .eLocation_LastKnown:
                return NSAttributedString(string: isSimulated ? "Simulated Location" : "Have Location", attributes:[NSForegroundColorAttributeName : AppStyle.color(.eColor_Available)])
        }
    }

    class func formatReachabilityChangeForUILabel(reachbilityChange : eDeviceEvent_Reachability) -> NSAttributedString
    {
        switch reachbilityChange
        {
            case .eReachability_Unknown:
                return NSAttributedString(string:"Unknown", attributes:[NSForegroundColorAttributeName : AppStyle.color(.eColor_NotAvailable)])

            case .eReachability_NotReachable:
                return NSAttributedString(string:"Not Reachable", attributes:[NSForegroundColorAttributeName : AppStyle.color(.eColor_NotAvailable)])

            case .eReachability_IsReachable:
                return NSAttributedString(string:"Reachable", attributes:[NSForegroundColorAttributeName : AppStyle.color(.eColor_Available)])
        }
    }
}