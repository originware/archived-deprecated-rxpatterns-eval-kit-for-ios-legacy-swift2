//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import QuartzCore

public enum eColorElement : Int
{
    case eColor_borderColor
    case eColor_buttonColor
    case eColor_recordColor
    case eColor_playColor

    case eColor_sliderTrackColor
    case eColor_sliderNotchColor

    case eColor_Error
    case eColor_errorTextColor
    case eColor_statusTextColor

    case eColor_LogTimeStamp
    case eColor_LogEventText
    case eColor_LogHighLightedEventText

    case eColor_POISearchResultText
    case eColor_POIHighlightBackground
    case eColor_POIText

    case eColor_disabled
    case eColor_enabled

    case eColor_Available
    case eColor_NotAvailable
}

public enum eFontElement : Int
{
    case eFont_POIResultsText
}

public class AppStyle
{
    private static let m_color: [UIColor] = [

        UIColor(hue:0.0, saturation: 0.0, brightness: 0.5, alpha: 0.5),
        UIColor(hue:1.0, saturation: 0.0, brightness: 1.0, alpha: 1.0),
        UIColor(hue:1.0, saturation: 1.0, brightness: 0.9, alpha: 1.0),
        UIColor(hue:0.66, saturation: 1.0, brightness: 0.5, alpha: 1.0),

        UIColor(white: 0.7, alpha: 0.6),
        UIColor(white:0.6, alpha: 0.7),

        UIColor.redColor(),
        UIColor.redColor(),
        UIColor.blueColor(),

        UIColor.grayColor(),
        UIColor.orangeColor(),
        UIColor.purpleColor(),

        UIColor(red:0.5, green: 0.5, blue: 0.45, alpha: 1.0),
        UIColor(red:0.8, green: 0.8, blue: 1.0, alpha: 0.1),
        UIColor(red:0.2, green: 0.9, blue: 0.2, alpha: 1.0),

        UIColor.lightGrayColor(),
        UIColor(red:0.4, green: 0.8, blue: 0.5, alpha: 1.0),

        UIColor(red:0.2, green: 0.2, blue: 0.8, alpha: 1.0),
        UIColor(red:0.8, green: 0.2, blue: 0.2, alpha: 1.0),
    ]

    public static func color(item : eColorElement) -> UIColor
    {
        return m_color[item.rawValue]
    }

    public static func font(item : eFontElement) -> UIFont
    {
        switch item
        {
            case .eFont_POIResultsText:
                return UIFont(name:"Helvetica", size:18.0)!

        }
    }

    class func styleSearchResults(uiTextView : UITextView)
    {
        // Round all corners.
        uiTextView.layer.cornerRadius = 10.0
        uiTextView.layer.borderWidth = 1.0
        uiTextView.layer.borderColor = color(.eColor_borderColor).CGColor

        uiTextView.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10)
        uiTextView.scrollRangeToVisible(NSRange(location: 0, length: 0))
    }

    class func styleViewBorder(view : UIView)
    {
        var borderColor : UIColor = color(.eColor_borderColor)

        // Round all corners.
        view.layer.cornerRadius = 10.0
        view.layer.borderWidth = 1.0
        view.layer.borderColor = borderColor.CGColor

        if let button = view as? UIButton
        {
            // Border colour taken from the text colour.
            let textColor : UIColor = button.titleLabel!.textColor

            borderColor = textColor
        }
    }

    class func styleTextBorder(view : UIView)
    {
        var borderColor : UIColor = UIColor.whiteColor()

        // Round all corners.
        view.layer.cornerRadius = 10.0
        view.layer.borderWidth = 1.0
        view.layer.borderColor = borderColor.CGColor

        if let button = view as? UIButton
        {
            // Border colour taken from the text colour.
            let textColor : UIColor = button.titleLabel!.textColor

            borderColor = textColor
        }
    }

    class func styleButton(view : UIView)
    {
        var borderColor : UIColor = UIColor.whiteColor()

        // Round all corners.
        view.layer.cornerRadius = 10.0
        view.layer.borderWidth = 1.0
        view.layer.borderColor = borderColor.CGColor

        if let button = view as? UIButton
        {
            // Border colour taken from the text colour.
            let textColor : UIColor = button.titleLabel!.textColor

            borderColor = textColor
        }
    }

    class func styleTitleViewContainer(view : UIView)
    {
        let borderColor : UIColor = color(.eColor_borderColor)

        view.layer.borderWidth = 1.0
        view.layer.borderColor = borderColor.CGColor
    }

    class func createSliderThumbImage(sliderSize : CGSize, trackHeight : CGFloat) -> UIImage
    {
        let sectorSize : CGFloat = 1.0 / 3.0
        let sliderWidth : CGFloat = sliderSize.width
        let sectorNotchWidth : CGFloat = sliderSize.width * 0.1
        let thumbWidth : CGFloat = sliderSize.height
        let trackCentreYPos : CGFloat = (sliderSize.height - trackHeight) / 2.0
        let notchColour : UIColor = AppStyle.color(eColorElement.eColor_sliderNotchColor)
        let trackColour : UIColor = AppStyle.color(eColorElement.eColor_sliderTrackColor)

        UIGraphicsBeginImageContext(sliderSize)

        let context : CGContextRef = UIGraphicsGetCurrentContext()!

        // Draw the track background.
        CGContextSetFillColorWithColor(context, trackColour.CGColor)
        CGContextFillRect(context, CGRectMake(thumbWidth / 2, trackCentreYPos, sliderSize.width - thumbWidth, trackHeight))

        // Draw the notches in the track.
        CGContextSetFillColorWithColor(context, notchColour.CGColor)

        CGContextFillRect(context, CGRectMake(thumbWidth / 2 + sliderWidth * (0.5 - sectorSize) - sectorNotchWidth / 2.0, trackCentreYPos, sectorNotchWidth, trackHeight))
        CGContextFillRect(context, CGRectMake((sliderWidth - sectorNotchWidth) / 2.0, trackCentreYPos, sectorNotchWidth, trackHeight))
        CGContextFillRect(context, CGRectMake(-thumbWidth / 2 + sliderWidth * (0.5 + sectorSize) - sectorNotchWidth / 2.0, trackCentreYPos, sectorNotchWidth, trackHeight))

        let image = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()

        return image
    }

    class func getPOIKeywordsPlaceHolderParagraphStyle(columnCount : Int) -> NSMutableParagraphStyle
    {
        let entryLength = 150
        let paraStyle = NSMutableParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle

        let terms = NSTextTab.columnTerminatorsForLocale(NSLocale.currentLocale())
        var tabs = [NSTextTab]()

        for i in 1..<columnCount
        {
            let options = [NSTabColumnTerminatorsAttributeName: terms]
            let tab = NSTextTab(textAlignment:NSTextAlignment.Left, location:CGFloat(entryLength * i), options:options)

            tabs.append(tab)
        }

        paraStyle.tabStops = tabs
        paraStyle.headIndent = 0
        paraStyle.alignment = NSTextAlignment.Left

        return paraStyle
    }

    class func getFormattedPOIKeywordsPlaceHolder(columnCount : Int) -> NSAttributedString
    {
        let text                = NSMutableAttributedString()
        var currentColumnNumber = 0

        // Add POI Types with four columns.
        for poiKeyword in AppSettings.allPOIkeywords!
        {
            let lineText = poiKeyword
            let nextColumnNumber = currentColumnNumber + 1
            let terminator = (nextColumnNumber % columnCount) == 0 ? "\n" : "\t"

            currentColumnNumber = nextColumnNumber
            text.appendAttributedString(NSAttributedString(string: lineText + terminator))
        }

        let paraStyle = getPOIKeywordsPlaceHolderParagraphStyle(columnCount)

        text.addAttribute(NSParagraphStyleAttributeName, value:paraStyle, range:NSMakeRange(0, text.length))

        return text
    }
}