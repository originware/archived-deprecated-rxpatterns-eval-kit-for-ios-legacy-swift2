//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// AppEvent: App event representation.
///

class AppEvent : CustomStringConvertible
{
    /// Timestamp of the event.
    let timestamp : NSDate

    /// Indicator if the event is authentic or simulated.
    let isSimulatedEvent: Bool

    /// The type of event.
    let appEventType : eAppEventType

    /// CustomStringConvertible conformance.
    var description : String {

        let isReplayed: String = isSimulatedEvent ? "(replayed)" : ""

        return appEventType.description + isReplayed
    }

    /// - Parameter eAppEventType: The type of event.
    /// - Parameter timestamp: The time of the event.
    /// - Parameter isSimulatedEvent: Indicator of whether the event is simulated.
    init(appEventType : eAppEventType, timestamp : NSDate = NSDate(), isSimulatedEvent: Bool = false)
    {
        self.timestamp = timestamp
        self.isSimulatedEvent = isSimulatedEvent
        self.appEventType = appEventType
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eAppEventType: App event types.
///
/// - eLocationChange: Location change event.
/// - eOrientation: Orientation change event.
/// - eKeyboardChange: Keyboard Show/Hide change event.
/// - eNetworkReachabilityChange: Network Reachability change event.
/// - eUITextFieldChange: UITextField text change event.
/// - eUIControlTouch: Touch event on a UIControl
/// - ePOIKeywordMatchSet: POI keyword match on text entered.
/// - ePOILocateRequest: Generation of POI Locate Request event.
/// - ePOILocateResult: Receipt of POI Request Reply event.
/// - ePresentConfirmation: A confirmation dialog was presented.
///

enum eAppEventType
{
    case eLocationChange(eDeviceEvent_Location)
    case eOrientation(UIDeviceOrientation)
    case eKeyboardChange(eDeviceEvent_Keyboard)
    case eNetworkReachabilityChange(eDeviceEvent_Reachability)
    case eUITextFieldChange(eDeviceEvent_TextField)
    case eUIControlTouch((UIControl, eDeviceEvent_Touch))
    case ePOIKeywordMatchSet(POIKeywordEvent_Match)
    case ePOILocateRequest(POILocateEvent_Request)
    case ePOILocateResult(POILocateEvent_Reply)
    case ePresentConfirmation(String)

    var locationChange: eDeviceEvent_Location
    {
        switch self
        {
            case eLocationChange(let locationChange):
                return locationChange

            default:
                fatalError("Expected AppEvent to be an eLocationChange")
        }
    }

    var reachabilityChange: eDeviceEvent_Reachability
    {
        switch self
        {
            case eNetworkReachabilityChange(let reachabilityChange):
                return reachabilityChange

            default:
                fatalError("Expected AppEvent to be an eNetworkReachability")
        }
    }

    var poikeywordCollectionMatches: POIKeywordEvent_Match
    {
        switch self
        {
            case ePOIKeywordMatchSet(let matchSet):
                return matchSet

            default:
                fatalError("Expected AppEvent to be an ePOIKeywordMatchSet")
        }
    }

    var textFieldChange : eDeviceEvent_TextField
    {
        switch self
        {
            case eUITextFieldChange(let textFieldChange):
                return textFieldChange

            default:
                fatalError("Expected AppEvent to be an eUITextFieldChange")
        }
    }

    var touchControl : (UIControl, eDeviceEvent_Touch)
    {
        switch self
        {
            case eUIControlTouch(let (control, touchTarget)):
                return (control, touchTarget)

            default:
                fatalError("Expected AppEvent to be an eUIControlTouch")
        }
    }

    var confirmation : String
    {
        switch self
        {
            case ePresentConfirmation(let message):
                return message

            default:
                fatalError("Expected AppEvent to be an ePresentConfirmation")
        }
    }

    /// CustomStringConvertible conformance.
    var description : String
    {
        switch self
        {
            case eOrientation(let orientation):
                return "Orientation Change: \(eAppEventType.orientationNames[orientation.rawValue])"

            case eKeyboardChange:
                return "Keyboard Change"

            case eNetworkReachabilityChange:
                return "Network Reachability"

            case eUITextFieldChange:
                return "UITextField text change"

            case eUIControlTouch:
                return "UIControl Touch"

            case eLocationChange:
                return "Location Change"

            case ePOIKeywordMatchSet(let poiKeywordCollection_Matches):
                let count = poiKeywordCollection_Matches.count

                if count <= 3
                {
                    return "POI keyword Match: \(poiKeywordCollection_Matches.description)"
                }
                else
                {
                    return "POI keyword Match: \(count) keywords"
                }

            case ePOILocateRequest:
                return "POI Locate Request"

            case ePOILocateResult:
                return "POI Locate Result"

            case ePresentConfirmation:
                return "Present Confirmation"
        }
    }

    private static var orientationNames : [String] = [

        "Unknown", "Portrait", "PortraitUpsideDown", "LandscapeLeft", "LandscapeRight", "FaceUp", "FaceDown"
    ]
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eAppOpEventType: The App Operation Events that are presented on the App Operation UIView.
///

enum eAppOpEventType : String
{
    case eFromKeyboard_To_POIKeywordMatcherPath             = "FromKeyboard_To_POIKeywordMatcherPath"
    case eFromLocation_To_POIRequestor                      = "FromLocation_To_POIRequestor"
    case eFromReachability_To_POIRequestor                  = "FromReachability_To_POIRequestor"

    case eFromPOIKeywordMatcher_To_POIKeywordsResults       = "FromPOIKeywordMatcher_To_POIKeywordsResult"
    case eFromPOIKeywordMatcher_To_LogAdapter               = "FromPOIKeywordMatcher_To_LogAdapter"

    case eFromPOIKeywordMatcher_To_POIRequestor             = "FromPOIKeywordMatcher_To_POIRequestor"
    case eFromPOIRequestor_To_POILocator                    = "FromPOIRequestor_To_POILocator"

    case eFromPOILocator_To_LogAdaptor                      = "FromPOILocator_To_LogAdaptor"
    case eFromPOILocator_To_MapAdaptor                      = "FromPOILocator_To_MapAdaptor"

    case eFromPOILocator_To_POIPortalRxPeer                 = "FromPOILocator_To_POIPortalRxPeer"
    case eFromPOIPortalRxPeer_To_POILocator                 = "FromPOIPortalRxPeer_To_POILocator"

    case eFromPOIPortalRxPeer_To_URLSessionRxPeer           = "FromPOIPortalRxPeer_To_URLSessionRxPeer"
    case eFromURLSessionRxPeer_To_POIPortalRxPeer           = "FromURLSessionRxPeer_To_POIPortalRxPeer"

    /// CustomStringConvertible conformance.
    var description : String {

        return self.rawValue
    }
}

