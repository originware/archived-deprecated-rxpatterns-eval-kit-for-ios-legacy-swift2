//
// Created by Terry Stillone (http://www.originware.com) on 13/07/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import CoreLocation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Device Events
///

public enum eDeviceEvent_Reachability: CustomStringConvertible
{
    case eReachability_Unknown
    case eReachability_NotReachable
    case eReachability_IsReachable

    public var description : String {

        switch self
        {
            case .eReachability_Unknown:
                return "Reachability Unknown"

            case .eReachability_NotReachable:
                return "Network Is Not Reachable"

            case .eReachability_IsReachable:
                return "Network Is Reachable"
        }
    }
}

public enum eDeviceEvent_Location: CustomStringConvertible
{
    case eLocation_Invalid
    case eLocation_LastKnown(CLLocation)

    public var description : String {

        switch self
        {
            case .eLocation_Invalid:
                return "Invalid Location"

            case .eLocation_LastKnown(let location):
                return "Location: (\(location.coordinate.latitude), \(location.coordinate.longitude))"
        }
    }
}

public enum eDeviceEvent_Keyboard
{
    case eKeyboard_inValid
    case eKeyboard_didShow
    case eKeyboard_didHide
}

public enum eDeviceEvent_TextField
{
    case eText_Change(UITextField, NSAttributedString)
    case eText_Clear(UITextField)

    public var attributedText: NSAttributedString
    {
        switch self
        {
            case .eText_Change(let (_, text)):
                return text

            case .eText_Clear:
                return NSAttributedString(string: "")
        }
    }

    public var description : String {

        switch self
        {
            case .eText_Change:
                return "UITextField text change"

            case .eText_Clear:
                return "UITextField: clear"
        }
    }
}

public enum eDeviceEvent_Touch: String
{
    case eTouch_clearSearchButton   = "Clear Search Button"
    case eTouch_clearLogButton = "Clear Log Button"
    case eTouch_clearPOIKeywordMatchesButton = "Clear POI Results Button"
    case eTouch_centerMapButton = "Centre Map Button"
}
