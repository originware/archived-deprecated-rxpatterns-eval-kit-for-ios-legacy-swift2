//
// Created by Terry Stillone (http://www.originware.com) on 30/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import CoreLocation
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// State_POILocator: The state entity for the POI Locator Service.
///

private class State_POILocator
{
    private struct Constant
    {
        static let POINearByDistance = CLLocationDistance(100)
    }

    private lazy var activePOIKeywords = Set<String>()
    private var m_lastLocation: CLLocation? = nil

    /// Indicate if the locate request does have keywords that are already requested.
    /// - Paramater poiLocateRequest: The POI Locate request to check.
    func locaterequestHasActiveKeywords(poiLocateRequest: POILocateEvent_Request) -> Bool
    {
        let hasSomeActiveKeywords = !poiLocateRequest.poiKeywords.intersect(activePOIKeywords).isEmpty

        return isNearLastLocation(poiLocateRequest.location) && hasSomeActiveKeywords
    }

    /// Update the active keywords state from the given locate request.
    /// - Paramater poiLocateRequest: The POI Locate request to update state from.
    func update_POIKeywordTracking_FromRequest(poiLocateRequest : POILocateEvent_Request)
    {
        activePOIKeywords.unionInPlace(Set<String>(poiLocateRequest.poiKeywords))
        m_lastLocation = poiLocateRequest.location
    }

    /// Update the active keywords state from the given locate reply.
    /// - Paramater poiLocateResult: The POI Locate reply to update state from.
    func update_POIKeywordTracking_FromReply(poiLocateResult : POILocateEvent_Reply)
    {
        if poiLocateResult.error != nil
        {
            // Remove entries from the active list that were not located due to an error.
            activePOIKeywords.subtractInPlace(poiLocateResult.poiKeywords)
            m_lastLocation = nil
        }
    }

    /// Clear all active keywords in state.
    func clearState()
    {
        activePOIKeywords.removeAll()
    }

    /// Indicate if the given location is near the last known location.
    /// - Parameter location: The location to check.
    private func isNearLastLocation(location : CLLocation) -> Bool
    {
        return (m_lastLocation == nil) || (m_lastLocation!.distanceFromLocation(location) < Constant.POINearByDistance)
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxService_POILocator: The RxService that acts as a POI Locator using a registered POI Locator service.
///
///   This implementation uses the RxService_GooglePlacesPOILocator service.
///

class RxService_POILocator: RxService<POILocateEvent_Request, AppEvent>
{
    typealias InRequestType = POILocateEvent_Request
    typealias OutRequestType = POILocateEvent_Request
    typealias InReplyType = POILocateEvent_Reply
    typealias OutReplyType = AppEvent

    private var m_state = State_POILocator()
    private var m_appOpEventMonitor = AppViewController.SubRxDir.appOpEventMonitor

    init(tag : String, poiLocator:  RxService<POILocateEvent_Request, POILocateEvent_Reply>)
    {
        super.init(tag : tag)

        // Add the locator service the list of POI location services we use.
        addPOILocator(poiLocator)

        // Begin an active POI locate session.
        sessionBegin()
    }
    
    deinit
    {
        // End any active POI locate session.
        sessionEnd(nil)
    }

    /// Add the given locator service to the services we use.
    /// - Parameter poiLocator: The POI Locator service to use.
    func addPOILocator(poiLocator : RxService<POILocateEvent_Request, POILocateEvent_Reply>)
    {
        weak var weakSelf = self

        func inRequestToOutRequestMap(inRequest : InRequestType) -> OutRequestType?
        {
            var outRequest : OutRequestType? = inRequest

            guard let strongSelf = weakSelf else { return nil }

            if strongSelf.m_state.locaterequestHasActiveKeywords(inRequest)
            {
                outRequest = inRequest.createPOILocateRequest_WithoutKeywords(strongSelf.m_state.activePOIKeywords)
            }

            if outRequest != nil
            {
                strongSelf.m_state.update_POIKeywordTracking_FromRequest(outRequest!)
            }

            strongSelf.m_appOpEventMonitor?.notifyItem(.eFromPOIRequestor_To_POILocator)

            return outRequest
        }

        func inReplyToOutReplyMap(inReply : InReplyType) -> OutReplyType?
        {
            let outReply : OutReplyType? = AppEvent(appEventType: eAppEventType.ePOILocateResult(inReply))

            guard let strongSelf = weakSelf else { return nil }

            strongSelf.m_state.update_POIKeywordTracking_FromReply(inReply)
            strongSelf.m_appOpEventMonitor?.notifyItem(.eFromPOIPortalRxPeer_To_POILocator)

            return outReply
        }

        connect(poiLocator, requestToPeerRequestMap : inRequestToOutRequestMap, peerReplyToSelfReplyMap: inReplyToOutReplyMap)
    }

    /// End a session with a client.
    override func sessionEnd(error : IRxError?)
    {
        disconnectAll()
        
        super.sessionEnd(error)
    }

    /// Handle completed replies we get from locators.
    override func notifyReplyCompleted(error : IRxError?)
    {
        super.notifyReplyCompleted(error)

        // Disconnect with all locators on Locator completion (presumably an error).
        disconnectAll()
    }

    /// Handle state change replies we get from locators.
    override func notifyReplyStateChange(stateChange : eRxEvalStateChange)
    {
        super.notifyReplyStateChange(stateChange)

        switch stateChange
        {
            case eRxEvalStateChange.eEvalEnd:
                // Disconnect with all locators on Locator termination.
                disconnectAll()

            default:
                // do nothing
                break
        }
    }

    /// Clear state.
    func clearLocatedPOIKeywords()
    {
        m_state.clearState()
    }
}
