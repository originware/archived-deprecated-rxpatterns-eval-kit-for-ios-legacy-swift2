//
// Created by Terry Stillone (http://www.originware.com) on 29/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POIKeyword_Matcher: Match given text for POI keyword matches and sub-string matches. Notify matches.
///

class POIKeyword_Matcher : RxSource<AppEvent>
{
    private let m_minSubstringLength: Int
    private var m_POIKeywordMatcherByPOIKeyword = [String : SubstringMatcher]()
    private var m_notifier : ARxNotifier<AppEvent>? = nil

    init(tag: String, minSubstringLength : Int)
    {
        m_minSubstringLength = minSubstringLength

        super.init(tag : tag, subscriptionType: .eHot)

        // Load keywords to be matched.
        for poiKeyword in AppSettings.allPOIkeywords!
        {
            m_POIKeywordMatcherByPOIKeyword[poiKeyword] = SubstringMatcher(string: poiKeyword, minSubstringLength : minSubstringLength)
        }
    }

    override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode: RxEvalNode<AppEvent, AppEvent>) in

            evalNode.stateChangeDelegate = { [unowned evalNode]  (stateChange: eRxEvalStateChange, notifier: ARxNotifier<AppEvent>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        self.m_notifier = evalNode.asyncOutNotifier

                    case eRxEvalStateChange.eEvalEnd:

                        self.m_notifier = nil

                    default:
                        break
                }
            }
        }
    }

    /// Match the given keywords
    /// - Parameter stringToMatch: The text to match for POI keywords/
    func beginMatchingForPOIKeywords(stringToMatch: String)
    {
        let substringMatcher : SubstringMatcher = SubstringMatcher(string: stringToMatch, minSubstringLength : m_minSubstringLength)
        let matches = POIKeywordEvent_Match()
        let stringToMatchLength = stringToMatch.startIndex.distanceTo(stringToMatch.endIndex)
        let matchThresholdLength = stringToMatchLength <= 2 ? 2 : stringToMatchLength - 1

        for (_, matcher) in m_POIKeywordMatcherByPOIKeyword
        {
            let poiKeywordMatches = matcher.getCommonSubstringMatches(substringMatcher)

            if (poiKeywordMatches.totalMatchLength >= matchThresholdLength) && (poiKeywordMatches.maxMatchLength >= 3)
            {
                matches.addMatchesForPOIKeyword(poiKeywordMatches)
            }
        }

        if matches.count > 0
        {
            // Matches found, notify.
            m_notifier?.notifyItem(AppEvent(appEventType: .ePOIKeywordMatchSet(matches)))
        }
    }
}
