//
// Created by Terry Stillone (http://www.originware.com) on 1/08/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

class ExpandAnimatedTransition: NSObject, UIViewControllerAnimatedTransitioning
{
    var duration = 0.0
    var isPresenting = true
    var originFrame = CGRect.zero

    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?)-> NSTimeInterval
    {
        return isPresenting ? duration : duration * 3
    }

    func animateTransition(transitionContext: UIViewControllerContextTransitioning)
    {
        let containerView = transitionContext.containerView()
        let fromView = transitionContext.viewForKey(UITransitionContextFromViewKey)!
        let toView = transitionContext.viewForKey(UITransitionContextToViewKey)!
        let animatedView = isPresenting ? toView : fromView

        let startFrame = isPresenting ? originFrame : fromView.frame
        let endFrame = isPresenting ? toView.frame : originFrame

        let xScaleFactor = isPresenting ? startFrame.width / endFrame.width : endFrame.width / startFrame.width
        let yScaleFactor = isPresenting ? startFrame.height / endFrame.height :  endFrame.height / startFrame.height
        let scaleTransform = CGAffineTransformMakeScale(xScaleFactor, yScaleFactor)

        if isPresenting
        {
            animatedView.transform = scaleTransform
            animatedView.center = CGPoint(x: CGRectGetMidX(startFrame), y: CGRectGetMidY(startFrame))
            animatedView.clipsToBounds = true
            animatedView.alpha = 0.0
        }

        containerView!.addSubview(toView)
        containerView!.bringSubviewToFront(animatedView)

        UIView.animateWithDuration(

            duration,
            delay:0.0,
            usingSpringWithDamping: 0.7,
            initialSpringVelocity: isPresenting ? 10.0 : -5.0,
            options:isPresenting ? UIViewAnimationOptions.CurveEaseOut : UIViewAnimationOptions.CurveEaseIn,
            animations: {

                animatedView.transform = self.isPresenting ? CGAffineTransformIdentity : scaleTransform
                animatedView.center = CGPoint(x: CGRectGetMidX(endFrame), y: CGRectGetMidY(endFrame))
                animatedView.alpha = self.isPresenting ? 1.0 : 0.2

           }, completion:{_ in

               transitionContext.completeTransition(true)
        })
    }
}
