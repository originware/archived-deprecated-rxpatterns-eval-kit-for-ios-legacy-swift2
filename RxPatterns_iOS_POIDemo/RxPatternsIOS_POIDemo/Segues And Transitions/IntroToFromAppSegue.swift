//
//  Created by Terry Stillone on 25/07/2015.
//  Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

class IntroToFromAppSegue: UIStoryboardSegue
{
    var srcViewController : UIViewController { return self.sourceViewController }
    var destViewController : UIViewController { return self.destinationViewController }

    override func perform()
    {
        if self.identifier == nil
        {
            fatalError("Cannot transition to view controller")
        }

        switch self.identifier!
        {
            case "idIntroToApp":

                let appViewController = self.destViewController as! AppViewController
                let introViewController = self.srcViewController as! AppIntroViewController
                let transition = appViewController.transition

                transition.originFrame = introViewController.TapToContinueButton.frame

                appViewController.transitioningDelegate = appViewController
                
                self.srcViewController.presentViewController(self.destViewController, animated: true, completion: nil)

            case "idAppToIntro":

                self.srcViewController.dismissViewControllerAnimated(true, completion: nil)

            default:
                fatalError("Cannot transition to view controller")
        }
    }
}