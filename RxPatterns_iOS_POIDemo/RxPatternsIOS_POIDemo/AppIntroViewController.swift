//
//  Created by Terry Stillone on 25/07/2015.
//  Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

class UILabelWithMargin : UILabel
{
    override func drawTextInRect(rect : CGRect)
    {
        let insets = UIEdgeInsets(top:0, left:0, bottom:20, right:0)

        super.drawTextInRect(UIEdgeInsetsInsetRect(rect, insets))
    }
}

class AppIntroViewController: UIViewController
{
    struct Constant
    {
        static let WebsiteURL = "http://www.originware.com/"
        static let PortraitBackgroundImage = "LaunchBackground-portrait"
        static let LandscapeBackgroundImage = "LaunchBackground-landscape"
    }

    struct BackgroundImageOrienter
    {
        private var m_NSNotificationCenterObserver : AnyObject?
        private var m_currentOrientation = UIDeviceOrientation.Unknown
        private var m_view: UIView
        private var m_backgroundImage: UIImageView

        private var notificationCenter : NSNotificationCenter           { return NSNotificationCenter.defaultCenter() }

        init(view: UIView, imageView : UIImageView)
        {
            m_view = view
            m_backgroundImage = imageView

            registerForOrientatingBackgroundImage()
        }

        mutating func registerForOrientatingBackgroundImage()
        {
            if m_NSNotificationCenterObserver == nil
            {
                let device = UIDevice.currentDevice()

                device.beginGeneratingDeviceOrientationNotifications()

                m_NSNotificationCenterObserver = notificationCenter.addObserverForName("UIDeviceOrientationDidChangeNotification", object: device, queue: nil, usingBlock: { _ in

                    self.orientate()
                })
            }
            else
            {
                orientate()
            }
        }

        mutating func deregisterForOrientatingBackgroundImage()
        {
            if m_NSNotificationCenterObserver != nil
            {
                notificationCenter.removeObserver(m_NSNotificationCenterObserver!)
            }

            m_NSNotificationCenterObserver = nil
        }

        mutating func orientate()
        {
            let orientation = UIDevice.currentDevice().orientation

            if orientation != self.m_currentOrientation
            {
                self.m_currentOrientation = orientation

                switch orientation
                {
                    case .LandscapeLeft, .LandscapeRight:

                        self.m_backgroundImage.image = UIImage(named: Constant.LandscapeBackgroundImage)

                    case .Portrait, .PortraitUpsideDown:

                        self.m_backgroundImage.image = UIImage(named: Constant.PortraitBackgroundImage)

                    default:
                        break
                }

                self.m_view.setNeedsLayout()
            }
        }
    }

    @IBOutlet weak var BackgroundImage: UIImageView!
    @IBOutlet weak var TitleText: UILabel!
    @IBOutlet weak var DescriptionText: UITextView!
    @IBOutlet weak var HowToUse: UITextView!
    @IBOutlet weak var TapToContinueButton: UIButton!
    @IBOutlet weak var TopToTitleVerticalConstraint: NSLayoutConstraint!

    @IBAction func unwindFromApp(segue: UIStoryboardSegue)
    {
        let srcViewController = segue.sourceViewController 
        let destViewController = segue.destinationViewController
        let customSegue = IntroToFromAppSegue(identifier: "idAppToIntro", source: srcViewController, destination: destViewController)

        customSegue.perform()
    }

    private var m_backgroundImageOrienter : BackgroundImageOrienter? = nil
    private var m_animationCenter = CGPoint(x: 0, y:0)

    var animationCenter: CGPoint                                    { return m_animationCenter }

    override func viewDidLoad()
    {
        super.viewDidLoad()

        m_backgroundImageOrienter = BackgroundImageOrienter(view: self.view, imageView: self.BackgroundImage)

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onWebLinkTap(_:)))

        tapGesture.numberOfTapsRequired = 1

        self.DescriptionText.addGestureRecognizer(tapGesture)

        // Style the "Tap To Continue" button.

        AppStyle.styleButton(self.TapToContinueButton)
        AppStyle.styleTextBorder(self.TitleText)
        AppStyle.styleTextBorder(self.DescriptionText)
        AppStyle.styleTextBorder(self.HowToUse)

    }

    override func viewWillAppear(animated : Bool)
    {
        super.viewWillAppear(animated)

        m_backgroundImageOrienter!.registerForOrientatingBackgroundImage()
    }
    
    override func viewDidAppear(animated: Bool)
    {
        self.m_animationCenter = self.TapToContinueButton.center

        super.viewDidAppear(animated)
    }

    override func viewWillDisappear(animated : Bool)
    {
        super.viewWillDisappear(animated)

        m_backgroundImageOrienter!.deregisterForOrientatingBackgroundImage()
    }

    override func updateViewConstraints()
    {
        let orientation = UIDevice.currentDevice().orientation

        switch orientation
        {
            case .LandscapeLeft, .LandscapeRight:

                self.TopToTitleVerticalConstraint.constant =  CGFloat(100)

            case .Portrait, .PortraitUpsideDown:

                self.TopToTitleVerticalConstraint.constant =  CGFloat(200)

            default:
                break
        }

        super.updateViewConstraints()
    }

    func onWebLinkTap(gestureRecognizer : UIGestureRecognizer)
    {
        UIApplication.sharedApplication().openURL(NSURL(string: Constant.WebsiteURL)!)
    }
}
