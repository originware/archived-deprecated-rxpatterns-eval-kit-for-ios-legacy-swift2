//
// Created by Terry Stillone (http://www.originware.com) on 25/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import UIKit
import CoreLocation
import MapKit
import RxPatternsSDK
import RxPatternsLib

class AppViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, UIViewControllerTransitioningDelegate
{
    struct Constant
    {
        static let MapRegionSpan : CLLocationDistance = 1000
        static let MapPitch : CGFloat = 30
        static let POITypePlaceHolderColumns = 3
        static let LocationSimulationTimeout : RxDuration = 5
        static let LocationAndReachabilityFixTimeout : RxDuration = 9
        static let MinMatchLength = 3
        static let KeyboardDebounceInSeconds = 0.4
        static let TransitionDuration = 0.5
        static let VCTag = "AppViewController"
    }
    
    struct SubRxDir
    {
        static let searchTextFieldInAdapterURI = "inadapters/uitextfields/search"
        static var searchTextFieldInAdapter : EventAdapter_UITextField?             { return get(searchTextFieldInAdapterURI) as? EventAdapter_UITextField }
        
        static let keywordMatcherURI = "poi/keywordMatcher"
        static var keywordMatcher : POIKeyword_Matcher?                             { return get(keywordMatcherURI) as? POIKeyword_Matcher }
        
        static let logAdapterURI = "outadapters/uitextviews/log"
        static var logAdapter : ViewAdapter_Log_UITextView<AppEvent>?               { return get(logAdapterURI) as? ViewAdapter_Log_UITextView<AppEvent> }
        
        static let poiLocatorURI = "poi/poiLocator"
        static var poiLocator : RxService_POILocator?                               { return get(poiLocatorURI) as? RxService_POILocator }
        
        static let confirmAdapterURI = "outadapters/confirmation"
        static var confirmAdapter : ViewAdapter_Confirmation<AppEvent>?             { return get(confirmAdapterURI) as? ViewAdapter_Confirmation<AppEvent> }
            
        static let keywordMatchResultURI = "outadapters/uitextviews/keywordMatchResults"
        static var keywordMatchResult : ViewAdapter_KeywordResults_UITextView<AppEvent>?  { return get(keywordMatchResultURI) as? ViewAdapter_KeywordResults_UITextView<AppEvent> }
        
        static let uiMatchedKeywordsAdapterURI = "outadapters/uicollections/poiResults"
        static var uiMatchedKeywordsAdapter : ViewAdapter_UICollection<AppEvent>?   { return get(uiMatchedKeywordsAdapterURI) as? ViewAdapter_UICollection<AppEvent> }
        
        static let buttonTapAdapterURI = "inadapters/uicontrol/touch"
        static var buttonTapAdapter : EventAdapter_UITouch?                         { return get(buttonTapAdapterURI) as? EventAdapter_UITouch }
        
        static let mapAdapterURI = "outadapters/maps/poiMap"
        static var mapAdapter : ViewAdapter_MapKit<AppEvent>?                       { return get(mapAdapterURI) as? ViewAdapter_MapKit<AppEvent> }
        
        static let appOpEventMonitorURI = "monitors/appopevent"
        static var appOpEventMonitor : RxRelaySubject<eAppOpEventType>?             { return get(appOpEventMonitorURI) as? RxRelaySubject<eAppOpEventType> }
        
        static let locationLabelAdapterURI =  "outadapters/labels/location"
        static var locationLabelAdapter : ViewAdapter_Label<AppEvent>?              { return get(locationLabelAdapterURI) as? ViewAdapter_Label<AppEvent> }
        
        static let reachabilityLabelAdapterURI =  "outadapters/labels/reachability"
        static var reachabilityLabelAdapter : ViewAdapter_Label<AppEvent>?          { return get(reachabilityLabelAdapterURI) as? ViewAdapter_Label<AppEvent> }

        static func store(relativeURI : String, _ object : RxObject)
        {
            let namespace = eRxDirectory_NameSpace.eStore.rawValue

            RxDirectory.put(namespace + ":/" + AppConstant.ConfigScenario + "/" + relativeURI, object)
        }

        static func get(relativeURI : String) -> AnyObject?
        {
            let namespace = eRxDirectory_NameSpace.eRun.rawValue

            return RxDirectory.get(namespace + ":/" + relativeURI)
        }
    }
    
    struct State
    {
        /// Indicates whether the MapKit region has been set.
        var hasSetMapRegion = false
        
        /// Formatter and Writer and to the App log.
        private var logWriter = AppStyle_LogFormatter()
    }

    // Outlets

    @IBOutlet weak var locationStatus : UILabel!
    @IBOutlet weak var reachabilityStatus: UILabel!

    @IBOutlet weak var clearSearchTextButton : UIButton!
    @IBOutlet weak var appEventClearButton : UIButton!

    @IBOutlet weak var clearPOIMatchesButton : UIButton!
    @IBOutlet weak var centreMapButton : UIButton!

    @IBOutlet weak var searchTextField : UITextField!
    @IBOutlet weak var searchResultsTextView : UITextView!
    @IBOutlet weak var appEventLogTextView : UITextView!

    @IBOutlet weak var selectorSliderContainer: UIView!
    @IBOutlet weak var appEventLogContainer : UIView!
    @IBOutlet weak var poiSearchContainer : UIView!
    @IBOutlet weak var poiMatchContainer : UIView!
    @IBOutlet weak var appTitle : UIView!
    @IBOutlet weak var searchTextFieldContainer : UIView!

    @IBOutlet weak var appOperationViewContainer: UIView!
    @IBOutlet weak var appOperationView: AppOperationView!
    @IBOutlet weak var appOperationScrollView: TappableUIScrollView!
    
    @IBOutlet weak var mapViewContainer : UIView!
    @IBOutlet weak var networkStatusPanel : UIView!
    @IBOutlet weak var locationStatusPanel : UIView!

    @IBOutlet weak var mapView : MKMapView!

    @IBOutlet weak var selectorSlider : UISlider!

    @IBOutlet weak var foundPOITypesCollectionView : UICollectionView!

    @IBOutlet weak var BackButton: UIButton!

    @IBOutlet weak var InternalOperationLabel: UILabel!
    @IBOutlet weak var ShowMapLabel: UILabel!

    /// Transitions
    var transition = ExpandAnimatedTransition()

    /// State
    var m_state = State()

    override func viewDidLoad()
    {
        super.viewDidLoad()

        configureControls()

        let gestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(onSliderTapped(_:)))

        self.selectorSlider.addGestureRecognizer(gestureRecognizer)

        // Hide views not to be shown on first load.
        appOperationViewContainer.hidden = true
        mapViewContainer.hidden = true
    }

    override func viewWillAppear(animated : Bool)
    {
        super.viewWillAppear(animated)

        #if DEBUG
            RxMonReports.save()
        #endif

        styleControls()
        styleSlider()

        clearPOISearchTextField()

        // Add the View RxAdapters, Observers and Observables to the RxDirectory
        configureRxDirectory()

        func deviceSubscriptions()
        {
            RxDeviceDir.orientationAdapter!.subscribe(SubRxDir.logAdapter!).addToDirectory(Constant.VCTag)

            RxDeviceDir.reachabilityAdapter!.subscribe(SubRxDir.logAdapter!).addToDirectory(Constant.VCTag)
            RxDeviceDir.reachabilityAdapter!.subscribe(SubRxDir.reachabilityLabelAdapter!).addToDirectory(Constant.VCTag)
            
            RxDeviceDir.locationAdapter!.subscribe(SubRxDir.logAdapter!).addToDirectory(Constant.VCTag)
            RxDeviceDir.locationAdapter!.subscribe(SubRxDir.locationLabelAdapter!).addToDirectory(Constant.VCTag)

            RxDeviceDir.locationAdapter!.subscribe({ (appEvent: AppEvent) in

                switch appEvent.appEventType
                {
                    case .eLocationChange:

                        if appEvent.isSimulatedEvent
                        {
                            SubRxDir.confirmAdapter!.notifyItem(AppEvent(appEventType: .ePresentConfirmation("As LocationServices are not available,\nthe location will be simulated as\nCambridge, UK")))
                            self.setMapViewLocation(self.mapView, location: AppConstant.SimulatedPosition)
                        }

                    default:
                        break
                }
            }).addToDirectory(Constant.VCTag)
        }

        func textInputSubscriptions()
        {
            let searchTextFieldInAdapter = SubRxDir.searchTextFieldInAdapter!

            searchTextFieldInAdapter.subscribe({ (appEvent : AppEvent) in

                RxSDK.evalQueue.UIThreadQueue.dispatchAsync({

                    let textFieldChange = appEvent.appEventType.textFieldChange
                    let attributedText = textFieldChange.attributedText

                    if appEvent.isSimulatedEvent
                    {
                        // Set the UITextField with the text to be replayed.
                        self.searchTextField.attributedText = attributedText
                    }
                    else if attributedText.string.characters.count > 2
                    {
                        // Commence keyword matching.
                        SubRxDir.keywordMatcher!.beginMatchingForPOIKeywords(attributedText.string)
                    }
                    else
                    {
                        // Present the standard POI Keywords placeholder in the search results UITextView.
                        self.searchResultsTextView.attributedText = AppStyle.getFormattedPOIKeywordsPlaceHolder(Constant.POITypePlaceHolderColumns)
                    }

                    SubRxDir.appOpEventMonitor?.notifyItem(.eFromKeyboard_To_POIKeywordMatcherPath)
                })

            }).addToDirectory(Constant.VCTag)

            // Notify the log of text entered, in chunks of debounced values.
            searchTextFieldInAdapter.debounce(Constant.KeyboardDebounceInSeconds).subscribe(SubRxDir.logAdapter!).addToDirectory(Constant.VCTag)
        }

        func poiKeywordsMatchSubscriptions()
        {
            var haveGivenConfirmation = false

            SubRxDir.keywordMatcher!.debounce(Constant.KeyboardDebounceInSeconds

            ).doOnNotify({ (appEvent : AppEvent) in

                // Present the matches on the keyword match UITextView
                SubRxDir.keywordMatchResult!.notifyItem(appEvent)

                // Log the keyword match results.
                SubRxDir.logAdapter!.notifyItem(appEvent)

                // Place in the matched keywords list view.
                SubRxDir.uiMatchedKeywordsAdapter!.notifyItem(appEvent)

            }).POIRequestor(RxDeviceDir.locationAdapter!, reachabilityObservable : RxDeviceDir.reachabilityAdapter!, timeout: Constant.LocationAndReachabilityFixTimeout

            ).subscribe({ (appEvent : AppEvent) in

                switch appEvent.appEventType
                {
                    case .ePOILocateRequest(let poiRequest):

                        haveGivenConfirmation = false
                        SubRxDir.poiLocator!.request(poiRequest)

                    case .ePresentConfirmation:

                        if !haveGivenConfirmation
                        {
                            haveGivenConfirmation = true
                            SubRxDir.confirmAdapter!.notifyItem(appEvent)
                        }

                    default:
                        fatalError("Unexpected code point")
                }

            }).addToDirectory(Constant.VCTag)
        }

        func poiLocationSubscriptions()
        {
            // POI Locator results to Map
            SubRxDir.poiLocator!.subscribe(SubRxDir.mapAdapter!).addToDirectory(Constant.VCTag)
            
            // POI Locator results to Log
            SubRxDir.poiLocator!.subscribe(SubRxDir.logAdapter!).addToDirectory(Constant.VCTag)
            
            // POI Locator results to POI Results Collection.
            SubRxDir.poiLocator!.subscribe(SubRxDir.uiMatchedKeywordsAdapter!).addToDirectory(Constant.VCTag)
        }

        func buttonTouchSubscriptions()
        {
            SubRxDir.buttonTapAdapter!.registerForUITouch(clearSearchTextButton, touchTarget : eDeviceEvent_Touch.eTouch_clearSearchButton)
            SubRxDir.buttonTapAdapter!.registerForUITouch(appEventClearButton, touchTarget : eDeviceEvent_Touch.eTouch_clearLogButton)
            SubRxDir.buttonTapAdapter!.registerForUITouch(clearPOIMatchesButton, touchTarget : eDeviceEvent_Touch.eTouch_clearPOIKeywordMatchesButton)
            SubRxDir.buttonTapAdapter!.registerForUITouch(centreMapButton, touchTarget : eDeviceEvent_Touch.eTouch_centerMapButton)

            SubRxDir.buttonTapAdapter!.subscribe(SubRxDir.uiMatchedKeywordsAdapter!).addToDirectory(Constant.VCTag)
            SubRxDir.buttonTapAdapter!.subscribe(SubRxDir.mapAdapter!).addToDirectory(Constant.VCTag)
            SubRxDir.buttonTapAdapter!.subscribe(SubRxDir.logAdapter!).addToDirectory(Constant.VCTag)

            SubRxDir.buttonTapAdapter!.subscribe({ (appEvent : AppEvent) in

                RxSDK.evalQueue.UIThreadQueue.dispatchSync({

                    let (_, touchTarget) = appEvent.appEventType.touchControl

                    switch touchTarget
                    {
                        case .eTouch_clearSearchButton:
                            self.clearPOISearchTextField()

                        case .eTouch_clearPOIKeywordMatchesButton:
                            self.clearPOISearchTextField()
                            SubRxDir.poiLocator!.clearLocatedPOIKeywords()

                        default:
                            // do nothing
                            return
                    }
                })

            }).addToDirectory(Constant.VCTag)
        }

        buttonTouchSubscriptions()
        deviceSubscriptions()
        textInputSubscriptions()
        poiKeywordsMatchSubscriptions()
        poiLocationSubscriptions()
    }

    override func viewDidAppear(animated : Bool)
    {
        super.viewDidAppear(animated)

        appOperationViewContainer.hidden = false
        mapViewContainer.hidden = false
    }

    override func viewWillDisappear(animated : Bool)
    {
        // Stop confirmations from being presented.
        SubRxDir.confirmAdapter?.enabled = false

        // Shutdown processing in views
        appOperationView.shutdown()

        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(animated: Bool)
    {
        // Unsubscribe all subscriptions related to this view.
        RxDirectory.removeAllWithTagRegexpr(Constant.VCTag)
        
        super.viewDidDisappear(animated)

        #if DEBUG
            RxMonReports.reportInstanceStats()
            RxMonReports.restore()

        #endif
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>

    func onSelectorSliderEvent(slider : UISlider)
    {
        let sectorSize : CGFloat = 1.0 / 3.0
        let value : CGFloat = CGFloat(slider.value)

        let archPosValue: CGFloat = Helpers.clamptoNegPos1((value - (0.5 - sectorSize)) / sectorSize)
        let poiMatchesPosValue: CGFloat = Helpers.clamptoNegPos1((value - 0.5) / sectorSize)
        let mapViewPosValue: CGFloat = Helpers.clamptoNegPos1((value - (0.5 + sectorSize)) / sectorSize)

        func calcAlpha(value : CGFloat) -> CGFloat
        {
            let pi : CGFloat = 3.14159
            let paraValue = 1.0 - (value * value)
            let sinValue = sin(paraValue * pi / 8.0) / 0.3

            return Helpers.clamptoZeroOne(sinValue)
        }

        let mapViewAlpha : CGFloat = calcAlpha(mapViewPosValue) * fabs(poiMatchesPosValue)
        let poiMatchesAlpha : CGFloat = calcAlpha(poiMatchesPosValue) * fabs(archPosValue)
        let archAlpha : CGFloat = calcAlpha(archPosValue)

        self.appOperationViewContainer.alpha = Helpers.threshold(archAlpha)
        self.poiMatchContainer.alpha = Helpers.threshold(poiMatchesAlpha)
        self.mapViewContainer.alpha = Helpers.threshold(mapViewAlpha)
    }

    func onSliderTapped(gestureRecognizer : UIGestureRecognizer)
    {
        func sliderNotch(index : Int) -> Float
        {
            let sectorSize : Float  = 1.0 / 3.0
            let index1 : Float = Float(index) - 1.0

            return 0.5 + index1 * sectorSize
        }

        func isInSliderNotch(index : Int, value : Float, delta : Float) -> Bool
        {
            let notchCentre : Float = sliderNotch(index)
            let notchStart : Float  = notchCentre - delta / 2
            let notchEnd : Float  = notchCentre + delta / 2

            return (notchStart <= value) && (value <= notchEnd)
        }

        func clampToSliderNotch(value : Float, delta : Float) -> Float
        {
            for i in 0..<3
            {
                if isInSliderNotch(i, value: value, delta: delta)
                {
                    return sliderNotch(i)
                }
            }

            return -1.0
        }

        if let slider = gestureRecognizer.view as? UISlider
        {
            if slider.highlighted { return }

            let point = gestureRecognizer.locationInView(slider)
            let factor = Float(point.x / slider.bounds.size.width)
            let value = slider.minimumValue + factor * (slider.maximumValue - slider.minimumValue)
            let notchValue = clampToSliderNotch(value, delta: 0.2)

            if (notchValue > 0.0)
            {
                slider.setValue(notchValue, animated:true)
                onSelectorSliderEvent(slider)
            }
        }
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>

    func clearPOISearchTextField()
    {
        searchTextField.text = ""
        searchResultsTextView.attributedText = AppStyle.getFormattedPOIKeywordsPlaceHolder(Constant.POITypePlaceHolderColumns)
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}

extension AppViewController // Configuration and Styling.
{
    private func configureRxDirectory()
    {
        // Add View Adapters to the RxDirectory
        
        func tag(subTag : String) -> String
        {
            return Constant.VCTag + "/" + subTag
        }

        SubRxDir.store(SubRxDir.searchTextFieldInAdapterURI, EventAdapter_UITextField(tag: tag("search"), textField: searchTextField))
        SubRxDir.store(SubRxDir.buttonTapAdapterURI, EventAdapter_UITouch(tag: tag("EventAdapter_UITouch")))
        SubRxDir.store(SubRxDir.locationLabelAdapterURI, ViewAdapter_Label<AppEvent>(tag : tag("ViewAdapter_Label/location"), label: locationStatus))
        SubRxDir.store(SubRxDir.reachabilityLabelAdapterURI, ViewAdapter_Label<AppEvent>(tag : tag("ViewAdapter_Label/reachability"), label: reachabilityStatus))
        SubRxDir.store(SubRxDir.uiMatchedKeywordsAdapterURI, ViewAdapter_UICollection<AppEvent>(tag: tag("ViewAdapter_UICollection"), collectionView: foundPOITypesCollectionView))
        SubRxDir.store(SubRxDir.confirmAdapterURI, ViewAdapter_Confirmation<AppEvent>(tag : tag("ViewAdapter_Confirmation")))

        SubRxDir.store(SubRxDir.keywordMatchResultURI, ViewAdapter_KeywordResults_UITextView<AppEvent>(tag: tag("ViewAdapter_KeywordResults_UITextView"), textView: searchResultsTextView, logFormatter: m_state.logWriter))
        SubRxDir.store(SubRxDir.logAdapterURI, ViewAdapter_Log_UITextView<AppEvent>(tag: tag("ViewAdapter_Log_UITextView"), textView: appEventLogTextView, logFormatter: m_state.logWriter))

        SubRxDir.store(SubRxDir.mapAdapterURI, ViewAdapter_MapKit<AppEvent>(tag: tag("MapKitAnnotationAdapter"), mapView: mapView))
        
        // Add the RxServices to the RxDirectory
        SubRxDir.store(SubRxDir.poiLocatorURI, RxService_POILocator(tag: tag("POILocator Service"), poiLocator: RxService_GooglePlacesPOILocator(tag: tag("Google Places"))))
            
        SubRxDir.store(SubRxDir.keywordMatcherURI, POIKeyword_Matcher(tag: tag("POIKeyword_Matcher"), minSubstringLength: Constant.MinMatchLength))
    }

    private func configureControls()
    {
        // Configure map MKMapView.
        self.mapView.delegate = self

        // Configure the App Operation View UIScrollView.
        self.appOperationScrollView.containerView = self.appOperationView
        self.appOperationScrollView.minimumZoomScale = 0.95
        self.appOperationScrollView.maximumZoomScale = 4.0
        self.appOperationScrollView.zoomScale = 0.95

        // Configure the Located-POIs UICollectionView.
        let collectionViewLayout = self.foundPOITypesCollectionView.collectionViewLayout as! UICollectionViewFlowLayout

        collectionViewLayout.sectionInset = UIEdgeInsetsMake(7, 0, 0, 0)
        collectionViewLayout.minimumLineSpacing = 2
        collectionViewLayout.minimumInteritemSpacing = 2
        collectionViewLayout.scrollDirection = UICollectionViewScrollDirection.Vertical

        self.searchResultsTextView.contentInset = UIEdgeInsetsMake(2, 0, 2, 0)
    }

    private func styleControls()
    {
        AppStyle.styleTitleViewContainer(self.appTitle)
        AppStyle.styleTitleViewContainer(self.selectorSliderContainer)
        AppStyle.styleTitleViewContainer(self.appEventLogContainer)
        AppStyle.styleViewBorder(self.appEventLogContainer)

        AppStyle.styleViewBorder(self.networkStatusPanel)
        AppStyle.styleViewBorder(self.locationStatusPanel)
        AppStyle.styleViewBorder(self.searchTextFieldContainer)

        AppStyle.styleViewBorder(self.searchTextField)
        AppStyle.styleViewBorder(self.poiSearchContainer)
        AppStyle.styleViewBorder(self.foundPOITypesCollectionView)

        AppStyle.styleSearchResults(self.searchResultsTextView)
    }

    private func styleSlider()
    {
        let maxWidth = max(self.view.bounds.width, self.view.bounds.height)
        let trackImage = AppStyle.createSliderThumbImage(CGSizeMake(maxWidth, self.selectorSlider.bounds.size.height), trackHeight:6)
        let stretchableTrackImage =  trackImage.stretchableImageWithLeftCapWidth(9, topCapHeight:0)

        selectorSlider.addTarget(self, action:#selector(onSelectorSliderEvent(_:)), forControlEvents:UIControlEvents.ValueChanged)
        selectorSlider.setMinimumTrackImage(stretchableTrackImage, forState:UIControlState.Normal)
        selectorSlider.setMaximumTrackImage(stretchableTrackImage, forState:UIControlState.Normal)

        onSelectorSliderEvent(self.selectorSlider)
    }
}

extension AppViewController // MKMapViewDelegate
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>

    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation)
    {
        let userCoordinate = userLocation.location!.coordinate

        // Set the map region on the first location determination.

        if (userCoordinate.latitude != 0.0) && (userCoordinate.longitude != 0.0) && !m_state.hasSetMapRegion
        {
            // First set a dummy location so that we can animate to the users location.
            let dummyCoordinate = CLLocationCoordinate2DMake(0, 0)

            mapView.setCenterCoordinate(dummyCoordinate, animated:false)

            // Set the map region span.
            let region = MKCoordinateRegionMakeWithDistance(userCoordinate, Constant.MapRegionSpan, Constant.MapRegionSpan)
            let fitRegion = mapView.regionThatFits(region)

            mapView.setRegion(fitRegion, animated:true)

            // Set the camera pitch.
            let mapCamera = MKMapCamera()

            mapCamera.centerCoordinate = mapView.camera.centerCoordinate
            mapCamera.pitch = Constant.MapPitch
            mapCamera.altitude = mapView.camera.altitude
            mapCamera.heading = mapView.camera.heading

            mapView.camera = mapCamera

            m_state.hasSetMapRegion = true
        }
    }

    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?
    {
        // Add a map annotation to mark the POI on the map.
        if let flagAnnotation = annotation as? FlagAnnotation
        {
            if let flagAnnotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(flagAnnotation.poi.poiKeyword) as? FlagAnnotationView
            {
                return flagAnnotationView
            }

            let flagAnnotationView = FlagAnnotationView(annotation:annotation, reuseIdentifier:flagAnnotation.poi.poiKeyword)

            flagAnnotationView.poi = flagAnnotation.poi

            return flagAnnotationView
        }

        return nil
    }

    func setMapViewLocation(mapView: MKMapView, location: CLLocation)
    {
        // Set the map region on the first location determination.

        if !m_state.hasSetMapRegion
        {
            // Set the map region span.
            let region = MKCoordinateRegionMakeWithDistance(location.coordinate, Constant.MapRegionSpan, Constant.MapRegionSpan)
            let fitRegion = mapView.regionThatFits(region)

            mapView.setCenterCoordinate(location.coordinate, animated:false)
            mapView.setRegion(fitRegion, animated:true)
            
            // Set the camera pitch.
            let mapCamera = MKMapCamera()

            mapCamera.centerCoordinate = mapView.camera.centerCoordinate
            mapCamera.pitch = Constant.MapPitch
            mapCamera.altitude = mapView.camera.altitude
            mapCamera.heading = mapView.camera.heading

            mapView.camera = mapCamera
            
            // Core Location Bug fix: make sure the region is set.
            mapView.setRegion(fitRegion, animated:true)

            m_state.hasSetMapRegion = true
        }
    }
}

extension AppViewController // UIViewControllerTransitioningDelegate
{
    func animationControllerForPresentedController(
            presented: UIViewController,
            presentingController presenting: UIViewController,
            sourceController source: UIViewController) ->
            UIViewControllerAnimatedTransitioning?
    {
        transition.isPresenting = true
        transition.duration = Constant.TransitionDuration

        return transition
    }

    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        transition.isPresenting = false
        transition.duration = Constant.TransitionDuration * 3

        return transition
    }
}
