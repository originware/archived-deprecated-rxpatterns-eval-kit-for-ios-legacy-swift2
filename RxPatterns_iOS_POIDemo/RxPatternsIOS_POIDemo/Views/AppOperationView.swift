//
// Created by Terry Stillone (http://www.originware.com) on 21/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import QuartzCore

import RxPatternsSDK
import RxPatternsLib

private enum eAnimationRequest
{
    case eStartBeadAnimation(UIBezierPath, CALayer, CALayer, CALayer)
    case eStopBeadAnimations(CALayer, CALayer, CALayer)
}

private enum eAnimationReply
{
    case eAnimationsCompleted
}

private enum eAnimationCommand
{
    case eTranslateBeadAlongPath(CALayer, UIBezierPath)
    case eMorphPath(CALayer, UIBezierPath)
    case eFadeAllOut

    static func isTranslateBeadAlongPathID(value : Int) -> Bool     { return (value & 2 << 10) != 0 }
    static func isMorphPathID(value : Int) -> Bool                  { return (value & 2 << 11) != 0 }
    static func isFadeAllOutID(value : Int) -> Bool                 { return (value & 2 << 12) != 0 }

    func nextAnimationID(inout counter : Int) -> Int
    {
        counter += 1

        if counter >= (2 << 10)
        {
            counter = 0
        }

        return counter | BaseID
    }

    private var BaseID : Int {

        switch self
        {
            case eTranslateBeadAlongPath:                           return 2 << 10
            case eMorphPath:                                        return 2 << 11
            case eFadeAllOut:                                       return 2 << 12
        }
    }
}

private struct Layers
{
    class LayerDelegateDirector: NSObject
    {
        var m_view : UIView

        init(view : UIView)
        {
            m_view = view
        }

        override func drawLayer(layer: CALayer, inContext context: CGContext)
        {
            UIGraphicsPushContext(context)
            m_view.drawLayer(layer, inContext: context)
            UIGraphicsPopContext()
        }
    }

    static func createLayerWithDelegateDirector(bounds : CGRect, director: Layers.LayerDelegateDirector) -> CALayer
    {
        let layer = CALayer()

        layer.opacity = 1.0
        layer.backgroundColor = UIColor.clearColor().CGColor
        layer.frame = bounds
        layer.delegate = director

        return layer
    }

    static func createPathMorphShapeLayer(bounds : CGRect, pathColor : CGColor) -> CAShapeLayer
    {
        let layer = CAShapeLayer()

        layer.strokeColor = pathColor
        layer.fillColor = nil
        layer.opacity = 1.0

        layer.backgroundColor = UIColor.clearColor().CGColor
        layer.frame = bounds

        return layer
    }

    static func createBeadShapeLayer() -> CAShapeLayer
    {
        let ovalPath = UIBezierPath(ovalInRect: CGRectMake(-12, -7, 24, 14))
        let layer = CAShapeLayer()

        layer.path = ovalPath.CGPath
        layer.backgroundColor = UIColor.clearColor().CGColor
        layer.strokeColor = UIColor.greenColor().CGColor
        layer.fillColor = UIColor.blueColor().CGColor

        layer.lineWidth = 4
        layer.opacity = 0.0

        return layer
    }
}

private struct Animations
{
    static let BaseAnimationTime = CFTimeInterval(0.2)
    static let FadeAnimationTime = CFTimeInterval(5.0)
    static let KeyID: String = "AnimationID"

    private static func createBeadTranslateAlongPath_Animation(path: UIBezierPath) -> CAAnimation
    {
        // Create the keyframe animation of the bead following along the path.
        let translateBeadAlongPathAnimation: CAKeyframeAnimation = CAKeyframeAnimation(keyPath: "position")

        translateBeadAlongPathAnimation.path = path.CGPath
        translateBeadAlongPathAnimation.timingFunctions = [CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)]
        translateBeadAlongPathAnimation.removedOnCompletion = false

        return translateBeadAlongPathAnimation
    }

    private static func createMorphPath_Animation(path: UIBezierPath) -> CABasicAnimation
    {
        // Create the animation to morph the thickness of the line-track of the bead.
        let morphPathAnimation: CABasicAnimation = CABasicAnimation(keyPath: "lineWidth")

        morphPathAnimation.fromValue = 2.0
        morphPathAnimation.toValue = 30.0

        morphPathAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        morphPathAnimation.removedOnCompletion = false
        morphPathAnimation.autoreverses = false
        morphPathAnimation.fillMode = kCAFillModeBoth

        return morphPathAnimation
    }

    private static func createFadeAllOut_Animation() -> CAAnimation
    {
        let pathSlowFadeOutAnimation = CAKeyframeAnimation(keyPath: "opacity")

        pathSlowFadeOutAnimation.values = [1.0, 0.0]
        pathSlowFadeOutAnimation.keyTimes = [0.0, 1.0]
        pathSlowFadeOutAnimation.removedOnCompletion = false
        pathSlowFadeOutAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        pathSlowFadeOutAnimation.fillMode = kCAFillModeBoth

        return pathSlowFadeOutAnimation
    }

    private static func createFadingAnimationFromAnimation(animation: CAAnimation) -> CAAnimationGroup
    {
        // Create a keyframe opacity animation to bring the bead into view and then out.
        let alphaAnimation = CAKeyframeAnimation(keyPath: "opacity")

        alphaAnimation.values = [0.0, 1.0, 1.0, 0.0]
        alphaAnimation.keyTimes = [0.1, 0.2, 0.8, 0.9]
        alphaAnimation.removedOnCompletion = false

        // Group the move and opacity animations.
        let groupAnimation = CAAnimationGroup()

        groupAnimation.animations = [animation, alphaAnimation]
        groupAnimation.removedOnCompletion = false

        return groupAnimation
    }
}

private class AnimationHandler: NSObject
{
    private let m_replyNotifier: ARxNotifier<eAnimationReply>
    private let m_enhancePathsLayer: CALayer
    private let m_animationRequestQueue : RxNotificationQueue<eAnimationRequest>
    private let m_requestEvalQueue : RxEvalQueue

    private var m_currentAnimationTime: CFTimeInterval = Animations.BaseAnimationTime
    private var m_isFadingOut = false
    private var m_currentIDCounter = 0

    private init(enhancePathsLayer: CALayer, replyNotifier: ARxNotifier<eAnimationReply>, animationRequestQueue: RxNotificationQueue<eAnimationRequest>, requestEvalQueue : RxEvalQueue)
    {
        self.m_enhancePathsLayer = enhancePathsLayer
        self.m_replyNotifier = replyNotifier
        self.m_animationRequestQueue = animationRequestQueue
        self.m_requestEvalQueue = requestEvalQueue
    }

    func startAnimation(popItem : Bool)
    {
        if let animationRequest = getRequest(popItem)
        {
            switch animationRequest
            {
                case .eStartBeadAnimation(let (path, beadTranslateAnimationLayer, pathMorphAnimationLayer, enhancePathsLayer)):

                    // Cancel any fade outs.
                    enhancePathsLayer.removeAllAnimations()
                    self.m_isFadingOut = false

                    // Dispatch the animation.
                    runAnimationCommand(eAnimationCommand.eTranslateBeadAlongPath(beadTranslateAnimationLayer, path))
                    runAnimationCommand(eAnimationCommand.eMorphPath(pathMorphAnimationLayer, path))

                case .eStopBeadAnimations(let (beadTranslateAnimationLayer, pathMorphAnimationLayer, enhancePathsLayer)):

                    beadTranslateAnimationLayer.removeAllAnimations()
                    pathMorphAnimationLayer.removeAllAnimations()
                    enhancePathsLayer.removeAllAnimations()

                    break
            }
        }
    }

    private func getRequest(popItem : Bool) -> eAnimationRequest?
    {
        var result : eAnimationRequest? = nil

        m_requestEvalQueue.dispatchSync({ [unowned self] in

            if popItem
            {
                self.m_animationRequestQueue.popItem()
            }

            result = self.m_animationRequestQueue.firstItem
        })

        return result
    }

    private func runAnimationCommand(animationCommand: eAnimationCommand)
    {
        RxSDK.evalQueue.UIThreadQueue.dispatchAsync({ [unowned self] in

            switch animationCommand
            {
                case .eTranslateBeadAlongPath(let (layer, path)):

                    // Speed up animations if the animation queue grows beyond threshold.
                    if self.m_animationRequestQueue.itemCount > 2
                    {
                        self.m_currentAnimationTime *= 0.9
                    }

                    let beadAnimation = Animations.createBeadTranslateAlongPath_Animation(path)
                    let fadingBeadAnimation = Animations.createFadingAnimationFromAnimation(beadAnimation)
                    let animationID = animationCommand.nextAnimationID(&self.m_currentIDCounter)

                    fadingBeadAnimation.delegate = self
                    fadingBeadAnimation.duration = self.m_currentAnimationTime
                    fadingBeadAnimation.setValue(animationID, forKey:Animations.KeyID)

                    layer.addAnimation(fadingBeadAnimation, forKey: Animations.KeyID)

                case .eMorphPath(let (layer, path)):

                    let morphPathAnimation = Animations.createMorphPath_Animation(path)
                    let fadingMorphPathAnimation = Animations.createFadingAnimationFromAnimation(morphPathAnimation)
                    let shapeLayer = layer as! CAShapeLayer
                    let animationID = animationCommand.nextAnimationID(&self.m_currentIDCounter)

                    fadingMorphPathAnimation.delegate = self
                    fadingMorphPathAnimation.duration = self.m_currentAnimationTime / 2
                    fadingMorphPathAnimation.setValue(animationID, forKey:Animations.KeyID)

                    shapeLayer.path = path.CGPath
                    layer.addAnimation(fadingMorphPathAnimation, forKey: Animations.KeyID)

                default:

                    // do nothing
                    break;
            }
        })
    }

    override func animationDidStop(animation: CAAnimation, finished flag: Bool)
    {
        if let animationID = animation.valueForKey(Animations.KeyID) as? Int
        {
            switch animationID
            {
                case _ where eAnimationCommand.isTranslateBeadAlongPathID(animationID):

                    startAnimation(true)

                    if !m_isFadingOut
                    {
                        m_isFadingOut = true

                        let fadeAnimationID = eAnimationCommand.eFadeAllOut.nextAnimationID(&self.m_currentIDCounter)
                        let fadeOutAllAnimation = Animations.createFadeAllOut_Animation()

                        fadeOutAllAnimation.duration = Animations.FadeAnimationTime
                        fadeOutAllAnimation.delegate = self
                        fadeOutAllAnimation.setValue(fadeAnimationID, forKey:Animations.KeyID)

                        self.m_enhancePathsLayer.addAnimation(fadeOutAllAnimation, forKey: Animations.KeyID)
                    }

                case _ where eAnimationCommand.isFadeAllOutID(animationID):

                    if flag
                    {
                        m_replyNotifier.notifyItem(eAnimationReply.eAnimationsCompleted)

                        m_currentAnimationTime = Animations.BaseAnimationTime
                        m_enhancePathsLayer.opacity = 0.0
                    }

                default:
                    break
            }
        }
    }
}

private class AnimationService : RxService<eAnimationRequest, eAnimationReply>
{
    private var m_animationHandler : AnimationHandler? = nil
    private var m_animationRequestQueue = RxNotificationQueue<eAnimationRequest>(tag: "Animation Queue")
    private let m_requestEvalQueue: RxEvalQueue

    private init(tag: String,  enhancePathsLayer : CALayer)
    {
        m_requestEvalQueue = RxEvalQueue(sourceTag: tag + "/request", evalQueueType: .eSerial)

        super.init(tag : tag)

        m_animationHandler = AnimationHandler(enhancePathsLayer: enhancePathsLayer, replyNotifier: self.replyNotifier, animationRequestQueue: m_animationRequestQueue, requestEvalQueue : m_requestEvalQueue)
    }

    deinit
    {
        m_animationHandler = nil
    }

    override func request(animationRequest : eAnimationRequest)
    {
        self.m_requestEvalQueue.dispatchSync({

            self.m_animationRequestQueue.queueItem(animationRequest)

            if self.m_animationRequestQueue.itemCount == 1
            {
                self.m_animationHandler!.startAnimation(false)
            }
        })
    }
}

class AppOperationView : UIView
{
    private struct Constants
    {
        static let StrokeColor = UIColor(red: 0.023, green: 0.071, blue: 0.20, alpha: 1)
        static let StrokeEnhanceColor = UIColor.greenColor()
        static let StrokeEnhanceWidth = CGFloat(40)
        static let Tag = "AppOperationView"
    }

    private struct Paths
    {
        var pathsAreSet = false

        var fromKeyboard_To_POIKeywordMatcher: UIBezierPath? = nil
        var fromLocation_To_POIRequestor: UIBezierPath? = nil
        var fromReachability_To_POIRequestor: UIBezierPath? = nil

        var fromPOIKeywordMatcher_To_POIKeywordsResults: UIBezierPath? = nil
        var fromPOIKeywordMatcher_To_LogAdapter: UIBezierPath? = nil

        var fromPOIKeywordMatcher_To_POIRequestor: UIBezierPath? = nil
        var fromPOIKeywordMatcher_To_POILocator: UIBezierPath? = nil

        var fromPOILocator_To_LogAdaptor: UIBezierPath? = nil
        var fromPOILocator_To_MapAdaptor: UIBezierPath? = nil

        var fromPOILocator_To_POIPortalRxPeer: UIBezierPath? = nil
        var fromPOIPortalRxPeer_To_POILocator: UIBezierPath? = nil

        var fromPOIPortalRxPeer_To_URLSessionRxPeer: UIBezierPath? = nil
        var fromURLSessionRxPeer_To_POIPortalRxPeer: UIBezierPath? = nil
    }

    private class State
    {
        var paths = Paths()

        // Animations.
        var beadTranslateAnimationLayer: CAShapeLayer
        var pathMorphAnimationLayer: CAShapeLayer
        var enhancePathsLayer: CALayer
        var diagramLayer: CALayer
        var diagramLayerDirector: Layers.LayerDelegateDirector
        var enhancedPathLayerDirector: Layers.LayerDelegateDirector

        // Pathing
        var pathsToEnhance = [UIBezierPath]()
        var currentActiveAppEvents = Set<eAppOpEventType>()

        // Animation Service.
        var animationService: AnimationService

        init(parentView : UIView)
        {
            // Directors
            diagramLayerDirector = Layers.LayerDelegateDirector(view: parentView)
            enhancedPathLayerDirector = Layers.LayerDelegateDirector(view: parentView)

            // Layers
            beadTranslateAnimationLayer = Layers.createBeadShapeLayer()
            pathMorphAnimationLayer = Layers.createPathMorphShapeLayer(parentView.bounds, pathColor: Constants.StrokeColor.CGColor)
            enhancePathsLayer = Layers.createLayerWithDelegateDirector(parentView.bounds, director: enhancedPathLayerDirector)
            diagramLayer = Layers.createLayerWithDelegateDirector(parentView.bounds, director: diagramLayerDirector)

            // Services
            animationService = AnimationService(tag: Constants.Tag + "/AnimationService", enhancePathsLayer: enhancePathsLayer)

            parentView.layer.addSublayer(enhancePathsLayer)
            parentView.layer.addSublayer(pathMorphAnimationLayer)
            parentView.layer.addSublayer(diagramLayer)
            parentView.layer.addSublayer(beadTranslateAnimationLayer)
        }

        deinit
        {
            animationService.request(eAnimationRequest.eStopBeadAnimations(beadTranslateAnimationLayer, pathMorphAnimationLayer, enhancePathsLayer))
            animationService.disconnectAll()

            beadTranslateAnimationLayer.removeFromSuperlayer()
            pathMorphAnimationLayer.removeFromSuperlayer()
            enhancePathsLayer.removeFromSuperlayer()
            diagramLayer.removeFromSuperlayer()
        }
    }

    private var m_state : State? = nil

    override func didMoveToSuperview()
    {
        if superview != nil
        {
            // Subscribe for App Op Events for notifications of App Operational Events.
            if let appOpEventMonitor = AppViewController.SubRxDir.appOpEventMonitor
            {
                appOpEventMonitor.subscribe({ (appOpEvent : eAppOpEventType) in

                    RxSDK.evalQueue.UIThreadQueue.dispatchAsync({

                        self.notifyAppOpEvent(appOpEvent)

                    })
                }).addToDirectory(Constants.Tag)
            }

            // Subscribe for orientation changes.

            if let orientationInAdapter = RxDeviceDir.orientationAdapter
            {
                orientationInAdapter.subscribe({ (appEvent : AppEvent) in

                    RxSDK.evalQueue.UIThreadQueue.dispatchAsync({

                        // Re-layout view on orientation change.
                        self.setNeedsLayout()
                    })

                }).addToDirectory(Constants.Tag)
            }

            // Subscribe to the animation service for notification of animation completions.
            m_state!.animationService.subscribe({ (animationEvent: eAnimationReply) in

                RxSDK.evalQueue.UIThreadQueue.dispatchAsync({

                    switch animationEvent
                    {
                        case .eAnimationsCompleted:
                            self.m_state!.pathsToEnhance.removeAll()
                            self.m_state!.currentActiveAppEvents.removeAll()
                    }
                })
            }).addToDirectory(Constants.Tag)
        }
    }

    override func willMoveToSuperview(newSuperview: UIView?)
    {
        if newSuperview != nil
        {
            m_state = State(parentView: self)

            layer.setNeedsLayout()
            m_state!.diagramLayer.setNeedsDisplay()
        }
        else
        {
            shutdown()
        }
    }

    func notifyAppOpEvent(appOpEvent: eAppOpEventType)
    {
        if (self.layer.opacity == 0) || (m_state == nil) { return }

        let pathAlreadyDrawn = m_state!.currentActiveAppEvents.contains(appOpEvent)
        let path = appOpEventToPath(appOpEvent)

        if path == nil { return }

        // Clear any running fade out animations.
        m_state!.animationService.request(eAnimationRequest.eStartBeadAnimation(path!, m_state!.beadTranslateAnimationLayer, m_state!.pathMorphAnimationLayer, m_state!.enhancePathsLayer))

        if !pathAlreadyDrawn
        {
            m_state!.currentActiveAppEvents.insert(appOpEvent)
            m_state!.pathsToEnhance.append(path!)

            m_state!.enhancePathsLayer.removeAllAnimations()
            m_state!.enhancePathsLayer.opacity = 1.0

            m_state!.enhancePathsLayer.setNeedsDisplay()
        }
    }

    func shutdown()
    {
        RxDirectory.removeAllWithTagRegexpr(Constants.Tag)

        m_state = nil
    }

    private func appOpEventToPath(appOpEvent: eAppOpEventType) -> UIBezierPath?
    {
        if m_state == nil    { return nil }

        let paths = m_state!.paths

        switch appOpEvent
        {
            case .eFromKeyboard_To_POIKeywordMatcherPath:
                return paths.fromKeyboard_To_POIKeywordMatcher

            case .eFromLocation_To_POIRequestor:
                return paths.fromLocation_To_POIRequestor

            case .eFromReachability_To_POIRequestor:
                return paths.fromReachability_To_POIRequestor

            case .eFromPOIKeywordMatcher_To_POIKeywordsResults:
                return paths.fromPOIKeywordMatcher_To_POIKeywordsResults

            case .eFromPOIKeywordMatcher_To_LogAdapter:
                return paths.fromPOIKeywordMatcher_To_LogAdapter

            case .eFromPOIKeywordMatcher_To_POIRequestor:
                return paths.fromPOIKeywordMatcher_To_POIRequestor

            case .eFromPOIRequestor_To_POILocator:
                return paths.fromPOIKeywordMatcher_To_POILocator

            case .eFromPOILocator_To_LogAdaptor:
                return paths.fromPOILocator_To_LogAdaptor

            case .eFromPOILocator_To_MapAdaptor:
                return paths.fromPOILocator_To_MapAdaptor

            case .eFromPOILocator_To_POIPortalRxPeer:
                return paths.fromPOILocator_To_POIPortalRxPeer

            case .eFromPOIPortalRxPeer_To_POILocator:
                return paths.fromPOIPortalRxPeer_To_POILocator

            case .eFromPOIPortalRxPeer_To_URLSessionRxPeer:
                return paths.fromPOIPortalRxPeer_To_URLSessionRxPeer

            case .eFromURLSessionRxPeer_To_POIPortalRxPeer:
                return paths.fromURLSessionRxPeer_To_POIPortalRxPeer
        }
    }

    override func drawLayer(layer: CALayer, inContext context: CGContext)
    {
        switch layer
        {
            case m_state!.enhancePathsLayer:
                drawEnhancedPathsLayer(layer, inContext: context)

            case m_state!.diagramLayer:
                drawDiagramLayer(layer, inContext: context)

            default:
                super.drawLayer(layer, inContext: context)
        }
    }

    func drawEnhancedPathsLayer(layer: CALayer, inContext context: CGContext)
    {
        func strokeLines()
        {
            let shadowOffset = CGSizeMake(2, 2)

            CGContextSetLineWidth(context, Constants.StrokeEnhanceWidth)
            CGContextSetStrokeColorWithColor(context, Constants.StrokeEnhanceColor.CGColor)
            CGContextSetShadowWithColor(context, shadowOffset, 20.0, Constants.StrokeEnhanceColor.CGColor)

            for path in m_state!.pathsToEnhance
            {
                path.stroke()
            }
        }

        UIGraphicsPushContext(context)
        strokeLines()
        UIGraphicsPopContext()
    }

    func drawDiagramLayer(layer: CALayer!, inContext context: CGContext!)
    {
        // Drawing code generated by PaintCode 2.3.2
        
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()
        
        // Clear the view.
        CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 0.0)
        CGContextFillRect(context, layer.bounds)

        //// Color Declarations
        let color3 = UIColor(red: 0.023, green: 0.071, blue: 0.206, alpha: 1.000)
        let gradientColor2 = UIColor(red: 0.822, green: 0.840, blue: 0.924, alpha: 1.000)
        let shadow2Color = UIColor(red: 1.000, green: 0.989, blue: 0.989, alpha: 1.000)
        let color4 = UIColor(red: 0.310, green: 0.373, blue: 0.550, alpha: 1.000)
        let gradientColor = UIColor(red: 0.660, green: 0.637, blue: 0.851, alpha: 0.911)
        let greenGradientColor = UIColor(red: 0.292, green: 0.643, blue: 0.530, alpha: 1.000)
        let greenGradientColor2 = UIColor(red: 0.858, green: 0.974, blue: 0.861, alpha: 1.000)
        let gradientColor3 = UIColor(red: 0.672, green: 0.788, blue: 0.337, alpha: 1.000)
        let gradientColor4 = UIColor(red: 0.617, green: 0.692, blue: 0.289, alpha: 1.000)
        let softBlackShadowColor = UIColor(red: 0.000, green: 0.000, blue: 0.000, alpha: 0.580)
        let gradient2Color = UIColor(red: 0.381, green: 0.391, blue: 0.966, alpha: 1.000)
        let blueGradientColor = UIColor(red: 0.833, green: 0.876, blue: 0.931, alpha: 1.000)
        let color7 = UIColor(red: 0.902, green: 0.809, blue: 1.000, alpha: 1.000)
        
        //// Gradient Declarations
        let purpleGradient = CGGradientCreateWithColors(CGColorSpaceCreateDeviceRGB(), [gradientColor.CGColor, gradientColor2.CGColor], [0.08, 1])
        let greenGradient = CGGradientCreateWithColors(CGColorSpaceCreateDeviceRGB(), [greenGradientColor.CGColor, greenGradientColor2.CGColor], [0.1, 0.65, 1])
        let gradient = CGGradientCreateWithColors(CGColorSpaceCreateDeviceRGB(), [gradientColor3.CGColor, gradientColor4.CGColor], [0, 1])
        let blueGradient = CGGradientCreateWithColors(CGColorSpaceCreateDeviceRGB(), [gradient2Color.CGColor, blueGradientColor.CGColor], [0, 1])
        
        //// Shadow Declarations
        let hardBlackShadow = NSShadow()
        hardBlackShadow.shadowColor = UIColor.blackColor()
        hardBlackShadow.shadowOffset = CGSizeMake(3.1, 3.1)
        hardBlackShadow.shadowBlurRadius = 5
        let softBlackShadow = NSShadow()
        softBlackShadow.shadowColor = softBlackShadowColor
        softBlackShadow.shadowOffset = CGSizeMake(3.1, 3.1)
        softBlackShadow.shadowBlurRadius = 11
        let whiteShadow = NSShadow()
        whiteShadow.shadowColor = shadow2Color.colorWithAlphaComponent(0.43)
        whiteShadow.shadowOffset = CGSizeMake(3.1, 2.1)
        whiteShadow.shadowBlurRadius = 2
        
        //// fromPOIPortalRxPeer_To_POILocator Drawing
        let fromPOIPortalRxPeer_To_POILocatorPath = UIBezierPath()
        fromPOIPortalRxPeer_To_POILocatorPath.moveToPoint(CGPointMake(372.21, 500))
        fromPOIPortalRxPeer_To_POILocatorPath.addCurveToPoint(CGPointMake(372.21, 438), controlPoint1: CGPointMake(372.21, 494.56), controlPoint2: CGPointMake(372.21, 438))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        fromPOIPortalRxPeer_To_POILocatorPath.lineWidth = 8
        fromPOIPortalRxPeer_To_POILocatorPath.stroke()
        CGContextRestoreGState(context)
        
        
        //// fromURLSessionRxPeer_To_POIPortalRxPeer Drawing
        let fromURLSessionRxPeer_To_POIPortalRxPeerPath = UIBezierPath()
        fromURLSessionRxPeer_To_POIPortalRxPeerPath.moveToPoint(CGPointMake(371.21, 598))
        fromURLSessionRxPeer_To_POIPortalRxPeerPath.addCurveToPoint(CGPointMake(371.21, 536), controlPoint1: CGPointMake(371.21, 592.56), controlPoint2: CGPointMake(371.21, 536))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        fromURLSessionRxPeer_To_POIPortalRxPeerPath.lineWidth = 8
        fromURLSessionRxPeer_To_POIPortalRxPeerPath.stroke()
        CGContextRestoreGState(context)
        
        
        //// fromPOIKeywordMatcher_To_LogAdapter Drawing
        let fromPOIKeywordMatcher_To_LogAdapterPath = UIBezierPath()
        fromPOIKeywordMatcher_To_LogAdapterPath.moveToPoint(CGPointMake(445.21, 199.93))
        fromPOIKeywordMatcher_To_LogAdapterPath.addCurveToPoint(CGPointMake(467.97, 199.93), controlPoint1: CGPointMake(445.21, 199.93), controlPoint2: CGPointMake(453.54, 199.41))
        fromPOIKeywordMatcher_To_LogAdapterPath.addCurveToPoint(CGPointMake(489.79, 199.93), controlPoint1: CGPointMake(473.68, 200.14), controlPoint2: CGPointMake(484.45, 198.02))
        fromPOIKeywordMatcher_To_LogAdapterPath.addCurveToPoint(CGPointMake(528.69, 366), controlPoint1: CGPointMake(507.82, 206.4), controlPoint2: CGPointMake(511.82, 338.05))
        fromPOIKeywordMatcher_To_LogAdapterPath.addCurveToPoint(CGPointMake(556.2, 389), controlPoint1: CGPointMake(535.33, 377), controlPoint2: CGPointMake(540.4, 386.11))
        fromPOIKeywordMatcher_To_LogAdapterPath.addCurveToPoint(CGPointMake(596.04, 389), controlPoint1: CGPointMake(577.99, 392.99), controlPoint2: CGPointMake(596.04, 389))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        fromPOIKeywordMatcher_To_LogAdapterPath.lineWidth = 8
        fromPOIKeywordMatcher_To_LogAdapterPath.stroke()
        CGContextRestoreGState(context)
        
        
        //// fromPOIKeywordMatcher_To_POILocator Drawing
        let fromPOIKeywordMatcher_To_POILocatorPath = UIBezierPath()
        fromPOIKeywordMatcher_To_POILocatorPath.moveToPoint(CGPointMake(354.3, 321.16))
        fromPOIKeywordMatcher_To_POILocatorPath.addLineToPoint(CGPointMake(354.05, 370.16))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        fromPOIKeywordMatcher_To_POILocatorPath.lineWidth = 8
        fromPOIKeywordMatcher_To_POILocatorPath.stroke()
        CGContextRestoreGState(context)
        
        
        //// fromPOIKeywordMatcher_To_POIKeywordsResults Drawing
        let fromPOIKeywordMatcher_To_POIKeywordsResultsPath = UIBezierPath()
        fromPOIKeywordMatcher_To_POIKeywordsResultsPath.moveToPoint(CGPointMake(437.02, 196.71))
        fromPOIKeywordMatcher_To_POIKeywordsResultsPath.addCurveToPoint(CGPointMake(452.8, 200), controlPoint1: CGPointMake(437.02, 196.71), controlPoint2: CGPointMake(428.57, 199.87))
        fromPOIKeywordMatcher_To_POIKeywordsResultsPath.addCurveToPoint(CGPointMake(468.92, 200), controlPoint1: CGPointMake(460.87, 200.04), controlPoint2: CGPointMake(468.92, 200))
        fromPOIKeywordMatcher_To_POIKeywordsResultsPath.addCurveToPoint(CGPointMake(502.73, 207.12), controlPoint1: CGPointMake(478.97, 200.11), controlPoint2: CGPointMake(495.68, 198.04))
        fromPOIKeywordMatcher_To_POIKeywordsResultsPath.addCurveToPoint(CGPointMake(547.66, 234), controlPoint1: CGPointMake(513.51, 221), controlPoint2: CGPointMake(516.76, 233.34))
        fromPOIKeywordMatcher_To_POIKeywordsResultsPath.addCurveToPoint(CGPointMake(614.22, 234.33), controlPoint1: CGPointMake(594.15, 235), controlPoint2: CGPointMake(614.22, 234.33))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        fromPOIKeywordMatcher_To_POIKeywordsResultsPath.lineWidth = 8
        fromPOIKeywordMatcher_To_POIKeywordsResultsPath.stroke()
        CGContextRestoreGState(context)
        
        
        //// fromPOILocator_To_LogAdaptor Drawing
        let fromPOILocator_To_LogAdaptorPath = UIBezierPath()
        fromPOILocator_To_LogAdaptorPath.moveToPoint(CGPointMake(426.23, 413))
        fromPOILocator_To_LogAdaptorPath.addCurveToPoint(CGPointMake(458.98, 408.62), controlPoint1: CGPointMake(426.23, 413), controlPoint2: CGPointMake(450.82, 410.21))
        fromPOILocator_To_LogAdaptorPath.addCurveToPoint(CGPointMake(514.46, 390), controlPoint1: CGPointMake(466.77, 407.11), controlPoint2: CGPointMake(486.1, 388.93))
        fromPOILocator_To_LogAdaptorPath.addCurveToPoint(CGPointMake(596.04, 390.03), controlPoint1: CGPointMake(541.02, 391), controlPoint2: CGPointMake(596.04, 390.03))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        fromPOILocator_To_LogAdaptorPath.lineWidth = 8
        fromPOILocator_To_LogAdaptorPath.stroke()
        CGContextRestoreGState(context)
        
        
        //// fromPOILocator_To_MapAdaptor Drawing
        let fromPOILocator_To_MapAdaptorPath = UIBezierPath()
        fromPOILocator_To_MapAdaptorPath.moveToPoint(CGPointMake(428.13, 410))
        fromPOILocator_To_MapAdaptorPath.addCurveToPoint(CGPointMake(458.97, 419), controlPoint1: CGPointMake(428.13, 410), controlPoint2: CGPointMake(449.25, 407.81))
        fromPOILocator_To_MapAdaptorPath.addCurveToPoint(CGPointMake(514.46, 535), controlPoint1: CGPointMake(462.44, 423), controlPoint2: CGPointMake(501.12, 535))
        fromPOILocator_To_MapAdaptorPath.addCurveToPoint(CGPointMake(604.58, 534.71), controlPoint1: CGPointMake(558.1, 535), controlPoint2: CGPointMake(604.58, 534.71))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        fromPOILocator_To_MapAdaptorPath.lineWidth = 8
        fromPOILocator_To_MapAdaptorPath.stroke()
        CGContextRestoreGState(context)
        
        
        //// fromLocation_To_POIRequestor Drawing
        let fromLocation_To_POIRequestorPath = UIBezierPath()
        fromLocation_To_POIRequestorPath.moveToPoint(CGPointMake(272.55, 295.36))
        fromLocation_To_POIRequestorPath.addCurveToPoint(CGPointMake(265.06, 295.36), controlPoint1: CGPointMake(272.55, 295.36), controlPoint2: CGPointMake(270.61, 295.36))
        fromLocation_To_POIRequestorPath.addCurveToPoint(CGPointMake(221.03, 295.36), controlPoint1: CGPointMake(253.88, 295.36), controlPoint2: CGPointMake(234.14, 295.36))
        fromLocation_To_POIRequestorPath.addCurveToPoint(CGPointMake(185.44, 327.58), controlPoint1: CGPointMake(202.3, 295.36), controlPoint2: CGPointMake(195.74, 289.22))
        fromLocation_To_POIRequestorPath.addCurveToPoint(CGPointMake(167.64, 378.2), controlPoint1: CGPointMake(182.87, 337.16), controlPoint2: CGPointMake(176.63, 375.14))
        fromLocation_To_POIRequestorPath.addCurveToPoint(CGPointMake(124.56, 378.2), controlPoint1: CGPointMake(162.34, 380), controlPoint2: CGPointMake(124.56, 378.2))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        fromLocation_To_POIRequestorPath.lineWidth = 8
        fromLocation_To_POIRequestorPath.stroke()
        CGContextRestoreGState(context)
        
        
        //// fromPOIKeywordMatcher_To_POIRequestor Drawing
        let fromPOIKeywordMatcher_To_POIRequestorPath = UIBezierPath()
        fromPOIKeywordMatcher_To_POIRequestorPath.moveToPoint(CGPointMake(351.5, 214.93))
        fromPOIKeywordMatcher_To_POIRequestorPath.addLineToPoint(CGPointMake(351.29, 254.93))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        fromPOIKeywordMatcher_To_POIRequestorPath.lineWidth = 8
        fromPOIKeywordMatcher_To_POIRequestorPath.stroke()
        CGContextRestoreGState(context)
        
        
        //// Rectangle 33 Drawing
        let rectangle33Path = UIBezierPath(roundedRect: CGRectMake(262, 175, 187, 40), cornerRadius: 10)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        CGContextBeginTransparencyLayer(context, nil)
        rectangle33Path.addClip()
        CGContextDrawLinearGradient(context, purpleGradient, CGPointMake(301.67, 141.17), CGPointMake(409.33, 248.83), [])
        CGContextEndTransparencyLayer(context)
        CGContextRestoreGState(context)
        
        
        
        //// fromPOIPortalRxPeer_To_URLSessionRxPeer Drawing
        let fromPOIPortalRxPeer_To_URLSessionRxPeerPath = UIBezierPath()
        fromPOIPortalRxPeer_To_URLSessionRxPeerPath.moveToPoint(CGPointMake(329.7, 526.16))
        fromPOIPortalRxPeer_To_URLSessionRxPeerPath.addLineToPoint(CGPointMake(329.38, 587.16))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        fromPOIPortalRxPeer_To_URLSessionRxPeerPath.lineWidth = 8
        fromPOIPortalRxPeer_To_URLSessionRxPeerPath.stroke()
        CGContextRestoreGState(context)
        
        
        //// fromPOILocator_To_POIPortalRxPeer Drawing
        let fromPOILocator_To_POIPortalRxPeerPath = UIBezierPath()
        fromPOILocator_To_POIPortalRxPeerPath.moveToPoint(CGPointMake(329.76, 410.16))
        fromPOILocator_To_POIPortalRxPeerPath.addLineToPoint(CGPointMake(329.38, 482.16))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        fromPOILocator_To_POIPortalRxPeerPath.lineWidth = 8
        fromPOILocator_To_POIPortalRxPeerPath.stroke()
        CGContextRestoreGState(context)
        
        
        //// fromKeyboard_To_POIKeywordMatcher Drawing
        let fromKeyboard_To_POIKeywordMatcherPath = UIBezierPath()
        fromKeyboard_To_POIKeywordMatcherPath.moveToPoint(CGPointMake(137, 227.73))
        fromKeyboard_To_POIKeywordMatcherPath.addCurveToPoint(CGPointMake(163, 226.03), controlPoint1: CGPointMake(137, 227.73), controlPoint2: CGPointMake(155.29, 229))
        fromKeyboard_To_POIKeywordMatcherPath.addCurveToPoint(CGPointMake(192.02, 201.34), controlPoint1: CGPointMake(170.37, 223.2), controlPoint2: CGPointMake(177.58, 201.27))
        fromKeyboard_To_POIKeywordMatcherPath.addCurveToPoint(CGPointMake(258.44, 201.83), controlPoint1: CGPointMake(249.78, 201.65), controlPoint2: CGPointMake(258.44, 201.83))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        fromKeyboard_To_POIKeywordMatcherPath.lineWidth = 8
        fromKeyboard_To_POIKeywordMatcherPath.stroke()
        CGContextRestoreGState(context)
        
        
        //// Bezier 7 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 669.52, 236)
        CGContextRotateCTM(context, -179.95 * CGFloat(M_PI) / 180)
        
        let bezier7Path = UIBezierPath()
        bezier7Path.moveToPoint(CGPointMake(-37.52, -66.97))
        bezier7Path.addCurveToPoint(CGPointMake(51.69, -0.97), controlPoint1: CGPointMake(-36.37, -63.75), controlPoint2: CGPointMake(50.55, -29.94))
        bezier7Path.addCurveToPoint(CGPointMake(-37.52, 65.03), controlPoint1: CGPointMake(52.83, 28.01), controlPoint2: CGPointMake(-37.52, 65.03))
        bezier7Path.addLineToPoint(CGPointMake(-37.52, -66.97))
        bezier7Path.addLineToPoint(CGPointMake(-37.52, -66.97))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        CGContextBeginTransparencyLayer(context, nil)
        bezier7Path.addClip()
        CGContextDrawLinearGradient(context, greenGradient, CGPointMake(51.7, -0.97), CGPointMake(-37.52, -0.97), [])
        CGContextEndTransparencyLayer(context)
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        bezier7Path.lineWidth = 1.5
        bezier7Path.stroke()
        
        CGContextRestoreGState(context)
        
        
        //// Text 13 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 667.77, 236.92)
        CGContextRotateCTM(context, 90 * CGFloat(M_PI) / 180)
        
        let text13Rect = CGRectMake(-55.92, -38.42, 111.84, 76.84)
        let text13TextContent = NSString(string: "POI Keyword\nMatch Results\nView\nAdapter")
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text13Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text13Style.alignment = NSTextAlignment.Center
        
        let text13FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 13)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text13Style]
        
        let text13TextHeight: CGFloat = text13TextContent.boundingRectWithSize(CGSizeMake(text13Rect.width, CGFloat.infinity), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: text13FontAttributes, context: nil).size.height
        CGContextSaveGState(context)
        CGContextClipToRect(context, text13Rect);
        text13TextContent.drawInRect(CGRectMake(text13Rect.minX, text13Rect.minY + (text13Rect.height - text13TextHeight) / 2, text13Rect.width, text13TextHeight), withAttributes: text13FontAttributes)
        CGContextRestoreGState(context)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 5 Drawing
        let rectangle5Path = UIBezierPath(roundedRect: CGRectMake(285, 273, 145, 64), cornerRadius: 10)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        CGContextBeginTransparencyLayer(context, nil)
        rectangle5Path.addClip()
        CGContextDrawLinearGradient(context, gradient, CGPointMake(357.5, 273), CGPointMake(357.5, 337), [])
        CGContextEndTransparencyLayer(context)
        CGContextRestoreGState(context)
        
        
        
        //// Text 14 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 357.4, 302.99)
        CGContextRotateCTM(context, -0.39 * CGFloat(M_PI) / 180)
        
        let text14Rect = CGRectMake(-57.55, -21.01, 115.11, 42.03)
        let text14TextContent = NSString(string: "POI Requestor")
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text14Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text14Style.alignment = NSTextAlignment.Center
        
        let text14FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 14)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text14Style]
        
        let text14TextHeight: CGFloat = text14TextContent.boundingRectWithSize(CGSizeMake(text14Rect.width, CGFloat.infinity), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: text14FontAttributes, context: nil).size.height
        CGContextSaveGState(context)
        CGContextClipToRect(context, text14Rect);
        text14TextContent.drawInRect(CGRectMake(text14Rect.minX, text14Rect.minY + (text14Rect.height - text14TextHeight) / 2, text14Rect.width, text14TextHeight), withAttributes: text14FontAttributes)
        CGContextRestoreGState(context)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 10 Drawing
        let rectangle10Path = UIBezierPath(roundedRect: CGRectMake(256, 495, 189, 38), cornerRadius: 10)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        CGContextBeginTransparencyLayer(context, nil)
        rectangle10Path.addClip()
        CGContextDrawLinearGradient(context, purpleGradient, CGPointMake(296.67, 460.17), CGPointMake(404.33, 567.83), [])
        CGContextEndTransparencyLayer(context)
        CGContextRestoreGState(context)
        
        
        
        //// Text 5 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 350.15, 514.23)
        
        let text5Rect = CGRectMake(-92.16, -8.77, 184.32, 17.54)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text5Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text5Style.alignment = NSTextAlignment.Center
        
        let text5FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 14)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text5Style]
        
        "POI Portal RxService\n".drawInRect(text5Rect, withAttributes: text5FontAttributes)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 11 Drawing
        let rectangle11Path = UIBezierPath(roundedRect: CGRectMake(256, 589, 190, 39), cornerRadius: 10)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        CGContextBeginTransparencyLayer(context, nil)
        rectangle11Path.addClip()
        CGContextDrawLinearGradient(context, purpleGradient, CGPointMake(296.67, 554.17), CGPointMake(405.33, 662.83), [])
        CGContextEndTransparencyLayer(context)
        CGContextRestoreGState(context)
        
        
        
        //// Text 15 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 353, 608.73)
        
        let text15Rect = CGRectMake(-83, -12.27, 166, 24.54)
        let text15TextContent = NSString(string: "URL Session RxService")
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text15Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text15Style.alignment = NSTextAlignment.Center
        
        let text15FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 14)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text15Style]
        
        let text15TextHeight: CGFloat = text15TextContent.boundingRectWithSize(CGSizeMake(text15Rect.width, CGFloat.infinity), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: text15FontAttributes, context: nil).size.height
        CGContextSaveGState(context)
        CGContextClipToRect(context, text15Rect);
        text15TextContent.drawInRect(CGRectMake(text15Rect.minX, text15Rect.minY + (text15Rect.height - text15TextHeight) / 2, text15Rect.width, text15TextHeight), withAttributes: text15FontAttributes)
        CGContextRestoreGState(context)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 13 Drawing
        let rectangle13Path = UIBezierPath()
        rectangle13Path.moveToPoint(CGPointMake(122.08, 242.5))
        rectangle13Path.addLineToPoint(CGPointMake(142.95, 228.5))
        rectangle13Path.addLineToPoint(CGPointMake(122.08, 214.5))
        rectangle13Path.addLineToPoint(CGPointMake(122.08, 242.5))
        rectangle13Path.closePath()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle13Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle13Path.lineWidth = 1
        rectangle13Path.stroke()
        
        
        //// Bezier 5 Drawing
        let bezier5Path = UIBezierPath()
        bezier5Path.moveToPoint(CGPointMake(37.5, 159))
        bezier5Path.addCurveToPoint(CGPointMake(127.14, 227), controlPoint1: CGPointMake(38.65, 162.32), controlPoint2: CGPointMake(125.99, 197.15))
        bezier5Path.addCurveToPoint(CGPointMake(37.5, 295), controlPoint1: CGPointMake(128.29, 256.85), controlPoint2: CGPointMake(37.5, 295))
        bezier5Path.addLineToPoint(CGPointMake(37.5, 159))
        bezier5Path.addLineToPoint(CGPointMake(37.5, 159))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        CGContextBeginTransparencyLayer(context, nil)
        bezier5Path.addClip()
        CGContextDrawLinearGradient(context, blueGradient, CGPointMake(127.15, 227), CGPointMake(37.5, 227), [])
        CGContextEndTransparencyLayer(context)
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        bezier5Path.lineWidth = 1.5
        bezier5Path.stroke()
        
        
        //// Text 10 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 86.04, 227.42)
        CGContextRotateCTM(context, -90 * CGFloat(M_PI) / 180)
        
        let text10Rect = CGRectMake(-43.42, -40.96, 86.84, 81.93)
        let text10TextContent = NSString(string: "Keyboard\nDevice \nAdaptor\n")
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text10Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text10Style.alignment = NSTextAlignment.Center
        
        let text10FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 13)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text10Style]
        
        let text10TextHeight: CGFloat = text10TextContent.boundingRectWithSize(CGSizeMake(text10Rect.width, CGFloat.infinity), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: text10FontAttributes, context: nil).size.height
        CGContextSaveGState(context)
        CGContextClipToRect(context, text10Rect);
        text10TextContent.drawInRect(CGRectMake(text10Rect.minX, text10Rect.minY + (text10Rect.height - text10TextHeight) / 2, text10Rect.width, text10TextHeight), withAttributes: text10FontAttributes)
        CGContextRestoreGState(context)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Text 4 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 375.79, 104.43)
        
        let text4Rect = CGRectMake(-205.08, -21.04, 410.15, 42.09)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, softBlackShadow.shadowOffset, softBlackShadow.shadowBlurRadius, (softBlackShadow.shadowColor as! UIColor).CGColor)
        let text4Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text4Style.alignment = NSTextAlignment.Center
        
        let text4FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 20)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text4Style]
        
        "Live App Operation Visualisation".drawInRect(text4Rect, withAttributes: text4FontAttributes)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Text 6 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 373.71, 133.46)
        
        let text6Rect = CGRectMake(-357.29, -15.54, 714.58, 31.08)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text6Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text6Style.alignment = NSTextAlignment.Center
        
        let text6FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 15)!, NSForegroundColorAttributeName: UIColor.darkGrayColor(), NSParagraphStyleAttributeName: text6Style]
        
        "Visualisation of Event Notifications Flowing Through The App - enter text to observe".drawInRect(text6Rect, withAttributes: text6FontAttributes)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle Drawing
        let rectanglePath = UIBezierPath()
        rectanglePath.moveToPoint(CGPointMake(116.49, 393.5))
        rectanglePath.addLineToPoint(CGPointMake(137.37, 379.5))
        rectanglePath.addLineToPoint(CGPointMake(116.49, 365.5))
        rectanglePath.addLineToPoint(CGPointMake(116.49, 393.5))
        rectanglePath.closePath()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectanglePath.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectanglePath.lineWidth = 1
        rectanglePath.stroke()
        
        
        //// Bezier Drawing
        let bezierPath = UIBezierPath()
        bezierPath.moveToPoint(CGPointMake(40, 321))
        bezierPath.addCurveToPoint(CGPointMake(124.08, 378), controlPoint1: CGPointMake(41.08, 323.78), controlPoint2: CGPointMake(123, 352.98))
        bezierPath.addCurveToPoint(CGPointMake(40, 435), controlPoint1: CGPointMake(125.16, 403.02), controlPoint2: CGPointMake(40, 435))
        bezierPath.addLineToPoint(CGPointMake(40, 321))
        bezierPath.addLineToPoint(CGPointMake(40, 321))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        CGContextBeginTransparencyLayer(context, nil)
        bezierPath.addClip()
        CGContextDrawLinearGradient(context, blueGradient, CGPointMake(124.09, 378), CGPointMake(40, 378), [])
        CGContextEndTransparencyLayer(context)
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        bezierPath.lineWidth = 1.5
        bezierPath.stroke()
        
        
        //// Text Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 81.41, 378.42)
        CGContextRotateCTM(context, -90 * CGFloat(M_PI) / 180)
        
        let textRect = CGRectMake(-43.42, -33.59, 86.84, 67.18)
        let textTextContent = NSString(string: "Location\nDevice \nAdaptor\n")
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let textStyle = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        textStyle.alignment = NSTextAlignment.Center
        
        let textFontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 13)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: textStyle]
        
        let textTextHeight: CGFloat = textTextContent.boundingRectWithSize(CGSizeMake(textRect.width, CGFloat.infinity), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: textFontAttributes, context: nil).size.height
        CGContextSaveGState(context)
        CGContextClipToRect(context, textRect);
        textTextContent.drawInRect(CGRectMake(textRect.minX, textRect.minY + (textRect.height - textTextHeight) / 2, textRect.width, textTextHeight), withAttributes: textFontAttributes)
        CGContextRestoreGState(context)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 6 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 602.59, 244.45)
        CGContextRotateCTM(context, 179.68 * CGFloat(M_PI) / 180)
        
        let rectangle6Path = UIBezierPath(rect: CGRectMake(0, 0, 9, 16))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle6Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle6Path.lineWidth = 1
        rectangle6Path.stroke()
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 8 Drawing
        let rectangle8Path = UIBezierPath()
        rectangle8Path.moveToPoint(CGPointMake(602.5, 249.5))
        rectangle8Path.addLineToPoint(CGPointMake(624.5, 235.5))
        rectangle8Path.addLineToPoint(CGPointMake(602.5, 221.5))
        rectangle8Path.addLineToPoint(CGPointMake(602.5, 249.5))
        rectangle8Path.closePath()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle8Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle8Path.lineWidth = 1
        rectangle8Path.stroke()
        
        
        //// Rectangle 12 Drawing
        let rectangle12Path = UIBezierPath(roundedRect: CGRectMake(258, 390, 182, 39), cornerRadius: 10)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        CGContextBeginTransparencyLayer(context, nil)
        rectangle12Path.addClip()
        CGContextDrawLinearGradient(context, purpleGradient, CGPointMake(349, 390), CGPointMake(349, 429), [])
        CGContextEndTransparencyLayer(context)
        CGContextRestoreGState(context)
        
        
        
        //// Text 16 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 349.62, 409.73)
        
        let text16Rect = CGRectMake(-78.62, -9.27, 157.23, 18.54)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text16Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text16Style.alignment = NSTextAlignment.Center
        
        let text16FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 14)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text16Style]
        
        "POI Locator RxService".drawInRect(text16Rect, withAttributes: text16FontAttributes)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 15 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 248.95, 209.46)
        CGContextRotateCTM(context, 179.68 * CGFloat(M_PI) / 180)
        
        let rectangle15Path = UIBezierPath(rect: CGRectMake(0, 0, 6.39, 16.01))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle15Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle15Path.lineWidth = 1
        rectangle15Path.stroke()
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 16 Drawing
        let rectangle16Path = UIBezierPath()
        rectangle16Path.moveToPoint(CGPointMake(248.6, 214.5))
        rectangle16Path.addLineToPoint(CGPointMake(263.5, 200.5))
        rectangle16Path.addLineToPoint(CGPointMake(248.6, 186.5))
        rectangle16Path.addLineToPoint(CGPointMake(248.6, 214.5))
        rectangle16Path.closePath()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle16Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle16Path.lineWidth = 1
        rectangle16Path.stroke()
        
        
        //// Group 12
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 331, 488.5)
        CGContextRotateCTM(context, 90.37 * CGFloat(M_PI) / 180)
        
        
        
        //// Rectangle 25 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, -9.34, 8.98)
        CGContextRotateCTM(context, 179.68 * CGFloat(M_PI) / 180)
        
        let rectangle25Path = UIBezierPath(rect: CGRectMake(0, 0, 6.1, 15.98))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle25Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle25Path.lineWidth = 1
        rectangle25Path.stroke()
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 26 Drawing
        let rectangle26Path = UIBezierPath()
        rectangle26Path.moveToPoint(CGPointMake(-9.4, 14))
        rectangle26Path.addLineToPoint(CGPointMake(5.5, 0))
        rectangle26Path.addLineToPoint(CGPointMake(-9.4, -14))
        rectangle26Path.addLineToPoint(CGPointMake(-9.4, 14))
        rectangle26Path.closePath()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle26Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle26Path.lineWidth = 1
        rectangle26Path.stroke()
        
        
        
        CGContextRestoreGState(context)
        
        
        //// Group 14
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 331, 592.5)
        CGContextRotateCTM(context, 90.37 * CGFloat(M_PI) / 180)
        
        
        
        //// Rectangle 27 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, -9.34, 8.98)
        CGContextRotateCTM(context, 179.68 * CGFloat(M_PI) / 180)
        
        let rectangle27Path = UIBezierPath(rect: CGRectMake(0, 0, 6.1, 15.98))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle27Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle27Path.lineWidth = 1
        rectangle27Path.stroke()
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 28 Drawing
        let rectangle28Path = UIBezierPath()
        rectangle28Path.moveToPoint(CGPointMake(-9.4, 14))
        rectangle28Path.addLineToPoint(CGPointMake(5.5, 0))
        rectangle28Path.addLineToPoint(CGPointMake(-9.4, -14))
        rectangle28Path.addLineToPoint(CGPointMake(-9.4, 14))
        rectangle28Path.closePath()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle28Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle28Path.lineWidth = 1
        rectangle28Path.stroke()
        
        
        
        CGContextRestoreGState(context)
        
        
        //// Group
        //// Rectangle 29 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 380.73, 436.87)
        CGContextRotateCTM(context, 90.06 * CGFloat(M_PI) / 180)
        
        let rectangle29Path = UIBezierPath(rect: CGRectMake(0, 0, 6.1, 15.16))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle29Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle29Path.lineWidth = 1
        rectangle29Path.stroke()
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 30 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 360, 436.91)
        CGContextRotateCTM(context, -89.63 * CGFloat(M_PI) / 180)
        
        let rectangle30Path = UIBezierPath()
        rectangle30Path.moveToPoint(CGPointMake(0, 27))
        rectangle30Path.addLineToPoint(CGPointMake(14.9, 13.5))
        rectangle30Path.addLineToPoint(CGPointMake(0, 0))
        rectangle30Path.addLineToPoint(CGPointMake(0, 27))
        rectangle30Path.closePath()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle30Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle30Path.lineWidth = 1
        rectangle30Path.stroke()
        
        CGContextRestoreGState(context)
        
        
        
        
        //// Text 11 Drawing
        let text11Rect = CGRectMake(220, 552, 110, 16)
        let text11Path = UIBezierPath(rect: text11Rect)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, softBlackShadow.shadowOffset, softBlackShadow.shadowBlurRadius, (softBlackShadow.shadowColor as! UIColor).CGColor)
        UIColor.whiteColor().setFill()
        text11Path.fill()
        CGContextRestoreGState(context)
        
        let text11Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text11Style.alignment = NSTextAlignment.Center
        
        let text11FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica", size: 12)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text11Style]
        
        "URL HTTP Request".drawInRect(text11Rect, withAttributes: text11FontAttributes)
        
        
        //// Text 18 Drawing
        let text18Rect = CGRectMake(372, 552, 104, 16)
        let text18Path = UIBezierPath(rect: text18Rect)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, softBlackShadow.shadowOffset, softBlackShadow.shadowBlurRadius, (softBlackShadow.shadowColor as! UIColor).CGColor)
        UIColor.whiteColor().setFill()
        text18Path.fill()
        CGContextRestoreGState(context)
        
        let text18Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text18Style.alignment = NSTextAlignment.Center
        
        let text18FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica", size: 12)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text18Style]
        
        "URL HTTP Reply".drawInRect(text18Rect, withAttributes: text18FontAttributes)
        
        
        //// Text 3 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 355.57, 195.89)
        CGContextRotateCTM(context, -0.39 * CGFloat(M_PI) / 180)
        
        let text3Rect = CGRectMake(-86.31, -17.69, 172.63, 35.38)
        let text3TextContent = NSString(string: "POI Keyword Matcher ")
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text3Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text3Style.alignment = NSTextAlignment.Center
        
        let text3FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 14)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text3Style]
        
        let text3TextHeight: CGFloat = text3TextContent.boundingRectWithSize(CGSizeMake(text3Rect.width, CGFloat.infinity), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: text3FontAttributes, context: nil).size.height
        CGContextSaveGState(context)
        CGContextClipToRect(context, text3Rect);
        text3TextContent.drawInRect(CGRectMake(text3Rect.minX, text3Rect.minY + (text3Rect.height - text3TextHeight) / 2, text3Rect.width, text3TextHeight), withAttributes: text3FontAttributes)
        CGContextRestoreGState(context)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Group 10
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 356, 389.5)
        CGContextRotateCTM(context, 90.37 * CGFloat(M_PI) / 180)
        
        
        
        //// Rectangle 23 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, -9.34, 8.98)
        CGContextRotateCTM(context, 179.68 * CGFloat(M_PI) / 180)
        
        let rectangle23Path = UIBezierPath(rect: CGRectMake(0, 0, 6.1, 15.98))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle23Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle23Path.lineWidth = 1
        rectangle23Path.stroke()
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 24 Drawing
        let rectangle24Path = UIBezierPath()
        rectangle24Path.moveToPoint(CGPointMake(-9.4, 14))
        rectangle24Path.addLineToPoint(CGPointMake(5.5, 0))
        rectangle24Path.addLineToPoint(CGPointMake(-9.4, -14))
        rectangle24Path.addLineToPoint(CGPointMake(-9.4, 14))
        rectangle24Path.closePath()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle24Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle24Path.lineWidth = 1
        rectangle24Path.stroke()
        
        
        
        CGContextRestoreGState(context)
        
        
        //// Text 19 Drawing
        let text19Rect = CGRectMake(204, 450, 129, 16)
        let text19Path = UIBezierPath(rect: text19Rect)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, softBlackShadow.shadowOffset, softBlackShadow.shadowBlurRadius, (softBlackShadow.shadowColor as! UIColor).CGColor)
        UIColor.whiteColor().setFill()
        text19Path.fill()
        CGContextRestoreGState(context)
        
        let text19Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text19Style.alignment = NSTextAlignment.Center
        
        let text19FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica", size: 12)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text19Style]
        
        "POI Location Request Request".drawInRect(text19Rect, withAttributes: text19FontAttributes)
        
        
        //// Text 21 Drawing
        let text21Rect = CGRectMake(456, 399, 133, 16)
        let text21Path = UIBezierPath(rect: text21Rect)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, softBlackShadow.shadowOffset, softBlackShadow.shadowBlurRadius, (softBlackShadow.shadowColor as! UIColor).CGColor)
        UIColor.whiteColor().setFill()
        text21Path.fill()
        CGContextRestoreGState(context)
        
        let text21Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text21Style.alignment = NSTextAlignment.Center
        
        let text21FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica", size: 12)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text21Style]
        
        "POI Location Result".drawInRect(text21Rect, withAttributes: text21FontAttributes)
        
        
        //// Text 7 Drawing
        let text7Rect = CGRectMake(300, 228, 125, 16)
        let text7Path = UIBezierPath(rect: text7Rect)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, softBlackShadow.shadowOffset, softBlackShadow.shadowBlurRadius, (softBlackShadow.shadowColor as! UIColor).CGColor)
        UIColor.whiteColor().setFill()
        text7Path.fill()
        CGContextRestoreGState(context)
        
        let text7Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text7Style.alignment = NSTextAlignment.Center
        
        let text7FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica", size: 12)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text7Style]
        
        "POI Keyword Matches".drawInRect(text7Rect, withAttributes: text7FontAttributes)
        
        
        //// Group 9
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 353.01, 267.55)
        CGContextRotateCTM(context, 90.37 * CGFloat(M_PI) / 180)
        
        
        
        //// Rectangle 21 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, -7.39, 8.98)
        CGContextRotateCTM(context, 179.68 * CGFloat(M_PI) / 180)
        
        let rectangle21Path = UIBezierPath(rect: CGRectMake(0, 0, 6.1, 15.98))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle21Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle21Path.lineWidth = 1
        rectangle21Path.stroke()
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 22 Drawing
        let rectangle22Path = UIBezierPath()
        rectangle22Path.moveToPoint(CGPointMake(-7.45, 14))
        rectangle22Path.addLineToPoint(CGPointMake(7.45, -0))
        rectangle22Path.addLineToPoint(CGPointMake(-7.45, -14))
        rectangle22Path.addLineToPoint(CGPointMake(-7.45, 14))
        rectangle22Path.closePath()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle22Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle22Path.lineWidth = 1
        rectangle22Path.stroke()
        
        
        
        CGContextRestoreGState(context)
        
        
        //// Text 22 Drawing
        let text22Rect = CGRectMake(275, 354, 151, 16)
        let text22Path = UIBezierPath(rect: text22Rect)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, softBlackShadow.shadowOffset, softBlackShadow.shadowBlurRadius, (softBlackShadow.shadowColor as! UIColor).CGColor)
        UIColor.whiteColor().setFill()
        text22Path.fill()
        CGContextRestoreGState(context)
        
        let text22Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text22Style.alignment = NSTextAlignment.Center
        
        let text22FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica", size: 12)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text22Style]
        
        "POI Location Request".drawInRect(text22Rect, withAttributes: text22FontAttributes)
        
        
        //// Bezier 12 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 662.21, 392)
        CGContextRotateCTM(context, -179.95 * CGFloat(M_PI) / 180)
        
        let bezier12Path = UIBezierPath()
        bezier12Path.moveToPoint(CGPointMake(-44.3, -61.97))
        bezier12Path.addCurveToPoint(CGPointMake(44.29, -0), controlPoint1: CGPointMake(-43.16, -58.94), controlPoint2: CGPointMake(43.15, -27.2))
        bezier12Path.addCurveToPoint(CGPointMake(-44.3, 61.97), controlPoint1: CGPointMake(45.42, 27.2), controlPoint2: CGPointMake(-44.3, 61.97))
        bezier12Path.addLineToPoint(CGPointMake(-44.3, -61.97))
        bezier12Path.addLineToPoint(CGPointMake(-44.3, -61.97))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        CGContextBeginTransparencyLayer(context, nil)
        bezier12Path.addClip()
        CGContextDrawLinearGradient(context, greenGradient, CGPointMake(44.3, -0), CGPointMake(-44.3, -0), [])
        CGContextEndTransparencyLayer(context)
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        bezier12Path.lineWidth = 1.5
        bezier12Path.stroke()
        
        CGContextRestoreGState(context)
        
        
        //// Text 12 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 690.74, 372.37)
        CGContextRotateCTM(context, 90 * CGFloat(M_PI) / 180)
        
        let text12Rect = CGRectMake(-30.11, -15.5, 94.41, 62.37)
        let text12TextContent = NSString(string: "App Log\nView\nAdapter")
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text12Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text12Style.alignment = NSTextAlignment.Center
        
        let text12FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 13)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text12Style]
        
        let text12TextHeight: CGFloat = text12TextContent.boundingRectWithSize(CGSizeMake(text12Rect.width, CGFloat.infinity), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: text12FontAttributes, context: nil).size.height
        CGContextSaveGState(context)
        CGContextClipToRect(context, text12Rect);
        text12TextContent.drawInRect(CGRectMake(text12Rect.minX, text12Rect.minY + (text12Rect.height - text12TextHeight) / 2, text12Rect.width, text12TextHeight), withAttributes: text12FontAttributes)
        CGContextRestoreGState(context)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Bezier 20 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 664.11, 534.5)
        CGContextRotateCTM(context, -179.95 * CGFloat(M_PI) / 180)
        
        let bezier20Path = UIBezierPath()
        bezier20Path.moveToPoint(CGPointMake(-44.29, -69.49))
        bezier20Path.addCurveToPoint(CGPointMake(44.28, -2.52), controlPoint1: CGPointMake(-43.16, -66.23), controlPoint2: CGPointMake(43.15, -31.92))
        bezier20Path.addCurveToPoint(CGPointMake(-44.29, 64.46), controlPoint1: CGPointMake(45.42, 26.89), controlPoint2: CGPointMake(-44.29, 64.46))
        bezier20Path.addLineToPoint(CGPointMake(-44.29, -69.49))
        bezier20Path.addLineToPoint(CGPointMake(-44.29, -69.49))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        CGContextBeginTransparencyLayer(context, nil)
        bezier20Path.addClip()
        CGContextDrawLinearGradient(context, greenGradient, CGPointMake(44.29, -2.52), CGPointMake(-44.29, -2.52), [])
        CGContextEndTransparencyLayer(context)
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        bezier20Path.lineWidth = 1.5
        bezier20Path.stroke()
        
        CGContextRestoreGState(context)
        
        
        //// Text 17 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 694.74, 521.2)
        CGContextRotateCTM(context, 90 * CGFloat(M_PI) / 180)
        
        let text17Rect = CGRectMake(-31.33, -15.5, 98.21, 62.37)
        let text17TextContent = NSString(string: "Map MKAnnotation\nAdapter")
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text17Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text17Style.alignment = NSTextAlignment.Center
        
        let text17FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 13)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text17Style]
        
        let text17TextHeight: CGFloat = text17TextContent.boundingRectWithSize(CGSizeMake(text17Rect.width, CGFloat.infinity), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: text17FontAttributes, context: nil).size.height
        CGContextSaveGState(context)
        CGContextClipToRect(context, text17Rect);
        text17TextContent.drawInRect(CGRectMake(text17Rect.minX, text17Rect.minY + (text17Rect.height - text17TextHeight) / 2, text17Rect.width, text17TextHeight), withAttributes: text17FontAttributes)
        CGContextRestoreGState(context)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 2 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 604.59, 543.45)
        CGContextRotateCTM(context, 179.68 * CGFloat(M_PI) / 180)
        
        let rectangle2Path = UIBezierPath(rect: CGRectMake(0, 0, 9, 16))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle2Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle2Path.lineWidth = 1
        rectangle2Path.stroke()
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 3 Drawing
        let rectangle3Path = UIBezierPath()
        rectangle3Path.moveToPoint(CGPointMake(604.5, 548.5))
        rectangle3Path.addLineToPoint(CGPointMake(626.5, 534.5))
        rectangle3Path.addLineToPoint(CGPointMake(604.5, 520.5))
        rectangle3Path.addLineToPoint(CGPointMake(604.5, 548.5))
        rectangle3Path.closePath()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle3Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle3Path.lineWidth = 1
        rectangle3Path.stroke()
        
        
        //// Rectangle 9 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 601.59, 398.45)
        CGContextRotateCTM(context, 179.68 * CGFloat(M_PI) / 180)
        
        let rectangle9Path = UIBezierPath(rect: CGRectMake(0, 0, 9, 16))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle9Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle9Path.lineWidth = 1
        rectangle9Path.stroke()
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 14 Drawing
        let rectangle14Path = UIBezierPath()
        rectangle14Path.moveToPoint(CGPointMake(601.5, 403.5))
        rectangle14Path.addLineToPoint(CGPointMake(623.5, 389.5))
        rectangle14Path.addLineToPoint(CGPointMake(601.5, 375.5))
        rectangle14Path.addLineToPoint(CGPointMake(601.5, 403.5))
        rectangle14Path.closePath()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle14Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle14Path.lineWidth = 1
        rectangle14Path.stroke()
        
        
        //// Rectangle 4 Drawing
        let rectangle4Path = UIBezierPath(roundedRect: CGRectMake(18, 152, 25, 150), cornerRadius: 6)
        color7.setFill()
        rectangle4Path.fill()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        rectangle4Path.lineWidth = 2
        rectangle4Path.stroke()
        CGContextRestoreGState(context)
        
        
        //// Text 23 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 33.01, 225.78)
        CGContextRotateCTM(context, -90 * CGFloat(M_PI) / 180)
        
        let text23Rect = CGRectMake(-42.11, -10.78, 84.22, 21.57)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text23Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text23Style.alignment = NSTextAlignment.Center
        
        let text23FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 14)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text23Style]
        
        "Keyboard".drawInRect(text23Rect, withAttributes: text23FontAttributes)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 19 Drawing
        let rectangle19Path = UIBezierPath(roundedRect: CGRectMake(18, 313, 25, 133), cornerRadius: 6)
        color7.setFill()
        rectangle19Path.fill()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        rectangle19Path.lineWidth = 2
        rectangle19Path.stroke()
        CGContextRestoreGState(context)
        
        
        //// Text 24 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 32.97, 377.87)
        CGContextRotateCTM(context, -90 * CGFloat(M_PI) / 180)
        
        let text24Rect = CGRectMake(-38.87, -8.85, 77.74, 17.69)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text24Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text24Style.alignment = NSTextAlignment.Center
        
        let text24FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 14)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text24Style]
        
        "GPS".drawInRect(text24Rect, withAttributes: text24FontAttributes)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 34 Drawing
        let rectangle34Path = UIBezierPath(roundedRect: CGRectMake(706.5, 324, 25, 136), cornerRadius: 6)
        color7.setFill()
        rectangle34Path.fill()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        rectangle34Path.lineWidth = 2
        rectangle34Path.stroke()
        CGContextRestoreGState(context)
        
        
        //// Text 26 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 719.84, 388.67)
        CGContextRotateCTM(context, 90 * CGFloat(M_PI) / 180)
        
        let text26Rect = CGRectMake(-41.67, -10.37, 83.33, 20.74)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text26Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text26Style.alignment = NSTextAlignment.Center
        
        let text26FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 14)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text26Style]
        
        "App Log\n".drawInRect(text26Rect, withAttributes: text26FontAttributes)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 35 Drawing
        let rectangle35Path = UIBezierPath(roundedRect: CGRectMake(707, 466, 25, 142), cornerRadius: 6)
        color7.setFill()
        rectangle35Path.fill()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        rectangle35Path.lineWidth = 2
        rectangle35Path.stroke()
        CGContextRestoreGState(context)
        
        
        //// Text 27 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 719.79, 531.29)
        CGContextRotateCTM(context, 90 * CGFloat(M_PI) / 180)
        
        let text27Rect = CGRectMake(-41.71, -10.37, 83.43, 20.74)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text27Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text27Style.alignment = NSTextAlignment.Center
        
        let text27FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 14)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text27Style]
        
        "Map\n".drawInRect(text27Rect, withAttributes: text27FontAttributes)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 20 Drawing
        let rectangle20Path = UIBezierPath(roundedRect: CGRectMake(706, 162, 25, 148), cornerRadius: 6)
        color7.setFill()
        rectangle20Path.fill()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        rectangle20Path.lineWidth = 2
        rectangle20Path.stroke()
        CGContextRestoreGState(context)
        
        
        //// Text 25 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 717.36, 236.5)
        CGContextRotateCTM(context, 90 * CGFloat(M_PI) / 180)
        
        let text25Rect = CGRectMake(-74.5, -10.78, 149, 21.57)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text25Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text25Style.alignment = NSTextAlignment.Center
        
        let text25FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 14)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text25Style]
        
        "POI  Results".drawInRect(text25Rect, withAttributes: text25FontAttributes)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Text 2 Drawing
        let text2Rect = CGRectMake(486, 184, 125, 16)
        let text2Path = UIBezierPath(rect: text2Rect)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, softBlackShadow.shadowOffset, softBlackShadow.shadowBlurRadius, (softBlackShadow.shadowColor as! UIColor).CGColor)
        UIColor.whiteColor().setFill()
        text2Path.fill()
        CGContextRestoreGState(context)
        
        let text2Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text2Style.alignment = NSTextAlignment.Center
        
        let text2FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica", size: 12)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text2Style]
        
        "POI Keyword Matches".drawInRect(text2Rect, withAttributes: text2FontAttributes)
        
        
        //// Text 28 Drawing
        let text28Rect = CGRectMake(454, 270, 125, 16)
        let text28Path = UIBezierPath(rect: text28Rect)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, softBlackShadow.shadowOffset, softBlackShadow.shadowBlurRadius, (softBlackShadow.shadowColor as! UIColor).CGColor)
        UIColor.whiteColor().setFill()
        text28Path.fill()
        CGContextRestoreGState(context)
        
        let text28Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text28Style.alignment = NSTextAlignment.Center
        
        let text28FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica", size: 12)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text28Style]
        
        "POI Keyword Matches".drawInRect(text28Rect, withAttributes: text28FontAttributes)
        
        
        //// Text 20 Drawing
        let text20Rect = CGRectMake(467, 482, 133, 16)
        let text20Path = UIBezierPath(rect: text20Rect)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, softBlackShadow.shadowOffset, softBlackShadow.shadowBlurRadius, (softBlackShadow.shadowColor as! UIColor).CGColor)
        UIColor.whiteColor().setFill()
        text20Path.fill()
        CGContextRestoreGState(context)
        
        let text20Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text20Style.alignment = NSTextAlignment.Center
        
        let text20FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica", size: 12)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text20Style]
        
        "POI Location Result".drawInRect(text20Rect, withAttributes: text20FontAttributes)
        
        
        //// Text 29 Drawing
        let text29Rect = CGRectMake(352, 450, 121, 16)
        let text29Path = UIBezierPath(rect: text29Rect)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, softBlackShadow.shadowOffset, softBlackShadow.shadowBlurRadius, (softBlackShadow.shadowColor as! UIColor).CGColor)
        UIColor.whiteColor().setFill()
        text29Path.fill()
        CGContextRestoreGState(context)
        
        let text29Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text29Style.alignment = NSTextAlignment.Center
        
        let text29FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica", size: 12)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text29Style]
        
        "POI Location Result".drawInRect(text29Rect, withAttributes: text29FontAttributes)
        
        
        //// fromReachability_To_POIRequestor Drawing
        let fromReachability_To_POIRequestorPath = UIBezierPath()
        fromReachability_To_POIRequestorPath.moveToPoint(CGPointMake(271, 321.87))
        fromReachability_To_POIRequestorPath.addCurveToPoint(CGPointMake(263.64, 321.87), controlPoint1: CGPointMake(271, 321.87), controlPoint2: CGPointMake(269.09, 321.87))
        fromReachability_To_POIRequestorPath.addCurveToPoint(CGPointMake(221, 322), controlPoint1: CGPointMake(252.65, 321.87), controlPoint2: CGPointMake(221, 322))
        fromReachability_To_POIRequestorPath.addCurveToPoint(CGPointMake(193, 402.98), controlPoint1: CGPointMake(221, 322), controlPoint2: CGPointMake(203.13, 306.7))
        fromReachability_To_POIRequestorPath.addCurveToPoint(CGPointMake(169, 513.34), controlPoint1: CGPointMake(190.47, 427.05), controlPoint2: CGPointMake(186, 498.54))
        fromReachability_To_POIRequestorPath.addCurveToPoint(CGPointMake(125.56, 529.82), controlPoint1: CGPointMake(158.97, 522.07), controlPoint2: CGPointMake(125.56, 529.82))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        fromReachability_To_POIRequestorPath.lineWidth = 8
        fromReachability_To_POIRequestorPath.stroke()
        CGContextRestoreGState(context)
        
        
        //// Rectangle 36 Drawing
        let rectangle36Path = UIBezierPath()
        rectangle36Path.moveToPoint(CGPointMake(115.49, 543.5))
        rectangle36Path.addLineToPoint(CGPointMake(136.37, 529.5))
        rectangle36Path.addLineToPoint(CGPointMake(115.49, 515.5))
        rectangle36Path.addLineToPoint(CGPointMake(115.49, 543.5))
        rectangle36Path.closePath()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle36Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle36Path.lineWidth = 1
        rectangle36Path.stroke()
        
        
        //// Bezier 2 Drawing
        let bezier2Path = UIBezierPath()
        bezier2Path.moveToPoint(CGPointMake(39, 471))
        bezier2Path.addCurveToPoint(CGPointMake(123.08, 528), controlPoint1: CGPointMake(40.08, 473.78), controlPoint2: CGPointMake(122, 502.98))
        bezier2Path.addCurveToPoint(CGPointMake(39, 585), controlPoint1: CGPointMake(124.16, 553.02), controlPoint2: CGPointMake(39, 585))
        bezier2Path.addLineToPoint(CGPointMake(39, 471))
        bezier2Path.addLineToPoint(CGPointMake(39, 471))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        CGContextBeginTransparencyLayer(context, nil)
        bezier2Path.addClip()
        CGContextDrawLinearGradient(context, blueGradient, CGPointMake(123.09, 528), CGPointMake(39, 528), [])
        CGContextEndTransparencyLayer(context)
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        bezier2Path.lineWidth = 1.5
        bezier2Path.stroke()
        
        
        //// Text 30 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 74.41, 529.42)
        CGContextRotateCTM(context, -90 * CGFloat(M_PI) / 180)
        
        let text30Rect = CGRectMake(-43.42, -24.59, 86.84, 49.18)
        let text30TextContent = NSString(string: "Reachability\nDevice \nAdaptor")
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text30Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text30Style.alignment = NSTextAlignment.Center
        
        let text30FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 13)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text30Style]
        
        let text30TextHeight: CGFloat = text30TextContent.boundingRectWithSize(CGSizeMake(text30Rect.width, CGFloat.infinity), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: text30FontAttributes, context: nil).size.height
        CGContextSaveGState(context)
        CGContextClipToRect(context, text30Rect);
        text30TextContent.drawInRect(CGRectMake(text30Rect.minX, text30Rect.minY + (text30Rect.height - text30TextHeight) / 2, text30Rect.width, text30TextHeight), withAttributes: text30FontAttributes)
        CGContextRestoreGState(context)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 37 Drawing
        let rectangle37Path = UIBezierPath(roundedRect: CGRectMake(17, 463, 25, 133), cornerRadius: 6)
        color7.setFill()
        rectangle37Path.fill()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color3.setStroke()
        rectangle37Path.lineWidth = 2
        rectangle37Path.stroke()
        CGContextRestoreGState(context)
        
        
        //// Text 31 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 29.97, 526.87)
        CGContextRotateCTM(context, -90 * CGFloat(M_PI) / 180)
        
        let text31Rect = CGRectMake(-47.87, -8.85, 95.74, 17.69)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, whiteShadow.shadowOffset, whiteShadow.shadowBlurRadius, (whiteShadow.shadowColor as! UIColor).CGColor)
        let text31Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text31Style.alignment = NSTextAlignment.Center
        
        let text31FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica-Bold", size: 14)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text31Style]
        
        "Reachability\ny\n".drawInRect(text31Rect, withAttributes: text31FontAttributes)
        CGContextRestoreGState(context)
        
        
        CGContextRestoreGState(context)
        
        
        //// Text 8 Drawing
        let text8Rect = CGRectMake(154, 282, 66, 16)
        let text8Path = UIBezierPath(rect: text8Rect)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, softBlackShadow.shadowOffset, softBlackShadow.shadowBlurRadius, (softBlackShadow.shadowColor as! UIColor).CGColor)
        UIColor.whiteColor().setFill()
        text8Path.fill()
        CGContextRestoreGState(context)
        
        let text8Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text8Style.alignment = NSTextAlignment.Center
        
        let text8FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica", size: 12)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text8Style]
        
        "Location".drawInRect(text8Rect, withAttributes: text8FontAttributes)
        
        
        //// Text 9 Drawing
        let text9Rect = CGRectMake(141, 185, 94, 16)
        let text9Path = UIBezierPath(rect: text9Rect)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, softBlackShadow.shadowOffset, softBlackShadow.shadowBlurRadius, (softBlackShadow.shadowColor as! UIColor).CGColor)
        UIColor.whiteColor().setFill()
        text9Path.fill()
        CGContextRestoreGState(context)
        
        let text9Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text9Style.alignment = NSTextAlignment.Center
        
        let text9FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica", size: 12)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text9Style]
        
        "Keyboard Text".drawInRect(text9Rect, withAttributes: text9FontAttributes)
        
        
        //// Text 32 Drawing
        let text32Rect = CGRectMake(152, 393, 80, 16)
        let text32Path = UIBezierPath(rect: text32Rect)
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, softBlackShadow.shadowOffset, softBlackShadow.shadowBlurRadius, (softBlackShadow.shadowColor as! UIColor).CGColor)
        UIColor.whiteColor().setFill()
        text32Path.fill()
        CGContextRestoreGState(context)
        
        let text32Style = NSParagraphStyle.defaultParagraphStyle().mutableCopy() as! NSMutableParagraphStyle
        text32Style.alignment = NSTextAlignment.Center
        
        let text32FontAttributes = [NSFontAttributeName: UIFont(name: "Helvetica", size: 12)!, NSForegroundColorAttributeName: UIColor.blackColor(), NSParagraphStyleAttributeName: text32Style]
        
        "Reachability".drawInRect(text32Rect, withAttributes: text32FontAttributes)
        
        
        //// Group 3
        //// Rectangle 17 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 273.95, 301.46)
        CGContextRotateCTM(context, 179.68 * CGFloat(M_PI) / 180)
        
        let rectangle17Path = UIBezierPath(rect: CGRectMake(0, 0, 6.39, 16.01))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle17Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle17Path.lineWidth = 1
        rectangle17Path.stroke()
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 18 Drawing
        let rectangle18Path = UIBezierPath()
        rectangle18Path.moveToPoint(CGPointMake(273.6, 306.5))
        rectangle18Path.addLineToPoint(CGPointMake(288.5, 292.5))
        rectangle18Path.addLineToPoint(CGPointMake(273.6, 278.5))
        rectangle18Path.addLineToPoint(CGPointMake(273.6, 306.5))
        rectangle18Path.closePath()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle18Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle18Path.lineWidth = 1
        rectangle18Path.stroke()
        
        
        
        
        //// Group 4
        //// Rectangle 39 Drawing
        let rectangle39Path = UIBezierPath()
        rectangle39Path.moveToPoint(CGPointMake(269.6, 335.5))
        rectangle39Path.addLineToPoint(CGPointMake(284.5, 321.5))
        rectangle39Path.addLineToPoint(CGPointMake(269.6, 307.5))
        rectangle39Path.addLineToPoint(CGPointMake(269.6, 335.5))
        rectangle39Path.closePath()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle39Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle39Path.lineWidth = 1
        rectangle39Path.stroke()
        
        
        
        
        //// Rectangle 40 Drawing
        let rectangle40Path = UIBezierPath(rect: CGRectMake(0, 661, 748, 12))
        UIColor.grayColor().setFill()
        rectangle40Path.fill()
        
        
        //// Group 5
        //// Rectangle 7 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 379.73, 536.87)
        CGContextRotateCTM(context, 90.06 * CGFloat(M_PI) / 180)
        
        let rectangle7Path = UIBezierPath(rect: CGRectMake(0, 0, 6.1, 15.16))
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle7Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle7Path.lineWidth = 1
        rectangle7Path.stroke()
        
        CGContextRestoreGState(context)
        
        
        //// Rectangle 41 Drawing
        CGContextSaveGState(context)
        CGContextTranslateCTM(context, 359, 536.91)
        CGContextRotateCTM(context, -89.63 * CGFloat(M_PI) / 180)
        
        let rectangle41Path = UIBezierPath()
        rectangle41Path.moveToPoint(CGPointMake(0, 27))
        rectangle41Path.addLineToPoint(CGPointMake(14.9, 13.5))
        rectangle41Path.addLineToPoint(CGPointMake(0, 0))
        rectangle41Path.addLineToPoint(CGPointMake(0, 27))
        rectangle41Path.closePath()
        CGContextSaveGState(context)
        CGContextSetShadowWithColor(context, hardBlackShadow.shadowOffset, hardBlackShadow.shadowBlurRadius, (hardBlackShadow.shadowColor as! UIColor).CGColor)
        color4.setFill()
        rectangle41Path.fill()
        CGContextRestoreGState(context)
        
        UIColor.blackColor().setStroke()
        rectangle41Path.lineWidth = 1
        rectangle41Path.stroke()
        
        CGContextRestoreGState(context)

        if (m_state != nil) && !m_state!.paths.pathsAreSet
        {
            m_state!.paths.fromKeyboard_To_POIKeywordMatcher = fromKeyboard_To_POIKeywordMatcherPath
            m_state!.paths.fromLocation_To_POIRequestor = fromLocation_To_POIRequestorPath
            m_state!.paths.fromReachability_To_POIRequestor = fromReachability_To_POIRequestorPath

            m_state!.paths.fromPOIKeywordMatcher_To_POIKeywordsResults = fromPOIKeywordMatcher_To_POIKeywordsResultsPath
            m_state!.paths.fromPOIKeywordMatcher_To_LogAdapter = fromPOIKeywordMatcher_To_LogAdapterPath

            m_state!.paths.fromPOILocator_To_LogAdaptor = fromPOILocator_To_LogAdaptorPath
            m_state!.paths.fromPOILocator_To_MapAdaptor = fromPOILocator_To_MapAdaptorPath

            m_state!.paths.fromPOIKeywordMatcher_To_POIRequestor = fromPOIKeywordMatcher_To_POIRequestorPath
            m_state!.paths.fromPOIKeywordMatcher_To_POILocator = fromPOIKeywordMatcher_To_POILocatorPath

            m_state!.paths.fromPOILocator_To_POIPortalRxPeer = fromPOILocator_To_POIPortalRxPeerPath
            m_state!.paths.fromPOIPortalRxPeer_To_POILocator = fromPOIPortalRxPeer_To_POILocatorPath

            m_state!.paths.fromPOIPortalRxPeer_To_URLSessionRxPeer = fromPOIPortalRxPeer_To_URLSessionRxPeerPath
            m_state!.paths.fromURLSessionRxPeer_To_POIPortalRxPeer = fromURLSessionRxPeer_To_POIPortalRxPeerPath

            m_state!.paths.pathsAreSet = true
        }
    }
}