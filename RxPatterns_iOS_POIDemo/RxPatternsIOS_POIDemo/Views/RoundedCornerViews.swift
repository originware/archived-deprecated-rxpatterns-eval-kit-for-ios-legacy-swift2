//
// Created by Terry Stillone (http://www.originware.com) on 26/07/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

class RoundedCornersView : UIView
{
    static let CornerRadius = CGFloat(10.0)

    private let m_cornerMask : UIRectCorner

    required init?(coder: NSCoder)
    {
        m_cornerMask = UIRectCorner.AllCorners

        super.init(coder: coder)
    }

    init?(coder: NSCoder, cornerMask : UIRectCorner)
    {
        m_cornerMask = cornerMask

        super.init(coder: coder)
    }

    override func layoutSubviews()
    {
        let maskLayer = CAShapeLayer()
        let cornerRadii = CGSize(width: RoundedCornersView.CornerRadius, height: RoundedCornersView.CornerRadius)
        let maskPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: m_cornerMask, cornerRadii: cornerRadii)

        maskLayer.path = maskPath.CGPath

        self.layer.mask = maskLayer;
    }
}

class TopRoundedCornersView : RoundedCornersView
{
    required init?(coder: NSCoder)
    {
        super.init(coder: coder, cornerMask : [UIRectCorner.TopLeft, UIRectCorner.TopRight])
    }

    override func drawRect(rect: CGRect)
    {
        let context = UIGraphicsGetCurrentContext()

        CGContextSaveGState(context)

        let colorspace = CGColorSpaceCreateDeviceRGB()
        let grStop0 = UIColor(red: 0.7, green: 0.7, blue: 0.3, alpha: 1)
        let grStop1 = UIColor(red: 0.59, green: 0.56, blue: 0.24, alpha: 1)

        let grStops: CFArray = [grStop0.CGColor, grStop1.CGColor]
        let grLocations: [CGFloat] = [0, 0.7]

        let gradient = CGGradientCreateWithColors(colorspace, grStops, grLocations)

        CGContextDrawLinearGradient(context, gradient, CGPointMake(0, 0), CGPointMake(0, self.bounds.height), [])

        CGContextRestoreGState(context)
    }
}

class BottomRoundedCornersView : RoundedCornersView
{
    required init?(coder: NSCoder)
    {
        super.init(coder: coder, cornerMask : [UIRectCorner.BottomLeft, UIRectCorner.BottomRight])
    }
}

class AllRoundedCornersView : RoundedCornersView
{
    required init?(coder: NSCoder)
    {
        super.init(coder: coder, cornerMask: UIRectCorner.AllCorners)
    }
}
