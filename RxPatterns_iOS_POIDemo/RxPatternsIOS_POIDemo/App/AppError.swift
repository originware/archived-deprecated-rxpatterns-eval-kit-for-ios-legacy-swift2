//
//  Created by Terry Stillone on 11/09/2015.
//  Copyright © 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// eRxLibError: Error representation for the RxPatternsLib.
///

public struct AppError : IRxError
{
    /// The type of error.
    public let errorDescription : String
    
    /// The optional error context.
    public var context : RxErrorContext?        { return m_errorContext }
    
    /// CustomDebugStringConvertible Protocol conformance.
    public var description: String              { return errorDescription }
    
    private let m_errorContext : RxErrorContext?
    
    /// Initialise the error with the error description and optional context.
    /// - Parameter errorType: Indicates the error type.
    /// - Parameter context: The optional error context.
    public init(_ errorDescription: String, context : RxErrorContext? = nil)
    {
        self.errorDescription = errorDescription
        self.m_errorContext = context
    }
    
    public init(_ nsError: NSError)
    {
        self.errorDescription = nsError.description
        self.m_errorContext = RxErrorContext(["NSError" : nsError])
    }

    /// Equator.
    public func isEqual(other : IRxError) -> Bool
    {
        // Not required for app.
        fatalError("Unexpected code point")
    }
}

