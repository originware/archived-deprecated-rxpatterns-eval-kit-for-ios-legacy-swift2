//
// Created by Terry Stillone (http://www.originware.com) on 13/09/2015.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxDeviceDir: The Sub-RxDirectory for Devices accessed by the App.
///

struct RxDeviceDir
{
    /// The Location Change Device.
    static let locationAdapterURI = "inadapters/location"
    static var locationAdapter : DeviceAdapter_LocationWithSimulateFailover?  { return get(locationAdapterURI) as? DeviceAdapter_LocationWithSimulateFailover }

    /// The Network Reacability Change Device.
    static let reachabilityAdapterURI = "inadapters/reachability"
    static var reachabilityAdapter : DeviceAdapter_Reachability?            { return get(reachabilityAdapterURI) as? DeviceAdapter_Reachability }

    /// The Orientation Change Device.
    static let orientationAdapterURI = "inadapters/orientation"
    static var orientationAdapter : DeviceAdapter_Orientation?              { return get(orientationAdapterURI) as? DeviceAdapter_Orientation }

    /// The keyboard Show/Hide Change Device.
    static let keyboardAdapterURI = "inadapters/keyboard"
    static var keyboardAdapter : EventAdapter_keyboard?                     { return get(keyboardAdapterURI) as? EventAdapter_keyboard }

    /// The App Event Monitor.
    static let appOpEventMonitorURI = "monitors/appopevent"
    static var appOpEventMonitor : RxRelaySubject<eAppOpEventType>?         { return get(appOpEventMonitorURI) as? RxRelaySubject<eAppOpEventType> }

    /// Store the given object by the given relative path.
    /// - Parameter relativePath: The relative path.
    /// - Parameter object: The object to store in the RxDirectory.
    static func store(relativePath : String, object : RxObject)
    {
        let namespace = eRxDirectory_NameSpace.eStore.rawValue

        RxDirectory.put(namespace + ":/" + AppConstant.ConfigScenario + "/" + relativePath, object)
    }

    /// Get the object associated with the given relative path.
    /// - Parameter relativePath: The relative path of the object to lookup in the RxDirectory.
    /// - Returns: The object from the RxDirectory, otherwise nil.
    static func get(relativePath : String) -> AnyObject?
    {
        let namespace = eRxDirectory_NameSpace.eRun.rawValue

        return RxDirectory.get(namespace + ":/" + relativePath)
    }
}
