//
// Created by Terry Stillone (http://www.originware.com) on 13/09/2015.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import CoreLocation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The POI Representation.
///

struct POI : Equatable
{
    /// The POI keyword for the POI.
    let poiKeyword : String

    /// The POI Location.
    let location : CLLocation?

    /// The POI Name.
    let name: String

    /// The POI Address.
    let address: String

    var coordinate : CLLocationCoordinate2D
    {
        return (location != nil) ? location!.coordinate : CLLocationCoordinate2DMake(0, 0)
    }
}

/// Equatable conformance.
func ==(lhs: POI, rhs: POI) -> Bool
{
    return (lhs.poiKeyword == rhs.poiKeyword) && (lhs.name == rhs.name) && (lhs.address == rhs.address) && (lhs.location == rhs.location)
}
