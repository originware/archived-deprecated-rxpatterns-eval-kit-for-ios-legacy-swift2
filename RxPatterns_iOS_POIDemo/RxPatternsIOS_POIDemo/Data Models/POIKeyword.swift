//
// Created by Terry Stillone (http://www.originware.com) on 13/09/2015.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POI Keyword Match
///

public class POIKeywordMatch
{
    public let poiKeyword : String
    public var keywordMatchRanges = [Range<String.Index>]()

    public var totalMatchLength : Int {

        var total = 0

        for range in keywordMatchRanges
        {
            total += range.startIndex.distanceTo(range.endIndex)
        }

        return total
    }

    public var maxMatchLength : Int {

        var maxMatchLength = 0

        for range in keywordMatchRanges
        {
            maxMatchLength = max(maxMatchLength, range.startIndex.distanceTo(range.endIndex))
        }

        return maxMatchLength
    }

    public var haveFullMatch : Bool {

        return maxMatchLength == poiKeyword.characters.count
    }

    public init(poiKeyword : String)
    {
        self.poiKeyword = poiKeyword
    }

    func addMatchForPOIKeyword(matchRange : Range<String.Index>)
    {
        func isMatchRangeUnique() -> Bool
        {
            for range in keywordMatchRanges
            {
                // If the matchRange intersects with another match, discard it.
                if Helpers.doRangesIntersect(poiKeyword, range, matchRange)
                {
                    return false
                }
            }

            return true
        }

        if isMatchRangeUnique()
        {
            keywordMatchRanges.append(matchRange)
        }
    }
}
