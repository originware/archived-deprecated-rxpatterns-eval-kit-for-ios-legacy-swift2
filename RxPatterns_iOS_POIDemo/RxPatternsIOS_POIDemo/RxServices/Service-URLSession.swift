//
// Created by Terry Stillone (http://www.originware.com) on 13/07/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// URLRequestReply: The RxService_URLSession request/reply entity.
///

public struct URLRequestReply
{
    /// The URL of the request/reply.
    let url : NSURL

    /// The http request data.
    let requestData : Any?

    /// The http replay data.
    let replyData: Any?

    /// Indicator of error during the request/reply operation.
    let error : NSError?

    var hasError : Bool { return error != nil }

    /// Initialise with url, request data, reply data and optional error.
    /// - Parameter url: The url of the HTTP request.
    /// - Parameter requestData: The HTTP request data.
    /// - Parameter replyData: The HTTP reply data.
    /// - Parameter error: Optional error.
    init(url: NSURL, requestData: Any? = nil, replyData: Any? = nil, error: NSError? = nil)
    {
        self.url = url
        self.requestData = requestData
        self.replyData = replyData
        self.error = error
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxService_URLSession: The RxService performs online URL request/replies using NSURLSession.
///

class RxService_URLSession : RxService<URLRequestReply, URLRequestReply>
{
    /// The AppEvent monitor.
    private var m_appOpEventMonitor = AppViewController.SubRxDir.appOpEventMonitor

    /// Initialise with tag for this RxObject.
    override init(tag : String)
    {
        super.init(tag : tag)
    }

    /// Handle a HTTP request.
    /// - Parameter urlRequest: The HTTP URL Request.
    override func request(urlRequest : URLRequestReply)
    {
        // Request AppEvent monitoring notification.
        self.m_appOpEventMonitor?.notifyItem(.eFromPOIPortalRxPeer_To_URLSessionRxPeer)

        // Create a URL Session HTTP request.
        let dataTask: NSURLSessionDataTask = NSURLSession.sharedSession().dataTaskWithURL(urlRequest.url) { (data: NSData?, response: NSURLResponse?, error: NSError?) in

            // Handle a HTTP reply.

            switch (error, response)
            {
                case (.None, .Some(let httpResponse as NSHTTPURLResponse)):

                    // NSURLSession issued a reply.

                    switch httpResponse.statusCode
                    {
                        case 201, 200, 401:

                            let json = JSON(data: data!)
                            let urlReply = URLRequestReply(url: urlRequest.url, requestData: urlRequest.requestData, replyData: json)

                            // Notify the reply, with the reply data.
                            self.replyNotifier.notifyItem(urlReply)

                        default:
                            // Ignore:
                            break
                    }

                case (.Some(let httpError), _):

                    // NSURLSession issued a direct error.

                    let urlReply = URLRequestReply(url: urlRequest.url, requestData: urlRequest.requestData, error: httpError)

                    // Notify the reply, flagged as an error.
                    self.replyNotifier.notifyItem(urlReply)

                default:

                    // NSURLSession didn't get a reply within the expected time.

                    let errorDict = [NSLocalizedDescriptionKey : "URLSessionPeer: bad response"]
                    let error = NSError(domain: "URLSessionPeer", code: 0, userInfo: errorDict)
                    let urlReply = URLRequestReply(url: urlRequest.url, requestData: urlRequest.replyData, error: error)

                    // Notify the reply, flagged as an error.
                    self.replyNotifier.notifyItem(urlReply)
            }
        }

        // Perform the HTTP request.
        dataTask.resume()
    }
}

