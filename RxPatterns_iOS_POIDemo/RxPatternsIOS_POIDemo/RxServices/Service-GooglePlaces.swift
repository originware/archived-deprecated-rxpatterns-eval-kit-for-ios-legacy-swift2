//
// Created by Terry Stillone (http://www.originware.com) on 22/09/2015.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import CoreLocation
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxService_GooglePlacesPOILocator: The RxService that acts as a POI Locator using the Google Places online service.
///

class RxService_GooglePlacesPOILocator: RxService<POILocateEvent_Request, POILocateEvent_Reply>
{
    typealias InRequestType = POILocateEvent_Request
    typealias OutRequestType = URLRequestReply
    typealias InReplyType = URLRequestReply
    typealias OutReplyType = POILocateEvent_Reply

    /// The URLService begin used to handle HTTP requests and replies.
    private var m_urlSessionPeer: RxService_URLSession? = nil

    /// The subscription to the URLService
    private var m_urlSessionSubscription : RxSubscription? = nil

    /// App Event monitoring of this service.
    private var m_appOpEventMonitor = AppViewController.SubRxDir.appOpEventMonitor

    override init(tag : String)
    {
        super.init(tag : tag)
    }

    /// Begin a session with the URLSession service.
    override func sessionBegin()
    {
        if m_urlSessionSubscription == nil
        {
            onURLSessionBegin()
        }

        super.sessionBegin()
    }

    /// End a session with the URLSession service.
    override func sessionEnd(error : IRxError?)
    {
        if m_urlSessionSubscription != nil
        {
            onURLSessionEnd()
        }

        super.sessionEnd(error)
    }

    /// Handle a URL Session begin.
    private func onURLSessionBegin()
    {
        weak var weakSelf = self

        func inRequestToOutRequestMap(inRequest : InRequestType) -> OutRequestType?
        {
            if let strongSelf = weakSelf
            {
                strongSelf.m_appOpEventMonitor?.notifyItem(.eFromPOILocator_To_POIPortalRxPeer)

                return RequestReplyMap_GooglePlaces.poiLocateRequest_To_requestURLMap(inRequest)
            }

            return nil
        }

        func inReplyToOutReplyMap(inReply : InReplyType) -> OutReplyType?
        {
            if let strongSelf = weakSelf
            {
                strongSelf.m_appOpEventMonitor?.notifyItem(.eFromURLSessionRxPeer_To_POIPortalRxPeer)

                return RequestReplyMap_GooglePlaces.replyURL_To_poiLocateResultMap(inReply)
            }

            return nil
        }

        m_urlSessionPeer = RxService_URLSession(tag: tag)

        // Connect to the URL Session.
        m_urlSessionSubscription = connect(
            m_urlSessionPeer!,
            requestToPeerRequestMap: inRequestToOutRequestMap,
            peerReplyToSelfReplyMap: inReplyToOutReplyMap
        )
    }

    /// Handle a URL Session end.
    private func onURLSessionEnd()
    {
        disconnectAll()

        m_urlSessionSubscription?.unsubscribe()
        m_urlSessionSubscription = nil
        m_urlSessionPeer = nil

        replyNotifier.clear()
        requestor.clear()
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RequestReplyMap_GooglePlaces: The request/reply encoder/decoder for google places.
///

private struct RequestReplyMap_GooglePlaces
{
    private struct Constant
    {
        static let GoogleAPIKey = "AIzaSyBjBR4UyV_ypKrtJKUu0CyBQZdN1dk9GuQ"
        static let PortalName = "Google Places"
        static let POILocateRadius = 1000
    }

    static func poiLocateRequest_To_requestURLMap(poiLocateRequest : POILocateEvent_Request) -> URLRequestReply?
    {
        func generateURLAsString(poiKeywords: Set<String>) -> String?
        {
            let poiKeywordsAsString = poiKeywords.joinWithSeparator("|")
            let coordinate = poiLocateRequest.location.coordinate
            let urlAsString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(coordinate.latitude),\(coordinate.longitude)&radius=\(Constant.POILocateRadius)&types=\(poiKeywordsAsString)&key=\(Constant.GoogleAPIKey)"

            return urlAsString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        }

        return generateURLAsString(poiLocateRequest.poiKeywords)?.toNSURL?.toURLRequestReply(poiLocateRequest)
    }

    static func replyURL_To_poiLocateResultMap(urlReply : URLRequestReply) -> POILocateEvent_Reply?
    {
        let poiLocateRequest = urlReply.requestData as! POILocateEvent_Request
        let requestedPOIKeywords = poiLocateRequest.poiKeywords

        if !urlReply.hasError
        {
            let json = urlReply.replyData! as! JSON
            let pois = extractPOIsFromJSON(json, requestedPOIKeywords: requestedPOIKeywords)

            if pois == nil
            {
                // Ignore replies with no data.
                return nil
            }

            return POILocateEvent_Reply(locateSourceName: Constant.PortalName, requestPOIKeywords: requestedPOIKeywords, location : poiLocateRequest.location, pois: pois!)
        }
        else
        {
            return POILocateEvent_Reply(locateSourceName: Constant.PortalName, requestPOIKeywords: requestedPOIKeywords, location : poiLocateRequest.location, error: urlReply.error!)
        }
    }

    static func extractPOIsFromJSON(json : JSON, requestedPOIKeywords: Set<String>) -> [POI]?
    {
        var pois = [POI]()

        if let results = json["results"].array {

            for entry in results
            {
                let coordinate = entry["geometry"]["location"].dictionaryValue
                let lat = coordinate["lat"]?.stringValue.toDouble
                let lng = coordinate["lng"]?.stringValue.toDouble
                let name = entry["name"].stringValue
                let address = entry["vicinity"].stringValue
                let keywordTypes = entry["types"].arrayObject

                if (lat != nil) && (lng != nil)
                {
                    let keywordTypeSet = Set<String>(keywordTypes as! [String])
                    let commonKeywords = keywordTypeSet.intersect(requestedPOIKeywords)

                    if !commonKeywords.isEmpty
                    {
                        for poiKeyword in commonKeywords
                        {
                            let location = CLLocation(latitude: lat!, longitude:lng!)
                            let poi = POI(poiKeyword: poiKeyword, location: location, name: name, address: address)

                            pois.append(poi)
                        }
                    }
                }
            }
        }

        return !pois.isEmpty ? pois : nil
    }
}