//
// Created by Terry Stillone (http://www.originware.com) on 25/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// EventAdapter_keyboard: Adapt from UITextField NSNotifications to AppEvent RxNotifications.
///

class EventAdapter_UITextField: RxSource<AppEvent>
{
    /// The UITextField being observed for NSNotifications
    let textField: UITextField

    /// The NSNotifications observer.
    private var m_NSNotificationCenterObserver : AnyObject? = nil

    private var notificationCenter : NSNotificationCenter
    {
        return NSNotificationCenter.defaultCenter()
    }

    /// Initialise with RxObject tag and UITextField.
    /// - Parameter tag: The RxObject tag.
    /// - Parameter textField: The UItextField to observe.
    init(tag: String, textField : UITextField)
    {
        self.textField = textField

        super.init(tag : tag + "/EventAdapter_UITextField", subscriptionType: .eHot)
    }

    override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode: RxEvalNode<AppEvent, AppEvent>) in

            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange : eRxEvalStateChange, notifier : ARxNotifier<AppEvent>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        self.registerForNSNotifications(evalNode.asyncOutNotifier)

                    case eRxEvalStateChange.eEvalEnd:

                        self.deregisterForNSNotifications()

                    default:
                        break
                }
            }
        }
    }

    /// Register for NSNotifications.
    /// - Parameter notifier: The notifier to notify UITextField changes to.
    private func registerForNSNotifications(notifier : ARxNotifier<AppEvent>)
    {
        let textField = self.textField

        m_NSNotificationCenterObserver = notificationCenter.addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: nil, usingBlock: { [weak weakNotifier = notifier] _ in

            if let strongNotifier = weakNotifier
            {
                strongNotifier.notifyItem(AppEvent(appEventType: eAppEventType.eUITextFieldChange(eDeviceEvent_TextField.eText_Change(textField, textField.attributedText!))))
            }
        })
    }

    // De-register for NSNotifications.
    private func deregisterForNSNotifications()
    {
        if m_NSNotificationCenterObserver != nil
        {
            notificationCenter.removeObserver(m_NSNotificationCenterObserver!)
            m_NSNotificationCenterObserver = nil
        }
    }
}