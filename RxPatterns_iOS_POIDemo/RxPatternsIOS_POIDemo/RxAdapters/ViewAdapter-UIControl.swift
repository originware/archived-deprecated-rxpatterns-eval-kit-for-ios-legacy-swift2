//
// Created by Terry Stillone (http://www.originware.com) on 19/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

public class ViewAdapter_UIControl<ItemType> : ARxObserver<ItemType>
{
    /// The UI EvalQueue to perform UI.
    private let UIQueue = RxSDK.evalQueue.UIThreadQueue

    init()
    {
        // The observer runs in the UIThread.
        super.init(tag : "ViewAdapter_UIControl")
    }

    public override final func notifyItem(item: ItemType)
    {
        if let appEvent = item as? AppEvent
        {
            UIQueue.dispatchAsync({ [weak self] in

                if let strongSelf = self
                {
                    strongSelf.onAppEvent(appEvent)
                }
            })
        }
        else
        {
            fatalError("Expected an AppEvent item")
        }
    }

    private func onAppEvent(appEvent: AppEvent)
    {
        switch appEvent.appEventType
        {
            case .eUIControlTouch(let (uiControl, _)):

                if appEvent.isSimulatedEvent
                {
                    if let button = uiControl as? UIButton
                    {
                        button.sendActionsForControlEvents(UIControlEvents.TouchDown)
                    }
                }

            default:

                fatalError("Unexpected AppEvent: \(appEvent.description)")
        }
    }
}
