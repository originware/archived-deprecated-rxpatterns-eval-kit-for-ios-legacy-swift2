//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ViewAdapter_Label: An adapter to present labels triggered from AppEvent notifications.
///

public class ViewAdapter_Label<ItemType> : ARxObserver<ItemType>
{
    /// The label to present to.
    private let m_label: UILabel

    /// The UI EvalQueue to perform UI.
    private let UIQueue = RxSDK.evalQueue.UIThreadQueue

    public init(tag: String, label: UILabel)
    {
        self.m_label = label

        super.init(tag: tag)
    }

    public override final func notifyItem(item: ItemType)
    {
        if let appEvent = item as? AppEvent
        {
            UIQueue.dispatchAsync({ [weak self] in

                if let strongSelf = self
                {
                    strongSelf.onAppEvent(appEvent)
                }
            })
        }
        else
        {
            fatalError("Expected an AppEvent item")
        }
    }

    private func onAppEvent(appEvent: AppEvent)
    {
        func updateLabelTitle(text: NSAttributedString)
        {
            if text != m_label.attributedText
            {
                m_label.attributedText = text
            }
        }

        switch appEvent.appEventType
        {
            case .eLocationChange(let locationChange):
                updateLabelTitle(AppStyle.formatLocationChangeForUILabel(locationChange, isSimulated: appEvent.isSimulatedEvent))

            case .eNetworkReachabilityChange(let reachability):
                updateLabelTitle(AppStyle.formatReachabilityChangeForUILabel(reachability))

            default:

                fatalError("Unexpected AppEvent: \(appEvent.description)")
        }
    }
}