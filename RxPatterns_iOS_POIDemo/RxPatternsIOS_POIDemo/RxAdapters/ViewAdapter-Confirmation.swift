//
// Created by Terry Stillone (http://www.originware.com) on 16/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ViewAdapter_Confirmation: An adapter to present confirmations triggered from AppEvent notifications.
///

public class ViewAdapter_Confirmation<ItemType> : ARxObserver<ItemType>
{
    // Enabler.
    public var enabled = true

    /// The current alerts begin presented.
    private lazy var m_pendingAlertQueue = [String]()

    /// The UI EvalQueue to perform UI.
    private let UIQueue = RxSDK.evalQueue.UIThreadQueue

    /// Get the top most controller.
    private var topMostController : UIViewController?
    {
        func isVisible(viewController : UIViewController?) -> Bool
        {
            return (viewController != nil) && viewController!.isViewLoaded() && (viewController!.view.window != nil)
        }

        var topController : UIViewController? = UIApplication.sharedApplication().keyWindow?.rootViewController

        while topController != nil
        {
            let nextPresentedController = topController!.presentedViewController

            /// Check its view is loaded and in the view hierarchy.
            if !isVisible(nextPresentedController)
            {
                break
            }

            topController = nextPresentedController
        }

        return isVisible(topController) ? topController : nil
    }

    init(tag : String)
    {
        super.init(tag : tag)
    }

    public override func notifyItem(item: ItemType)
    {
        if !enabled { return }

        if let appEvent = item as? AppEvent
        {
            let message = appEvent.appEventType.confirmation

            // If the alert has not already been presented, then either queue or present.
            if !m_pendingAlertQueue.contains(message)
            {
                m_pendingAlertQueue.append(message)

                if m_pendingAlertQueue.count == 1
                {
                    UIQueue.dispatchAsync ({
                        self.presentConfirmation(message)
                    })
                }
            }
        }
        else
        {
            fatalError("Expected a String based item")
        }
    }

    private func presentConfirmation(message : String)
    {
        if !enabled { return }

        // Get present the confirmation on the top most view controller.
        if let viewController = topMostController
        {
            let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertControllerStyle.Alert)

            alert.modalPresentationStyle = UIModalPresentationStyle.Popover
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { [unowned self] action in

                // Dismiss the presented confirmation.
                alert.dismissViewControllerAnimated(true, completion: nil)

                // Remove the last alert request.
                self.m_pendingAlertQueue.removeAtIndex(0)

                // If there are more messages queued, present the next.
                if let nextMessage = self.m_pendingAlertQueue.first
                {
                    self.presentConfirmation(nextMessage)
                }
            }))

            if let popoverController = alert.popoverPresentationController
            {
                popoverController.sourceView = viewController.view
                popoverController.sourceRect = viewController.view.bounds
            }

            // Present the confirmation.
            viewController.presentViewController(alert, animated: true, completion: nil)
        }
    }
}