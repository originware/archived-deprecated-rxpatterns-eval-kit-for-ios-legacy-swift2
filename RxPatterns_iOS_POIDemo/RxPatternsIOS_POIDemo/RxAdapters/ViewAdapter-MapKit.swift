//
// Created by Terry Stillone (http://www.originware.com) on 19/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import MapKit
import RxPatternsSDK
import RxPatternsLib

@available(iOS 7, *)

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ViewAdapter_MapKit: An adapter to MapKit Annotations. Triggers off AppEvents for POI locations and the map center button touch event.
///

public class ViewAdapter_MapKit<ItemType> : ARxObserver<ItemType>
{
    /// The MapKit view to present to.
    private var m_mapView : MKMapView

    /// The Location Device Adapter to get the current map center (either real or simulated).
    private var locationAdapter = RxDeviceDir.locationAdapter

    /// The UI EvalQueue to perform UI.
    private let UIQueue = RxSDK.evalQueue.UIThreadQueue

    /// AppEvent monitoring.
    private var m_appOpEventMonitor = AppViewController.SubRxDir.appOpEventMonitor

    /// Get the map center.
    private var mapCenter : CLLocationCoordinate2D? {

        let userCoord = m_mapView.userLocation.coordinate

        func isValidUserCoord(coordinate : CLLocationCoordinate2D) -> Bool
        {
            return (coordinate.latitude != 0) && (coordinate.longitude != 0)
        }

        return isValidUserCoord(userCoord) ? userCoord : locationAdapter?.currentLocation?.coordinate
    }
    
    init(tag: String, mapView : MKMapView)
    {
        self.m_mapView = mapView

        super.init(tag : tag)
    }

    public override final func notifyItem(item: ItemType)
    {
        if let appEvent = item as? AppEvent
        {
            UIQueue.dispatchAsync({ [weak self] in

                if let strongSelf = self
                {
                    strongSelf.onAppEvent(appEvent)
                }
            })
        }
        else
        {
            fatalError("Expected an AppEvent item")
        }
    }

    private func onAppEvent(appEvent: AppEvent)
    {
        switch appEvent.appEventType
        {
            case .ePOILocateResult(let poiLocateResult):        // Handle POI Locate events.

                m_appOpEventMonitor?.notifyItem(.eFromPOILocator_To_MapAdaptor)

                for pois in poiLocateResult.poiByPOIKeyword.values
                {
                    for poi in pois
                    {
                        addPOIToMap(poi)
                    }
                }

            case .eUIControlTouch(let (_, touchTarget)):        // Handle touch events.

                switch touchTarget
                {
                    case .eTouch_centerMapButton:               // Trigger on Map Center button touch.

                        if let currentMapCenter = mapCenter
                        {
                            m_mapView.setCenterCoordinate(currentMapCenter, animated: true)
                        }

                    case .eTouch_clearPOIKeywordMatchesButton:  // Trigger on clear Located POIs button touch.

                        m_mapView.removeAnnotations(m_mapView.annotations)

                    case .eTouch_clearSearchButton:             // Trigger on search clear button, not handled here.
                        break

                    case .eTouch_clearLogButton:                // Trigger on clear log button, not handled here.
                        break
                }

            default:

                fatalError("Unexpected AppEvent: \(appEvent.description)")
        }
    }

    /// Add a POI annotation to the map.
    /// - Parameter poi: The POI details to be used to create a POI annotation.
    private func addPOIToMap(poi : POI)
    {
        for annotation in m_mapView.annotations
        {
            if let flagAnnotation = annotation as? FlagAnnotation
            {
                if poi == flagAnnotation.poi
                {
                    return
                }
            }
        }

        let flagAnnotation = FlagAnnotation(poi:poi)

        m_mapView.addAnnotation(flagAnnotation)
    }
}
