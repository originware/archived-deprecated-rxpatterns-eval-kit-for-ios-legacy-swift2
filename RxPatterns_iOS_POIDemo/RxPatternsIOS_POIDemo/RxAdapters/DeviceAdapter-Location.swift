//
// Created by Terry Stillone (http://www.originware.com) on 16/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import CoreLocation
import RxPatternsSDK
import RxPatternsLib

@available(iOS 8, *)


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// DeviceAdapter_LocationWithSimulateFailover: Adapts from the Core Location Services to AppEvent RxNotifications.
///
///    Will simulate the location if Core Location Services fails.
///

class DeviceAdapter_LocationWithSimulateFailover : RxObservable<AppEvent>
{
    var currentLocation : CLLocation? = nil
    
    private let m_baseSource : DeviceAdapter_Location
    
    /// Initialise with the RxObject tag.
    /// - Parameter tag: The RxObject tag for this instance.
    init(tag: String)
    {
        m_baseSource = DeviceAdapter_Location(tag: tag + "/baseSource")
        
        super.init(tag : tag)
        
        self.chainObservable(m_baseSource)
    }
    
    override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode: RxEvalNode<AppEvent, AppEvent>) in
            
            let window = RxWindow(tag: self.tag + "/timeout")
            let emitNotifier = evalNode.syncOutNotifier

            func notifySimulatedLocation(notifier : ARxNotifier<AppEvent>)
            {
                let location = AppConstant.SimulatedPosition
                let appEvent = AppEvent(appEventType: eAppEventType.eLocationChange(eDeviceEvent_Location.eLocation_LastKnown(location)), isSimulatedEvent: true)
                
                self.currentLocation = location
                
                notifier.notifyItem(appEvent)
            }

            // Configure window.
            window <- .eSetEvalQueue(evalNode.evalQueue) <- .eSetWindowEndAction({ notifySimulatedLocation(emitNotifier) } )

            evalNode.itemDelegate = { (item : AppEvent, notifier : ARxNotifier<AppEvent>) in
                
                // Cancel pending windows.
                if window.isActive
                {
                    window.cancelAll()
                }
                
                // Update the currentLocation
                if case .eLocation_LastKnown(let location) = item.appEventType.locationChange
                {
                    self.currentLocation = location
                }

                // Emit the actual location.
                notifier.notifyItem(item)
            }
            
            evalNode.completedDelegate = { (error : IRxError?, notifier : ARxNotifier<AppEvent>) in
                
                // Cancel pending windows.
                window.cancelAll()
                
                if error != nil
                {
                    // If location services fails, then emit the simulated location.
                    notifySimulatedLocation(notifier)
                }
                else
                {
                    // If location services completed, then complete as well.
                    notifier.notifyCompleted()
                }
            }
            
            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange : eRxEvalStateChange, notifier : ARxNotifier<AppEvent>) in
                
                func scheduleLocationTimeout(notifier: ARxNotifier<AppEvent>)
                {
                    let timeout = AppViewController.Constant.LocationSimulationTimeout

                    window.createSingleWindow(RxTime(), duration: timeout)
                }
                
                switch stateChange
                {
                    case eRxEvalStateChange.eSubscriptionBegin:

                        // Emit an initial location when a subscription occurs.
                        if self.currentLocation != nil
                        {
                            notifySimulatedLocation(notifier)
                        }
                        else if !window.isActive
                        {
                            scheduleLocationTimeout(evalNode.asyncOutNotifier)
                        }

                    case eRxEvalStateChange.eEvalEnd:

                        window.cancelAll()
                        self.currentLocation = nil

                    default:
                        break
                }
            }
        }
    }
}

class DeviceAdapter_Location : RxSource<AppEvent>
{
    /// The Location Manager delegate handler.
    private var m_coreLocationHandler: CoreLocationHandler? = nil

    /// Initialise with the RxObject tag.
    /// - Parameter tag: The RxObject tag for this instance.
    init(tag: String)
    {
        super.init(tag : tag, subscriptionType: .eHot)
    }

    override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode: RxEvalNode<AppEvent, AppEvent>) in
            
            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange : eRxEvalStateChange, notifier : ARxNotifier<AppEvent>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eNewSubscriptionInSubscriptionThread:

                        if self.m_coreLocationHandler == nil
                        {
                            // Create the CoreLocationController in the Main Thread with a RunLoop.
                            // This is required for Core Location Services to run properly.

                            self.m_coreLocationHandler = CoreLocationHandler(notifier : evalNode.asyncOutNotifier)
                        }
                        
                   case eRxEvalStateChange.eSubscriptionBegin:
                
                        // Notify subscriptions with an initial location notification.
                        self.m_coreLocationHandler?.notifyLastLocation(notifier)
   
                    case eRxEvalStateChange.eEvalEnd:

                        /// Destroy CoreLocationController on the last subscription.
                        self.m_coreLocationHandler = nil

                    default:
                        break
                }
            }
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// CoreLocationHandler: The Core location delegate handler.
///

final class CoreLocationHandler: NSObject, CLLocationManagerDelegate
{
    /// The Constants for Core Location Handling.
    struct Constant
    {
        static let LocationChangeUpdateDistance : CLLocationDistance = 100
    }

    private var m_locationManager : CLLocationManager? = nil
    private let m_useSignificantChanges = CLLocationManager.significantLocationChangeMonitoringAvailable()
    private var m_lastLocation : CLLocation? = nil
    private var m_enabled = false
    private var m_notifier : ARxNotifier<AppEvent>

    var enabled : Bool { return m_enabled }

    init(notifier : ARxNotifier<AppEvent>)
    {
        self.m_notifier = notifier

        super.init()

        // Request the service to start on initialisation.
        requestService()
    }

    deinit
    {
        // Stop the service to start on deinitialisation.
        stopService(nil)
    }

    /// The Core location didUpdateLocations handler.
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let currentLocation: CLLocation = locations.last
        {
            if (m_lastLocation == nil) || (m_lastLocation! != currentLocation)
            {
                m_lastLocation = currentLocation

                notifyLastLocation(self.m_notifier)
            }
        }
    }

    /// The Core location didFailWithError handler.
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError)
    {
        if error.code != CLError.LocationUnknown.rawValue
        {
            stopService(AppError(error))
        }
    }

    /// The Core location didChangeAuthorizationStatus handler.
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        switch status
        {
            case .NotDetermined:
                m_locationManager!.requestAlwaysAuthorization()

            case .Restricted, .Denied:
                break

            default:
                startService()
        }
    }

    /// Request the Location Service to start, check authorisation.
    private func requestService()
    {
        if CLLocationManager.locationServicesEnabled()
        {
            m_enabled = true

            m_locationManager = CLLocationManager()
            m_locationManager!.delegate = self
        }
        else
        {
            m_enabled = false
        }
    }

    /// Start Location Services.
    private func startService()
    {
        m_enabled = true

        if m_useSignificantChanges
        {
            m_locationManager?.distanceFilter = Constant.LocationChangeUpdateDistance
            m_locationManager?.desiredAccuracy = kCLLocationAccuracyBest

            m_locationManager?.startMonitoringSignificantLocationChanges()
        }
        else
        {
            m_locationManager?.distanceFilter = Constant.LocationChangeUpdateDistance
            m_locationManager?.desiredAccuracy = kCLLocationAccuracyBest
            
            m_locationManager?.startUpdatingLocation()
        }
    }

    /// Stop Location Services.
    private func stopService(error : IRxError?)
    {
        if m_enabled
        {
            m_enabled = false

            if m_useSignificantChanges
            {
                m_locationManager?.stopMonitoringSignificantLocationChanges()
            }
            else
            {
                m_locationManager?.stopUpdatingLocation()
            }

            m_notifier.notifyCompleted(error)
        }
    }

    /// Notify location change given by m_lastLocation.
    /// - Parameter notifier: The Notifier to notify of the location change.
    private func notifyLastLocation(notifier : ARxNotifier<AppEvent>)
    {
        if m_enabled && m_lastLocation != nil
        {
            let appEvent = AppEvent(appEventType: eAppEventType.eLocationChange(eDeviceEvent_Location.eLocation_LastKnown(self.m_lastLocation!)), isSimulatedEvent: false)

            notifier.notifyItem(appEvent)
        }
        else if !m_enabled
        {
            m_notifier.notifyCompleted(AppError("Location Services Not Enabled"))
        }
    }
}
