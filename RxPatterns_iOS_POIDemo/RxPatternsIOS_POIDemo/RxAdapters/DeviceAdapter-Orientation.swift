//
// Created by Terry Stillone (http://www.originware.com) on 16/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// DeviceAdapter_Orientation: The Adapt from Orientation NSNotifications to AppEvent RxNotifications.
///

class DeviceAdapter_Orientation: RxSource<AppEvent>
{
    /// The registered NSNotificationCenter observer.
    private var m_NSNotificationCenterObserver : AnyObject? = nil

    /// The previous orientation event notified. Used to emit only distinct notifications.
    private var m_prevOrientationEvent : UIDeviceOrientation = .Unknown

    private var notificationCenter : NSNotificationCenter
    {
        return NSNotificationCenter.defaultCenter()
    }

    /// Initialise with the RxObject tag.
    /// - Parameter tag: The tag for this RxObject.
    init(tag: String)
    {
        super.init(tag : tag, subscriptionType: .eHot)
    }

    /// Create the EvalOp behaviour.
    override func createEvalOpDelegate() -> RxEvalOp
    {
        return { (evalNode: RxEvalNode<AppEvent, AppEvent>) in

            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange : eRxEvalStateChange, notifier : ARxNotifier<AppEvent>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eSubscriptionBegin:

                        if evalNode.subscriptionCount > 1
                        {
                            // On subsequent subscriptions, notify an initial orientation.
                            let orientation = self.m_prevOrientationEvent ?? UIDevice.currentDevice().orientation
                            
                            self.notifyOrientationChange(orientation, notifier: notifier)
                        }

                    case eRxEvalStateChange.eEvalBegin:

                        self.registerForOrientationNSNotifications(evalNode.asyncOutNotifier)

                    case eRxEvalStateChange.eEvalEnd:

                        // On the last subscription deregister for NSNotification orientation changes.
                        self.deregisterForOrientationNSNotifications()

                    default:
                        break
                }
            }
        }
    }

    /// Register for NSNotification orientation changes.
    private func registerForOrientationNSNotifications(notifier : ARxNotifier<AppEvent>)
    {
        let device = UIDevice.currentDevice()

        m_NSNotificationCenterObserver = notificationCenter.addObserverForName("UIDeviceOrientationDidChangeNotification", object: device, queue: nil, usingBlock: { [weak self, weak notifier] _ in

            if let strongSelf = self, strongNotifier = notifier
            {
                let orientation = UIDevice.currentDevice().orientation

                if strongSelf.m_prevOrientationEvent != orientation
                {
                    strongSelf.notifyOrientationChange(orientation, notifier: strongNotifier)
                }
            }
        })

        device.beginGeneratingDeviceOrientationNotifications()
    }

    /// Deregister for NSNotification orientation changes.
    private func deregisterForOrientationNSNotifications()
    {
        if m_NSNotificationCenterObserver != nil
        {
            let device = UIDevice.currentDevice()

            device.endGeneratingDeviceOrientationNotifications()

            notificationCenter.removeObserver(m_NSNotificationCenterObserver!)
        }
    }

    private func notifyOrientationChange(orientation : UIDeviceOrientation, notifier : ARxNotifier<AppEvent>)
    {
        if orientation != .Unknown
        {
            let appEvent = AppEvent(appEventType : eAppEventType.eOrientation(orientation))

            notifier.notifyItem(appEvent)

            m_prevOrientationEvent = orientation
        }
    }
}