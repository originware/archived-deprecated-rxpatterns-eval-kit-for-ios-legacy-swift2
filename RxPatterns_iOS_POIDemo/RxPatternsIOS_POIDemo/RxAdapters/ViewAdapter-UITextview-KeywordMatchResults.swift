//
// Created by Terry Stillone (http://www.originware.com) on 7/07/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ViewAdapter_KeywordResults_UITextView: An adapter to present POI keyword matches.
///

public class ViewAdapter_KeywordResults_UITextView<ItemType>: ARxObserver<ItemType>
{
    /// The UITextView to present to.
    private let m_textView: UITextView

    /// The UI EvalQueue to perform UI.
    private let UIQueue = RxSDK.evalQueue.UIThreadQueue


    private var m_appOpEventMonitor = AppViewController.SubRxDir.appOpEventMonitor

    init(tag : String, textView: UITextView, logFormatter: AppStyle_LogFormatter)
    {
        self.m_textView = textView

        // The observer runs in the UIThread.
        super.init(tag: tag)
    }

    public override final func notifyItem(item: ItemType)
    {
        if let appEvent = item as? AppEvent
        {
            UIQueue.dispatchAsync({ [weak self] in

                if let strongSelf = self
                {
                    strongSelf.onAppEvent(appEvent)
                }
            })
        }
        else
        {
            fatalError("Expected an AppEvent item")
        }
    }

    private func onAppEvent(appEvent: AppEvent)
    {
        m_appOpEventMonitor?.notifyItem(.eFromPOIKeywordMatcher_To_POIKeywordsResults)

        switch appEvent.appEventType
        {
            case .ePOIKeywordMatchSet:

                // Process Keyword Match AppEvent notifications.
                m_textView.attributedText = formatPOIKeywordMatchResultsText(appEvent)

            default:

                fatalError("Unexpected AppEvent: \(appEvent.description)")
        }
    }

    /// Format a collection of keyword matches from an AppEvent.
    /// - Parameter appEvent: The appevent with the Keyword Match.
    /// - Returns: The formatted keywords as an NSAttributedString.
    private func formatPOIKeywordMatchResultsText(appEvent : AppEvent) -> NSAttributedString
    {
        func formatLogText(string : String) -> NSMutableAttributedString
        {
            let font = AppStyle.font(.eFont_POIResultsText)
            let textColor = AppStyle.color(.eColor_POISearchResultText)
            let paragraphStyle = NSMutableParagraphStyle()
            
            paragraphStyle.lineHeightMultiple = 1
            paragraphStyle.lineBreakMode = NSLineBreakMode.ByWordWrapping
            paragraphStyle.alignment = NSTextAlignment.Center
            
            return NSMutableAttributedString(string: string + "\n", attributes: [
                        NSFontAttributeName : font,
                        NSForegroundColorAttributeName : textColor,
                        NSParagraphStyleAttributeName : paragraphStyle])
        }
        
        let poikeywordCollectionMatches = appEvent.appEventType.poikeywordCollectionMatches
        
        if poikeywordCollectionMatches.count == 0
        {
            return formatLogText("")
        }
        
        let matchText : NSMutableAttributedString = formatLogText("")
        
        let highlightColor : UIColor = AppStyle.color(.eColor_POIHighlightBackground)
        let matchedTextColor : UIColor = AppStyle.color(.eColor_POIText)
        
        for (poiKeyword, matches) in poikeywordCollectionMatches.poiKeywordMatchesByPOIKeyword
        {
            let text = formatLogText(poiKeyword)
            
            for range in matches.keywordMatchRanges
            {
                let nsRange = Helpers.rangeToNSRange(poiKeyword, range)
                
                text.addAttribute(NSBackgroundColorAttributeName, value:highlightColor, range:nsRange)
                text.addAttribute(NSForegroundColorAttributeName, value:matchedTextColor, range:nsRange)
            }
            
            matchText.appendAttributedString(text)
        }
        
        return matchText
    }
}
