//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ViewAdapter_Log_UITextView: An adapter to present textual logs from AppEvents.
///

public class ViewAdapter_Log_UITextView<ItemType>: ARxObserver<ItemType>
{
    /// The UITextView to present to.
    private let m_textView: UITextView

    /// The text formatter.
    private var m_logFormatter: AppStyle_LogFormatter

    /// The UI EvalQueue to perform UI.
    private let UIQueue = RxSDK.evalQueue.UIThreadQueue

    /// The app internal operation monitor.
    private var m_appOpEventMonitor = AppViewController.SubRxDir.appOpEventMonitor

    init(tag: String, textView : UITextView, logFormatter : AppStyle_LogFormatter)
    {
        self.m_textView = textView
        self.m_logFormatter = logFormatter

        // The observer runs in the UIThread.
        super.init(tag : tag)
    }

    public override final func notifyItem(item: ItemType)
    {
        if let appEvent = item as? AppEvent
        {
            UIQueue.dispatchAsync({ [weak self] in

                if let strongSelf = self
                {
                    strongSelf.onAppEvent(appEvent)
                }
            })
        }
        else
        {
            fatalError("Expected an AppEvent item")
        }
    }

    private func onAppEvent(appEvent: AppEvent)
    {
        /// Set the log text in the UITextView associated with this Adapter.
        /// - Parameter attributedText: The log text.
        func setText(attributedText : NSAttributedString)
        {
            m_textView.attributedText = attributedText
            m_textView.scrollRangeToVisible(NSMakeRange(attributedText.length, 0))
        }

        switch appEvent.appEventType
        {
            case .eLocationChange(let locationChange):
                let simulatedNote = appEvent.isSimulatedEvent ? " [Simulated]" : ""

                setText(m_logFormatter.formatLogLineEntry(appEvent, locationChange.description + simulatedNote, AppStyle.color(.eColor_LogEventText)))

            case .eNetworkReachabilityChange(let reachabilityChange):
                setText(m_logFormatter.formatLogLineEntry(appEvent, reachabilityChange.description, AppStyle.color(.eColor_LogEventText)))

            case .ePOILocateResult(let poilocateResult):
                m_appOpEventMonitor?.notifyItem(eAppOpEventType.eFromPOILocator_To_LogAdaptor)

                setText(m_logFormatter.formatPOILocateResultForLog(appEvent, poilocateResult : poilocateResult))

            case .eOrientation:
                setText(m_logFormatter.formatLogLineEntry(appEvent, appEvent.description, AppStyle.color(.eColor_LogEventText)))

            case .eUITextFieldChange(let uiTextField):
                setText(m_logFormatter.formatLogLineEntry(appEvent, "Search Input Text: \(uiTextField.attributedText.string)", AppStyle.color(.eColor_LogEventText)))

            case .ePOIKeywordMatchSet:
                m_appOpEventMonitor?.notifyItem(eAppOpEventType.eFromPOIKeywordMatcher_To_LogAdapter)

                setText(m_logFormatter.formatPOIKeywordMatchLogText(appEvent))

            case .eUIControlTouch(let (_, touchTarget)):

                setText(m_logFormatter.formatLogLineEntry(appEvent, "Tap: \(touchTarget.rawValue)", AppStyle.color(.eColor_LogEventText)))

                if touchTarget == .eTouch_clearLogButton
                {
                    setText(NSAttributedString(string: ""))
                    m_logFormatter.clearLog()
                }

            default:
                fatalError("Unexpected AppEvent: \(appEvent.description)")
        }
    }
}