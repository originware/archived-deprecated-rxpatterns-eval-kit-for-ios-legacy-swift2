//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// EventAdapter_keyboard: Adapt from Keyboard NSNotifications to AppEvent RxNotifications.
///

class EventAdapter_keyboard: RxSource<AppEvent>
{
    /// The show keyboard NSNotification observers.
    private var m_NSNotificationCenterShowObserver: AnyObject? = nil

    /// The hide keyboard NSNotification observers.
    private var m_NSNotificationCenterHideObserver: AnyObject? = nil

    /// Initialise with The RxObject tag for this instance.
    /// - Parameter tag: The RxObject tag.
    init(tag : String)
    {
        super.init(tag : tag, subscriptionType: .eHot)
    }

    override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode: RxEvalNode<AppEvent, AppEvent>) in

            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange : eRxEvalStateChange, notifier : ARxNotifier<AppEvent>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        self.registerForKeyboardNSNotifications(evalNode.asyncOutNotifier)

                    case eRxEvalStateChange.eEvalEnd:

                        self.deregisterForKeyboardNSNotifications()

                    default:
                        break
                }
            }
        }
    }

    /// Register for NSNotification keyboard events.
    /// - Parameter notifier: The notifier to notify keyboard changes to.
    private func registerForKeyboardNSNotifications(notifier : ARxNotifier<AppEvent>)
    {
        m_NSNotificationCenterShowObserver = NSNotificationCenter.defaultCenter().addObserverForName("UIKeyboardDidShowNotification", object: nil, queue: nil, usingBlock: { [weak weakNotifier = notifier] (notification : NSNotification!) in

            if let strongNotifier = weakNotifier
            {
                strongNotifier.notifyItem(AppEvent(appEventType: .eKeyboardChange(.eKeyboard_didShow)))
            }
        })

        m_NSNotificationCenterHideObserver = NSNotificationCenter.defaultCenter().addObserverForName("UIKeyboardDidHideNotification", object: nil, queue: nil, usingBlock: { [weak weakNotifier = notifier] (notification : NSNotification!) in

            if let strongNotifier = weakNotifier
            {
                strongNotifier.notifyItem(AppEvent(appEventType: .eKeyboardChange(.eKeyboard_didHide)))
            }
        })
    }

    /// De-register for NSNotification keyboard events.
    private func deregisterForKeyboardNSNotifications()
    {
        if m_NSNotificationCenterShowObserver != nil
        {
            NSNotificationCenter.defaultCenter().removeObserver(m_NSNotificationCenterShowObserver!)
        }

        if m_NSNotificationCenterHideObserver != nil
        {
            NSNotificationCenter.defaultCenter().removeObserver(m_NSNotificationCenterHideObserver!)
        }
    }
}