//
// Created by Terry Stillone (http://www.originware.com) on 18/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import RxPatternsSDK
import RxPatternsLib

@available(iOS 7, *)

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POIEntryState: The state (index, color and text) of a POI entry in the UICollectionView.
///

private class POIEntryState
{
    let poiKeyword : String
    var hasChanged : Bool
    var index : Int
    var error: NSError? = nil

    private var m_resultCount: Int

    var enabled : Bool          { return (m_resultCount > 0) && (error == nil) }
    var resultCount: Int        { return m_resultCount }

    init(poiKeyword : String, index : Int)
    {
        self.poiKeyword = poiKeyword
        self.index = index
        self.hasChanged = true
        self.m_resultCount = 0
    }

    func updateState(newError: NSError?, newResultCount : Int)
    {
        if (m_resultCount != newResultCount) || (error != newError)
        {
            m_resultCount = newResultCount
            error = newError
            hasChanged = true
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POIEntryStyle: The styler for a POI entry in the UICollectionView.
///

private class POIEntryStyle
{
    class func styleButton(inout button : UIButton, buttonState : POIEntryState)
    {
        func getButtonText(buttonState: POIEntryState, normalisedText : String) -> String
        {
            switch (buttonState.error, buttonState.enabled)
            {
                case (.None, true), (.None, false):
                    return "\(normalisedText) (\(buttonState.resultCount))"

                default:
                    return "\(normalisedText) (no reply)"
            }
        }
        
        func getButtonColor(buttonState: POIEntryState) -> UIColor
        {
            switch (buttonState.error, buttonState.enabled)
            {
            case (.None, true):
                return AppStyle.color(.eColor_enabled)
                
            case (.None, false):
                return AppStyle.color(.eColor_disabled)
                
            default:
                return AppStyle.color(.eColor_Error)
            }
        }

        func formatButtonText(keyword : String, buttonColor : UIColor, buttonState: POIEntryState) -> NSAttributedString
        {
            let normalisedText = keyword.stringByReplacingOccurrencesOfString("_", withString: " ")
            let textWithCount = getButtonText(buttonState, normalisedText: normalisedText)

            return NSAttributedString(string: textWithCount, attributes: [NSForegroundColorAttributeName : buttonColor, NSFontAttributeName: UIFont(name: "Helvetica", size: 14)!])
        }

        let buttonColor = getButtonColor(buttonState)

        button.titleLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
        button.titleLabel?.textAlignment = NSTextAlignment.Center
        button.tag = buttonState.index
        button.layer.cornerRadius = 10.0

        button.setAttributedTitle(formatButtonText(buttonState.poiKeyword, buttonColor: buttonColor, buttonState: buttonState), forState:UIControlState.Normal)
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POICollectionController: The data source and delegate for the UICollectionView presenting POI locate statuses.
///

private class POICollectionController: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    private var m_collectionView: UICollectionView
    private var m_poiButtonStateByPOIKeyword = [String : POIEntryState]()

    init(collectionView : UICollectionView)
    {
        self.m_collectionView = collectionView

        super.init()

        self.m_collectionView.delegate = self
        self.m_collectionView.dataSource = self
    }

    deinit
    {
        self.m_collectionView.delegate = nil
        self.m_collectionView.dataSource = nil
    }

    func clear()
    {
        m_poiButtonStateByPOIKeyword.removeAll()
        reload()
    }

    func reload()
    {
        self.m_collectionView.reloadData()
        self.m_collectionView.performBatchUpdates({
            
                var indexPaths = [NSIndexPath]()
                
                for buttonInfo in self.m_poiButtonStateByPOIKeyword.values
                {
                    if buttonInfo.hasChanged
                    {
                        let newIndexPath = NSIndexPath(index : buttonInfo.index)
                        
                        indexPaths.append(newIndexPath)
                        
                        buttonInfo.hasChanged = false
                    }
                }
            
            }, completion: { (finished : Bool) in
                
                self.m_collectionView.reloadData()
        })
    }

    @objc func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }

    @objc func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return m_poiButtonStateByPOIKeyword.count
    }

    @objc func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let index = indexPath.row
        let buttonState = getPOIButtonStateForIndex(index)

        assert(buttonState != nil, "Expected a valid POIButtonInfo for the index:\(index)")

        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("POITypeCell", forIndexPath : indexPath)

        for view in cell.contentView.subviews
        {
            if var button = view as? UIButton
            {
                POIEntryStyle.styleButton(&button, buttonState : buttonState!)
                break
            }
        }

        return cell
    }
    
    @objc func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView
    {
        if kind == UICollectionElementKindSectionHeader
        {
            let sectionHeader = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "Header", forIndexPath : indexPath)
            let borderColor : UIColor = AppStyle.color(.eColor_borderColor)
            
            sectionHeader.layer.cornerRadius = 10.0
            sectionHeader.layer.borderWidth = 0.5
            sectionHeader.layer.borderColor = borderColor.CGColor
            
            return sectionHeader
        }
    

        fatalError("Cannot get UICollectionReusableView(located POI header)")
    }

    func getPOIButtonStateForIndex(index : Int) -> POIEntryState?
    {
        for buttonInfo in m_poiButtonStateByPOIKeyword.values
        {
            if buttonInfo.index == index
            {
                return buttonInfo
            }
        }

        return nil
    }

    func itemsHaveChanged() -> Bool
    {
        for buttonInfo in m_poiButtonStateByPOIKeyword.values
        {
            if buttonInfo.hasChanged
            {
                return true
            }
        }

        return false
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ViewAdapter_UICollection: An adapter to present POI lookup results in a collection view.
///

public class ViewAdapter_UICollection<ItemType> : ARxObserver<ItemType>
{
    /// The UICollectionView to present to.
    private let m_collectionController : POICollectionController

    /// The current index to the UICollectionView.
    private var m_currentIndex : Int = 0

    /// The UI EvalQueue to perform UI.
    private let UIQueue = RxSDK.evalQueue.UIThreadQueue

    init(tag : String, collectionView : UICollectionView)
    {
        m_collectionController = POICollectionController(collectionView : collectionView)

        // The observer runs in the UIThread.
        super.init(tag : tag)
    }

    public override final func notifyItem(item: ItemType)
    {
        if let appEvent = item as? AppEvent
        {
            UIQueue.dispatchAsync( { [weak self] in

                if let strongSelf = self
                {
                    strongSelf.onAppEvent(appEvent)
                }
            })
        }
        else
        {
            fatalError("Expected an AppEvent based item")
        }
    }

    private func onAppEvent(appEvent: AppEvent)
    {
        switch appEvent.appEventType
        {
            case .ePOIKeywordMatchSet(let poiKeywordMatches):

                for (poiKeyword, _) in poiKeywordMatches.poiKeywordMatchesByPOIKeyword
                {
                    // Create the button state and store.

                    if m_collectionController.m_poiButtonStateByPOIKeyword[poiKeyword] == nil
                    {
                        let poiButtonState = POIEntryState(poiKeyword: poiKeyword, index: m_currentIndex)

                        m_currentIndex += 1
                        m_collectionController.m_poiButtonStateByPOIKeyword[poiKeyword] = poiButtonState
                    }
                }

                if m_collectionController.itemsHaveChanged()
                {
                    m_collectionController.reload()
                }

            case .ePOILocateResult(let poiLocateResult):

                for poiKeyword in poiLocateResult.poiKeywords
                {
                    // Update the button state.

                    // Assume that the buttons were created before hand.
                    let poiButtonState = m_collectionController.m_poiButtonStateByPOIKeyword[poiKeyword]

                    poiButtonState?.updateState(poiLocateResult.error, newResultCount: poiLocateResult.countForPOIType(poiKeyword))
                }

                if m_collectionController.itemsHaveChanged()
                {
                    m_collectionController.reload()
                }

            case .eUIControlTouch(let (_, touchTarget)):

                if touchTarget == .eTouch_clearPOIKeywordMatchesButton
                {
                    clearPOIMatches()
                }

            default:

                fatalError("Unexpected AppEvent: \(appEvent.description)")
        }
    }

    /// Clear all enteries in the UICollectionView.
    func clearPOIMatches()
    {
        m_collectionController.clear()
        m_currentIndex = 0
    }
}
