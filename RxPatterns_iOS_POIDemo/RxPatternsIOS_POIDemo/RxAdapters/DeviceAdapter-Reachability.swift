//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import SystemConfiguration
import RxPatternsSDK
import RxPatternsLib

@available(iOS 8, *)

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// DeviceAdapter_Reachability: The Adapt from SCNetworkReachability updates to AppEvent RxNotifications.
///

class DeviceAdapter_Reachability : RxSource<AppEvent>
{
    /// The host name to check reachability to.
    private var m_hostname: String

    /// The SCNetworkReachability handler.
    private var m_reachabilityHandler: SCNReachabilityHandler? = nil

    /// Indicator of current reachability.
    var isReachable :Bool {

        return m_reachabilityHandler?.lastReachabilityEvent == .eReachability_IsReachable
    }

    /// Initialise with RxObject tag and reachability host name.
    /// - Parameter tag: The RxObject tag for this instance.
    /// - Parameter hostname: The reachability host name.
    init(tag: String, hostname: String)
    {
        m_hostname = hostname
        
        super.init(tag: tag, subscriptionType: .eHot)
    }

    override func createEvalOpDelegate() -> RxEvalOp
    {
        return { [unowned self] (evalNode: RxEvalNode<AppEvent, AppEvent>) in

            evalNode.stateChangeDelegate = { [unowned evalNode] (stateChange: eRxEvalStateChange, notifier: ARxNotifier<AppEvent>) in

                switch stateChange
                {
                    case eRxEvalStateChange.eEvalBegin:

                        if evalNode.subscriptionCount == 1
                        {
                            // On the first subscription create the SCNReachability handler which is fail-able.
                            // This code needs to run in the Main Thread.
                            if let handler = SCNReachabilityHandler(hostname: self.m_hostname, notifier: evalNode.asyncOutNotifier)
                            {
                                self.m_reachabilityHandler = handler
                            }
                            else
                            {
                                notifier.notifyCompleted(AppError("Cannot start Reachability Service"))
                            }
                        }
                        else
                        {
                            // On subsequent subscriptions notify initial reachability.
                            evalNode.evalQueue.dispatchAsync({

                                self.notifyLastReachability(notifier)
                            })
                        }

                    case eRxEvalStateChange.eEvalEnd:

                        self.m_reachabilityHandler = nil

                    default:
                        break
                }
            }
        }
    }

    /// Notify reachability.
    /// - Parameter notifier: The notifier to notify reachability.
    private func notifyLastReachability(notifier: ARxNotifier<AppEvent>)
    {
        if (m_reachabilityHandler != nil) && (m_reachabilityHandler!.lastReachabilityEvent != .eReachability_Unknown)
        {
            notifier.notifyItem(AppEvent(appEventType: .eNetworkReachabilityChange(self.m_reachabilityHandler!.lastReachabilityEvent)))
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// SCNReachabilityHandler: The SCNetworkReachability handler.
///

private final class SCNReachabilityHandler
{
    /// The host name to check reachability to.
    private var hostname: String

    /// The last reachability event issued by SCNReachability
    private var lastReachabilityEvent: eDeviceEvent_Reachability = .eReachability_Unknown

    /// The notifier to notify of reachability changes.
    private var notifier : ARxNotifier<AppEvent>

    /// The reachability context.
    private var m_contextPointer : UnsafeMutablePointer<SCNetworkReachabilityContext>

    /// Initialise with hostname and notify to notify reachability to.
    /// - Parameter hostname: The reachability host name.
    /// - Parameter notifier: The notifier to notify reachability changes to.
    init?(hostname: String, notifier : ARxNotifier<AppEvent>)
    {
        self.hostname = hostname
        self.notifier = notifier
        self.m_contextPointer = UnsafeMutablePointer<SCNetworkReachabilityContext>.alloc(1)

        if !registerForReachabilityChanges()
        {
            // Fail init if SCNReachability fails.
            return nil
        }
    }

    /// De-initialise and de-register with SCNetworkReachability.
    deinit
    {
        deregisterForReachabilityChanges()
    }

    /// Register with SCNetworkReachability for reachability changes.
    private func registerForReachabilityChanges() -> Bool
    {
        if let reachability = SCNetworkReachabilityCreateWithName(nil, hostname)
        {
            let selfPointer = UnsafeMutablePointer<Void>(Unmanaged.passUnretained(self).toOpaque())
            let context = SCNetworkReachabilityContext(version: 0, info: selfPointer, retain: nil, release: nil, copyDescription: nil)


            func callback(reachability:SCNetworkReachability, flags: SCNetworkReachabilityFlags, info: UnsafeMutablePointer<Void>) {

                let isReachable : Bool = flags.contains(SCNetworkReachabilityFlags.Reachable)
                let needsConnection : Bool = flags.contains(SCNetworkReachabilityFlags.ConnectionRequired)

                let currentReachability : eDeviceEvent_Reachability = (isReachable && !needsConnection) ? .eReachability_IsReachable : .eReachability_NotReachable;
                let handler = Unmanaged<SCNReachabilityHandler>.fromOpaque(COpaquePointer(info)).takeUnretainedValue()

                if (handler.lastReachabilityEvent != currentReachability)
                {
                    handler.lastReachabilityEvent = currentReachability
                    handler.notifier.notifyItem(AppEvent(appEventType: .eNetworkReachabilityChange(currentReachability)))
                }
            }

            m_contextPointer.initialize(context)

            SCNetworkReachabilitySetCallback(reachability, callback, m_contextPointer)
            SCNetworkReachabilityScheduleWithRunLoop(reachability, CFRunLoopGetMain(), kCFRunLoopCommonModes)

            return true
        }

        return false
    }

    /// De-register with SCNetworkReachability for reachability changes.
    private func deregisterForReachabilityChanges()
    {
        if let reachability = SCNetworkReachabilityCreateWithName(nil, hostname)
        {
            SCNetworkReachabilitySetCallback(reachability, nil, nil)
            m_contextPointer.destroy()
        }
    }
}
