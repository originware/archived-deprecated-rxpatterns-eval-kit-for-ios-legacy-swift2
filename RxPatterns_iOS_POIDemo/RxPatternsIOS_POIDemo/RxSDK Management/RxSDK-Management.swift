//
// Created by Terry Stillone (http://www.originware.com) on 24/06/15.
// Copyright (c) 2015 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import CoreLocation

import RxPatternsSDK
import RxPatternsLib

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxSDKManagement: The management (startup and shutdown) of the RxPatterns SDK.
///

struct RxSDKManagement
{
    /// RxMon management.
    private struct RxMonManagement
    {

        static func start()
        {
            #if DEBUG
                RxMon.addMonitor(AppConstant.InstanceMonTag, monitor: RxMonInstances())
            #endif
        }

        static func stop()
        {
            #if DEBUG
                RxMon.shutdown()
            #endif
        }
    }

    /// RxDirectory management.
    private struct RxDirectoryManagement
    {
        static func start()
        {
            let tag : (String) -> String = { return AppConstant.AppTag + "/" + $0 }

            RxDeviceDir.store(RxDeviceDir.locationAdapterURI, object: DeviceAdapter_LocationWithSimulateFailover(tag: tag("DeviceAdapter_LocationWithSimulateFailover")))
            RxDeviceDir.store(RxDeviceDir.reachabilityAdapterURI, object: DeviceAdapter_Reachability(tag: tag("DeviceAdapter_Reachability"), hostname: AppConstant.ReachabilityHostname))
            RxDeviceDir.store(RxDeviceDir.orientationAdapterURI, object: DeviceAdapter_Orientation(tag: tag("EventAdapter_Orientation")))
            RxDeviceDir.store(RxDeviceDir.keyboardAdapterURI, object: EventAdapter_keyboard(tag: tag("EventAdapter_keyboard")))
            RxDeviceDir.store(RxDeviceDir.appOpEventMonitorURI, object: RxRelaySubject<eAppOpEventType>(tag: tag("AppOpEvent Monitor")))

            // Use the production scenario in the directory.
            RxDirectory.addMap("/", "/production")
        }

        static func stop()
        {
            RxDirectory.removeAllWithTagRegexpr("App")
        }
    }

    /// RxTraceManagement management.
    private struct RxTraceManagement
    {
        #if DEBUG
            static func report()
            {
                RxMonReports.reportInstanceStats()
            }
        #endif

        static func start()
        {
            #if DEBUG
                RxSDK.log.logEnable = true
                RxSDK.log.logErrorEnable = true
            #else
                RxSDK.log.logEnable = false
                RxSDK.log.logErrorEnable = false
            #endif
        }

        static func stop()
        {
            #if DEBUG
                RxLog.log(">>> RxSDK ------------------------------------ shutting down\n")

                report()

                RxLog.log(">>> RxSDK ------------------------------------ shut down\n")
            #endif
        }
    }

    /// Start the SDK.
    static func startRxSDK()
    {
        /// Start SDK
        RxSDK.control.start()
        
        // Configure and start Monitoring.
        RxMonManagement.start()

        // Configure and Start RxDirectory with devices.
        RxDirectoryManagement.start()

        // Configure tracing.
        RxTraceManagement.start()
    }

    /// Shutdown the SDK.
    static func shutdownRxSDK()
    {
        // Shutdown RxDirectory.
        RxDirectoryManagement.stop()

        /// Shutdown Tracing.
        RxTraceManagement.stop()

        /// Shutdown SDK.
        RxSDK.control.shutdown()
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RxMonReports: RxObject Monitoring reports used by RxMon.
///

struct RxMonReports
{
#if DEBUG
    static func save()
    {
        guard let instanceMon = RxMon.getMonitor(AppConstant.InstanceMonTag) as? RxMonInstances else { return }

        instanceMon.save()
    }

    static func restore()
    {
        guard let instanceMon = RxMon.getMonitor(AppConstant.InstanceMonTag) as? RxMonInstances else { return }

        instanceMon.restore()
    }

    static func reportInstanceStats()
    {
        guard let instanceMon = RxMon.getMonitor(AppConstant.InstanceMonTag) as? RxMonInstances else { return }

        RxLog.log("\n>>>>>> RxPatterns Stats Begin:\n\n")
        do
        {
            RxLog.log(instanceMon.reportObjectStats())
            RxLog.log(instanceMon.reportNonDeallocatedObjects())
        }
        RxLog.log("\n>>>>>> RxPatterns Stats End:\n\n")
    }

#endif
}
