 ---
# README: The [Originware](http://www.originware.com) RxPatterns (Swift) SDK Evaluation Kit v0.92 (iOS)

The RxPatterns SDK extends the classical Reactive Extension concepts to make it applicable to a number of domains, such as:

* iOS Apps
* OSX Apps
* IOT Applications
* Device and Instrument control and monitoring.

The core is the SDK itself, provided as a Framework Library and the extension is the **RxPatternsLib** source containing:

 * The **RxFoundation library**, the implemtation of the classical Reactive Extensions library.
 * The **RxDevice Platform**, a platform for device and instrument control and monitoring.

The package is intended to directly demonstrate the benefits of **RxPatterns** on software architecture and design through direct source examples. An iPad app and the implementation of the classic Reactive Extensions library is supplied in full source form. 

The [RxPatterns White Paper](http://www.originware.com/doc/RxPatterns%20Whitepaper.pdf) discusses the associated RxPatterns software design approach. This method utilises the visualisation of the topology of event-notifications as a fabric of RxNotifications. The method opens the way to working on the adaptive and dynamic design level rather than with static Rx-expressions.

### In The Evaluation Package

* Xcode Projects:	
	* The [RxPattermsLib](RxPatternsLib/RxPatternsLib.xcodeproj) Library.
	* The iPad [POI (Point Of Interest) Demonstration App](RxPatterns_iOS_POIDemo/RxPatternsIOS_POIDemo.xcodeproj)  full Swift app source (demonstrates the practical use of RxPatterns).
	* A Swift [Playground](RxPatterns_iOS_Playground/RxPatterns_iOS_Playground_Workspace.xcworkspace) for experimenting with the RxPatternsSDK and RxPatternsLib.

* Documentation:
	* The [RxPatternsSDK](Doc/RxPatternsSDK/index.html) and [RxPatternsLib](Doc/RxPatternsLib/index.html) Generated **API Documentation** as a HTML doc set.

* Notes:
	* [The Eval Kit Release Notes](ReleaseNotes.md).
	* [The Overview of the Eval Kit](EvalKitOverview.md) guide.


### Licensing

The source in this package is provided for under the Apache Version 2.0 license, see the [License.txt](License.txt) file.

### For More Information

See the Originware [RxPatterns Technology](http://www.originware.com/rxtechnology.html) and [Eval Kit](http://www.originware.com/rxpatevalkit.html) webpages. The [Blog](http://www.originware.com/blog) also has  ongoing articles.

Please direct comments and feedback to [Terry Stillone](mailto:terry@originware.com) at Originware.com
